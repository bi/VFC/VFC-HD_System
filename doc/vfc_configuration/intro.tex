\chapter{Introduction}
\label{chap:intro}


The VFC card contains flash memory, which can be used for storing firmware images. The FPGA tries to load firmware image from address 0x0 on boot. The image can
contain the whole application code (application image) or it can contain a very basic firmware (factory image/golden image) which is then capable of loading another image(s) from a different address in the flash.

The advantage of the latter approach is that the factory image will survive an unsuccessful flash of a application image, which would render the card uncontrollable otherwise. Additionally, the area
where the factory image resides can be write protected. It is possible to store as many images as the flash can handle.


\section{Hardware overview}
\label{sec:hardware}
The flash memory used for the VFC cards is Quad-Serial Configuration (EPCQ) Device EPCQ256 of 33554432 bytes. The memory is connected to the FPGA via Active Serial Memory Interface (ASMI), it is not connected
directly to JTAG chain. The only way to access the memory is through the FPGA, which has loaded an appropriate Altera IP core (Serial Flash Loader for JTAG access, ASMI parallel for user access, Remote Reconfiguration
for reconfig). The ASMI Interface consists of 4 data lines (one bi-directional), clock and chip select.

The flash is structured into 512 sectors, each of 65536 bytes. Bytes can be written individually, yet FW limitation of 4 byte (1 word) granularity of reads and writes is imposed. Bits programmed to value
of 0 need to be erased by a special erase command back to value of 1, smallest unit that can be erased is a sector. Hence it is strongly recommended to place all image starts to sector boundaries.

The maximum programming clock for the flash is 20 MHz.


\chapter{Firmware module}
\label{chap:firmware}
The firmware module consists of thee Altera IP cores and user control logic connected to Wishbone bus. ASMI parallel IP core ensures communication with the flash, Serial Flash Loader IP core provides
JTAG access to the flash and Remote Reconfiguration controls the reconfiguration process.

JTAG, when connected, requests access to the ASMI bus, during the time JTAG is active, the ASMI parallel can not and is not allowed use the ASMI bus.

 \begin{figure}[!htbp]
    \centering
    \resizebox{13cm}{!}{\includegraphics{firmware.png}}
     \caption{The vfc\_configuration.v block schematics with IP cores.}
 \end{figure}

\FloatBarrier

The supplied code can be used in both factory and application image. Control is granted through the Wishbone interface through control and status registers. Paged access to the flash is available.

\section{Registers and parameters}



\begin{longtable}{|p{5cm}|p{1.5cm}|p{1cm}|p{1cm}|p{5.5cm}|}
\caption{Registers} \\
\hline

\textbf{register} & \textbf{addr} & \textbf{width} & \textbf{r/w} & \textbf{description} \\ \hline \hline 
SECTOR & 0x0 & 9 & rw & Flash access page (sector) select \\ \hline 
COMMAND & 0x1 & 4 & w & Command register  \\ \hline 
STATUS & 0x2 & 32 & rw & Status register  \\ \hline 
RECONFIG TRIGGER & 0x3 & 32 & r & Reconfiguration trigger source  \\ \hline 
RECONFIG WD TIMEOUT & 0x4 & 12 & rw & Reconfiguration watchdog timeout, 12 MSB of a 29-bit register  \\ \hline 
RECONFIG WD ENABLE & 0x5 & 1 & rw & Reconfiguration watchdog enable  \\ \hline 
RECONFIG ADDRESS & 0x6 & 32 & rw & Application image address to load  \\ \hline 
RECONFIG ANF & 0x7 & 1 & rw & Application not factory image bit  \\ \hline 
FLASH & 0x4000-0x7FFF & 32 & rw & Word access in flash \\ \hline 
\end{longtable}


\begin{longtable}{|p{4cm}|p{1cm}|p{10cm}|}
\caption{Commands to be written to the COMMAND register} \\
\hline
\textbf{command} & \textbf{value} & \textbf{description} \\ \hline \hline 
ERASE FLASH & 0x1 & Erase entire flash. Bit 16 in the STATUS register stays high until complete.  \\ \hline 
ERASE SECTOR & 0x2 & Erase sector in a flash, given by the SECTOR register. Bit 16 in the STATUS register stays high until complete.  \\ \hline 
PROTECT FACTORY & 0x3 & Write protect factory image area, unprotect the rest.  \\ \hline 
PROTECT ALL & 0x4 & Write protect the whole flash.  \\ \hline 
UNPROTECT ALL & 0x5 & Write unprotect the whole flash.  \\ \hline 
EN4BADDR & 0x6 & Enable 4 byte addressing mode. \\ \hline 
SAVE SETUP & 0x7 & Save RECONFIG WD TIMEOUT, RECONFIG WD ENABLE, RECONFIG ADDRESS to the flash.  \\ \hline 
LOAD SETUP & 0x8 & Load RECONFIG WD TIMEOUT, RECONFIG WD ENABLE, RECONFIG ADDRESS from the flash.  \\ \hline 
RECONFIGURE & 0x9 & Reconfigure.  \\ \hline 
CANCEL RECONFIG & 0xa & Cancel on-boot reconfiguration countdown.\\ \hline 
EX4BADDR & 0xb & Disable 4 byte addressing mode.\\ \hline 
\end{longtable}

\begin{longtable}{|l|l|l|l|l|}
\caption{STATUS register 16 MSB} \\
\hline
31-24 reserved & 23-19 BOOT TRIGGER & POF ERROR & RECONFIG BUSY & FLASH BUSY  \\
\hline
\end{longtable}

The 16 MSB flags are set and unset by firmware. The BOOT TRIGGER corresponds to reconfiguration IP trigger condition, which was to cause for booting the current firmware;

\begin{longtable}{|l|l|l|l|l|}
\caption{BOOT TRIGGER} \\
\hline
Watchdog & nCONFIG pin & logic & nSTATUS pin & CRC error  \\
\hline
\end{longtable}

\begin{longtable}{|l|l|l|l|}
\caption{STATUS register 16 LSB} \\
\hline
15-3 reserved & INVALID ERASE & INVALID WRITE & OP NOT EXECUTED \\
\hline
\end{longtable}

The 16 LSB flags in the status register are persistent and can be removed only when 0 is written by user to the appropriate bit.

\begin{footnotesize}
\begin{longtable}{|p{6.5cm}|p{1cm}|p{1.5cm}|p{5.5cm}|}
\caption{Parameters} \\
\hline

\textbf{parameter} & \textbf{width} & \textbf{default} &  \textbf{description} \\ \hline \hline 
SETUP\_LOAD\_ONBOOT & 1 & 0x0 & Load boot setup parameters (WD TIMEOUT, WD ENABLE, ADDRESS) from flash on boot. \\ \hline 
AUTO\_APPLICATION\_LOAD & 1 & 0x0 & Enable the automatic application image load countdown on boot.  \\ \hline 
AUTO\_APPLICATION\_TIMEOUT\_WD & 32 & $200 \cdot 10^6$ & Countdown time with WD enabled (in clock counts - 20MHz by default)  \\ \hline 
AUTO\_APPLICATION\_TIMEOUT\_NOWD & 32 & $3600 \cdot 10^6$ & Countdown time with WD disabled (in clock counts - 20MHz by default)  \\ \hline 
FACTORY\_PROTECT\_AREA & 8 & 0x60 & Command used to protect flash area with factory image. 0x60 = protect 128 first sectors from bottom (0:127), 0x3c = (0:63), 0x38 = (0:31), 0x34 = (0:15), 0x30 = (0:8)  \\ \hline 
ALL\_PROTECT\_AREA & 8 & 0x7c & Write protect whole flash command  \\ \hline 
UNPROTECT\_AREA & 8 & 0x20 & Write unprotect whole flash command  \\ \hline 
BOOT\_SETUP\_ADDRESS\_SECTOR & 9 & 0x80 & Boot setup sector address  \\ \hline 
BOOT\_SETUP\_ADDRESS\_BYTE & 16 & 0x0 & Boot setup byte address in sector \\ \hline 
APPLICATION\_ADDRESS\_SECTOR & 9 & 0x81 & Default application image sector address \\ \hline 
APPLICATION\_ADDRESS\_BYTE & 16 & 0x0 & Default application image byte address in sector\\ \hline 
\end{longtable}
\end{footnotesize}

\section{Accessing the flash}
Before the first access ever, the EPCQ256 must be switched to the 4-byte addressing mode. The switch must be written to non-volatime memory of the flash, which will make sure that the correct addressing is
selected once the FPGA powers up and tries to configure. This command must be issued before any operation with the flash takes part. Currently the Altera IP core is incapable of writing the switch to the non-volatile
part of the flash (EN3BADDR and EX3BADDR commands are temporary only). To switch the flash to correct addressing persistently, first program .jic file with the Altera programmer.

The flash is divided into 512 sectors ($2^9$), each of 65536 bytes ($2^{16}$). To access the flash, first select the sector by writing the sector number to the SECTOR register (the first sector number is 0).
Then read/write from/to appropriate FLASH register. The FLASH address size is
14 bit ($2^{14}$). Data width on the VME bus is 32 bits, hence 4 bytes are written at once. The FLASH address is therefor address of 4 byte words inside sector. This approach allows for the 2 LSB of the VME address not to be used.
The VME bus is blocked until the read/write operation completes.

The set bits (0) can not be unset (written to 1), the corresponding sector or the whole flash must be erased (sets all bits to 1). To erase a sector, select by writing to SECTOR register and issue the ERASE SECTOR command. Entire flash
can be erased with ERASE FLASH command. The erase commands take substantial amount of time, FLASH BUSY flag is high in STATUS register until the operation completes. Any access to the flash will not be carried out when flash
is busy, OP NOT EXECUTED flag will be set. The VME bus is not blocked during the operation.

If any region of the flash is write protected when it is being written or erased, the write/erase will not occur and appropriate INVALID WRITE / ERASE flag will be set in STATUS register.

To write protect the factory image area only (and unprotect the rest), issue PROTECT FACTORY command. To write protect the whole flash issue PROTECT ALL, to unprotect the whole flash issue UNPROTECT ALL.
The operation is fast and blocks VME bus until complete.

By default, the flash is segmented into 3 region, factory image, reconfiguration setup and user image space. Only the factory image region is write protected when PROTECT FACTORY
command is executed by default. The reconfiguration setup sector holds data on where the default user image resides and if the watchdog functionality is active and with what timeout.

The default region partitioning can be changed with parameters FACTORY\_PROTECT\_AREA, BOOT\_SETUP\_ADDRESS\_SECTOR/WORD, APPLICATION\_ADDRESS\_SECTOR/WORD.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\textwidth]{flash}
\caption{The default flash memory space segmentation.}
\end{figure}

\section{Reconfiguration setup flash area}
The area holds 32 bit header and three 32 bit registers with configuration valued. On boot, if SETUP\_LOAD\_ONBOOT is enabled, the firmware looks for the header, if found, setup is read and applied.
Defaults are applied otherwise.

\begin{longtable}{|p{2cm}|p{2cm}|p{3cm}|l|}
\caption{Reconfiguration setup content} \\
\hline
\textbf{address} & \textbf{width} & \textbf{default} & \textbf{description} \\ \hline \hline 
\hline
0x0 & 32 & 0xeeee1234 & Header \\ \hline 
0x4 & 12 & 0x0 & Watchdog timeout  \\ \hline 
0x8 & 1 & 0x0 & Watchdog enabled  \\ \hline 
0xC & 25 & 0x810000 & Application image start address  \\ \hline 
\end{longtable}

\section{Writing configuration image to the flash}
The sequence of bits inside bytes of the generated configuration image must be mirrored prior to writing to the flash. During the configuration, the FPGA expects
to receive the least significant bit of the
least significant byte first. The flash shifts the most significant bit of the least significant byte first. Hence the user is requested to mirror the bit position inside
bytes before writing the configuration image file (.rpd) into the flash (bit 0 to bit 7, bit 1 to bit 6, etc.). This mirroring concerns the configuration images only, it does not apply to reconfiguration setup parameters
that are also written to the flash.

It is very strongly recommended that the image starts are aligned with sector start boundaries. The minimum requirement is that the 8 LSB of start address are zero.


\section{Reconfiguration}
The FPGA loads the image residing at address 0x0 on boot. It should be mostly the factory image that is located at address 0x0. If SETUP\_LOAD\_ONBOOT is set, the factory image reads the reconfiguration setup
from the flash BOOT\_SETUP\_ADDRESS\_SECTOR/BYTE address. Default values are used otherwise (also in cases when no saved setup is found). After, depending on the watchdog settings,
countdown of AUTO\_APPLICATION\_TIMEOUT\_WD/NOWD clock cycles is executed (default 10 s with watchdog ON or 2 min with watchdog OFF). At the end of the countdown, application image is loaded if AUTO\_APPLICATION\_LOAD is enabled
and RECONFIG ANF is 0 and the boot cause is not a failed CRC or watchdog timout (STATUS register bits 23 and 19 are not set).
The RECONFIG ANF is 0 when the first (factory) image is loaded on boot and set to 1 by the firmware on the first attempt to load the application image (the user should not operate the RECONFIG ANF register).

Prior to any attempt to load the application image, the image consistency is checked. If the image is found to be damaged, the load will not proceed and POF ERROR status bit is set.

The user can cancel the countdown by issuing the CANCEL RECONFIG command during the countdown time. The timeout delay must be sufficiently large, for scenarios where watchdog is not used, to allow the
VME crate to boot after a power cycle and the user to stop the countdown. Power cycle is the only way to boot to factory image in case of damaged application image and watchdog disabled.


Once the application firmware is loaded, the manipulation of the RECONFIG WD TIMEOUT, RECONFIG WD ENABLE, RECONFIG ADDRESS, RECONFIG ANF is not possible any more. It is however possible to issue the RECONFIGURE command,
which will always load the factory firmware at address 0x0. The behavior of the factory firmware will be identical to board power cycle, hence the user must stop the automatic application firmware load countdown, if the user
wishes to remain in the factory firmware.


\begin{figure}[!htbp]
    \centering
    \resizebox{13cm}{!}{\includegraphics{functional_diagram.png}}
     \caption{Functional diagram.}
\end{figure}


\section{Watchdog}
If the watchdog is enabled, the application image (the one loaded with RECONFIG ANF set to 1), must provide the reconfiguration IP core with a specific reset at least once in the timeout cycle. The timeout is measured
by internal oscillator of approx. 10 MHz. If no reset is received, image from address 0x0 is loaded and an appropriate bit in the RECONFIG TRIGGER register is set. The WD TIMEOUT register represents 12 MSB of the
29-bit WD timeout register.

In combination with the application image consistency check and write protection of the factory image area, the watchdog provides a strong protection against bricking the VFC board. It is highly recommended to enable it.

\chapter{Default VFC factory image}
\label{chap:firmware}
A factory image for the VFC cards is provided. The image contains VME bus access, the vfc\_configuration.v module and access to board temperature and unique ID. The image is instantiated with all default values, except
that SETUP\_LOAD\_ONBOOT and AUTO\_APPLICATION\_LOAD are enabled.

With this configuration, the image will try to load boot setup configuration data from flash address 0x800000 and load application image from address 0x810000 (if the boot setup does not specify differently).
The WD will be OFF (if the boot setup does not specify differently). The application image load timeout is 2 minutes without watchdog and 10 s with watchdog enabled.

\section{Basic factory image setup}
Switch the flash into 4 byte addressing using the Altera programmer.
To setup the board with the factory image, first load the factory image .sof file to the FPGA via JTAG.
Program the factory image .rpd file into the flash memory. The flash could be programmed directly with Altera programmer via JTAG using the supplied .jic file (the Altera programmer will use
a specific small firmware that it will put to the FPGA first to access the flash). Using the .jic file is not recommended as it was found to be very unstable with multiple programming failing without notice (Quartus 14.1).
It is useful for switching the flash to the 4 byte addressing though.

Write protect the factory image by issuing the PROTECT FACTORY command.

Configure the boot setup parameters by writing to RECONFIG WD TIMEOUT, RECONFIG WD ENABLE, RECONFIG ADDRESS registers and issuing the SAVE SETUP command. Or alternatively, write the values directly to the flash (0xeeee1234 to 0x800000,
WD timeout to 0x800004, WD enable to 0x800008, application image address to 0x80000C).

Write the application image to the flash, starting at the previously set application image address.

\section{Factory image VME address map}

The following map contains VME address space of the factory firmware. The 2 unused LSB of address are cut out. Shift up by 2 bits if accessing VME on the low level.

\begin{longtable}{|p{1.75cm}|p{4cm}|p{1cm}|p{1cm}|p{2cm}|p{3.25cm}|}
\caption{Address map} \\
\hline

\textbf{address} & \textbf{register} & \textbf{width} & \textbf{r/w} & \textbf{value} & \textbf{description} \\ \hline \hline 

0x0 & BOARDID & 32 & r & 0x56464346 & ASCII for ``VFCF'' (VFC Factory) \\ \hline 
0x1 & FPGAFIRMWARE & 32 & r &  & Firmware revision  \\ \hline 
0x2 & FPGACOMPILATION & 32 & r &  & Firmware compilation time  \\ \hline 
0x3DF6 & SCRATCHPADHI & 32 & r &  & Scratchpad register content high  \\ \hline 
0x3DF7 & SCRATCHPADLO & 32 & r &  & Scratchpad register content low  \\ \hline 
0x3DF8 & CPLDPLATFORM & 32 & r & 0x56464331 & ASCII for ``VFC1''  \\ \hline 
0x3DF8 & CAPABILITY & 32 & r & 0x33 & Board capability  \\ \hline 
0x3DF8 & SERIALHI & 32 & r &  & Unique ID high  \\ \hline 
0x3DF8 & SERIALLO & 32 & r &  & Unique ID low  \\ \hline 

0xC0000 - 0xC7FFF &  &  &  &  & reconfiguration module  \\ \hline 
\end{longtable}


\chapter{Preparation of the configuration image}
\label{chap:fwimgprep}

\section{Device configuration}
The device pin options must be configured to support the appropriate configuration scheme. Configure the chip to ``Auto-restart configuration after error'', ``Device initialization clock source'' to ``Internal oscillator'',
``Active serial x4'' configuration scheme, ``Remote'' configuration mode, ``Use configuration device: EPCQ256'', make sure that ``Generate compressed bitstreams'' is off (problems were found with generating compressed
bitstreams during compilation time.

\begin{figure}[!htbp]
    \centering
    \resizebox{13cm}{!}{\includegraphics{project_setup2.png}}
     \caption{Project setup.}
\end{figure}
\begin{figure}[!htbp]
    \centering
    \resizebox{13cm}{!}{\includegraphics{project_setup.png}}
     \caption{Project setup.}
\end{figure}

\FloatBarrier

\section{Programming file creation}
The best way to create programming file is to convert the compiled .sof into .jic and .rpd. In Quartus, go to File->Convert programming file. Select ``JTAG Indirect Configuration File (.jic)'' as output programming file type.
``EPCQ256'' as configuration device, ``Active Serial x4'' mode, check the ``Create Memory Map File'' and ``Create config data RPD'' boxes. The configuration must comply with the device configuration.

Select the ``Flash Loader'', click ``Add Device...'' button and select your device. Select the ``SOF Data'', click ``Add File'' and add the .sof programming file to convert. Select the file and click the ``Properties'' button,
check the ``Compression'' box. Click ``Generate'' to create the .jic, .rpd and .map file. The .map file contains information on the addresses where the resulting compressed firmware image starts and ends in the .rpd file.

The compression is necessary if more than one image is to be saved in the flash. To create programming files with multiple images, as a first step, hit the ``Add Sof Page'' button, which will add a flash page. Select the
``Page\_0'', hit the ``Properties'' button and select the address mode to ``Start'' and input ``0x0'' as the start address. Repeat the same for ``Page\_1'' with start address at ``0x810000'' (or any other, if the vfc\_configuration
module default application image address was changed). Follow with all the previously mentioned steps, select compression for both files.

To create programming file with application image only, shift the ``Page\_0'' start to 0x810000 (or other), space before will be willed with 0xF value. When writing to the flash, one starts at 0x810000 (or other) address.

\begin{figure}[!htbp]
    \centering
    \resizebox{13cm}{!}{\includegraphics{progfile_factory.png}}
     \caption{Factory programming file creation.}
\end{figure}
\begin{figure}[!htbp]
    \centering
    \resizebox{10cm}{!}{\includegraphics{progfile_compression.png}}
     \caption{Programming file compression.}
\end{figure}
\begin{figure}[!htbp]
    \centering
    \resizebox{8cm}{!}{\includegraphics{progfile_page.png}}
     \caption{Programming file page.}
\end{figure}
\begin{figure}[!htbp]
    \centering
    \resizebox{13cm}{!}{\includegraphics{progfile_fullimage.png}}
     \caption{Full programming file.}
\end{figure}


\chapter{Drivers and python scripts}
\label{chap:driversscripts}
Default drivers for L865 and L866 are supplied, together with python scripts that can access the flash and write and verify the programming files.

To install the driver, write
\begin{footnotesize}\begin{verbatim}#+# 5 0 VME 0 BI_VFCLSD1 0 N EX DP32 6000000 2000000 N -- ---- 0 0 0 0 7 -1 5 0x5c\end{verbatim}\end{footnotesize}

line to /etc/transfer.ref. Replace the ``6000000'' with correct
card slot (base) address. Select the appropriate architecture and run the ``./install\_bi\_vfclsd1.sh`` as root.

The bimfss\_write/read/read\_status/write\_eeprom/read\_eeprom/write\_progfile.py provide access to the registers and flash.

\begin{longtable}{|p{5cm}|p{1.5cm}|p{7.5cm}|}
\caption{Registers names in driver} \\
\hline

\textbf{register} & \textbf{addr} & \textbf{register name in driver} \\ \hline \hline 
SECTOR & 0x0 & VFCCONFSECTOR \\ \hline 
COMMAND & 0x1 & VFCCONFCOMMAND  \\ \hline 
STATUS & 0x2 & VFCCONFSTATUS  \\ \hline 
RECONFIG TRIGGER & 0x3 & VFCCONFTRIGGER  \\ \hline 
RECONFIG WD TIMEOUT & 0x4 & VFCCONFWDTIMEOUT  \\ \hline 
RECONFIG WD ENABLE & 0x5 & VFCCONFENABLE  \\ \hline 
RECONFIG ADDRESS & 0x6 & VFCCONFBOOT  \\ \hline 
RECONFIG ANF & 0x7 & VFCCONFANF  \\ \hline 
FLASH & 0x4000-0x7FFF & VFCCONFEEPROM \\ \hline 
\end{longtable}

\chapter{Altera programmer usage}
\label{chap:altproguse}
This chapter shows how to program .jic file into the uninitialized board using the Altera programmer. It is also the necessary initial step to switch the flash into 4 byte addressing mode.

Start the programmer, let it detect the FPGA chip. Right click the FPGA chip, chose ''Edit`` - ''Add Flash Device``, choose the EPCQ256. Add the .jic programming file to the flash.
It is recommended to erase the entire flash before programming the .jic file. It is not necessary to select any programming image for the FPGA, the programmer will use a default Altera factory.

\begin{figure}[!htbp]
    \centering
    \resizebox{13cm}{!}{\includegraphics{programmer_flash.png}}
     \caption{Flash selection in programmer.}
\end{figure}
\begin{figure}[!htbp]
    \centering
    \resizebox{10cm}{!}{\includegraphics{programmer_setup.png}}
     \caption{Altera programmer setup.}
\end{figure}

\section{Example board initialization}
\label{sec:boardinit}
Example sequence to initiate a new board, after the vfc\_factory.sof file was loaded into the FPGA with JTAG and the flash was switched to 4 byte addressing using
the Altera programmer.

\begin{enumerate}
 \item python ./bimfss\_read\_status.py  -- verify the communication with the board
 \item python ./bimfss\_write.py VFCCONFCOMMAND 0x5   -- write unprotect the flash (do not worry about readback)
 \item python ./bimfss\_write.py VFCCONFCOMMAND 0x1   -- erase the entire flash (do not worry about readback)
 \item python ./bimfss\_read.py VFCCONFSTATUS   -- check the status and wait for the busy bit (16) to go to 0, the erase takes substantial time (minutes)
 \item python ./bimfss\_read\_eeprom.py 0x0 0x0   -- read flash at sector 0x0 address 0x0, should return 0xFFFFFFFF
 \item python ./bimfss\_write\_eeprom.py 0x0 0x0 0x12345678   -- write to 0x0 0x0, check for correct readback
 \item python ./bimfss\_write.py VFCCONFCOMMAND 0x2   -- erase the sector 0x0 (sector 0x0 already selected)
 \item python ./bimfss\_read\_eeprom.py 0x0 0x0   -- read flash at sector 0x0 address 0x0, should return 0xFFFFFFFF
 \item python ./bimfss\_read.py VFCCONFSTATUS   -- check the status and wait for the busy bit (16) to go to 0
 \item python ./bimfss\_write\_progfile.py vfc\_factory\_auto.rpd   -- write the factory programming file (should take about 3 minutes)
 \item python ./bimfss\_write.py VFCCONFCOMMAND 0x3   -- write protect the factory image
 \item python ./bimfss\_write\_eeprom.py 0x80 0x0 0xeeee1234   -- write header to the boot configuration sector (0x80)
 \item python ./bimfss\_write\_eeprom.py 0x80 0x1 0x200   -- set approx 10s timeout for the watchdog
 \item python ./bimfss\_write\_eeprom.py 0x80 0x2 0x1   -- enable the watchdog
 \item python ./bimfss\_write\_eeprom.py 0x80 0x3 0x810000   -- set application image address to 0x810000 (sector 0x81)
 \end{enumerate}

Note: disable the watchdog if your application image does not support it. In such case, the boot delay from factory to application will be 2 minutes by default.

