# VFC-HD_System

This repository contains the "system" part of the VFC-HD project. It implements the functionality that is common to every project based on the VFC-HD.

## Usage

The top level module is:

https://gitlab.cern.ch/bi/VFC/VFC-HD_System/blob/master/hdl/modules/VfcHdSystem.sv

To include it in your Quartus project, a QIP file is provided:

https://gitlab.cern.ch/bi/VFC/VFC-HD_System/blob/master/hdl/modules/VfcHdSystem.qip

## Memory Map

The memory map is defined by the Cheby file:

https://gitlab.cern.ch/bi/VFC/VFC-HD_System/-/blob/master/hdl/cheby/System.cheby

The following table is provided as a summary:

| name          | vme addr | size  | mode | comment
| ------------- | -------- | ----- | ---- | -------
| SysRelease    | 0x00     | 1     | r    | System release ID
| SysIdent      | 0x04     | 3     | r    | System ident string (12 chars)
| AppRelease    | 0x10     | 1     | r    | Application release ID (YYMMDDxx)
| AppIdent      | 0x14     | 11    | r    | Application ident string (44 chars)
| IntSource     | 0x40     | 1     | r    | Interrupt source
| IntConfig     | 0x44     | 1     | rw   | Interrupt configuration
| IntMask       | 0x48     | 1     | rw   | Interrupt source mask
| IntVector     | 0x4C     | 1     | rw   | Interrupt vector
| IntLevel      | 0x50     | 1     | rw   | Interrupt level
| IntReqCount   | 0x54     | 1     | r    | Interrupt requested count
| IntAckCount   | 0x58     | 1     | r    | Interrupt acknowledged count
| IntSrcRdCount | 0x5C     | 1     | r    | Interrupt source read count
| UniqueId      | 0x60     | 2     | r    | Unique ID
| BoardTemp     | 0x68     | 1     | r    | Board temperature
| ChipTemp      | 0x6C     | 1     | r    | Temperature from internal FPGA sensor
| I2cMAECntrl   | 0x80     | 1     | rw   | I2C mux and IO expander arbiter control reg
| I2cMAEAdrDat  | 0x84     | 1     | rw   | I2C mux and IO expander arbiter address/data reg
| I2cMAECmdSts  | 0x88     | 1     | rw   | I2C mux and IO expander arbiter command/status reg
| VadjCtrl      | 0x90     | 1     | rw   | Manual Vadj voltage control
| ApSfp1Status  | 0xA0     | 1     | r    | Application SFP 1 status
| ApSfp2Status  | 0xA4     | 1     | r    | Application SFP 2 status
| ApSfp3Status  | 0xA8     | 1     | r    | Application SFP 3 status
| ApSfp4Status  | 0xAC     | 1     | r    | Application SFP 4 status
| EthSfpStatus  | 0xB0     | 1     | r    | Ethernet SFP status
| BstSfpStatus  | 0xB4     | 1     | r    | BST SFP status
| PreVadj       | 0xC0     | 1     | r    | PRE_VADJ voltage
| VccPdAdj      | 0xC4     | 1     | r    | VCCPD_ADJ voltage
| Vadj          | 0xC8     | 1     | r    | VADJ voltage
| VioBM2C       | 0xCC     | 1     | r    | VIO_B_M2C voltage
| V3p3Fpga      | 0xD0     | 1     | r    | V3P3_FPGA voltage
| V3p3Fmc       | 0xD4     | 1     | r    | V3P3_FMC voltage
| V2p5          | 0xD8     | 1     | r    | V2P5 voltage
| V12p0Fmc      | 0xDC     | 1     | r    | V12P0_FMC voltage
| BstIdent      | 0x800    | 1     | r    | BST decoder ident
| BstRelease    | 0x804    | 1     | r    | BST decoder release
| BstStatus     | 0x808    | 1     | r    | BST decoder SFP and CDR status
| BstDelay      | 0x80C    | 1     | rw   | BST decoder bunch/turn clock delay
| BstMessage    | 0xC00    | 256   | r    | BST decoder message
| ConfSector    | 0x20000  | 1     | rw   | Reconfiguration sector address
| ConfCommand   | 0x20004  | 1     | w    | Reconfiguration command
| ConfStatus    | 0x20008  | 1     | r    | Reconfiguration status
| ConfTrigger   | 0x2000C  | 1     | rw   | Reconfiguration trigger source
| ConfWdTimeout | 0x20010  | 1     | rw   | Reconfiguration watchdog timeout
| ConfWdEnable  | 0x20014  | 1     | rw   | Reconfiguration watchdog enable
| ConfBoot      | 0x20018  | 1     | rw   | Reconfiguration boot address
| ConfAnf       | 0x2001C  | 1     | rw   | Reconfiguration AnF flag
| ConfShiftBuf  | 0x20020  | 1     | rw   | Reconfiguration shift buffer
| ConfWrPage    | 0x20024  | 1     | rw   | Reconfiguration write buffer in EEPROM
| ConfEeprom    | 0x30000  | 16384 | rw   | Reconfiguration EEPROM access
