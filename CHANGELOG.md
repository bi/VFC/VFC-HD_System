# VFC-HD_System changelog

## V2.6 (00020600)

### Application interface changes from V2.5

There are no changes to the application interface.

### Driver interface changes from V2.5

Adds the registers `ConfShiftBuf` and `ConfWrPage` to allow faster parallel
flashing of multiple VFCs.

| name          | vme addr | size  | mode | comment
| ------------- | -------- | ----- | ---- | -------
| ConfShiftBuf  | 0x20020  | 1     | rw   | Reconfiguration shift buffer
| ConfWrPage    | 0x20024  | 1     | rw   | Reconfiguration write buffer in EEPROM

## V2.5 (00020500)

### Application interface changes from V2.4

There are no changes to the application interface.

### Driver interface changes from V2.4

Adds the registers `I2cMAEAdrDat` and `I2cMAECmdSts` to allow I2C transactions
to be initiated via VME. Previously this functionality was only available as
a WB interface from the Application part.

| name          | vme addr | size  | mode | comment
| ------------- | -------- | ----- | ---- | -------
| I2cMAECntrl   | 0x80     | 1     | rw   | I2C mux and IO expander arbiter control reg
| I2cMAEAdrDat  | 0x84     | 1     | rw   | I2C mux and IO expander arbiter address/data reg
| I2cMAECmdSts  | 0x88     | 1     | rw   | I2C mux and IO expander arbiter command/status reg

## V2.4 (00020400)

### Application interface changes from V2.3

#### Extra diagnostics

The chip temperature and VME geographical address are now propagated to the
Application. This implies the following changes to the ports:

```diff
     input          UniqueIdValid_i,
     input   [15:0] Temp_ib16,
     input          TempRead_ip,
+    input   [ 7:0] ChipTemp_ib8,
+    input          ChipTempRead_ip,
     input   [11:0] Voltage0_ib12,   // PRE_VADJ
     input   [11:0] Voltage1_ib12,   // VCCPD_ADJ
     input   [11:0] Voltage2_ib12,   // VADJ
     input   [11:0] Voltage3_ib12,   // VIO_B_M2C
     input   [11:0] Voltage4_ib12,   // V3P3_FPGA
     input   [11:0] Voltage5_ib12,   // V3P3_FMC
     input   [11:0] Voltage6_ib12,   // V2P5
     input   [11:0] Voltage7_ib12,   // V12P0_FMC
-    input          VoltageRead_ip
+    input          VoltageRead_ip,
+
+    // VME geographical address
+    input   [ 4:0] VmeGa_inb5,
+    input          VmeGap_in
 );
 
 `include "VadjConfig.vh"
```

#### Renamed AppInfo to AppIdent

For consistency the AppInfo has been renamed to AppIdent. This implies the
renaming of the generic:

```diff
               g_ApplicationReleaseMonth_b8 = 8'h00,
               g_ApplicationReleaseYear_b8  = 8'h00,
 
-    // Application info string, up to 352 bits (44 ASCII characters)
-    parameter g_AppInfo_b352 = "VFC-HD Golden Image"
+    // Application ident string, up to 352 bits (44 ASCII characters)
+    parameter g_AppIdent_b352 = "VFC-HD Golden Image"
 ) (
```

port:

```diff
     input          VmeSysReset_iarn,    // Asynchronous VME Sys Reset signal
     input          AppReset_iqr,        // Synchronous reset for the application
 
-    // Application release ID and info string
+    // Application release ID and ident string
     output [  7:0] AppVersion_ob8, AppReleaseDay_ob8, AppReleaseMonth_ob8, AppReleaseYear_ob8,
-    output [351:0] AppInfo_ob352,
+    output [351:0] AppIdent_ob352,
 
     // Vadj control
     output         VfmcEnable_oe,

```

and assignment:

```diff
 assign AppReleaseDay_ob8   = g_ApplicationReleaseDay_b8  [7:0];
 assign AppReleaseMonth_ob8 = g_ApplicationReleaseMonth_b8[7:0];
 assign AppReleaseYear_ob8  = g_ApplicationReleaseYear_b8 [7:0];
-assign AppInfo_ob352       = {g_AppInfo_b352, {352-$size(g_AppInfo_b352){1'b0}}};
+assign AppIdent_ob352      = {g_AppIdent_b352, {352-$size(g_AppIdent_b352){1'b0}}};
 
 //----------------------------------------------------------------------
 // Fixed assignments
```

#### Combine AppRelease*_ob8 into AppRelease_ob32

The previous `AppRelease*_ob8` ports have been combined into a single 32-bit
output port. This implies the renaming of the port:

```diff
     input          AppReset_iqr,        // Synchronous reset for the application
 
     // Application release ID and ident string
-    output [  7:0] AppVersion_ob8, AppReleaseDay_ob8, AppReleaseMonth_ob8, AppReleaseYear_ob8,
+    output [ 31:0] AppRelease_ob32,
     output [351:0] AppIdent_ob352,
 
     // Vadj control
```

and the assignment:

```diff
 // Application release info
 //----------------------------------------------------------------------
 
-assign AppVersion_ob8      = g_ApplicationVersion_b8     [7:0];
-assign AppReleaseDay_ob8   = g_ApplicationReleaseDay_b8  [7:0];
-assign AppReleaseMonth_ob8 = g_ApplicationReleaseMonth_b8[7:0];
-assign AppReleaseYear_ob8  = g_ApplicationReleaseYear_b8 [7:0];
-assign AppIdent_ob352      = {g_AppIdent_b352, {352-$size(g_AppIdent_b352){1'b0}}};
+assign AppRelease_ob32 = {g_ApplicationReleaseYear_b8 [7:0],
+                          g_ApplicationReleaseMonth_b8[7:0],
+                          g_ApplicationReleaseDay_b8  [7:0],
+                          g_ApplicationVersion_b8     [7:0]};
+assign AppIdent_ob352  = {g_AppIdent_b352, {352-$size(g_AppIdent_b352){1'b0}}};
 
 //----------------------------------------------------------------------
 // Fixed assignments
```

#### Boot ROM moved from System to Application

The Boot ROM functionality has been removed from the System part and now should
be implemented in your Application. You should remove the `BOOT_ROM`,
`BOOT_ROM_MAX_TIME` and `BOOT_ROM_MAX_WAIT` from your **VfcHdConfig.vh** (if
present) and implement the following logic in your Application, modifying the
generics based on the values you had set:

```verilog
//----------------------------------------------------------------------
// Boot ROM
//----------------------------------------------------------------------

wire [21:0] WbSlaveAdr_ob22;

BootRomWbMaster #(
    .g_ClkFrequency             (g_ClkFrequency),
    .g_InitFile                 ("../mif/BootRom.mif"),
    .p_MaxBootTimeMicrosec      (3_000_000),    // Maximum overall time, in us
    .p_MaxAckWaitTimeMicrosec   (10_000)        // Maximum ack wait time for each command, in us
) i_BootRomWbMaster (
    .Clk_ik     (Clk_k),
    .Rst_irq    (AppReset_iqr),                 // Reset (initiate) on application reset release
    .SelBoot_oq (),                             // Bus takeover flag
    .Cyc_oq     (WbSlaveCyc_o),
    .Stb_oq     (WbSlaveStb_o),
    .We_oq      (WbSlaveWr_o),
    .Adr_oqb22  (WbSlaveAdr_ob25[21:0]),
    .Dat_oqb32  (WbSlaveDat_ob32),
    .Ack_i      (WbSlaveAck_i));

assign WbSlaveAdr_ob25[24:22] = 3'b0;
```

### Driver interface changes from V2.3

There are no changes to the driver interface.

## V2.3 (00020300)

Move to a "semantic versioning" scheme for the system release (read from the
*SysRelease* register). The following scheme is used:

*  **SysRelease[31:24]:** Always zero to help distingush old vs. new system
   release formats. For old (datecode) release formats this byte will contain
   the year and hence will not be zero.
*  **SysRelease[23:16]:** Major version (i.e "2"). Will be incremented when
   any change is made to the register map and implies a change of the driver is
   required.
*  **SysRelease[15:8]:** Minor version (i.e "3"). Will be incremented when
   any change is made to the HDL interface towards the application part and
   implies a change of user HDL code is required, but no change on the driver
   is required.
*  **SysRelease[7:0]:** Tiny version (i.e. "0"). Will be incremented for
   any release which does not change either the driver or HDL interface.

## V2.2 (19042900)

### Application interface changes from V2.1

#### Updated BST interface

The BST interface has been modified. The port *TurnClkFlagDly_ob12* has been
removed, the delay value is now taken from a register in the system part. Both
the undelayed and delayed versions of the *BunchClkFlag* and *TurnClkFlag* are
now provided to the application. If additional delayed versions of the flags
are required, the module *BstDelay* from the [BST_FPGA repository](https://gitlab.cern.ch/bi/HDL_Cores/BST_FPGA)
can be implemented in the application part.

In addition the port *BstByte_ib8* has been renamed *BstByteData_ib8* for
consistency and the port *BstOn_i* has been replaced by *BstRst_ir*.

```diff
     output [  8:1] Led_ob8,
     input  [  8:1] LedStatus_ib8,
 
-    // BST interface
-    input          BstOn_i,
-    input          BstClk_ik,           // _|"|_|"|_|"|_|"|_|"|_|"|_ ~160 MHz
-    input          BunchClkFlag_i,      // _|"""|___________|"""|___  ~40 MHz
-    input          TurnClkFlag_i,       // _|"""|___________________  ~11 kHz (LHC) / ~43kHz (SPS)
-    output [ 11:0] TurnClkFlagDly_ob12,
-    input  [  7:0] BstByteAddress_ib8,
-    input  [  7:0] BstByte_ib8,
+    // BST interface                         _   _   _   _   _   _   _   _
+    input          BstClk_ik,           // _/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_ ~160 MHz
+    input          BstRst_ir,           //  :___    :        ___
+    input          BunchClkFlag_i,      // _/   \___:___ ___/   \___ ___ ___
+    input          BunchClkFlagDly_i,   // _:___ ___/   \___________/   \___ } ~40 MHz
+    input          TurnClkFlag_i,       // _/   \___:____ __________________
+    input          TurnClkFlagDly_i,    // _:_______/    \__________________ } ~11 kHz (LHC) / ~43 kHz (SPS)
+                                        //  <--Dly-->
+    input  [  7:0] BstByteAddress_ib8,
+    input  [  7:0] BstByteData_ib8,
     input          BstByteStrobe_i,
     input          BstByteError_i,
```

### Driver interface changes from V2.1

The register block for the BST decoder has been expanded:

| name          | vme addr | size  | mode | comment
| ------------- | -------- | ----- | ---- | -------
| BstIdent      | 0x800    | 1     | r    | BST decoder ident
| BstRelease    | 0x804    | 1     | r    | BST decoder release
| BstStatus     | 0x808    | 1     | r    | BST decoder SFP and CDR status
| BstDelay      | 0x80C    | 1     | rw   | BST decoder bunch/turn clock delay

The change is backwards compatible with previous driver versions.

## V2.1 (19041000)

### Application interface changes from V2.0

#### Additional parameters

The following parameters are added to the *VfcHdApplication* module:

```diff
+    parameter g_ClkFrequency,
+    parameter g_Synthesis = 1'b1,
```

#### New reset scheme

The reset of the system part is now provided from the application part via the
port *SysReset_oar* and the *VmeSysReset_iarn* is now connected to the
application. The synchronous reset *AppReset_iqr* is maintained as a convenience
for use in the application part.

```diff
     // Resets Scheme:
-    input          AppReset_iqr,
+    input          AppReset_iqr,        // Synchronous reset for the application
+    output         SysReset_oar,        // Reset for the system part
+    input          VmeSysReset_iarn,    // Asynchronous VME Sys Reset signal
```

Unless a more complicated reset scheme is required, the following connection
should be made in most cases:

```diff
 //==== Clocks Scheme ====//
 
 // Clock for system part. The SYS_CLK_FREQ define in the VfcHdConfig.vh
 // must be updated with the frequency of the clock supplied here.
 assign SysClk_ok = GbitTrxClkRefR_ik;
 
+//==== Reset Scheme ====//
+
+// Asynchronous reset for the system part, by default VmeSysReset.
+// Note that AppReset_iqr provides a synchronised version of this reset
+// for convenience.
+assign SysReset_oar = !VmeSysReset_iarn;
+
```

#### Additional diagnostics connections

The diagnostics are now available in the application part:

```diff
     //==== OCT control sharing ====//
     input   [15:0] OctSeriesControl_ib16,
-    input   [15:0] OctParallelControl_ib16
+    input   [15:0] OctParallelControl_ib16,

+    // Diagnostics
+    input   [63:0] UniqueId_ib64,
+    input          UniqueIdValid_i,
+    input   [15:0] Temp_ib16,
+    input          TempRead_ip,
+    input   [11:0] Voltage0_ib12,   // PRE_VADJ
+    input   [11:0] Voltage1_ib12,   // VCCPD_ADJ
+    input   [11:0] Voltage2_ib12,   // VADJ
+    input   [11:0] Voltage3_ib12,   // VIO_B_M2C
+    input   [11:0] Voltage4_ib12,   // V3P3_FPGA
+    input   [11:0] Voltage5_ib12,   // V3P3_FMC
+    input   [11:0] Voltage6_ib12,   // V2P5
+    input   [11:0] Voltage7_ib12,   // V12P0_FMC
+    input   [ 7:0] VoltageOk_ib8,
+    input          VoltageRead_ip
```

### Driver interface changes from V2.0

There are no changes to the driver interface.

## V2.0 (18112300)

### Application interface changes from V1.x

### Vadj control

The control of the Vadj voltage is now from the application part using the
*VadjSelect_ob3* port (n.b. the enumeration values are available in the header
file *VadjConfig.vh*).

The active low *VfmcEnable_oen* port is replaced by and active high
*VfmcEnable_oe* port and a corresponding input *VfmcEnabled_i* is added to
communicate the actual status to the application.

```diff
     // TestIo MMCX Connectors:
     inout          TestIo1_io,
     inout          TestIo2_io,
+    // Vadj Control:
+    output         VfmcEnable_oe,
+    input          VfmcEnabled_i,
+    output [  2:0] VadjSelect_ob3,
     // FMC HPC Connector:
-    output         VfmcEnable_oen,
     input          FmcPrsntM2c_in,
```

The default connections are as follows:

```diff
 //==== Fixed Assignments ====//
 
 assign OeSi57x_oe     = 1'b0;
-assign VfmcEnable_oen = 1'b1; // FMC voltages disabled
-assign FmcPgC2m_o     = ~VfmcEnable_oen;
+assign VfmcEnable_oe  = 1'b0;               // FMC voltages disabled
+assign FmcPgC2m_o     = VfmcEnabled_i;
+assign VadjSelect_ob3 = c_VadjSelect_V2P50; // Values defined in VadjConfig.vh
```

#### System clock

The system clock is now provided by the application part. The input
*Sys125MhzClk_ik* has been removed and an output *SysClk_ok* has been added.

```diff
     //==== System-Application interface ====//
 
-    // Reference Clock for the WishBone Interface
-    input          Sys125MhzClk_ik,
+    // Clock for the System part:
+    output         SysClk_ok,
```

To use the 125 MHz VCXO for the system part, the following connection must be
made in the application:

```diff
 //==== Clocks Scheme ====//
 
+// Clock for system part. The SYS_CLK_FREQ define in the VfcHdConfig.vh
+// must be updated with the frequency of the clock supplied here.
+assign SysClk_ok = GbitTrxClkRefR_ik;
```

#### Interrupt available

A new input *InterruptAvailable_ib24* has been added to communicate whether
the interrupt controller will accept a particular interrupt:

```diff
     // Interrupt Request:
     output [ 23:0] InterruptRequest_ob24,
+    input  [ 23:0] InterruptAvailable_ib24,
```

### Driver interface changes from V1.x

The driver inteface has significant changes and is not backwards compatible
with V1.x.
