#!/usr/bin/env tclsh

#==============================================================================#
#                                                                              #
#  VFC-HD post-module TCL script                                               #
#                                                                              #
#  Author: Tom Levens <tom.levens@cern.ch>                                     #
#                                                                              #
#------------------------------------------------------------------------------#
#                                                                              #
#  Automatically create the JIC/RPD programming files and increment the        #
#  application version number.                                                 #
#                                                                              #
#  NOTE: include in the QSF with:                                              #
#                                                                              #
#   set_global_assignment -name POST_MODULE_SCRIPT_FILE "quartus_sh:post.tcl"  #
#                                                                              #
#==============================================================================#

# Application instance name
set inst_application "VfcHdApplication:i_VfcHdApplication"

# Property names
set prop_version     "g_ApplicationVersion_b8"

#------------------------------------------------------------------------------#

set module        [lindex $quartus(args) 0]
set project_name  [lindex $quartus(args) 1]
set revision_name [lindex $quartus(args) 2]

if {[string match "quartus_asm" $module]} {
    # Open project
    project_open $project_name -revision $revision_name

    post_message "post_module.tcl: opened project $project_name, revision $revision_name"

    # Convert programming file
    set cof $project_name
    append cof ".cof"

    if {[file exists $cof] == 0} {
        post_message -type critical_warning "post_module.tcl: could not convert programming file, $cof does not exist"
    } else {
        post_message "post_module.tcl: converting programming file using $cof"
        qexec "quartus_cpf -c $cof"
    }

    # Increment build number in QSF
    set build [expr [get_parameter -name $prop_version -to $inst_application] + 1]

    post_message "post_module.tcl: next build number will be $build"

    set_parameter -name $prop_version $build -to $inst_application

    # Close project
    project_close
}

