set_global_assignment -name PRE_FLOW_SCRIPT_FILE "quartus_sh:../tcl_scripts/pre_flow.tcl"
set_global_assignment -name POST_MODULE_SCRIPT_FILE "quartus_sh:../tcl_scripts/post_module.tcl"

set_global_assignment -name FAMILY "Arria V"
set_global_assignment -name DEVICE 5AGXMB1G4F40C4
set_global_assignment -name TOP_LEVEL_ENTITY VfcHdTop
set_global_assignment -name ORIGINAL_QUARTUS_VERSION 16.0.0
set_global_assignment -name PROJECT_CREATION_TIME_DATE "19:01:39  DECEMBER 12, 2016"
set_global_assignment -name LAST_QUARTUS_VERSION "19.1.0 Standard Edition"
set_global_assignment -name MIN_CORE_JUNCTION_TEMP 0
set_global_assignment -name MAX_CORE_JUNCTION_TEMP 85
set_global_assignment -name ERROR_CHECK_FREQUENCY_DIVISOR 256
set_global_assignment -name PARTITION_NETLIST_TYPE SOURCE -section_id Top
set_global_assignment -name PARTITION_FITTER_PRESERVATION_LEVEL PLACEMENT_AND_ROUTING -section_id Top
set_global_assignment -name PARTITION_COLOR 16764057 -section_id Top
set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "23 MM HEAT SINK WITH 200 LFPM AIRFLOW"
set_global_assignment -name POWER_BOARD_THERMAL_MODEL "NONE (CONSERVATIVE)"
set_global_assignment -name ENABLE_OCT_DONE OFF
set_global_assignment -name STRATIXV_CONFIGURATION_SCHEME "ACTIVE SERIAL X4"
set_global_assignment -name USE_CONFIGURATION_DEVICE ON
set_global_assignment -name STRATIXII_CONFIGURATION_DEVICE EPCQ256
set_global_assignment -name STRATIXIII_UPDATE_MODE REMOTE
set_global_assignment -name CRC_ERROR_OPEN_DRAIN ON
set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -rise
set_global_assignment -name OUTPUT_IO_TIMING_NEAR_END_VMEAS "HALF VCCIO" -fall
set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -rise
set_global_assignment -name OUTPUT_IO_TIMING_FAR_END_VMEAS "HALF SIGNAL SWING" -fall
set_global_assignment -name ACTIVE_SERIAL_CLOCK FREQ_100MHZ
set_global_assignment -name ENABLE_CONFIGURATION_PINS OFF
set_global_assignment -name ENABLE_BOOT_SEL_PIN OFF
set_global_assignment -name ON_CHIP_BITSTREAM_DECOMPRESSION OFF
set_global_assignment -name TIMING_ANALYZER_MULTICORNER_ANALYSIS ON
set_global_assignment -name SMART_RECOMPILE OFF
set_global_assignment -name NUM_PARALLEL_PROCESSORS ALL
set_global_assignment -name VERILOG_INPUT_VERSION SYSTEMVERILOG_2005
set_global_assignment -name VERILOG_SHOW_LMF_MAPPING_MESSAGES OFF
set_global_assignment -name ENABLE_SIGNALTAP OFF

set_parameter -name g_IsGoldenImage 1 -to "VfcHdSystem:i_VfcHdSystem"
set_parameter -name g_ApplicationVersion_b8 1 -to "VfcHdApplication:i_VfcHdApplication"
set_parameter -name g_ApplicationReleaseDay_b8 22 -to "VfcHdApplication:i_VfcHdApplication"
set_parameter -name g_ApplicationReleaseMonth_b8 16 -to "VfcHdApplication:i_VfcHdApplication"
set_parameter -name g_ApplicationReleaseYear_b8 32 -to "VfcHdApplication:i_VfcHdApplication"

set_global_assignment -name VHDL_INPUT_VERSION VHDL_2008
set_global_assignment -name VHDL_SHOW_LMF_MAPPING_MESSAGES OFF
set_global_assignment -name EDA_SIMULATION_TOOL "ModelSim (Verilog)"
set_global_assignment -name EDA_TIME_SCALE "1 ps" -section_id eda_simulation
set_global_assignment -name EDA_OUTPUT_DATA_FORMAT "VERILOG HDL" -section_id eda_simulation

# ####################
# VCCIO config
# ####################

# P3V3 BANKS
set_global_assignment -name IOBANK_VCCIO 3.3V -section_id 3A
set_global_assignment -name IOBANK_VCCIO 3.3V -section_id 3B
set_global_assignment -name IOBANK_VCCIO 3.3V -section_id 3C
set_global_assignment -name IOBANK_VCCIO 3.3V -section_id 3D

# P1V35 BANKS
set_global_assignment -name IOBANK_VCCIO 1.35V -section_id 4B
set_global_assignment -name IOBANK_VCCIO 1.35V -section_id 4A
set_global_assignment -name IOBANK_VCCIO 1.35V -section_id 4C
set_global_assignment -name IOBANK_VCCIO 1.35V -section_id 4D

# VIO_B BANKS
# Change also IO_STANDARD_VIOB in VfcHdTop.sv when changing this voltage.
set_global_assignment -name IOBANK_VCCIO 2.5V -section_id 7A
set_global_assignment -name IOBANK_VCCIO 2.5V -section_id 7B

# VADJ BANKS
# Change also IO_STANDARD_VADJ in VfcHdTop.sv when changing this voltage.
set_global_assignment -name IOBANK_VCCIO 2.5V -section_id 7C
set_global_assignment -name IOBANK_VCCIO 2.5V -section_id 7D
set_global_assignment -name IOBANK_VCCIO 2.5V -section_id 8D
set_global_assignment -name IOBANK_VCCIO 2.5V -section_id 8C
set_global_assignment -name IOBANK_VCCIO 2.5V -section_id 8B
set_global_assignment -name IOBANK_VCCIO 2.5V -section_id 8A

set_global_assignment -name SEARCH_PATH ../../modules/vfchd_golden
set_global_assignment -name SEARCH_PATH ../../modules/

set_global_assignment -name SYSTEMVERILOG_FILE ../../modules/vfchd_golden/VfcHdApplication.sv
set_global_assignment -name QIP_FILE ../../modules/VfcHdSystem.qip
set_global_assignment -name SDC_FILE VfcHd_Golden.sdc

set_global_assignment -name PROJECT_OUTPUT_DIRECTORY output_files


set_instance_assignment -name PARTITION_HIERARCHY root_partition -to | -section_id Top