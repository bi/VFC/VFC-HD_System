//----------------------------------------------------------------------
// Title      : SFP Status Registers
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : SfpStatusRegs.sv
// Author     : T. Levens
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Readback registers for the SFP statuses.
//----------------------------------------------------------------------

`timescale 1ns/100ps

module SfpStatusRegs (
    input             Clk_ik,
    input             Rst_irq,
    input             Cyc_i,
    input             Stb_i,
    input      [ 2:0] Adr_ib3,
    output reg [31:0] Dat_oab32,
    output reg        Ack_oa,
    input      [31:0] AppSfp1Status_ib32,
    input      [31:0] AppSfp2Status_ib32,
    input      [31:0] AppSfp3Status_ib32,
    input      [31:0] AppSfp4Status_ib32,
    input      [31:0] EthSfpStatus_ib32,
    input      [31:0] BstSfpStatus_ib32
);

always @(posedge Clk_ik)
    if (Rst_irq) begin
        Dat_oab32                  <= #1 32'h0;
        Ack_oa                     <= #1  1'b0;
    end else begin
        if (~Ack_oa) begin
            case (Adr_ib3)
                3'b000:  Dat_oab32 <= #1 AppSfp1Status_ib32;
                3'b001:  Dat_oab32 <= #1 AppSfp2Status_ib32;
                3'b010:  Dat_oab32 <= #1 AppSfp3Status_ib32;
                3'b011:  Dat_oab32 <= #1 AppSfp4Status_ib32;
                3'b100:  Dat_oab32 <= #1 EthSfpStatus_ib32;
                3'b101:  Dat_oab32 <= #1 BstSfpStatus_ib32;
                default: Dat_oab32 <= #1 32'h0;
            endcase
        end
        Ack_oa                     <= #1 Stb_i && Cyc_i;
    end

endmodule
