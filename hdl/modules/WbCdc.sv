//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: WbCdc.sv
//
// File versions history:
//
//  DATE        VERSION  AUTHOR                                 DESCRIPTION
//  11.10.2017  1        J. Pospisil <j.pospisil@cern.ch>       first version
//  25.01.2024  2        T. Levens   <tom.levens@cern.ch>       convert t_WbInterface to ports
//  15.02.2024  3        M. Saccani  <mathieu.saccani@cern.ch>  add FSM to properly handle the REQ/ACK for 
//                                                              consecutive accesses and slow slave clock
//  04.03.2025  4        M. Saccani  <mathieu.saccani@cern.ch>  add Wb pipelined compatibility 
//                                                              as VFC-HD VME MBLT access is WB pipelined
//
// Language: SystemVerilog 2012
//
// Targeted device:
//
//     - Vendor:  agnostic
//     - Model:   agnostic
//
// Description:
//
//      Cross Domain Crossing for Wishbone bus (compatible with WB pipelined)
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

module WbCdc #(
    parameter g_DataWidth = 32,
    parameter g_AddressWidth = 32
)(
    // towards master with some clock
    input  Master_Clk_ik,
    input  Master_Rst_i,
    input  Master_Cyc_i,
    input  Master_Stb_i,
    input  [g_AddressWidth-1:0] Master_Adr_ib,
    input  Master_We_i,
    input  [g_DataWidth-1:0] Master_DatMoSi_ib,
    output Master_Ack_o,
    output [g_DataWidth-1:0] Master_DatMiSo_ob,

    // towards slave with another clock
    input  Slave_Clk_ik,
    input  Slave_Rst_i,
    output Slave_Cyc_o,
    output Slave_Stb_o,
    output [g_AddressWidth-1:0] Slave_Adr_ob,
    output Slave_We_o,
    output [g_DataWidth-1:0] Slave_DatMoSi_ob,
    input  Slave_Ack_i,
    input  [g_DataWidth-1:0] Slave_DatMiSo_ib
);

    localparam c_SignalDelay = 3; // delay for main controlling signals
    localparam c_OtherDelay = 2;  // delay for data-like signals
    
    // Master FSM
    reg MasterState = s_MasterIdle;
    localparam s_MasterIdle = 1'h0;
    localparam s_MasterAck = 1'h1;

    
    wire Master_Ack;
    reg Master_Ack_int;
    reg MasterToggle;
    reg Master_Ack_q = 1'b0;
    wire [g_DataWidth-1:0] Master_DatMiSo;
    
    // Slave FSM
    reg SlaveToggle = 1'b0;
    reg SlaveToggle_q = 1'b0;
    reg SlaveState = s_SlaveIdle;
    localparam s_SlaveIdle = 1'h0;
    localparam s_SlaveAck = 1'h1;
    reg Slave_Ack;

    // WB Master state machine
    always @(posedge Master_Clk_ik)
    begin
        if( Master_Rst_i )
        begin
            Master_Ack_int    <= 1'b0;
            Master_DatMiSo_ob <= {g_DataWidth{1'b0}};
            MasterState       <= s_MasterIdle;
            MasterToggle      <=  1'b0;
        end
        else 
        begin
            //pipeline reg
            Master_Ack_q <= Master_Ack;
            
            case( MasterState )
                
                // wait for any activity
                s_MasterIdle: 
                begin
                    // on-going access
                    if( Master_Cyc_i == 1'b1 && Master_Stb_i == 1'b1)
                    begin
                        MasterState  <= s_MasterAck;
                        MasterToggle <= !MasterToggle;
                    end
                end
                
                // ACK
                s_MasterAck: 
                begin
                    // sample data on ACK falling edge (pulse)
                    if( Master_Ack == 1'b0 && Master_Ack_q == 1'b1)
                    begin
                        Master_DatMiSo_ob <= Master_DatMiSo;
                        Master_Ack_int    <= 1'b1;
                    end 
                    //generate a single pulse
                    else if( Master_Ack_int == 1'b1)
                    begin
                        Master_Ack_int <= 1'b0;
                    end
                    //wait for the wb release
                    else if( Master_Stb_i == 1'b0)
                    begin
                        MasterState    <= s_MasterIdle;
                    end
                end
            endcase
        end
    end
    
    //mask ack with stb (to save 1 cycle)
    //useful in MBLT when the wishbone makes pipelined access
    assign Master_Ack_o = Master_Ack_int && Master_Stb_i;

    //Synchronisers slave to master
    
    //data
    SignalSyncer #(
        .g_Width(g_DataWidth),
        .g_Delay(c_OtherDelay)
    ) i_Slave2MasterDataSyncer (
        .Clk_ik(Master_Clk_ik),
        .Data_ib(Slave_DatMiSo_ib),
        .Data_ob(Master_DatMiSo)
    );
        
    //ack
    SignalSyncer #(
        .g_Width(1),
        .g_Delay(c_SignalDelay)
    ) i_Slave2MasterCtrlSyncer (
        .Clk_ik(Master_Clk_ik),
        .Data_ib({Slave_Ack}),
        .Data_ob({Master_Ack})
    );
    
   // WB Slave state machine
   always @(posedge Slave_Clk_ik)
   begin
       if( Slave_Rst_i )
        begin
            SlaveToggle_q <= 1'b0;
            Slave_Cyc_o   <= 1'b0;
            Slave_Stb_o   <= 1'b0;
            Slave_Ack     <= 1'b0;
            SlaveState    <= s_SlaveIdle;
        end
        else 
        begin
        
            SlaveToggle_q <= SlaveToggle;
        
            case( SlaveState )
               
                // wait for any activity
                s_SlaveIdle: 
                begin
                    // on-going access
                    if( SlaveToggle ^ SlaveToggle_q )
                    begin
                        Slave_Cyc_o <= 1'b1;
                        Slave_Stb_o <= 1'b1;
                        SlaveState <= s_SlaveAck;
                    end
                end
               
                /// ACK
                s_SlaveAck: 
                begin
                    if( Slave_Ack_i == 1'b1 )
                    begin
                        Slave_Cyc_o <= 1'b0;
                        Slave_Stb_o <= 1'b0;
                        Slave_Ack   <= 1'b1;
                    end
                    // ack pulse
                    else if( Slave_Ack == 1'b1)
                    begin
                        Slave_Ack <= 1'b0;
                        SlaveState <= s_SlaveIdle;
                    end
                end
           endcase
       end
    end

    // Synchronizers master to slave
    
    //access request
    SignalSyncer #(
        .g_Width(1),
        .g_Delay(c_SignalDelay)
    ) i_Master2SlaveCtrlSyncer (
        .Clk_ik(Slave_Clk_ik),
        .Data_ib({MasterToggle}),
        .Data_ob({SlaveToggle})
    );    

    //address
    SignalSyncer #(
        .g_Width(g_AddressWidth),
        .g_Delay(c_OtherDelay)
    ) i_Master2SlaveAddressSyncer (
        .Clk_ik(Slave_Clk_ik),
        .Data_ib(Master_Adr_ib),
        .Data_ob(Slave_Adr_ob)
    );
    
    //data
    SignalSyncer #(
        .g_Width(g_DataWidth),
        .g_Delay(c_OtherDelay)
    ) i_Master2SlaveDataSyncer (
        .Clk_ik(Slave_Clk_ik),
        .Data_ib(Master_DatMoSi_ib),
        .Data_ob(Slave_DatMoSi_ob)
    );

    //write
    SignalSyncer #(
        .g_Width(1),
        .g_Delay(c_OtherDelay)
    ) i_Master2SlaveWriteSyncer (
        .Clk_ik(Slave_Clk_ik),
        .Data_ib({Master_We_i}),
        .Data_ob({Slave_We_o})
    );

    
endmodule
