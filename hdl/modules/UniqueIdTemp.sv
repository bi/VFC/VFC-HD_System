//----------------------------------------------------------------------
// Title      : Unique ID & Temperature Sensor
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : UniqueIdTemp.sv
// Author     : T. Levens
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Module that reads the unique ID and temperature periodically over the
// 1-wire bus from the DS18B20U+ chip.
//
// It also implements the readout of the internal FPGA temperature
// sensor.
//
// The Wishbone interface exposes three registers:
//   0x00: Unique ID         (64 bit)
//   0x02: Board temperature (32 bit, signed, 0.0625 deg/LSB)
//   0x03: FPGA temperature  (32 bit, signed, 0.0625 deg/LSB)
//----------------------------------------------------------------------

`timescale 1ns/100ps

module UniqueIdTemp #(
    parameter         g_ClkFrequency,
    parameter         g_RefreshFrequency = 1 // Hz
) (
    input             Clk20M_ik,    // For internal temperature sensor

    input             Clk_ik,
    input             Rst_irq,

    input             Cyc_i,
    input             Stb_i,
    input      [ 1:0] Adr_ib2,
    output reg [31:0] Dat_oab32,
    output reg        Ack_oa,

    output reg [63:0] Uid_ob64,
    output reg        UidValid_o,
    output reg [15:0] Temp_ob16,
    output reg        TempRead_o,
    output reg [ 7:0] ChipTemp_ob8,
    output reg        ChipTempRead_o,

    inout             OneWireBus_io
);

//----------------------------------------------------------------------
// Periodic refresh of measurements
//----------------------------------------------------------------------

initial begin
    assert (g_RefreshFrequency <= 1) else $error("The maximum refresh frequency is 1 Hz");
end

localparam int c_RefreshDelay = g_ClkFrequency / g_RefreshFrequency;
localparam int c_RefreshBits  = $clog2(c_RefreshDelay+1);

reg [c_RefreshBits-1:0] ReadoutCounter_c;
reg NewReading;

always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        NewReading              <= #1 1'b0;
        ReadoutCounter_c        <= #1 1'h0;
    end else begin
        if (!ReadoutCounter_c) begin
            NewReading          <= #1 1'b1;
            ReadoutCounter_c    <= #1 (c_RefreshBits)'(c_RefreshDelay);
        end else begin
            NewReading          <= #1 1'b0;
            ReadoutCounter_c    <= #1 ReadoutCounter_c - 1'h1;
        end
    end
end

//----------------------------------------------------------------------
// DS18B20 OneWire interface
//----------------------------------------------------------------------

wire Busy, CrcOk, Prsnt;
reg  WrStr, RdStr, BusRst;
reg  [ 6:0] BitCnt_b7;
reg  [15:0] WrData_b16;
wire [71:0] RdData_b72;

// State machine for DS18B20 control
enum {
    s_Idle,
    s_ReadUid_Rst,
    s_ReadUid_Cmd,
    s_ReadUid_Data,
    s_ConvTemp_Rst,
    s_ConvTemp_Cmd,
    s_ConvTemp_Wait,
    s_ReadTemp_Rst,
    s_ReadTemp_Cmd,
    s_ReadTemp_Data
} State_l;

wire RdDone  = !Busy && !RdStr;
wire WrDone  = !Busy && !WrStr;
wire RstDone = !Busy && !BusRst;

always @(posedge Clk_ik) begin
    BusRst      <= #1 1'b0;
    WrStr       <= #1 1'b0;
    RdStr       <= #1 1'b0;
    TempRead_o  <= #1 1'b0;
    if (Rst_irq) begin
        State_l     <= #1 s_Idle;
        UidValid_o  <= #1 1'b0;
        Uid_ob64    <= #1 64'h0;
        Temp_ob16   <= #1 16'h0;
    end else begin
        case (State_l)
            s_Idle: begin
                if (NewReading) begin
                    if (UidValid_o) begin
                        State_l     <= #1 s_ConvTemp_Rst;
                    end else begin
                        State_l     <= #1 s_ReadUid_Rst;
                    end
                    BusRst          <= #1 1'b1;
                end
            end
            s_ReadUid_Rst: begin
                if (RstDone) begin
                    if (Prsnt) begin
                        State_l     <= #1 s_ReadUid_Cmd;
                        WrData_b16  <= #1 8'h33;
                        BitCnt_b7   <= #1 7'd8;
                        WrStr       <= #1 1'b1;
                    end else begin
                        State_l     <= #1 s_Idle;
                    end
                end
            end
            s_ReadUid_Cmd: begin
                if (WrDone) begin
                    State_l         <= #1 s_ReadUid_Data;
                    BitCnt_b7       <= #1 7'd64;
                    RdStr           <= #1 1'b1;
                end
            end
            s_ReadUid_Data: begin
                if (RdDone) begin
                    if (CrcOk) begin
                        State_l     <= #1 s_ConvTemp_Rst;
                        BusRst      <= #1 1'b1;
                        Uid_ob64    <= #1 RdData_b72[71:8];
                        UidValid_o  <= #1 1'b1;
                    end else begin
                        State_l     <= #1 s_Idle;
                    end
                end
            end
            s_ConvTemp_Rst: begin
                if (RstDone) begin
                    if (Prsnt) begin
                        State_l     <= #1 s_ConvTemp_Cmd;
                        WrData_b16  <= #1 16'h44CC;
                        BitCnt_b7   <= #1 7'd16;
                        WrStr       <= #1 1'b1;
                    end else begin
                        State_l     <= #1 s_Idle;
                    end
                end
            end
            s_ConvTemp_Cmd: begin
                if (WrDone) begin
                    State_l         <= #1 s_ConvTemp_Wait;
                    BitCnt_b7       <= #1 7'd1;
                    RdStr           <= #1 1'b1;
                end
            end
            s_ConvTemp_Wait: begin
                if (RdDone) begin
                    if (RdData_b72[71]) begin
                        State_l     <= #1 s_ReadTemp_Rst;
                        BusRst      <= #1 1'b1;
                    end else begin
                        State_l     <= #1 s_ConvTemp_Wait;
                        BitCnt_b7   <= #1 7'd1;
                        RdStr       <= #1 1'b1;
                    end
                end
            end
            s_ReadTemp_Rst: begin
                if (RstDone) begin
                    if (Prsnt) begin
                        State_l     <= #1 s_ReadTemp_Cmd;
                        WrData_b16  <= #1 16'hBECC;
                        BitCnt_b7   <= #1 7'd16;
                        WrStr       <= #1 1'b1;
                    end else begin
                        State_l     <= #1 s_Idle;
                    end
                end
            end
            s_ReadTemp_Cmd: begin
                if (WrDone) begin
                    State_l         <= #1 s_ReadTemp_Data;
                    BitCnt_b7       <= #1 7'd72;
                    RdStr           <= #1 1'b1;
                end
            end
            s_ReadTemp_Data: begin
                if (RdDone) begin
                    State_l         <= #1 s_Idle;
                    if (CrcOk) begin
                        Temp_ob16   <= #1 RdData_b72[15:0];
                        TempRead_o  <= #1 1'b1;
                    end
                end
            end
        endcase
    end
end

OneWireBus #(.g_ClkFrequency(g_ClkFrequency)) i_OneWireBus (
    .Clk_ik         (Clk_ik),
    .Rst_irq        (Rst_irq),
    .Count_ib7      (BitCnt_b7),
    .WrData_ib72    ({56'h0, WrData_b16}),
    .RdData_ob72    (RdData_b72),
    .WrStr_i        (WrStr),
    .RdStr_i        (RdStr),
    .BusRst_i       (BusRst),
    .Busy_o         (Busy),
    .CrcOk_o        (CrcOk),
    .Prsnt_o        (Prsnt),
    .OneWireBus_io  (OneWireBus_io));

//----------------------------------------------------------------------
// FPGA temperature sensor
//----------------------------------------------------------------------

reg  [2:0] ChipTempDone_xb2;
wire       ChipTempDone_a;
wire [7:0] ChipTemp_ab8;

always @(posedge Clk_ik) ChipTempDone_xb2 <= {ChipTempDone_xb2[1:0], ChipTempDone_a};

always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        ChipTemp_ob8   <= #1 8'h00;
        ChipTempRead_o <= #1 1'b0;
    end else begin
        ChipTempRead_o <= #1 1'b0;
        if (ChipTempDone_xb2[1] && !ChipTempDone_xb2[2]) begin
            ChipTemp_ob8   <= #1 ChipTemp_ab8;
            ChipTempRead_o <= #1 1'b1;
        end
    end
end

reg ChipTempReadReq, ChipTempReadReq_x, ChipTempReadReq_a;
reg ChipTempReadAck, ChipTempReadAck_x;
always @(posedge Clk_ik) begin
    if (NewReading)
        ChipTempReadReq <= 1'b1;
    else if (ChipTempReadAck)
        ChipTempReadReq <= 1'b0;
end
always @(posedge Clk20M_ik) {ChipTempReadReq_a, ChipTempReadReq_x} <= {ChipTempReadReq_x, ChipTempReadReq};
always @(posedge Clk_ik)    {ChipTempReadAck,   ChipTempReadAck_x} <= {ChipTempReadAck_x, ChipTempReadReq_a};

fpga_temp_sensor i_FpgaTempSensor (
    .clk        (Clk20M_ik),
    .clr        (ChipTempReadReq_a),
    .tsdcaldone (ChipTempDone_a),
    .tsdcalo    (ChipTemp_ab8));

//----------------------------------------------------------------------
// Wishbone registers
//----------------------------------------------------------------------

always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        Dat_oab32 <= #1 32'h0;
        Ack_oa    <= #1  1'b0;
    end else begin
        if (~Ack_oa) begin
            case (Adr_ib2)
                2'b00: Dat_oab32 <= #1 Uid_ob64[63:32];
                2'b01: Dat_oab32 <= #1 Uid_ob64[31: 0];
                2'b10: Dat_oab32 <= #1 {{16{Temp_ob16[15]}}, Temp_ob16[15:0]};            // 16b signed, 0.0625 deg/LSB
                2'b11: Dat_oab32 <= #1 {{21{!ChipTemp_ob8[7]}}, ChipTemp_ob8[6:0], 4'b0}; // 8b offset binary, 1 deg/LSB
            endcase
        end
        Ack_oa <= #1 Stb_i && Cyc_i;
    end
end

endmodule
