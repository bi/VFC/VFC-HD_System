//----------------------------------------------------------------------
// Title      : SPI Master
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : SpiMaster.sv
// Author     : T. Levens
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// A simple SPI master designed for interfacing with another state
// machine inside the FPGA.
//----------------------------------------------------------------------

`timescale 1ns/100ps

module SpiMaster #(
    parameter       g_ClkFrequency,
    parameter       g_SpiFrequency,
    parameter       g_MaxBits       = 32,
    parameter logic g_Cpol          = 1'b0,
    parameter       g_Cpha          = 0
) (
    input                                Clk_ik,
    input                                Rst_ir,
    input                                Go_i,
    input      [$clog2(g_MaxBits+1)-1:0] Bits_ib,
    input      [g_MaxBits-1:0]           Data_ib,
    output reg [g_MaxBits-1:0]           Data_ob,
    output                               Done_o,
    output reg                           Cs_on,
    output reg                           Sclk_o,
    output reg                           Mosi_o,
    input                                Miso_i
);

localparam int c_ClkHalfDiv  = (g_ClkFrequency/g_SpiFrequency) / 2;
localparam int c_BitsWidth   = $clog2(g_MaxBits+1);
localparam int c_ClkDivWidth = $clog2(2*c_ClkHalfDiv);

initial begin
    // check parameters
    assert (c_ClkHalfDiv > 2)
        else $error("The SPI frequency is too high for the given clock frequency!");
end

localparam logic [  c_BitsWidth-1:0] c_BitsOne     =   (c_BitsWidth)'(1'b1);
localparam logic [c_ClkDivWidth-1:0] c_WaitOne     = (c_ClkDivWidth)'(1'b1);
localparam logic [c_ClkDivWidth-1:0] c_HalfWaitCnt = (c_ClkDivWidth)'(c_ClkHalfDiv - 2);
localparam logic [c_ClkDivWidth-1:0] c_FullWaitCnt = (c_ClkDivWidth)'(2*c_ClkHalfDiv - 2);

reg [c_ClkDivWidth-1:0] Wait_c;
reg [  c_BitsWidth-1:0] Bits_c;

enum {
    s_Idle,
    s_ShiftOut,
    s_Wait1,
    s_ShiftIn,
    s_Wait2,
    s_Done,
    s_Wait3
} SeqCurrentState, SeqNextState;

always @(posedge Clk_ik) begin
    if (Rst_ir)
        SeqCurrentState <= s_Idle;
    else
        SeqCurrentState <= SeqNextState;
end

always @(*) begin
    SeqNextState = SeqCurrentState;
    case (SeqCurrentState)
        s_Idle: begin
            if (Go_i) begin
                if (g_Cpha)
                    SeqNextState = s_Wait2;
                else
                    SeqNextState = s_ShiftOut;
            end
        end
        s_ShiftOut: begin
            SeqNextState = s_Wait1;
        end
        s_Wait1: begin
            if (!Wait_c) begin
                if (!g_Cpha && !Bits_c)
                    SeqNextState = s_Done;
                else
                    SeqNextState = s_ShiftIn;
            end
        end
        s_ShiftIn: begin
            SeqNextState = s_Wait2;
        end
        s_Wait2: begin
            if (!Wait_c) begin
                if (g_Cpha && !Bits_c)
                    SeqNextState = s_Done;
                else
                    SeqNextState = s_ShiftOut;
            end
        end
        s_Done: begin
            SeqNextState = s_Wait3;
        end
        s_Wait3: begin
            if (!Wait_c) begin
                SeqNextState = s_Idle;
            end
        end
        default: begin
            SeqNextState = s_Idle;
        end
    endcase
end

reg [g_MaxBits-1:0] ShiftInReg;
reg [g_MaxBits-1:0] ShiftOutReg;

wire SclkLvl = (g_Cpha) ? !g_Cpol : g_Cpol;

reg  Done, Done_d;
always @(posedge Clk_ik) Done_d <= #1 Done;
assign Done_o = Done && !Done_d;

always @(posedge Clk_ik) begin
    Done <= #1 1'b0;
    case (SeqCurrentState)
        s_Idle: begin
            Cs_on                 <= #1 1'b1;
            Sclk_o                <= #1 g_Cpol;
            Bits_c                <= #1 (g_Cpha) ? Bits_ib : Bits_ib + c_BitsOne;
            ShiftOutReg           <= #1 Data_ib;
            Data_ob               <= #1 ShiftInReg;
            Done                  <= #1 1'b1;
            Wait_c                <= #1 c_HalfWaitCnt;
        end
        s_ShiftOut: begin
            Cs_on                 <= #1 1'b0;
            Sclk_o                <= #1 SclkLvl;
            {Mosi_o, ShiftOutReg} <= #1 {ShiftOutReg[g_MaxBits-1:0], 1'b0};
            Bits_c                <= #1 Bits_c - c_BitsOne;
            Wait_c                <= #1 c_HalfWaitCnt;
        end
        s_Wait1: begin
            Cs_on                 <= #1 1'b0;
            Sclk_o                <= #1 SclkLvl;
            Wait_c                <= #1 Wait_c - c_WaitOne;
        end
        s_ShiftIn: begin
            Cs_on                 <= #1 1'b0;
            Sclk_o                <= #1 !SclkLvl;
            ShiftInReg            <= #1 {ShiftInReg[g_MaxBits-2:0], Miso_i};
            Wait_c                <= #1 c_HalfWaitCnt;
        end
        s_Wait2: begin
            Cs_on                 <= #1 1'b0;
            Sclk_o                <= #1 !SclkLvl;
            Wait_c                <= #1 Wait_c - c_WaitOne;
        end
        s_Done: begin
            Cs_on                 <= #1 1'b1;
            Sclk_o                <= #1 g_Cpol;
            Wait_c                <= #1 c_FullWaitCnt;
        end
        s_Wait3: begin
            Cs_on                 <= #1 1'b1;
            Sclk_o                <= #1 g_Cpol;
            Wait_c                <= #1 Wait_c - c_WaitOne;
        end
    endcase
end

endmodule
