@echo off

REM generates Qsys IP cores needed
REM hint: qsys-generate can be found at c:\intelFPGA\17.1\quartus\sopc_builder\bin\

set family="Arria V"
set part=5AGXMB1G4F40C4
set tmpDir=tmp_ip_generation
set prefix=
if "%1" == "--dry-run" (
    set prefix=echo
)

call :generate_qsys asmi_parallel
call :generate_qsys remote_update
call :generate_qsys serial_flash_loader
call :generate_qsys fpga_temp_sensor

exit /b

REM **************************************************
REM Functions
REM **************************************************

:generate_qsys
echo Generating QSYS IP core %~1

echo.
echo Backuping QSYS definition file
if not exist %~1\%~1.qsys (
    echo QSYS file cannot be found! Skipping this core.
    exit /b
)
%prefix% mkdir %tmpDir%
%prefix% copy %~1\%~1.qsys %tmpDir%\*

echo.
echo Removing old IP core
%prefix% rmdir /q /s %~1

echo.
echo Creating IP core directory structure
%prefix% mkdir %~1
%prefix% copy %tmpDir%\%~1.qsys %~1\*

echo.
echo Generating core
%prefix% qsys-generate %~1\%~1.qsys --synthesis=VERILOG --simulation=VERILOG --allow-mixed-language-simulation --output-directory=%~1 --family=%family% --part=%part%

echo.
echo Cleaning
%prefix% rmdir /q /s %tmpDir%

echo.
echo Done :-)
echo.

exit /b
