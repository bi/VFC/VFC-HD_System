// fpga_temp_sensor.v

// Generated using ACDS version 20.1 720

// Core generation workaround - still generated for arria 10.
// This patched solves the generation problem for ArriaV and must be applied by hand
// It resolves the issue regarding bad autogenerated port names for the simulation model

`timescale 1 ps / 1 ps
module fpga_temp_sensor (
		input  wire       clk,        //        clk.clk
		input  wire       clr,        //        clr.reset
		output wire       tsdcaldone, // tsdcaldone.tsdcaldone
		output wire [7:0] tsdcalo     //    tsdcalo.tsdcalo
	);
    
    wire [9:0] tmp;

	altera_temp_sense temp_sense_0 (
		.corectl (clk),        //        clk.clk
		.tempout (tmp),        //    tsdcalo.tsdcalo
		.eoc     (tsdcaldone), // tsdcaldone.tsdcaldone
		.reset   (clr)         //        clr.reset
	);
    
    assign tsdcalo = tmp[7:0];

endmodule
