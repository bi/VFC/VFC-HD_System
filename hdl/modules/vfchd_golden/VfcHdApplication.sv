//----------------------------------------------------------------------
// Title      : VFC-HD Golden Application Part
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : VfcHdApplication.sv
// Author     : -
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Application part of the VFC-HD Golden gateware image
//----------------------------------------------------------------------

`timescale 1ns/100ps

module VfcHdApplication #(
    parameter g_ClkFrequency,
    parameter g_Synthesis = 1'b1,

    // Application release info, updated automatically by the
    // pre_flow.tcl script during implementation
    parameter g_ApplicationVersion_b8      = 8'h00,
              g_ApplicationReleaseDay_b8   = 8'h00,
              g_ApplicationReleaseMonth_b8 = 8'h00,
              g_ApplicationReleaseYear_b8  = 8'h00,

    // Application ident string, up to 352 bits (44 ASCII characters)
    parameter g_AppIdent_b352 = "VFC-HD Golden Image"
) (
    //------------------------------------------------------------------
    // External ports
    //------------------------------------------------------------------

    // IMPORTANT NOTE: Some ports must be specifically configured in
    // VfcHdConfig.vh in order to be enabled.

    // SFP MGT lanes (enable in VfcHdConfig.vh)
    input          BstSfpRx_i,
    output         BstSfpTx_o,

    input          EthSfpRx_i,
    output         EthSfpTx_o,

    input  [  4:1] AppSfpRx_ib4,
    output [  4:1] AppSfpTx_ob4,

    // DDR3 (enable in VfcHdConfig.vh)
    output         Ddr3aCk_ok,
    output         Ddr3aCk_okn,
    output         Ddr3aCke_o,
    output         Ddr3aReset_orn,
    output         Ddr3aRas_on,
    output         Ddr3aCas_on,
    output         Ddr3aCs_on,
    output         Ddr3aWe_on,
    output         Ddr3aOdt_o,
    output [  2:0] Ddr3aBa_ob3,
    output [ 15:0] Ddr3aAdr_ob16,
    output [  1:0] Ddr3aDm_ob2,
    inout  [  1:0] Ddr3aDqs_iob2,
    inout  [  1:0] Ddr3aDqs_iob2n,
    inout  [ 15:0] Ddr3aDq_iob16,

    output         Ddr3bCk_ok,
    output         Ddr3bCk_okn,
    output         Ddr3bCke_o,
    output         Ddr3bReset_orn,
    output         Ddr3bRas_on,
    output         Ddr3bCas_on,
    output         Ddr3bCs_on,
    output         Ddr3bWe_on,
    output         Ddr3bOdt_o,
    output [  2:0] Ddr3bBa_ob3,
    output [ 15:0] Ddr3bAdr_ob16,
    output [  1:0] Ddr3bDm_ob2,
    inout  [  1:0] Ddr3bDqs_iob2,
    inout  [  1:0] Ddr3bDqs_iob2n,
    inout  [ 15:0] Ddr3bDq_iob16,

    // Test I/O MMCX connectors
    inout          TestIo1_io,
    inout          TestIo2_io,

    // FMC HPC connector
    input          FmcPrsntM2c_in,
    input          FmcPgM2c_i,
    output         FmcPgC2m_o,
    output         FmcTck_ok,
    output         FmcTms_o,
    output         FmcTdi_o,
    input          FmcTdo_i,
    output         FmcTrstL_orn,
    inout          FmcScl_iok,
    inout          FmcSda_io,
    input          FmcClkDir_i,
    input          FmcClk0M2cCmos_ik,
    input          FmcClk1M2cCmos_ik,
    input          FmcClk2Bidir_iok,        // (enable and direction in VfcHdConfig.vh)
//  output         FmcClk2Bidir_iok,
    input          FmcClk3Bidir_iok,        // (enable and direction in VfcHdConfig.vh)
//  output         FmcClk3Bidir_iok,
    input          FmcGbtClk0M2cLeft_ik,    // (enable in VfcHdConfig.vh)
    input          FmcGbtClk1M2cLeft_ik,    // (enable in VfcHdConfig.vh)
    input          FmcGbtClk0M2cRight_ik,   // (enable in VfcHdConfig.vh)
    input          FmcGbtClk1M2cRight_ik,   // (enable in VfcHdConfig.vh)
    inout  [ 33:0] FmcLaP_iob34,
    inout  [ 33:0] FmcLaN_iob34,
    inout  [ 23:0] FmcHaP_iob24,
    inout  [ 23:0] FmcHaN_iob24,
    inout  [ 21:0] FmcHbP_iob22,
    inout  [ 21:0] FmcHbN_iob22,
    output [  9:0] FmcDpC2m_ob10,           // (enable in VfcHdConfig.vh)
    input  [  9:0] FmcDpM2c_ib10,           // (enable in VfcHdConfig.vh)

    // Clocks
    input          Si57xClk_ik,             // Si57X 10-160MHz programmable oscillator
    input  [  3:0] ClkFb_ikb4,              // Feedback clock inputs
    output [  3:0] ClkFb_okb4,              // Feedback clock outputs
    input          Clk20VCOx_ik,            //  20 MHz VCXO for WR
    input          GbitTrxClkRefR_ik,       // 125 MHz VCXO for WR (enable in VfcHdConfig.vh)
    output         PllSourceMuxOut_ok,      // Si5338 PLL input
    input          PllRefClkOut_ik,         // Si5338 PLL output (enable in VfcHdConfig.vh)

    // DACs for tuning WR VCXOs
    output         PllDac20Sync_o,
    output         PllDac25Sync_o,
    output         PllDacSclk_ok,
    output         PllDacDin_o,

    // Si5338 PLL I2C control
    inout          PllRefSda_ioz,
    inout          PllRefScl_iokz,
    input          PllRefInt_i,

    // P2 RTM (bit 0 is a clock capable input)
    inout  [ 19:0] P2DataP_iob20,
    inout  [ 19:0] P2DataN_iob20,

    // P0 timing
    input  [  7:0] P0HwHighByte_ib8,
    input  [  7:0] P0HwLowByte_ib8,
    output         DaisyChain1Cntrl_o,
    output         DaisyChain2Cntrl_o,
    input          VmeP0BunchClk_ik,
    input          VmeP0Tclk_ik,

    // GPIO LEMO connectors
    inout  [  4:1] GpIo_iob4,

    // WhiteRabbit PROM
    inout          WrPromSda_io,
    output         WrPromScl_ok,

    // Miscellaneous
    output         OeSi57x_oe,
    input  [  8:1] DipSw_ib8,
    input          PushButtonN_in,

    //------------------------------------------------------------------
    // System <=> Application interface
    //------------------------------------------------------------------

    // Clock & reset
    output         SysClk_ok,           // Clock for the system part
    output         SysReset_oar,        // Reset for the system part

    input          VmeSysReset_iarn,    // Asynchronous VME Sys Reset signal
    input          AppReset_iqr,        // Synchronous reset for the application

    // Application release ID and ident string
    output [ 31:0] AppRelease_ob32,
    output [351:0] AppIdent_ob352,

    // Vadj control
    output         VfmcEnable_oe,
    input          VfmcEnabled_i,
    output [  2:0] VadjSelect_ob3,      // Values defined in VadjConfig.vh

    // WishBone interface
    input          WbMasterCyc_i,
    input          WbMasterStb_i,
    input  [ 24:0] WbMasterAdr_ib25,    // The actual width is 21 for the moment (top bits are 0)
    input          WbMasterWr_i,
    input  [ 31:0] WbMasterDat_ib32,
    output [ 31:0] WbMasterDat_ob32,
    output         WbMasterAck_o,
    output         WbSlaveCyc_o,
    output         WbSlaveStb_o,
    output [ 24:0] WbSlaveAdr_ob25,
    output         WbSlaveWr_o,
    output [ 31:0] WbSlaveDat_ob32,
    input  [ 31:0] WbSlaveDat_ib32,
    input          WbSlaveAck_i,

    // LEDs
    output [  8:1] Led_ob8,
    input  [  8:1] LedStatus_ib8,

    // BST interface                         _   _   _   _   _   _   _   _
    input          BstClk_ik,           // _/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_ ~160 MHz
    input          BstRst_ir,           //  :___    :        ___
    input          BunchClkFlag_i,      // _/   \___:___ ___/   \___ ___ ___
    input          BunchClkFlagDly_i,   // _:___ ___/   \___________/   \___ } ~40 MHz
    input          TurnClkFlag_i,       // _/   \___:____ __________________
    input          TurnClkFlagDly_i,    // _:_______/    \__________________ } ~11 kHz (LHC) / ~43 kHz (SPS)
                                        //  <--Dly-->
    input  [  7:0] BstByteAddress_ib8,
    input  [  7:0] BstByteData_ib8,
    input          BstByteStrobe_i,
    input          BstByteError_i,

    // Interrupt request
    output [ 23:0] InterruptRequest_ob24,
    input  [ 23:0] InterruptAvailable_ib24,

    // GPIO direction (1=Output, 0=Input)
    output         GpIo1DirOut_o,
    input          GpIo1DirOutStatus_i,
    output         GpIo2DirOut_o,
    input          GpIo2DirOutStatus_i,
    output         GpIo34DirOut_o,
    input          GpIo34DirOutStatus_i,

    // GPIO termination (1=Enable, 0=Disable)
    output         GpIo1EnTerm_o,
    input          GpIo1EnTermStatus_i,
    output         GpIo2EnTerm_o,
    input          GpIo2EnTermStatus_i,
    output         GpIo3EnTerm_o,
    input          GpIo3EnTermStatus_i,
    output         GpIo4EnTerm_o,
    input          GpIo4EnTermStatus_i,

    // BLMIn P0 value
    input  [  7:0] BlmIn_ib8,

    // SFP control
    input          AppSfp1Present_iq,
    input          AppSfp1TxFault_iq,
    input          AppSfp1Los_iq,
    output         AppSfp1TxDisable_o,
    output         AppSfp1RateSelect_o,
    input          StatusAppSfp1TxDisable_iq,
    input          StatusAppSfp1RateSelect_iq,

    input          AppSfp2Present_iq,
    input          AppSfp2TxFault_iq,
    input          AppSfp2Los_iq,
    output         AppSfp2TxDisable_o,
    output         AppSfp2RateSelect_o,
    input          StatusAppSfp2TxDisable_iq,
    input          StatusAppSfp2RateSelect_iq,

    input          AppSfp3Present_iq,
    input          AppSfp3TxFault_iq,
    input          AppSfp3Los_iq,
    output         AppSfp3TxDisable_o,
    output         AppSfp3RateSelect_o,
    input          StatusAppSfp3TxDisable_iq,
    input          StatusAppSfp3RateSelect_iq,

    input          AppSfp4Present_iq,
    input          AppSfp4TxFault_iq,
    input          AppSfp4Los_iq,
    output         AppSfp4TxDisable_o,
    output         AppSfp4RateSelect_o,
    input          StatusAppSfp4TxDisable_iq,
    input          StatusAppSfp4RateSelect_iq,

    input          BstSfpPresent_iq,
    input          BstSfpTxFault_iq,
    input          BstSfpLos_iq,
    output         BstSfpTxDisable_o,
    output         BstSfpRateSelect_o,
    input          StatusBstSfpTxDisable_iq,
    input          StatusBstSfpRateSelect_iq,

    input          EthSfpPresent_iq,
    input          EthSfpTxFault_iq,
    input          EthSfpLos_iq,
    output         EthSfpTxDisable_o,
    output         EthSfpRateSelect_o,
    input          StatusEthSfpTxDisable_iq,
    input          StatusEthSfpRateSelect_iq,

    // CDR control
    input          CdrLos_iq,
    input          CdrLol_iq,

    // WishBone interface for the I2C slaves on the muxes
    output         I2cWbCyc_o,
    output         I2cWbStb_o,
    output         I2cWbWe_o,
    output  [11:0] I2cWbAdr_ob12,
    output  [ 7:0] I2cWbDat_ob8,
    input   [ 7:0] I2cWbDat_ib8,
    input          I2cWbAck_i,

    // OCT control sharing
    input   [15:0] OctSeriesControl_ib16,
    input   [15:0] OctParallelControl_ib16,

    // Diagnostics
    input   [63:0] UniqueId_ib64,
    input          UniqueIdValid_i,
    input   [15:0] Temp_ib16,
    input          TempRead_ip,
    input   [ 7:0] ChipTemp_ib8,
    input          ChipTempRead_ip,
    input   [11:0] Voltage0_ib12,   // PRE_VADJ
    input   [11:0] Voltage1_ib12,   // VCCPD_ADJ
    input   [11:0] Voltage2_ib12,   // VADJ
    input   [11:0] Voltage3_ib12,   // VIO_B_M2C
    input   [11:0] Voltage4_ib12,   // V3P3_FPGA
    input   [11:0] Voltage5_ib12,   // V3P3_FMC
    input   [11:0] Voltage6_ib12,   // V2P5
    input   [11:0] Voltage7_ib12,   // V12P0_FMC
    input   [ 7:0] VoltageOk_ib8,
    input          VoltageRead_ip,

    // VME geographical address
    input   [ 4:0] VmeGa_inb5,
    input          VmeGap_in
);

`include "VadjConfig.vh"

//----------------------------------------------------------------------
// Application release info
//----------------------------------------------------------------------

assign AppRelease_ob32 = {g_ApplicationReleaseYear_b8 [7:0],
                          g_ApplicationReleaseMonth_b8[7:0],
                          g_ApplicationReleaseDay_b8  [7:0],
                          g_ApplicationVersion_b8     [7:0]};
assign AppIdent_ob352  = {g_AppIdent_b352, {352-$size(g_AppIdent_b352){1'b0}}};

//----------------------------------------------------------------------
// Fixed assignments
//----------------------------------------------------------------------

assign OeSi57x_oe     = 1'b0;               // Si57X oscillator disabled
assign VfmcEnable_oe  = 1'b0;               // FMC voltages disabled
assign FmcPgC2m_o     = VfmcEnabled_i;
assign VadjSelect_ob3 = c_VadjSelect_V2P50; // Values defined in VadjConfig.vh

//----------------------------------------------------------------------
// Clocks & reset
//----------------------------------------------------------------------

// Clock for system part. The SYS_CLK_FREQ define in the VfcHdConfig.vh
// must be updated with the frequency of the clock supplied here.
assign SysClk_ok = GbitTrxClkRefR_ik;

// Asynchronous reset for the system part, by default VmeSysReset.
// Note that AppReset_iqr provides a synchronised version of this reset
// for convenience.
assign SysReset_oar = !VmeSysReset_iarn;

//----------------------------------------------------------------------
// LEDs
//----------------------------------------------------------------------

reg  [25:0] LedCounter_c26 = 26'h0;
always @(posedge SysClk_ok) LedCounter_c26 <= #1 LedCounter_c26 + 26'h1;
assign Led_ob8 = {8{LedCounter_c26[25]}};

//----------------------------------------------------------------------
// WB Master from the System Block
//----------------------------------------------------------------------

always @(posedge SysClk_ok) begin
    WbMasterDat_ob32 = 32'hdeadbeef;
    WbMasterAck_o = WbMasterCyc_i && WbMasterStb_i;
end

//----------------------------------------------------------------------
// WB Slave to the System Block
//----------------------------------------------------------------------

assign WbSlaveCyc_o    =  1'b0;
assign WbSlaveStb_o    =  1'b0;
assign WbSlaveAdr_ob25 = 25'h0;
assign WbSlaveWr_o     =  1'b0;
assign WbSlaveDat_ob32 = 32'h0;

//----------------------------------------------------------------------
// Unused I/O
//----------------------------------------------------------------------

assign BstSfpTx_o               =  1'b0;
assign EthSfpTx_o               =  1'b0;
assign AppSfpTx_ob4             =  4'h0;
assign Ddr3aCk_ok               =  1'b0;
assign Ddr3aCk_okn              =  1'b0;
assign Ddr3aCke_o               =  1'b0;
assign Ddr3aReset_orn           =  1'b0;
assign Ddr3aRas_on              =  1'b0;
assign Ddr3aCas_on              =  1'b0;
assign Ddr3aCs_on               =  1'b0;
assign Ddr3aWe_on               =  1'b0;
assign Ddr3aOdt_o               =  1'b0;
assign Ddr3aBa_ob3              =  3'h0;
assign Ddr3aAdr_ob16            = 16'h0;
assign Ddr3aDm_ob2              =  2'h0;
assign Ddr3bCk_ok               =  1'b0;
assign Ddr3bCk_okn              =  1'b0;
assign Ddr3bCke_o               =  1'b0;
assign Ddr3bReset_orn           =  1'b0;
assign Ddr3bRas_on              =  1'b0;
assign Ddr3bCas_on              =  1'b0;
assign Ddr3bCs_on               =  1'b0;
assign Ddr3bWe_on               =  1'b0;
assign Ddr3bOdt_o               =  1'b0;
assign Ddr3bBa_ob3              =  3'h0;
assign Ddr3bAdr_ob16            = 16'h0;
assign Ddr3bDm_ob2              =  2'h0;
assign FmcTck_ok                =  1'b0;
assign FmcTms_o                 =  1'b0;
assign FmcTdi_o                 =  1'b0;
assign FmcTrstL_orn             =  1'b0;
assign FmcDpC2m_ob10            = 10'h0;
assign ClkFb_okb4               =  4'h0;
assign PllDac20Sync_o           =  1'b0;
assign PllDac25Sync_o           =  1'b0;
assign PllDacSclk_ok            =  1'b0;
assign PllDacDin_o              =  1'b0;
assign PllSourceMuxOut_ok       =  1'b0;
assign DaisyChain1Cntrl_o       =  1'b0;
assign DaisyChain2Cntrl_o       =  1'b0;
assign WrPromScl_ok             =  1'b0;
assign InterruptRequest_ob24    = 24'h0;
assign GpIo1DirOut_o            =  1'b0;
assign GpIo2DirOut_o            =  1'b0;
assign GpIo34DirOut_o           =  1'b0;
assign GpIo1EnTerm_o            =  1'b0;
assign GpIo2EnTerm_o            =  1'b0;
assign GpIo3EnTerm_o            =  1'b0;
assign GpIo4EnTerm_o            =  1'b0;
assign AppSfp1TxDisable_o       =  1'b1;
assign AppSfp1RateSelect_o      =  1'b0;
assign AppSfp2TxDisable_o       =  1'b1;
assign AppSfp2RateSelect_o      =  1'b0;
assign AppSfp3TxDisable_o       =  1'b1;
assign AppSfp3RateSelect_o      =  1'b0;
assign AppSfp4TxDisable_o       =  1'b1;
assign AppSfp4RateSelect_o      =  1'b0;
assign BstSfpTxDisable_o        =  1'b1;
assign BstSfpRateSelect_o       =  1'b0;
assign EthSfpTxDisable_o        =  1'b1;
assign EthSfpRateSelect_o       =  1'b0;
assign I2cWbCyc_o               =  1'b0;
assign I2cWbStb_o               =  1'b0;
assign I2cWbWe_o                =  1'b0;
assign I2cWbAdr_ob12            = 12'h0;
assign I2cWbDat_ob8             =  8'h0;
assign Ddr3aDqs_iob2            =  2'hZ;
assign Ddr3aDqs_iob2n           =  2'hZ;
assign Ddr3aDq_iob16            = 16'hZ;
assign Ddr3bDqs_iob2            =  2'hZ;
assign Ddr3bDqs_iob2n           =  2'hZ;
assign Ddr3bDq_iob16            = 16'hZ;
assign TestIo1_io               =  1'bZ;
assign TestIo2_io               =  1'bZ;
assign FmcScl_iok               =  1'bZ;
assign FmcSda_io                =  1'bZ;
//assign FmcLaP_iob34             = 34'hZ;
//assign FmcLaN_iob34             = 34'hZ;
//assign FmcHaP_iob24             = 24'hZ;
//assign FmcHaN_iob24             = 24'hZ;
//assign FmcHbP_iob22             = 22'hZ;
//assign FmcHbN_iob22             = 22'hZ;
assign PllRefSda_ioz            =  1'bZ;
assign PllRefScl_iokz           =  1'bZ;
//assign P2DataP_iob20            = 20'hZ;
//assign P2DataN_iob20            = 20'hZ;
assign GpIo_iob4                =  4'hZ;
assign WrPromSda_io             =  1'bZ;

endmodule
