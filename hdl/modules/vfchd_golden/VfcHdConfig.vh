`ifndef __VFC_HD_CONFIG_VH__
`define __VFC_HD_CONFIG_VH__

//===================================================================//
//          VFC-HD PARAMETERISED TOP LEVEL I/O CONFIGURATION         //
//===================================================================//

// Define the default IO_STANDARD for the FMC VADJ and VIO_B banks.
// Remember to change the appropriate "IOBANK_VCCIO" assignments in
// your VfcHd_Application.qsf file when changing these voltages.

`define IO_STANDARD_VADJ "2.5 V"
`define IO_STANDARD_VIOB "2.5 V"

// Clock frequency (in Hz) supplied to the System by the Application
// on the SysClk_ok pin.
`define SYS_CLK_FREQ 125e6

//=============================== SFPs ==============================//

// Note that ENABLE_BSTSFP is for the optional MGT trasciever for this
// SFP. The normal BST functionality uses an external CDR and is not
// affected by this setting.

//`define ENABLE_APPSFP1
//`define ENABLE_APPSFP2
//`define ENABLE_APPSFP3
//`define ENABLE_APPSFP4
//`define ENABLE_BSTSFP
//`define ENABLE_ETHSFP

//=============================== DDR3 ==============================//

//`define ENABLE_DDR3_BANKA
//`define ENABLE_DDR3_BANKB

// To enable both A and B:
//`define ENABLE_DDR3

//============================== CLOCKS =============================//

//`define ENABLE_FMC_CLK2_BIDIR
//`define ENABLE_FMC_CLK3_BIDIR
//`define FMC_CLK_BIDIR_OUTPUT

//`define ENABLE_FMC_GBT_CLK0_M2C_LEFT
//`define ENABLE_FMC_GBT_CLK1_M2C_LEFT
//`define ENABLE_FMC_GBT_CLK0_M2C_RIGHT
//`define ENABLE_FMC_GBT_CLK1_M2C_RIGHT

//`define ENABLE_PLL_REFCLK_OUT
`define ENABLE_GBIT_TRX_CLK_REF_R

//====================== FMC DIFFERENTIAL PAIRS =====================//

//`define ENABLE_FMC_DP0_C2M
//`define ENABLE_FMC_DP1_C2M
//`define ENABLE_FMC_DP2_C2M
//`define ENABLE_FMC_DP3_C2M
//`define ENABLE_FMC_DP4_C2M
//`define ENABLE_FMC_DP5_C2M
//`define ENABLE_FMC_DP6_C2M
//`define ENABLE_FMC_DP7_C2M
//`define ENABLE_FMC_DP8_C2M
//`define ENABLE_FMC_DP9_C2M
//`define ENABLE_FMC_DP0_M2C
//`define ENABLE_FMC_DP1_M2C
//`define ENABLE_FMC_DP2_M2C
//`define ENABLE_FMC_DP3_M2C
//`define ENABLE_FMC_DP4_M2C
//`define ENABLE_FMC_DP5_M2C
//`define ENABLE_FMC_DP6_M2C
//`define ENABLE_FMC_DP7_M2C
//`define ENABLE_FMC_DP8_M2C
//`define ENABLE_FMC_DP9_M2C

//================== FMC & VME P2 USER DEFINED I/O ==================//

// For each pin you can define the constants "IOSTANDARD_",
// "ATTRIBUTES_" and "DIRECTION_" followed by the port:
//
//  * FMC_LA_P0..33 / FMC_LA_N0..33
//  * FMC_HA_P0..23 / FMC_HA_N0..23
//  * FMC_HB_P0..21 / FMC_HB_N0..21
//  * VME_P2_P0..19 / VME_P2_N0..19
//
// IOSTANDARD can be any standard supported by Quartus, for example:
//   "2.5 V", "3.3-V LVCMOS", "LVDS", ...
// Note that the surrounding "" are required.
//
// ATTRIBUTES is in the format: "-name ATTR VALUE". Again, the
// surrounding "" are required. Multiple attributes can be separated
// by ; for example: "-name ATTR1 VALUE1; -name ATTR2 VALUE2 ...".
// There should be no ; after the final attribute.
//
// DIRECTION can be input, output or inout.
//
// For example:
//
//  `define IOSTANDARD_FMC_LA_P0 "3.3-V LVCMOS"
//  `define ATTRIBUTES_FMC_HA_N12 "-name WEAK_PULL_UP_RESISTOR ON"
//  `define DIRECTION_VME_P2_P3 input

`define DISABLE_FMC_LA
`define DISABLE_FMC_HA
`define DISABLE_FMC_HB
`define DISABLE_VME_P2

`endif
