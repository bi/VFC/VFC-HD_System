//----------------------------------------------------------------------
// Title      : Interrupt Manager
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : InterruptManagerWb.sv
// Author     : A. Boccardi, M. Barros Marin, T. Levens
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// The module provides 32 edge sensitive interrupt sources where each
// source can be masked individually.
//
// The interrupt requests are assumed to be synchronous to the system
// clock and a new request is registered on the rising edge of the line.
//
// The sources are prioritized by arrival time and a FIFO is used to
// store the pending interrupts. If two or more interrupt requests are
// asserted on the same clock edge then the interrupt source register
// will be the logical OR of the sources.
//
// Reading the interrupt source register clears the pending interrupt
// and removes the value from the FIFO. Therefore it should only be
// read once per interrupt. Note that the VME core will periodically
// reissue a pending interrupt until the source is read.
//
// Wishbone interface:
//
//   0x00: Interrupt source
//   0x01: Interrupt configuration (bit 0: interrupt enable)
//   0x02: Interrupt mask (1 masks the source)
//   0x03: Interrupt vector
//   0x04: Interrupt level
//   0x05: Interrupt request count
//   0x06: Interrupt acknowledge count
//   0x07: Interrupt source read count
//
//----------------------------------------------------------------------

`timescale 1ns/100ps

module InterruptManagerWb #(parameter g_FifoAddressWidth = 3)
(
   input              Rst_irq,
   input              Clk_ik,
   input              Cyc_i,
   input              Stb_i,
   input              We_i,
   input       [ 2:0] Adr_ib3,
   input       [31:0] Dat_ib32,
   output  reg [31:0] Dat_oab32,
   output  reg        Ack_oa,
   input       [31:0] IntRequest_ib32,
   output      [31:0] IntAvailable_ob32,
   output      [ 2:0] IntLevel_ob3,
   output      [ 7:0] IntVector_ob8,
   output  reg        IntReq_o,
   input              IntAck_i
);

localparam c_SourceRegAddr_b3 = 3'b000;
localparam c_ConfigRegAddr_b3 = 3'b001;
localparam c_MaskRegAddr_b3   = 3'b010;
localparam c_VectorRegAddr_b3 = 3'b011;
localparam c_LevelRegAddr_b3  = 3'b100;
localparam c_IntReqAddr_b3    = 3'b101;
localparam c_IntAckAddr_b3    = 3'b110;
localparam c_IntSrcRdAddr_b3  = 3'b111;

reg         Ack_d;
wire        FifoFull, FifoEmpty;
reg         IntSourceRd;
wire        IntRequest;
wire        IntEnable;
reg  [31:0] ConfigReg_bq32 = 32'h0000_0000;
reg  [31:0] MaskReg_bq32   = 32'hFFFF_FFFF;
reg  [31:0] VectorReg_bq32 = 32'h0000_0000;
reg  [31:0] LevelReg_bq32  = 32'h0000_0000;
wire [31:0] IntRequestMasked_b32;
reg  [31:0] IntRequest_db32;
wire [31:0] IntRequestEdge_b32;
reg  [31:0] InterruptRequested_c32 = 0;
reg  [31:0] InterruptAcknowledged_c32 = 0;
reg  [31:0] InterruptSourceRead_c32 = 0;
reg  [31:0] Fifo_mb32 [(1<<g_FifoAddressWidth)-1:0];
wire        FifoRdPhase, FifoWrPhase;
reg  [g_FifoAddressWidth  :0] FifoRdCount_c, FifoWrCount_c;
wire [g_FifoAddressWidth-1:0] FifoRdAddr_b, FifoWrAddr_b;

// Rising edge detection on interrupt request lines
always @(posedge Clk_ik) IntRequest_db32    <= #1 IntRequest_ib32;
assign IntRequestEdge_b32 = IntRequest_ib32 & ~IntRequest_db32;

// Interrupt masking. Note, this must be done after the edge detection
// to avoid spurious interrupt generation if an interrupt input is held
// high, the FIFO is full and the source is read.
assign IntAvailable_ob32 = ~MaskReg_bq32 & {32{IntEnable && !FifoFull}};
assign IntRequestMasked_b32 = IntRequestEdge_b32 & IntAvailable_ob32;
assign IntRequest = |IntRequestMasked_b32;

// Assert the interrupt request as long as the FIFO is not empty. The
// VME interface will periodically generate an interrupt on the bus
// while the request is held high. The output is forced to low for one
// clock cycle when the source is read to start a new transaction.
always @(posedge Clk_ik) IntReq_o <= #1 !FifoEmpty && !IntSourceRd;

// Interrupt source FIFO
integer i;
initial begin
    for (i=0; i<(1<<g_FifoAddressWidth); i=i+1) Fifo_mb32[i] = 32'h0;
end

always @(posedge Clk_ik) begin
    if (Rst_irq || !IntEnable) begin
        FifoRdCount_c <= #1 1'b0;
    end if (IntSourceRd && !FifoEmpty) begin
        FifoRdCount_c <= #1 FifoRdCount_c + 1'b1;
    end
end
assign FifoRdPhase  = FifoRdCount_c[g_FifoAddressWidth];
assign FifoRdAddr_b = FifoRdCount_c[g_FifoAddressWidth-1:0];

always @(posedge Clk_ik) begin
    if (Rst_irq || !IntEnable) begin
        FifoWrCount_c <= #1 1'b0;
    end else if (IntRequest && !FifoFull) begin
        FifoWrCount_c <= #1 FifoWrCount_c + 1'b1;
        Fifo_mb32[FifoWrAddr_b] <= #1 IntRequestMasked_b32;
    end
end
assign FifoWrPhase  = FifoWrCount_c[g_FifoAddressWidth];
assign FifoWrAddr_b = FifoWrCount_c[g_FifoAddressWidth-1:0];

assign FifoEmpty = (FifoWrAddr_b == FifoRdAddr_b) && (FifoWrPhase == FifoRdPhase);
assign FifoFull  = (FifoWrAddr_b == FifoRdAddr_b) && (FifoWrPhase != FifoRdPhase);

// Interrupt event counters
always @(posedge Clk_ik) begin
    if (Rst_irq || !IntEnable) begin
        InterruptRequested_c32    <= #1 32'h0;
        InterruptAcknowledged_c32 <= #1 32'h0;
        InterruptSourceRead_c32   <= #1 32'h0;
    end else begin
        if (IntRequest && !FifoFull) begin
            InterruptRequested_c32    <= #1 InterruptRequested_c32    + 32'h1;
        end
        if (IntAck_i) begin
            InterruptAcknowledged_c32 <= #1 InterruptAcknowledged_c32 + 32'h1;
        end
        if (IntSourceRd && !FifoEmpty) begin
            InterruptSourceRead_c32   <= #1 InterruptSourceRead_c32   + 32'h1;
        end
    end
end

// Wishbone interface
assign IntLevel_ob3  = LevelReg_bq32[2:0];
assign IntVector_ob8 = VectorReg_bq32[7:0];
assign IntEnable     = ConfigReg_bq32[0];

always @(posedge Clk_ik) IntSourceRd <= #1 (Adr_ib3 == c_SourceRegAddr_b3) && Ack_d && !Ack_oa;

always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        ConfigReg_bq32 <= #1 32'h0000_0000;
        MaskReg_bq32   <= #1 32'hFFFF_FFFF;
        VectorReg_bq32 <= #1 32'h0000_0000;
        LevelReg_bq32  <= #1 32'h0000_0000;
    end
    else begin
        if (Cyc_i && We_i && Stb_i) begin
            case (Adr_ib3)
                c_ConfigRegAddr_b3 : ConfigReg_bq32 <= #1 Dat_ib32;
                c_MaskRegAddr_b3   : MaskReg_bq32   <= #1 Dat_ib32;
                c_VectorRegAddr_b3 : VectorReg_bq32 <= #1 Dat_ib32;
                c_LevelRegAddr_b3  : LevelReg_bq32  <= #1 Dat_ib32;
            endcase
        end
    end
end

always @(posedge Clk_ik) begin
    Ack_oa <= #1 Stb_i && Cyc_i;
    Ack_d  <= #1 Ack_oa;
    case (Adr_ib3)
        c_SourceRegAddr_b3 : Dat_oab32 <= #1 (FifoEmpty) ? 32'h0000_0000 : Fifo_mb32[FifoRdAddr_b];
        c_ConfigRegAddr_b3 : Dat_oab32 <= #1 ConfigReg_bq32;
        c_MaskRegAddr_b3   : Dat_oab32 <= #1 MaskReg_bq32;
        c_VectorRegAddr_b3 : Dat_oab32 <= #1 VectorReg_bq32;
        c_LevelRegAddr_b3  : Dat_oab32 <= #1 LevelReg_bq32;
        c_IntReqAddr_b3    : Dat_oab32 <= #1 InterruptRequested_c32 ;
        c_IntAckAddr_b3    : Dat_oab32 <= #1 InterruptAcknowledged_c32;
        c_IntSrcRdAddr_b3  : Dat_oab32 <= #1 InterruptSourceRead_c32;
        default            : Dat_oab32 <= #1 32'hFFFF_FFFF;
    endcase
end

endmodule
