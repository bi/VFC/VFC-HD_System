//----------------------------------------------------------------------
// Title      : OneWire Bus Sequencer
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : OneWireBus.sv
// Author     : T. Levens
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Conducts a transaction on the OneWire bus. Note that for read
// transactions less than 72 bits, the data is returned right aligned.
//----------------------------------------------------------------------

`timescale 1ns/100ps

module OneWireBus #(parameter g_ClkFrequency) (
    input             Clk_ik,
    input             Rst_irq,

    input      [ 6:0] Count_ib7,
    input      [71:0] WrData_ib72,
    output reg [71:0] RdData_ob72,
    input             WrStr_i,
    input             RdStr_i,
    input             BusRst_i,

    output            Busy_o,
    output            CrcOk_o,
    output reg        Prsnt_o,

    inout             OneWireBus_io
);

// Time delays are based on recommended values from Maxim AN126
localparam int c_DlyCntBits = $clog2(int'((960 * g_ClkFrequency) / 1e6) + 1);
localparam c_960us = (c_DlyCntBits)'(int'((960 * g_ClkFrequency) / 1e6));
localparam c_480us = (c_DlyCntBits)'(int'((480 * g_ClkFrequency) / 1e6));
localparam c_410us = (c_DlyCntBits)'(int'((410 * g_ClkFrequency) / 1e6));
localparam c_70us  = (c_DlyCntBits)'(int'(( 70 * g_ClkFrequency) / 1e6));
localparam c_64us  = (c_DlyCntBits)'(int'(( 64 * g_ClkFrequency) / 1e6));
localparam c_55us  = (c_DlyCntBits)'(int'(( 55 * g_ClkFrequency) / 1e6));
localparam c_10us  = (c_DlyCntBits)'(int'(( 10 * g_ClkFrequency) / 1e6));
localparam c_0us   = (c_DlyCntBits)'(int'((  0 * g_ClkFrequency) / 1e6));

reg  WrBitData, WrBitStr, RdBitData, RdBitStr, RstBitStr;
wire BitSeqBusy;
reg  [71:0] BitSR_b72;
reg  [ 6:0] BitCnt_c7;
reg  [ 7:0] CRC_b8;
reg  OneWireBus_e;
reg  [c_DlyCntBits-1:0] DlyCnt_c;
reg  [c_DlyCntBits-1:0] c_Disable;
reg  [c_DlyCntBits-1:0] c_Sample;

// Transaction sequencer
enum {
    s_Idle,
    s_Reset,
    s_Write,
    s_Read
} State_l;

assign Busy_o  = (State_l != s_Idle);
assign CrcOk_o = (CRC_b8 == 8'h0);

always @(posedge Clk_ik) begin
    WrBitStr        <= #1 1'b0;
    RdBitStr        <= #1 1'b0;
    RstBitStr       <= #1 1'b0;
    if (Rst_irq) begin
        State_l     <= #1 s_Idle;
        WrBitData   <= #1 1'b0;
        BitSR_b72   <= #1 72'h0;
        BitCnt_c7   <= #1 7'h0;
        CRC_b8      <= #1 8'h0;
        RdData_ob72 <= #1 72'h0;
        Prsnt_o     <= #1 1'b0;
    end else begin
        case (State_l)
            s_Idle: begin
                if (BusRst_i) begin
                    State_l         <= #1 s_Reset;
                    RstBitStr       <= #1 1'b1;
                end else if (WrStr_i) begin
                    State_l         <= #1 s_Write;
                    BitCnt_c7       <= #1 Count_ib7;
                    BitSR_b72       <= #1 WrData_ib72;
                    CRC_b8          <= #1 8'h0;
                end else if (RdStr_i) begin
                    State_l         <= #1 s_Read;
                    BitCnt_c7       <= #1 Count_ib7;
                    BitSR_b72       <= #1 72'h0;
                    CRC_b8          <= #1 8'h0;
                end
            end
            s_Reset: begin
                if (!BitSeqBusy && !RstBitStr) begin
                    State_l         <= #1 s_Idle;
                    Prsnt_o         <= #1 !RdBitData;
                end
            end
            s_Write: begin
                if (!BitSeqBusy && !WrBitStr) begin
                    if (!BitCnt_c7) begin
                        State_l     <= #1 s_Idle;
                    end else begin
                        BitCnt_c7   <= #1 BitCnt_c7 - 7'b1;
                        WrBitStr    <= #1 1'b1;
                        BitSR_b72   <= #1 {1'b0, BitSR_b72[71:1]};
                        WrBitData   <= #1 BitSR_b72[0];
                    end
                end
            end
            s_Read: begin
                if (!BitSeqBusy && !RdBitStr) begin
                    CRC_b8[7]       <= #1 RdBitData ^ CRC_b8[0];
                    CRC_b8[6:4]     <= #1 CRC_b8[7:5];
                    CRC_b8[3]       <= #1 RdBitData ^ CRC_b8[0] ^ CRC_b8[4];
                    CRC_b8[2]       <= #1 RdBitData ^ CRC_b8[0] ^ CRC_b8[3];
                    CRC_b8[1:0]     <= #1 CRC_b8[2:1];
                    if (!BitCnt_c7) begin
                        State_l     <= #1 s_Idle;
                        RdData_ob72 <= #1 {RdBitData, BitSR_b72[71:1]};
                    end else begin
                        BitCnt_c7   <= #1 BitCnt_c7 - 7'b1;
                        RdBitStr    <= #1 1'b1;
                        BitSR_b72   <= #1 {RdBitData, BitSR_b72[71:1]};
                    end
                end
            end
        endcase
    end
end

// Bit sequencer
enum {
    s_BitIdle,
    s_BitRW
} BitState_l;

assign BitSeqBusy = (BitState_l != s_BitIdle);

always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        BitState_l   <= #1 s_BitIdle;
        OneWireBus_e <= #1 1'b0;
        RdBitData    <= #1 1'b0;
    end else begin
        case (BitState_l)
            s_BitIdle: begin
                if (RstBitStr) begin
                    BitState_l    <= #1 s_BitRW;
                    OneWireBus_e  <= #1 1'b1;
                    DlyCnt_c      <= #1 c_960us;
                    c_Disable     <= #1 c_480us;
                    c_Sample      <= #1 c_410us;
                end else if (WrBitStr) begin
                    BitState_l    <= #1 s_BitRW;
                    OneWireBus_e  <= #1 1'b1;
                    DlyCnt_c      <= #1 c_70us;
                    if (WrBitData) begin
                        c_Disable <= #1 c_64us;
                    end else begin
                        c_Disable <= #1 c_10us;
                    end
                    c_Sample      <= #1 c_0us;
                end else if (RdBitStr) begin
                    BitState_l    <= #1 s_BitRW;
                    OneWireBus_e  <= #1 1'b1;
                    DlyCnt_c      <= #1 c_70us;
                    c_Disable     <= #1 c_64us;
                    c_Sample      <= #1 c_55us;
                end
            end
            s_BitRW: begin
                if (DlyCnt_c == c_0us) begin
                    BitState_l    <= #1 s_BitIdle;
                end else if (DlyCnt_c == c_Sample) begin
                    RdBitData     <= #1 OneWireBus_io;
                end else if (DlyCnt_c == c_Disable) begin
                    OneWireBus_e  <= #1 1'b0;
                end
                DlyCnt_c <= #1 DlyCnt_c - 1'b1;
            end
        endcase
    end
end

assign OneWireBus_io = (OneWireBus_e) ? 1'b0 : 1'bZ;

endmodule
