//----------------------------------------------------------------------
// Title      : VFC-HD Generic Top Level
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : VfcHdTop.sv
// Author     : -
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Generic top level design for the VFC-HD.
//
// NOTE: This file is contained in the VFC-HD_System repository.
// Normally you should not need to modify it. Code specific to your
// application should be contained within the file VfcHdApplication.sv
// in your project repository.
//
// Many I/O features can be customised with the file VfcHdConfig.vh.
// The directory containing this file must be included in your QSF
// project file as a SEARCH_PATH in order to be found by Quartus.
//----------------------------------------------------------------------

`timescale 1ns/100ps

`include "VfcHdConfig.vh"

module VfcHdTop #(
    parameter g_Synthesis = 1'b1
) (
    // NOTE: chip_pin attribute for buses has the same order of elements
    // as the logic vector:
    //   Example:
    //     (* chip_pin = "A2, A1, A0" *) input [2:0] Data_ib3,
    //     --> Data_ib3[0] ~ A0
    //     --> Data_ib3[1] ~ A1
    //     --> Data_ib3[2] ~ A2

    //------------------------------------------------------------------
    // VME interface
    //------------------------------------------------------------------

    (* useioff = 1 *)(* chip_pin = "AE29" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  VmeAs_in,

    (* useioff = 1 *)(* chip_pin = "AW31,AU31,AV31,AG30,AH30,AD29" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  [ 5:0] VmeAm_ib6,

    (* useioff = 1 *)(* chip_pin = "AU28,AV28,AP28,AR28,AC27,AD27,AM28,AB27,AB28,AD28,AE28,AH28,AJ28,AK29,AL29,AF28,AG28,AB29,AC29,AN29,AP29,AN30,AP30,AT29,AU29,AU30,AV30,AR30,AT30,AK30,AL30" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  [31:1] VmeA_iob31,

    (* useioff = 1 *)(* chip_pin = "AW30" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  VmeLWord_ion,

    (* useioff = 1 *)(* chip_pin = "AK27" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output VmeAOe_oen,

    (* useioff = 1 *)(* chip_pin = "AJ27" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output VmeADir_o,

    (* useioff = 1 *)(* chip_pin = "AN22,AP22" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  [ 1:0] VmeDs_inb2,

    (* useioff = 1 *)(* chip_pin = "AW28" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input VmeWrite_in,

    (* useioff = 1 *)(* chip_pin = "AE22,AF22,AG23,AH23,AV21,AW21,AV22,AW22,AT22,AU22,AK23,AL23,AD22,AE23,AN23,AP23,AT23,AU23,AN24,AP24,AW23,AW24,AG24,AH24,AE24,AF24,AK24,AL24,AT24,AU24,AD23,AD24" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  [31:0] VmeD_iob32,

    (* useioff = 1 *)(* chip_pin = "AW20" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output VmeDOe_oen,

    (* useioff = 1 *)(* chip_pin = "AW19" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output VmeDDir_o,

    (* useioff = 1 *)(* chip_pin = "AL22" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output VmeDtAckOe_o,

    (* useioff = 1 *)(* chip_pin = "AT20,AU20,AG22,AH22,AR21,AT21,AK22" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output [ 7:1] VmeIrq_ob7,

    (* useioff = 1 *)(* chip_pin = "AN21" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input VmeIack_in,

    (* useioff = 1 *)(* chip_pin = "AM21" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input VmeIackIn_in,

    (* useioff = 1 *)(* chip_pin = "AK21" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output VmeIackOut_on,

    (* useioff = 1 *)(* chip_pin = "AL20" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  VmeSysClk_ik,

    (* useioff = 1 *)(* chip_pin = "AK20" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  VmeSysReset_irn,

    //------------------------------------------------------------------
    // P0 timing
    //------------------------------------------------------------------

    (* useioff = 1 *)(* chip_pin = "AW26,AE26,AN26,AP26,AG25,AE25,AE27,AF27" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  [ 7:0] P0HwHighByte_ib8,

    (* useioff = 1 *)(* chip_pin = "AD25,AC22,AN20,AW27,AW33,AW32,AR31,AT31" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  [ 7:0] P0HwLowByte_ib8,

    (* useioff = 1 *)(* chip_pin = "AR27" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output DaisyChain1Cntrl_o,

    (* useioff = 1 *)(* chip_pin = "AP27" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output DaisyChain2Cntrl_o,

    (* useioff = 1 *)(* chip_pin = "AV19" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  VmeP0BunchClk_ik,

    (* useioff = 1 *)(* chip_pin = "AU19" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  VmeP0Tclk_ik,

    //------------------------------------------------------------------
    // SFP MGT lanes
    //------------------------------------------------------------------

`ifdef ENABLE_APPSFP1
    (* useioff = 1 *)(* chip_pin = "AW37" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  AppSfpRx1_ib,            // Differential signal

    (* useioff = 1 *)(* chip_pin = "AU37" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output AppSfpTx1_ob,            // Differential signal
`endif

`ifdef ENABLE_APPSFP2
    (* useioff = 1 *)(* chip_pin = "AT39" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  AppSfpRx2_ib,            // Differential signal

    (* useioff = 1 *)(* chip_pin = "AR37" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output AppSfpTx2_ob,            // Differential signal
`endif

`ifdef ENABLE_APPSFP3
    (* useioff = 1 *)(* chip_pin = "AP39" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  AppSfpRx3_ib,            // Differential signal

    (* useioff = 1 *)(* chip_pin = "AN37" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output AppSfpTx3_ob,            // Differential signal
`endif

`ifdef ENABLE_APPSFP4
    (* useioff = 1 *)(* chip_pin = "AM39" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  AppSfpRx4_ib,            // Differential signal

    (* useioff = 1 *)(* chip_pin = "AL37" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output AppSfpTx4_ob,            // Differential signal
`endif

`ifdef ENABLE_BSTSFP
    (* useioff = 1 *)(* chip_pin = "AH39" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  BstSfpRx_i,              // Differential signal

    (* useioff = 1 *)(* chip_pin = "AG37" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output BstSfpTx_o,              // Differential signal
`endif

`ifdef ENABLE_ETHSFP
    (* useioff = 1 *)(* chip_pin = "AE1" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  EthSfpRx_i,              // Differential signal

    (* useioff = 1 *)(* chip_pin = "AD3" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output EthSfpTx_o,              // Differential signal
`endif

    //------------------------------------------------------------------
    // DDR3
    //------------------------------------------------------------------

`ifdef ENABLE_DDR3
    `define ENABLE_DDR3_BANKA
    `define ENABLE_DDR3_BANKB
`endif

`ifdef ENABLE_DDR3_BANKA
    (* chip_pin = "AF16" *)
    output Ddr3aCk_ok,

    (* chip_pin = "AE17" *)
    output Ddr3aCk_okn,

    (* chip_pin = "AM16" *)
    output Ddr3aCke_o,

    (* chip_pin = "AN15" *)
    output Ddr3aReset_orn,

    (* chip_pin = "AD17" *)
    output Ddr3aRas_on,

    (* chip_pin = "AR18" *)
    output Ddr3aCas_on,

    (* chip_pin = "AL17" *)
    output Ddr3aCs_on,

    (* chip_pin = "AP18" *)
    output Ddr3aWe_on,

    (* chip_pin = "AD19" *)
    output Ddr3aOdt_o,

    (* chip_pin = "AC18,AD18,AE18" *)
    output [ 2:0] Ddr3aBa_ob3,

    (* chip_pin = "AU17,AG18,AL18,AM18,AG17,AH17,AN17,AP17,AR16,AT16,AU16,AV16,AJ16,AK16,AN16,AP16" *)
    output [15:0] Ddr3aAdr_ob16,

    (* chip_pin = "AU13,AD16" *)
    output [ 1:0] Ddr3aDm_ob2,

    (* chip_pin = "AF15,AH16" *)
    inout  [ 1:0] Ddr3aDqs_iob2,

    (* chip_pin = "AE16,AG16" *)
    inout  [ 1:0] Ddr3aDqs_iob2n,

    (* chip_pin = "AT14,AU14,AL14,AN14,AP14,AH14,AD15,AE15,AW14,AW15,AL15,AV13,AW13,AH15,AT15,AU15" *)
    inout  [15:0] Ddr3aDq_iob16,
`endif

`ifdef ENABLE_DDR3_BANKB
    (* chip_pin = "AM10" *)
    output Ddr3bCk_ok,

    (* chip_pin = "AL10" *)
    output Ddr3bCk_okn,

    (* chip_pin = "AL11" *)
    output Ddr3bCke_o,

    (* chip_pin = "AR13" *)
    output Ddr3bReset_orn,

    (* chip_pin = "AK12" *)
    output Ddr3bRas_on,

    (* chip_pin = "AU11" *)
    output Ddr3bCas_on,

    (* chip_pin = "AJ12" *)
    output Ddr3bCs_on,

    (* chip_pin = "AT11" *)
    output Ddr3bWe_on,

    (* chip_pin = "AV10" *)
    output Ddr3bOdt_o,

    (* chip_pin = "AL12,AC13,AD13" *)
    output [ 2:0] Ddr3bBa_ob3,

    (* chip_pin = "AF13,AV9,AW9,AP11,AC12,AD11,AF12,AG12,AT9,AU9,AG11,AH11,AD12,AE12,AP10,AR10" *)
    output [15:0] Ddr3bAdr_ob16,

    (* chip_pin = "AK10,AU12" *)
    output [ 1:0] Ddr3bDm_ob2,

    (* chip_pin = "AW5,AW12" *)
    inout  [ 1:0] Ddr3bDqs_iob2,

    (* chip_pin = "AW6,AV12" *)
    inout  [ 1:0] Ddr3bDqs_iob2n,

    (* chip_pin = "AW8,AM9,AW7,AR9,AV6,AT8,AU7,AU8,AE14,AE13,AM13,AW10,AW11,AP12,AH13,AJ13" *)
    inout  [15:0] Ddr3bDq_iob16,
`endif

    (* useioff = 1 *)(* chip_pin = "AP7" *)
    (* altera_attribute = "-name IO_STANDARD \"SSTL-135\"" *)
    input  OctRzqin_i,

    //------------------------------------------------------------------
    // FMC connector
    //------------------------------------------------------------------

    (* useioff = 1 *)(* chip_pin = "M34" *)
    (* altera_attribute = {"-name IO_STANDARD \"", `IO_STANDARD_VADJ, "\""} *)
    input  FmcPrsntM2c_in,

    (* useioff = 1 *)(* chip_pin = "AM31" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  FmcPgM2c_i,

    (* useioff = 1 *)(* chip_pin = "AL31" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output FmcPgC2m_o,

    (* useioff = 1 *)(* chip_pin = "AP33" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output FmcTck_ok,

    (* useioff = 1 *)(* chip_pin = "AK32" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output FmcTms_o,

    (* useioff = 1 *)(* chip_pin = "AN33" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output FmcTdi_o,

    (* useioff = 1 *)(* chip_pin = "AL32" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  FmcTdo_i,

    (* useioff = 1 *)(* chip_pin = "AK31" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output FmcTrstL_orn,

    (* useioff = 1 *)(* chip_pin = "AP32" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  FmcScl_iok,

    (* useioff = 1 *)(* chip_pin = "AN32" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  FmcSda_io,

    (* useioff = 1 *)(* chip_pin = "AM34" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  FmcClkDir_i,

    (* useioff = 1 *)(* chip_pin = "AP34" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  FmcClk0M2cCmos_ik,

    (* useioff = 1 *)(* chip_pin = "AK34" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  FmcClk1M2cCmos_ik,

`ifdef ENABLE_FMC_CLK2_BIDIR
    (* useioff = 1 *)(* chip_pin = "B22" *)
`ifdef FMC_CLK_BIDIR_OUTPUT
    (* altera_attribute = "-name IO_STANDARD LVDS_E_3R" *)
    output FmcClk2Bidir_iok,        // Differential signal
`else
    (* altera_attribute = "-name IO_STANDARD LVDS" *)
    input  FmcClk2Bidir_iok,        // Differential signal
`endif
`endif

`ifdef ENABLE_FMC_CLK3_BIDIR
    (* useioff = 1 *)(* chip_pin = "H6" *)
`ifdef FMC_CLK_BIDIR_OUTPUT
    (* altera_attribute = "-name IO_STANDARD LVDS_E_3R" *)
    output FmcClk3Bidir_iok,        // Differential signal
`else
    (* altera_attribute = "-name IO_STANDARD LVDS" *)
    input  FmcClk3Bidir_iok,        // Differential signal
`endif
`endif

`ifdef ENABLE_FMC_GBT_CLK0_M2C_LEFT
    (* useioff = 1 *)(* chip_pin = "AE31" *)
    (* altera_attribute = "-name IO_STANDARD LVDS" *)
    input  FmcGbtClk0M2cLeft_ik,    // Differential signal
`endif

`ifdef ENABLE_FMC_GBT_CLK1_M2C_LEFT
    (* useioff = 1 *)(* chip_pin = "AC31" *)
    (* altera_attribute = "-name IO_STANDARD LVDS" *)
    input  FmcGbtClk1M2cLeft_ik,    // Differential signal
`endif

`ifdef ENABLE_FMC_GBT_CLK0_M2C_RIGHT
    (* useioff = 1 *)(* chip_pin = "AB9" *)
    (* altera_attribute = "-name IO_STANDARD LVDS" *)
    input  FmcGbtClk0M2cRight_ik,   // Differential signal
`endif

`ifdef ENABLE_FMC_GBT_CLK1_M2C_RIGHT
    (* useioff = 1 *)(* chip_pin = "Y9" *)
    (* altera_attribute = "-name IO_STANDARD LVDS" *)
    input  FmcGbtClk1M2cRight_ik,   // Differential signal
`endif

    //------------------------------------------------------------------
    // FMC and VME P2 user defined pins
    //------------------------------------------------------------------

`include "VfcHdUserIo.vh"

    //------------------------------------------------------------------
    // FMC differential pairs
    //------------------------------------------------------------------

    // C2M
`ifdef ENABLE_FMC_DP0_C2M
    (* useioff = 1 *)(* chip_pin = "P3" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output FmcDpC2m0_o,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP1_C2M
    (* useioff = 1 *)(* chip_pin = "T3" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output FmcDpC2m1_o,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP2_C2M
    (* useioff = 1 *)(* chip_pin = "V3" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output FmcDpC2m2_o,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP3_C2M
    (* useioff = 1 *)(* chip_pin = "Y3" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output FmcDpC2m3_o,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP4_C2M
    (* useioff = 1 *)(* chip_pin = "AM3" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output FmcDpC2m4_o,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP5_C2M
    (* useioff = 1 *)(* chip_pin = "AT3" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output FmcDpC2m5_o,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP6_C2M
    (* useioff = 1 *)(* chip_pin = "AE37" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output FmcDpC2m6_o,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP7_C2M
    (* useioff = 1 *)(* chip_pin = "AA37" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output FmcDpC2m7_o,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP8_C2M
    (* useioff = 1 *)(* chip_pin = "AC37" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output FmcDpC2m8_o,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP9_C2M
    (* useioff = 1 *)(* chip_pin = "AP3" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    output FmcDpC2m9_o,             // Differential signal
`endif

    // M2C
`ifdef ENABLE_FMC_DP0_M2C
    (* useioff = 1 *)(* chip_pin = "R1" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  FmcDpM2c0_i,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP1_M2C
    (* useioff = 1 *)(* chip_pin = "U1" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  FmcDpM2c1_i,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP2_M2C
    (* useioff = 1 *)(* chip_pin = "W1" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  FmcDpM2c2_i,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP3_M2C
    (* useioff = 1 *)(* chip_pin = "AA1" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  FmcDpM2c3_i,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP4_M2C
    (* useioff = 1 *)(* chip_pin = "AN1" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  FmcDpM2c4_i,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP5_M2C
    (* useioff = 1 *)(* chip_pin = "AU1" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  FmcDpM2c5_i,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP6_M2C
    (* useioff = 1 *)(* chip_pin = "AF39" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  FmcDpM2c6_i,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP7_M2C
    (* useioff = 1 *)(* chip_pin = "AB39" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  FmcDpM2c7_i,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP8_M2C
    (* useioff = 1 *)(* chip_pin = "AD39" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  FmcDpM2c8_i,             // Differential signal
`endif

`ifdef ENABLE_FMC_DP9_M2C
    (* useioff = 1 *)(* chip_pin = "AR1" *)
    (* altera_attribute = "-name IO_STANDARD \"1.5-V PCML\"" *)
    input  FmcDpM2c9_i,             // Differential signal
`endif

    //------------------------------------------------------------------
    // FMC voltage control
    //------------------------------------------------------------------

    (* useioff = 1 *)(* chip_pin = "AU27" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output VadjCs_o ,

    (* useioff = 1 *)(* chip_pin = "AT27" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output VadjSclk_ok,

    (* useioff = 1 *)(* chip_pin = "AN27" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output VadjDin_o,

    (* useioff = 1 *)(* chip_pin = "AM27" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output VfmcEnable_oen,

    //------------------------------------------------------------------
    // ADC voltage monitoring
    //------------------------------------------------------------------

    (* useioff = 1 *)(* chip_pin = "AG27" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  VAdcDout_i,

    (* useioff = 1 *)(* chip_pin = "AC25" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output  VAdcDin_o,

    (* useioff = 1 *)(* chip_pin = "AB25" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output  VAdcCs_o,

    (* useioff = 1 *)(* chip_pin = "AL33" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output  VAdcSclk_ok,

    //------------------------------------------------------------------
    // BST interface
    //------------------------------------------------------------------

    (* useioff = 1 *)(* chip_pin = "AH27" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  BstDataIn_i,

    (* useioff = 1 *)(* chip_pin = "AU32" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  CdrClkOut_ik,

    (* useioff = 1 *)(* chip_pin = "AT32" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  CdrDataOut_i,

    //------------------------------------------------------------------
    // Clocks
    //------------------------------------------------------------------

    (* useioff = 1 *)(* chip_pin = "AW29" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output OeSi57x_oe,

    (* useioff = 1 *)(* chip_pin = "AM33" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  Si57xClk_ik,

    (* chip_pin = "AK7, AT7, AW4, AR6" *)
    (* altera_attribute = "-name IO_STANDARD \"SSTL-135\"" *)
    input  [3:0]  ClkFb_ikb4,

    (* chip_pin = "AJ7, AR7, AV4, AP6" *)
    (* altera_attribute = "-name IO_STANDARD \"SSTL-135\"; -name OUTPUT_TERMINATION \"SERIES 40 OHM WITH CALIBRATION\"" *)
    output [3:0]  ClkFb_okb4,

    (* useioff = 1 *)(* chip_pin = "AD20" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  Clk20VCOx_ik,

`ifdef ENABLE_GBIT_TRX_CLK_REF_R
    (* useioff = 1 *)(* chip_pin = "AF8" *)
    (* altera_attribute = "-name IO_STANDARD LVDS" *)
    input GbitTrxClkRefR_ik,        // 125 MHz VCXO differential reference clock for the MGT lines
`endif

    //------------------------------------------------------------------
    // DACs for tuning WR VCXOs
    //------------------------------------------------------------------
`ifdef ENABLE_WR
    (* useioff = 1 *)(* chip_pin = "AF25" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output PllDac20Sync_o,

    (* useioff = 1 *)(* chip_pin = "AC24" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output PllDac25Sync_o,

    (* useioff = 1 *)(* chip_pin = "AH26" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output PllDacSclk_ok,

    (* useioff = 1 *)(* chip_pin = "AG26" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output PllDacDin_o,
`endif
    //------------------------------------------------------------------
    // Si5338 PLL
    //------------------------------------------------------------------

    (* useioff = 1 *)(* chip_pin = "AL34" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  PllRefSda_ioz,

    (* useioff = 1 *)(* chip_pin = "AK33" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  PllRefScl_iokz,

    (* useioff = 1 *)(* chip_pin = "AJ33" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  PllRefInt_i,

    (* useioff = 1 *)(* chip_pin = "AC21" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output PllSourceMuxOut_ok,

`ifdef ENABLE_PLL_REFCLK_OUT
    (* useioff = 1 *)(* chip_pin = "AG32" *)
    (* altera_attribute = "-name IO_STANDARD LVDS" *)
    input PllRefClkOut_ik,          // Differential reference clock for the MGT lines
`endif

    //------------------------------------------------------------------
    // I2C mux & IO expanders
    //------------------------------------------------------------------

    (* useioff = 1 *)(* chip_pin = "AN25" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  I2cMuxSda_ioz,

    (* useioff = 1 *)(* chip_pin = "AM25" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  I2cMuxScl_iokz,

    (* useioff = 1 *)(* chip_pin = "AT25" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"; -name WEAK_PULL_UP_RESISTOR ON" *)
    input  I2CIoExpIntApp12_in,

    (* useioff = 1 *)(* chip_pin = "AR25" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"; -name WEAK_PULL_UP_RESISTOR ON" *)
    input  I2CIoExpIntApp34_in,

    (* useioff = 1 *)(* chip_pin = "AT26" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"; -name WEAK_PULL_UP_RESISTOR ON" *)
    input  I2CIoExpIntBstEth_in,

    (* useioff = 1 *)(* chip_pin = "AU26" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"; -name WEAK_PULL_UP_RESISTOR ON" *)
    input  I2CIoExpIntBlmIn_in,

    (* useioff = 1 *)(* chip_pin = "AK25" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"; -name WEAK_PULL_UP_RESISTOR ON" *)
    input  I2CIoExpIntLos_in,

    //------------------------------------------------------------------
    // WR PROM
    //------------------------------------------------------------------

    (* useioff = 1 *)(* chip_pin = "AD26" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  WrPromSda_io,

    (* useioff = 1 *)(* chip_pin = "AH25" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output  WrPromScl_ok,

    //------------------------------------------------------------------
    // Miscellaneous
    //------------------------------------------------------------------

    (* useioff = 1 *)(* chip_pin = "T23,R23,N23,M23,K23,J23,J24,H24" *)
    (* altera_attribute = {"-name IO_STANDARD \"", `IO_STANDARD_VADJ, "\""} *)
    input  [ 8:1] DipSw_ib8,    // Contains also the part formerly used as No GA

    (* useioff = 1 *)(* chip_pin = "T21,R21,A24,A23,M22,L22,T22,R22" *)
    (* altera_attribute = {"-name IO_STANDARD \"", `IO_STANDARD_VADJ, "\""} *)
    input  [ 7:0] PcbRev_ib7,

    (* useioff = 1 *)(* chip_pin = "AP20" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  TestIo1_io,

    (* useioff = 1 *)(* chip_pin = "AH20" *)(* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  TestIo2_io,

    (* useioff = 1 *)(* chip_pin = "AV24,AL26,AG21,AH21" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  [ 4:1] GpIo_iob4,

    (* useioff = 1 *)(* chip_pin = "AW25" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    input  PushButtonN_in,

    (* useioff = 1 *)(* chip_pin = "AV27" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    inout  TempIdDq_ioz,

    (* useioff = 1 *)(* chip_pin = "AD21" *)
    (* altera_attribute = "-name IO_STANDARD \"3.3-V LVCMOS\"" *)
    output ResetFpgaConfigN_orn
);

//----------------------------------------------------------------------
// Clock frequency in Hz, defined in the VfcHdConfig.vh
//----------------------------------------------------------------------
// A workaround for what appears to be quartus issue. For some reason, it
// requires value assigned to this parameter to be real. If SYS_CLK_FREQ
// was defined as integer number, quartus consistently failed to compile.
// At this time, the below multiply by 1.0 seems to be the simplest
// workaround to support SYS_CLK_FREQ defined as integer or real.
//
localparam c_ClkFrequency = (`SYS_CLK_FREQ) *1.0;

//----------------------------------------------------------------------
// Local signals
//----------------------------------------------------------------------

wire [ 31:0] AppRelease_b32;
wire [351:0] AppIdent_b352;
wire         AppReset_r, ResetRequest_r;
wire         SysClk_k;
wire         SysReset_ar;
wire         Wb1SysToAppCyc, Wb1SysToAppStb, Wb1SysToAppWr, Wb1AppToSysAck;
wire [ 24:0] Wb1SysToAppAdr_b25;
wire [ 31:0] Wb1SysToAppDat_b32, Wb1AppToSysDat_b32;
wire         Wb2AppToSysCyc, Wb2AppToSysStb, Wb2AppToSysWr, Wb2SysToAppAck;
wire [ 24:0] Wb2AppToSysAdr_b25;
wire [ 31:0] Wb2AppToSysDat_b32, Wb2SysToAppDat_b32;
wire [  8:1] Led_b8;
wire         BstRst_r, BstClk_k, BunchClkFlag, TurnClkFlag, BunchClkFlagDly, TurnClkFlagDly;
wire [  7:0] BstByteAddress_b8, BstByteData_b8;
wire         BstByteStrobe, BstByteError;
wire [ 23:0] InterruptRequest_b24, InterruptAvailable_b24;
wire         GpIo1DirOut, GpIo1DirOutStatus, GpIo2DirOut, GpIo2DirOutStatus, GpIo34DirOut, GpIo34DirOutStatus;
wire         GpIo1EnTerm, GpIo1EnTermStatus, GpIo2EnTerm, GpIo2EnTermStatus,
             GpIo3EnTerm, GpIo3EnTermStatus, GpIo4EnTerm, GpIo4EnTermStatus;
wire [  7:0] BlmIn_b8;
wire [  4:1] AppSfpPresent_b4, AppSfpTxFault_b4, AppSfpLos_b4, AppSfpTxDisable_b4,
             AppSfpRateSelect_b4, StatusAppSfpTxDisable_b4, StatusAppSfpRateSelect_b4;
wire         BstSfpPresent, BstSfpTxFault, BstSfpLos, BstSfpTxDisable, BstSfpRateSelect,
             StatusBstSfpTxDisable, StatusBstSfpRateSelect;
wire         EthSfpPresent, EthSfpTxFault, EthSfpLos, EthSfpTxDisable, EthSfpRateSelect,
             StatusEthSfpTxDisable, StatusEthSfpRateSelect;
wire         CdrLos, CdrLol;
wire         I2cWbCyc, I2cWbStb, I2cWbWe, I2cWbAck;
wire [ 11:0] I2cWbAdr_b12;
wire [  7:0] I2cWbDatMoSi_b8, I2cWbDatMiSo_b8;
wire [  3:0] ClkFbOutput_kb4;
wire         ArriaTermSDO;
wire [ 15:0] OctParallelControl_b16;
wire [ 15:0] OctSeriesControl_b16;
wire [  8:1] LedStatus_b8;
wire         VfmcAppEnable;
wire [  2:0] VadjSelect_b3;
wire [ 63:0] UniqueId_b64;
wire         UniqueIdValid;
wire [ 15:0] Temp_b16;
wire         TempRead_p;
wire [  7:0] ChipTemp_b8;
wire         ChipTempRead_p;
wire [ 11:0] Voltage0_b12, Voltage1_b12, Voltage2_b12, Voltage3_b12,
             Voltage4_b12, Voltage5_b12, Voltage6_b12, Voltage7_b12;
wire [  7:0] VoltageOk_b8;
wire         VoltageRead_p;
wire [  4:0] VmeGa_nb5;
wire         VmeGap_n;

//----------------------------------------------------------------------
// Disabled I/O replacement nets to avoid warnings
//----------------------------------------------------------------------

// SFP MGT lanes
`ifndef ENABLE_APPSFP1
    wire AppSfpRx1_ib           = 1'b0;
    wire AppSfpTx1_ob           = 1'b0;
`endif
`ifndef ENABLE_APPSFP2
    wire AppSfpRx2_ib           = 1'b0;
    wire AppSfpTx2_ob           = 1'b0;
`endif
`ifndef ENABLE_APPSFP3
    wire AppSfpRx3_ib           = 1'b0;
    wire AppSfpTx3_ob           = 1'b0;
`endif
`ifndef ENABLE_APPSFP4
    wire AppSfpRx4_ib           = 1'b0;
    wire AppSfpTx4_ob           = 1'b0;
`endif
`ifndef ENABLE_BSTSFP
    wire BstSfpRx_i             = 1'b0;
    wire BstSfpTx_o             = 1'b0;
`endif
`ifndef ENABLE_ETHSFP
    wire EthSfpRx_i             = 1'b0;
    wire EthSfpTx_o             = 1'b0;
`endif

// Clocks
`ifndef ENABLE_PLL_REFCLK_OUT
    wire PllRefClkOut_ik        = 1'b0;
`endif
`ifndef ENABLE_GBIT_TRX_CLK_REF_R
    wire GbitTrxClkRefR_ik      = 1'b0;
`endif

// FMC clocks
`ifndef ENABLE_FMC_CLK2_BIDIR
    wire FmcClk2Bidir_iok       = 1'b0;
`endif
`ifndef ENABLE_FMC_CLK3_BIDIR
    wire FmcClk3Bidir_iok       = 1'b0;
`endif
`ifndef ENABLE_FMC_GBT_CLK0_M2C_LEFT
    wire FmcGbtClk0M2cLeft_ik   = 1'b0;
`endif
`ifndef ENABLE_FMC_GBT_CLK1_M2C_LEFT
    wire FmcGbtClk1M2cLeft_ik   = 1'b0;
`endif
`ifndef ENABLE_FMC_GBT_CLK0_M2C_RIGHT
    wire FmcGbtClk0M2cRight_ik  = 1'b0;
`endif
`ifndef ENABLE_FMC_GBT_CLK1_M2C_RIGHT
    wire FmcGbtClk1M2cRight_ik  = 1'b0;
`endif

// FMC DP M2C
`ifndef ENABLE_FMC_DP0_C2M
    wire FmcDpC2m0_o            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP1_C2M
    wire FmcDpC2m1_o            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP2_C2M
    wire FmcDpC2m2_o            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP3_C2M
    wire FmcDpC2m3_o            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP4_C2M
    wire FmcDpC2m4_o            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP5_C2M
    wire FmcDpC2m5_o            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP6_C2M
    wire FmcDpC2m6_o            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP7_C2M
    wire FmcDpC2m7_o            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP8_C2M
    wire FmcDpC2m8_o            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP9_C2M
    wire FmcDpC2m9_o            = 1'b0;
`endif

// FMC DP C2M
`ifndef ENABLE_FMC_DP0_M2C
    wire FmcDpM2c0_i            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP1_M2C
    wire FmcDpM2c1_i            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP2_M2C
    wire FmcDpM2c2_i            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP3_M2C
    wire FmcDpM2c3_i            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP4_M2C
    wire FmcDpM2c4_i            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP5_M2C
    wire FmcDpM2c5_i            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP6_M2C
    wire FmcDpM2c6_i            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP7_M2C
    wire FmcDpM2c7_i            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP8_M2C
    wire FmcDpM2c8_i            = 1'b0;
`endif
`ifndef ENABLE_FMC_DP9_M2C
    wire FmcDpM2c9_i            = 1'b0;
`endif

// FMC and VME P2 user defined pins
`include "VfcHdUserIo_unused.vh"

//----------------------------------------------------------------------
// ARRIA V I/O required primitives
//----------------------------------------------------------------------

arriav_termination i_arriav_termination (
    .rzqin      (OctRzqin_i),
    .serdataout (ArriaTermSDO));

arriav_termination_logic i_arriav_termination_logic (
    .serdata                    (ArriaTermSDO),
    .parallelterminationcontrol (OctParallelControl_b16),
    .seriesterminationcontrol   (OctSeriesControl_b16));

genvar i;

generate
    for (i = 0; i < 4; i++) begin: gen_ClkFbIob
        arriav_io_obuf #(
            .bus_hold          ("false"),
            .open_drain_output ("false"),
            .lpm_type          ("arriav_io_obuf"))
        i_ClkFbIob (
            .i                         (ClkFbOutput_kb4[i]),
            .o                         (ClkFb_okb4[i]),
            .obar                      (),
            .oe                        (1'b1),
            .dynamicterminationcontrol (1'b0),
            .parallelterminationcontrol(OctParallelControl_b16),
            .seriesterminationcontrol  (OctSeriesControl_b16)
            // synthesis translate_off
            ,
            .devoe(1'b1)
            // synthesis translate_on
        );
    end
endgenerate

//----------------------------------------------------------------------
// System module
//----------------------------------------------------------------------

VfcHdSystem #(
    .g_ClkFrequency             (c_ClkFrequency),
    .g_Synthesis                (g_Synthesis))
i_VfcHdSystem (
    // External ports
    .VmeAs_in                   (VmeAs_in),
    .VmeAm_ib6                  (VmeAm_ib6),
    .VmeA_iob31                 (VmeA_iob31),
    .VmeLWord_ion               (VmeLWord_ion),
    .VmeAOe_oen                 (VmeAOe_oen),
    .VmeADir_o                  (VmeADir_o),
    .VmeDs_inb2                 (VmeDs_inb2),
    .VmeWrite_in                (VmeWrite_in),
    .VmeD_iob32                 (VmeD_iob32),
    .VmeDOe_oen                 (VmeDOe_oen),
    .VmeDDir_o                  (VmeDDir_o),
    .VmeDtAckOe_o               (VmeDtAckOe_o),
    .VmeIrq_ob7                 (VmeIrq_ob7),
    .VmeIack_in                 (VmeIack_in),
    .VmeIackIn_in               (VmeIackIn_in),
    .VmeIackOut_on              (VmeIackOut_on),
    .VmeSysClk_ik               (VmeSysClk_ik),
    .VmeSysReset_irn            (VmeSysReset_irn),
    .I2cMuxSda_ioz              (I2cMuxSda_ioz),
    .I2cMuxScl_iokz             (I2cMuxScl_iokz),
    .I2CIoExpIntApp12_ian       (I2CIoExpIntApp12_in),
    .I2CIoExpIntApp34_ian       (I2CIoExpIntApp34_in),
    .I2CIoExpIntBstEth_ian      (I2CIoExpIntBstEth_in),
    .I2CIoExpIntBlmIn_ian       (I2CIoExpIntBlmIn_in),
    .I2CIoExpIntLos_ian         (I2CIoExpIntLos_in),
    .BstDataIn_i                (BstDataIn_i),
    .CdrClkOut_ik               (CdrClkOut_ik),
    .CdrDataOut_i               (CdrDataOut_i),
    .VAdcDout_i                 (VAdcDout_i),
    .VAdcDin_o                  (VAdcDin_o),
    .VAdcCs_o                   (VAdcCs_o),
    .VAdcSclk_ok                (VAdcSclk_ok),
    .Clk20VCOx_ik               (Clk20VCOx_ik),
    .VadjCs_o                   (VadjCs_o),
    .VadjSclk_ok                (VadjSclk_ok),
    .VadjDin_o                  (VadjDin_o),
    .PcbRev_ib7                 (PcbRev_ib7),
    .TempIdDq_ioz               (TempIdDq_ioz),
    .ResetFpgaConfigN_orn       (ResetFpgaConfigN_orn),

    // System <=> Application interface
    .SysClk_ik                  (SysClk_k),
    .SysReset_iar               (SysReset_ar),
    .AppRelease_ib32            (AppRelease_b32),
    .AppIdent_ib352             (AppIdent_b352),
    .AppReset_oqr               (AppReset_r),
    .VfmcEnable_oen             (VfmcEnable_oen),
    .VfmcAppEnable_i            (VfmcAppEnable),
    .VadjSelect_ib3             (VadjSelect_b3),
    .WbMasterCyc_oq             (Wb1SysToAppCyc),
    .WbMasterStb_oq             (Wb1SysToAppStb),
    .WbMasterAdr_oqb25          (Wb1SysToAppAdr_b25),
    .WbMasterWr_oq              (Wb1SysToAppWr),
    .WbMasterDat_oqb32          (Wb1SysToAppDat_b32),
    .WbMasterDat_ib32           (Wb1AppToSysDat_b32),
    .WbMasterAck_i              (Wb1AppToSysAck),
    .WbSlaveCyc_i               (Wb2AppToSysCyc),
    .WbSlaveStb_i               (Wb2AppToSysStb),
    .WbSlaveAdr_ib25            (Wb2AppToSysAdr_b25),
    .WbSlaveWr_i                (Wb2AppToSysWr),
    .WbSlaveDat_ib32            (Wb2AppToSysDat_b32),
    .WbSlaveDat_ob32            (Wb2SysToAppDat_b32),
    .WbSlaveAck_o               (Wb2SysToAppAck),
    .Led_ib8                    (Led_b8),
    .LedStatus_ob8              (LedStatus_b8),
    .BstClk_ok                  (BstClk_k),
    .BstRst_or                  (BstRst_r),
    .BunchClkFlag_o             (BunchClkFlag),
    .TurnClkFlag_o              (TurnClkFlag),
    .BunchClkFlagDly_o          (BunchClkFlagDly),
    .TurnClkFlagDly_o           (TurnClkFlagDly),
    .BstByteAddress_ob8         (BstByteAddress_b8),
    .BstByteData_ob8            (BstByteData_b8),
    .BstByteStrobe_o            (BstByteStrobe),
    .BstByteError_o             (BstByteError),
    .InterruptRequest_ib24      (InterruptRequest_b24),
    .InterruptAvailable_ob24    (InterruptAvailable_b24),
    .BlmIn_ob8                  (BlmIn_b8),
    .GpIo1DirOut_i              (GpIo1DirOut),
    .GpIo1DirOutStatus_o        (GpIo1DirOutStatus),
    .GpIo2DirOut_i              (GpIo2DirOut),
    .GpIo2DirOutStatus_o        (GpIo2DirOutStatus),
    .GpIo34DirOut_i             (GpIo34DirOut),
    .GpIo34DirOutStatus_o       (GpIo34DirOutStatus),
    .GpIo1EnTerm_i              (GpIo1EnTerm),
    .GpIo1EnTermStatus_o        (GpIo1EnTermStatus),
    .GpIo2EnTerm_i              (GpIo2EnTerm),
    .GpIo2EnTermStatus_o        (GpIo2EnTermStatus),
    .GpIo3EnTerm_i              (GpIo3EnTerm),
    .GpIo3EnTermStatus_o        (GpIo3EnTermStatus),
    .GpIo4EnTerm_i              (GpIo4EnTerm),
    .GpIo4EnTermStatus_o        (GpIo4EnTermStatus),
    .AppSfp1Present_oq          (AppSfpPresent_b4[1]),
    .AppSfp1TxFault_oq          (AppSfpTxFault_b4[1]),
    .AppSfp1Los_oq              (AppSfpLos_b4[1]),
    .AppSfp1TxDisable_i         (AppSfpTxDisable_b4[1]),
    .AppSfp1RateSelect_i        (AppSfpRateSelect_b4[1]),
    .StatusAppSfp1TxDisable_oq  (StatusAppSfpTxDisable_b4[1]),
    .StatusAppSfp1RateSelect_oq (StatusAppSfpRateSelect_b4[1]),
    .AppSfp2Present_oq          (AppSfpPresent_b4[2]),
    .AppSfp2TxFault_oq          (AppSfpTxFault_b4[2]),
    .AppSfp2Los_oq              (AppSfpLos_b4[2]),
    .AppSfp2TxDisable_i         (AppSfpTxDisable_b4[2]),
    .AppSfp2RateSelect_i        (AppSfpRateSelect_b4[2]),
    .StatusAppSfp2TxDisable_oq  (StatusAppSfpTxDisable_b4[2]),
    .StatusAppSfp2RateSelect_oq (StatusAppSfpRateSelect_b4[2]),
    .AppSfp3Present_oq          (AppSfpPresent_b4[3]),
    .AppSfp3TxFault_oq          (AppSfpTxFault_b4[3]),
    .AppSfp3Los_oq              (AppSfpLos_b4[3]),
    .AppSfp3TxDisable_i         (AppSfpTxDisable_b4[3]),
    .AppSfp3RateSelect_i        (AppSfpRateSelect_b4[3]),
    .StatusAppSfp3TxDisable_oq  (StatusAppSfpTxDisable_b4[3]),
    .StatusAppSfp3RateSelect_oq (StatusAppSfpRateSelect_b4[3]),
    .AppSfp4Present_oq          (AppSfpPresent_b4[4]),
    .AppSfp4TxFault_oq          (AppSfpTxFault_b4[4]),
    .AppSfp4Los_oq              (AppSfpLos_b4[4]),
    .AppSfp4TxDisable_i         (AppSfpTxDisable_b4[4]),
    .AppSfp4RateSelect_i        (AppSfpRateSelect_b4[4]),
    .StatusAppSfp4TxDisable_oq  (StatusAppSfpTxDisable_b4[4]),
    .StatusAppSfp4RateSelect_oq (StatusAppSfpRateSelect_b4[4]),
    .BstSfpPresent_oq           (BstSfpPresent),
    .BstSfpTxFault_oq           (BstSfpTxFault),
    .BstSfpLos_oq               (BstSfpLos),
    .BstSfpTxDisable_i          (BstSfpTxDisable),
    .BstSfpRateSelect_i         (BstSfpRateSelect),
    .StatusBstSfpTxDisable_oq   (StatusBstSfpTxDisable),
    .StatusBstSfpRateSelect_oq  (StatusBstSfpRateSelect),
    .EthSfpPresent_oq           (EthSfpPresent),
    .EthSfpTxFault_oq           (EthSfpTxFault),
    .EthSfpLos_oq               (EthSfpLos),
    .EthSfpTxDisable_i          (EthSfpTxDisable),
    .EthSfpRateSelect_i         (EthSfpRateSelect),
    .StatusEthSfpTxDisable_oq   (StatusEthSfpTxDisable),
    .StatusEthSfpRateSelect_oq  (StatusEthSfpRateSelect),
    .CdrLos_oq                  (CdrLos),
    .CdrLol_oq                  (CdrLol),
    .I2cWbCyc_i                 (I2cWbCyc),
    .I2cWbStb_i                 (I2cWbStb),
    .I2cWbWe_i                  (I2cWbWe),
    .I2cWbAdr_ib12              (I2cWbAdr_b12),
    .I2cWbDat_ib8               (I2cWbDatMoSi_b8),
    .I2cWbDat_ob8               (I2cWbDatMiSo_b8),
    .I2cWbAck_o                 (I2cWbAck),
    .UniqueId_ob64              (UniqueId_b64),
    .UniqueIdValid_o            (UniqueIdValid),
    .Temp_ob16                  (Temp_b16),
    .TempRead_op                (TempRead_p),
    .ChipTemp_ob8               (ChipTemp_b8),
    .ChipTempRead_op            (ChipTempRead_p),
    .Voltage0_ob12              (Voltage0_b12),
    .Voltage1_ob12              (Voltage1_b12),
    .Voltage2_ob12              (Voltage2_b12),
    .Voltage3_ob12              (Voltage3_b12),
    .Voltage4_ob12              (Voltage4_b12),
    .Voltage5_ob12              (Voltage5_b12),
    .Voltage6_ob12              (Voltage6_b12),
    .Voltage7_ob12              (Voltage7_b12),
    .VoltageOk_ob8              (VoltageOk_b8),
    .VoltageRead_op             (VoltageRead_p),
    .VmeGa_onb5                 (VmeGa_nb5),
    .VmeGap_on                  (VmeGap_n));

//----------------------------------------------------------------------
// Application module
//----------------------------------------------------------------------

VfcHdApplication #(
    .g_ClkFrequency             (c_ClkFrequency),
    .g_Synthesis                (g_Synthesis))
i_VfcHdApplication (
    // External ports
    .BstSfpRx_i                 (BstSfpRx_i),
    .BstSfpTx_o                 (BstSfpTx_o),
    .EthSfpRx_i                 (EthSfpRx_i),
    .EthSfpTx_o                 (EthSfpTx_o),
    .AppSfpRx_ib4               ({AppSfpRx4_ib, AppSfpRx3_ib, AppSfpRx2_ib, AppSfpRx1_ib}),
    .AppSfpTx_ob4               ({AppSfpTx4_ob, AppSfpTx3_ob, AppSfpTx2_ob, AppSfpTx1_ob}),
`ifdef ENABLE_DDR3_BANKA
    .Ddr3aCk_ok                 (Ddr3aCk_ok),
    .Ddr3aCk_okn                (Ddr3aCk_okn),
    .Ddr3aCke_o                 (Ddr3aCke_o),
    .Ddr3aReset_orn             (Ddr3aReset_orn),
    .Ddr3aRas_on                (Ddr3aRas_on),
    .Ddr3aCas_on                (Ddr3aCas_on),
    .Ddr3aCs_on                 (Ddr3aCs_on),
    .Ddr3aWe_on                 (Ddr3aWe_on),
    .Ddr3aOdt_o                 (Ddr3aOdt_o),
    .Ddr3aBa_ob3                (Ddr3aBa_ob3),
    .Ddr3aAdr_ob16              (Ddr3aAdr_ob16),
    .Ddr3aDm_ob2                (Ddr3aDm_ob2),
    .Ddr3aDqs_iob2              (Ddr3aDqs_iob2),
    .Ddr3aDqs_iob2n             (Ddr3aDqs_iob2n),
    .Ddr3aDq_iob16              (Ddr3aDq_iob16),
`endif //  `ifdef ENABLE_DDR3_BANKA
`ifdef ENABLE_DDR3_BANKB
    .Ddr3bCk_ok                 (Ddr3bCk_ok),
    .Ddr3bCk_okn                (Ddr3bCk_okn),
    .Ddr3bCke_o                 (Ddr3bCke_o),
    .Ddr3bReset_orn             (Ddr3bReset_orn),
    .Ddr3bRas_on                (Ddr3bRas_on),
    .Ddr3bCas_on                (Ddr3bCas_on),
    .Ddr3bCs_on                 (Ddr3bCs_on),
    .Ddr3bWe_on                 (Ddr3bWe_on),
    .Ddr3bOdt_o                 (Ddr3bOdt_o),
    .Ddr3bBa_ob3                (Ddr3bBa_ob3),
    .Ddr3bAdr_ob16              (Ddr3bAdr_ob16),
    .Ddr3bDm_ob2                (Ddr3bDm_ob2),
    .Ddr3bDqs_iob2              (Ddr3bDqs_iob2),
    .Ddr3bDqs_iob2n             (Ddr3bDqs_iob2n),
    .Ddr3bDq_iob16              (Ddr3bDq_iob16),
`endif //  `ifdef ENABLE_DDR3_BANKB
    .TestIo1_io                 (TestIo1_io),
    .TestIo2_io                 (TestIo2_io),
    .FmcPrsntM2c_in             (FmcPrsntM2c_in),
    .FmcPgM2c_i                 (FmcPgM2c_i),
    .FmcPgC2m_o                 (FmcPgC2m_o),
    .FmcTck_ok                  (FmcTck_ok),
    .FmcTms_o                   (FmcTms_o),
    .FmcTdi_o                   (FmcTdi_o),
    .FmcTdo_i                   (FmcTdo_i),
    .FmcTrstL_orn               (FmcTrstL_orn),
    .FmcScl_iok                 (FmcScl_iok),
    .FmcSda_io                  (FmcSda_io),
    .FmcClkDir_i                (FmcClkDir_i),
    .FmcClk0M2cCmos_ik          (FmcClk0M2cCmos_ik),
    .FmcClk1M2cCmos_ik          (FmcClk1M2cCmos_ik),
    .FmcClk2Bidir_iok           (FmcClk2Bidir_iok),
    .FmcClk3Bidir_iok           (FmcClk3Bidir_iok),
    .FmcGbtClk0M2cLeft_ik       (FmcGbtClk0M2cLeft_ik),
    .FmcGbtClk1M2cLeft_ik       (FmcGbtClk1M2cLeft_ik),
    .FmcGbtClk0M2cRight_ik      (FmcGbtClk0M2cRight_ik),
    .FmcGbtClk1M2cRight_ik      (FmcGbtClk1M2cRight_ik),
    .FmcLaP_iob34               ({FmcLaP33_io, FmcLaP32_io, FmcLaP31_io, FmcLaP30_io, FmcLaP29_io,
                                  FmcLaP28_io, FmcLaP27_io, FmcLaP26_io, FmcLaP25_io, FmcLaP24_io,
                                  FmcLaP23_io, FmcLaP22_io, FmcLaP21_io, FmcLaP20_io, FmcLaP19_io,
                                  FmcLaP18_io, FmcLaP17_io, FmcLaP16_io, FmcLaP15_io, FmcLaP14_io,
                                  FmcLaP13_io, FmcLaP12_io, FmcLaP11_io, FmcLaP10_io, FmcLaP9_io,
                                  FmcLaP8_io,  FmcLaP7_io,  FmcLaP6_io,  FmcLaP5_io,  FmcLaP4_io,
                                  FmcLaP3_io,  FmcLaP2_io,  FmcLaP1_io,  FmcLaP0_io}),
    .FmcLaN_iob34               ({FmcLaN33_io, FmcLaN32_io, FmcLaN31_io, FmcLaN30_io, FmcLaN29_io,
                                  FmcLaN28_io, FmcLaN27_io, FmcLaN26_io, FmcLaN25_io, FmcLaN24_io,
                                  FmcLaN23_io, FmcLaN22_io, FmcLaN21_io, FmcLaN20_io, FmcLaN19_io,
                                  FmcLaN18_io, FmcLaN17_io, FmcLaN16_io, FmcLaN15_io, FmcLaN14_io,
                                  FmcLaN13_io, FmcLaN12_io, FmcLaN11_io, FmcLaN10_io, FmcLaN9_io,
                                  FmcLaN8_io,  FmcLaN7_io,  FmcLaN6_io,  FmcLaN5_io,  FmcLaN4_io,
                                  FmcLaN3_io,  FmcLaN2_io,  FmcLaN1_io,  FmcLaN0_io}),
    .FmcHaP_iob24               ({FmcHaP23_io, FmcHaP22_io, FmcHaP21_io, FmcHaP20_io, FmcHaP19_io,
                                  FmcHaP18_io, FmcHaP17_io, FmcHaP16_io, FmcHaP15_io, FmcHaP14_io,
                                  FmcHaP13_io, FmcHaP12_io, FmcHaP11_io, FmcHaP10_io, FmcHaP9_io,
                                  FmcHaP8_io,  FmcHaP7_io,  FmcHaP6_io,  FmcHaP5_io,  FmcHaP4_io,
                                  FmcHaP3_io,  FmcHaP2_io,  FmcHaP1_io,  FmcHaP0_io}),
    .FmcHaN_iob24               ({FmcHaN23_io, FmcHaN22_io, FmcHaN21_io, FmcHaN20_io, FmcHaN19_io,
                                  FmcHaN18_io, FmcHaN17_io, FmcHaN16_io, FmcHaN15_io, FmcHaN14_io,
                                  FmcHaN13_io, FmcHaN12_io, FmcHaN11_io, FmcHaN10_io, FmcHaN9_io,
                                  FmcHaN8_io,  FmcHaN7_io,  FmcHaN6_io,  FmcHaN5_io,  FmcHaN4_io,
                                  FmcHaN3_io,  FmcHaN2_io,  FmcHaN1_io,  FmcHaN0_io}),
    .FmcHbP_iob22               ({FmcHbP21_io, FmcHbP20_io, FmcHbP19_io, FmcHbP18_io, FmcHbP17_io,
                                  FmcHbP16_io, FmcHbP15_io, FmcHbP14_io, FmcHbP13_io, FmcHbP12_io,
                                  FmcHbP11_io, FmcHbP10_io, FmcHbP9_io,  FmcHbP8_io,  FmcHbP7_io,
                                  FmcHbP6_io,  FmcHbP5_io,  FmcHbP4_io,  FmcHbP3_io,  FmcHbP2_io,
                                  FmcHbP1_io,  FmcHbP0_io}),
    .FmcHbN_iob22               ({FmcHbN21_io, FmcHbN20_io, FmcHbN19_io, FmcHbN18_io, FmcHbN17_io,
                                  FmcHbN16_io, FmcHbN15_io, FmcHbN14_io, FmcHbN13_io, FmcHbN12_io,
                                  FmcHbN11_io, FmcHbN10_io, FmcHbN9_io,  FmcHbN8_io,  FmcHbN7_io,
                                  FmcHbN6_io,  FmcHbN5_io,  FmcHbN4_io,  FmcHbN3_io,  FmcHbN2_io,
                                  FmcHbN1_io,  FmcHbN0_io}),
    .FmcDpC2m_ob10              ({FmcDpC2m9_o, FmcDpC2m8_o, FmcDpC2m7_o, FmcDpC2m6_o, FmcDpC2m5_o,
                                  FmcDpC2m4_o, FmcDpC2m3_o, FmcDpC2m2_o, FmcDpC2m1_o, FmcDpC2m0_o}),
    .FmcDpM2c_ib10              ({FmcDpM2c9_i, FmcDpM2c8_i, FmcDpM2c7_i, FmcDpM2c6_i, FmcDpM2c5_i,
                                  FmcDpM2c4_i, FmcDpM2c3_i, FmcDpM2c2_i, FmcDpM2c1_i, FmcDpM2c0_i}),
    .OeSi57x_oe                 (OeSi57x_oe),
    .Si57xClk_ik                (Si57xClk_ik),
    .ClkFb_ikb4                 (ClkFb_ikb4),
    .ClkFb_okb4                 (ClkFbOutput_kb4),
    .Clk20VCOx_ik               (Clk20VCOx_ik),
`ifdef ENABLE_WR
    .PllDac20Sync_o             (PllDac20Sync_o),
    .PllDac25Sync_o             (PllDac25Sync_o),
    .PllDacSclk_ok              (PllDacSclk_ok),
    .PllDacDin_o                (PllDacDin_o),
`endif
    .PllRefSda_ioz              (PllRefSda_ioz),
    .PllRefScl_iokz             (PllRefScl_iokz),
    .PllRefInt_i                (PllRefInt_i),
    .PllSourceMuxOut_ok         (PllSourceMuxOut_ok),
    .PllRefClkOut_ik            (PllRefClkOut_ik),
    .GbitTrxClkRefR_ik          (GbitTrxClkRefR_ik),
    .DipSw_ib8                  (DipSw_ib8),
    .P2DataP_iob20              ({P2DataP19_io, P2DataP18_io, P2DataP17_io, P2DataP16_io, P2DataP15_io,
                                  P2DataP14_io, P2DataP13_io, P2DataP12_io, P2DataP11_io, P2DataP10_io,
                                  P2DataP9_io,  P2DataP8_io,  P2DataP7_io,  P2DataP6_io,  P2DataP5_io,
                                  P2DataP4_io,  P2DataP3_io,  P2DataP2_io,  P2DataP1_io,  P2DataP0_io}),
    .P2DataN_iob20              ({P2DataN19_io, P2DataN18_io, P2DataN17_io, P2DataN16_io, P2DataN15_io,
                                  P2DataN14_io, P2DataN13_io, P2DataN12_io, P2DataN11_io, P2DataN10_io,
                                  P2DataN9_io,  P2DataN8_io,  P2DataN7_io,  P2DataN6_io,  P2DataN5_io,
                                  P2DataN4_io,  P2DataN3_io,  P2DataN2_io,  P2DataN1_io,  P2DataN0_io}),
    .P0HwHighByte_ib8           (P0HwHighByte_ib8),
    .P0HwLowByte_ib8            (P0HwLowByte_ib8),
    .DaisyChain1Cntrl_o         (DaisyChain1Cntrl_o),
    .DaisyChain2Cntrl_o         (DaisyChain2Cntrl_o),
    .VmeP0BunchClk_ik           (VmeP0BunchClk_ik),
    .VmeP0Tclk_ik               (VmeP0Tclk_ik),
    .GpIo_iob4                  (GpIo_iob4),
    .PushButtonN_in             (PushButtonN_in),
    .WrPromSda_io               (WrPromSda_io),
    .WrPromScl_ok               (WrPromScl_ok),

    // System <=> Application interface
    .SysClk_ok                  (SysClk_k),
    .SysReset_oar               (SysReset_ar),
    .VmeSysReset_iarn           (VmeSysReset_irn),
    .AppRelease_ob32            (AppRelease_b32),
    .AppIdent_ob352             (AppIdent_b352),
    .AppReset_iqr               (AppReset_r),
    .VfmcEnable_oe              (VfmcAppEnable),
    .VfmcEnabled_i              (~VfmcEnable_oen),
    .VadjSelect_ob3             (VadjSelect_b3),
    .WbMasterCyc_i              (Wb1SysToAppCyc),
    .WbMasterStb_i              (Wb1SysToAppStb),
    .WbMasterAdr_ib25           (Wb1SysToAppAdr_b25),
    .WbMasterWr_i               (Wb1SysToAppWr),
    .WbMasterDat_ib32           (Wb1SysToAppDat_b32),
    .WbMasterDat_ob32           (Wb1AppToSysDat_b32),
    .WbMasterAck_o              (Wb1AppToSysAck),
    .WbSlaveCyc_o               (Wb2AppToSysCyc),
    .WbSlaveStb_o               (Wb2AppToSysStb),
    .WbSlaveAdr_ob25            (Wb2AppToSysAdr_b25),
    .WbSlaveWr_o                (Wb2AppToSysWr),
    .WbSlaveDat_ob32            (Wb2AppToSysDat_b32),
    .WbSlaveDat_ib32            (Wb2SysToAppDat_b32),
    .WbSlaveAck_i               (Wb2SysToAppAck),
    .Led_ob8                    (Led_b8),
    .LedStatus_ib8              (LedStatus_b8),
    .BstClk_ik                  (BstClk_k),
    .BstRst_ir                  (BstRst_r),
    .BunchClkFlag_i             (BunchClkFlag),
    .TurnClkFlag_i              (TurnClkFlag),
    .BunchClkFlagDly_i          (BunchClkFlagDly),
    .TurnClkFlagDly_i           (TurnClkFlagDly),
    .BstByteAddress_ib8         (BstByteAddress_b8),
    .BstByteData_ib8            (BstByteData_b8),
    .BstByteStrobe_i            (BstByteStrobe),
    .BstByteError_i             (BstByteError),
    .InterruptRequest_ob24      (InterruptRequest_b24),
    .InterruptAvailable_ib24    (InterruptAvailable_b24),
    .BlmIn_ib8                  (BlmIn_b8),
    .GpIo1DirOut_o              (GpIo1DirOut),
    .GpIo1DirOutStatus_i        (GpIo1DirOutStatus),
    .GpIo2DirOut_o              (GpIo2DirOut),
    .GpIo2DirOutStatus_i        (GpIo2DirOutStatus),
    .GpIo34DirOut_o             (GpIo34DirOut),
    .GpIo34DirOutStatus_i       (GpIo34DirOutStatus),
    .GpIo1EnTerm_o              (GpIo1EnTerm),
    .GpIo1EnTermStatus_i        (GpIo1EnTermStatus),
    .GpIo2EnTerm_o              (GpIo2EnTerm),
    .GpIo2EnTermStatus_i        (GpIo2EnTermStatus),
    .GpIo3EnTerm_o              (GpIo3EnTerm),
    .GpIo3EnTermStatus_i        (GpIo3EnTermStatus),
    .GpIo4EnTerm_o              (GpIo4EnTerm),
    .GpIo4EnTermStatus_i        (GpIo4EnTermStatus),
    .AppSfp1Present_iq          (AppSfpPresent_b4[1]),
    .AppSfp1TxFault_iq          (AppSfpTxFault_b4[1]),
    .AppSfp1Los_iq              (AppSfpLos_b4[1]),
    .AppSfp1TxDisable_o         (AppSfpTxDisable_b4[1]),
    .AppSfp1RateSelect_o        (AppSfpRateSelect_b4[1]),
    .StatusAppSfp1TxDisable_iq  (StatusAppSfpTxDisable_b4[1]),
    .StatusAppSfp1RateSelect_iq (StatusAppSfpRateSelect_b4[1]),
    .AppSfp2Present_iq          (AppSfpPresent_b4[2]),
    .AppSfp2TxFault_iq          (AppSfpTxFault_b4[2]),
    .AppSfp2Los_iq              (AppSfpLos_b4[2]),
    .AppSfp2TxDisable_o         (AppSfpTxDisable_b4[2]),
    .AppSfp2RateSelect_o        (AppSfpRateSelect_b4[2]),
    .StatusAppSfp2TxDisable_iq  (StatusAppSfpTxDisable_b4[2]),
    .StatusAppSfp2RateSelect_iq (StatusAppSfpRateSelect_b4[2]),
    .AppSfp3Present_iq          (AppSfpPresent_b4[3]),
    .AppSfp3TxFault_iq          (AppSfpTxFault_b4[3]),
    .AppSfp3Los_iq              (AppSfpLos_b4[3]),
    .AppSfp3TxDisable_o         (AppSfpTxDisable_b4[3]),
    .AppSfp3RateSelect_o        (AppSfpRateSelect_b4[3]),
    .StatusAppSfp3TxDisable_iq  (StatusAppSfpTxDisable_b4[3]),
    .StatusAppSfp3RateSelect_iq (StatusAppSfpRateSelect_b4[3]),
    .AppSfp4Present_iq          (AppSfpPresent_b4[4]),
    .AppSfp4TxFault_iq          (AppSfpTxFault_b4[4]),
    .AppSfp4Los_iq              (AppSfpLos_b4[4]),
    .AppSfp4TxDisable_o         (AppSfpTxDisable_b4[4]),
    .AppSfp4RateSelect_o        (AppSfpRateSelect_b4[4]),
    .StatusAppSfp4TxDisable_iq  (StatusAppSfpTxDisable_b4[4]),
    .StatusAppSfp4RateSelect_iq (StatusAppSfpRateSelect_b4[4]),
    .BstSfpPresent_iq           (BstSfpPresent),
    .BstSfpTxFault_iq           (BstSfpTxFault),
    .BstSfpLos_iq               (BstSfpLos),
    .BstSfpTxDisable_o          (BstSfpTxDisable),
    .BstSfpRateSelect_o         (BstSfpRateSelect),
    .StatusBstSfpTxDisable_iq   (StatusBstSfpTxDisable),
    .StatusBstSfpRateSelect_iq  (StatusBstSfpRateSelect),
    .EthSfpPresent_iq           (EthSfpPresent),
    .EthSfpTxFault_iq           (EthSfpTxFault),
    .EthSfpLos_iq               (EthSfpLos),
    .EthSfpTxDisable_o          (EthSfpTxDisable),
    .EthSfpRateSelect_o         (EthSfpRateSelect),
    .StatusEthSfpTxDisable_iq   (StatusEthSfpTxDisable),
    .StatusEthSfpRateSelect_iq  (StatusEthSfpRateSelect),
    .CdrLos_iq                  (CdrLos),
    .CdrLol_iq                  (CdrLol),
    .I2cWbCyc_o                 (I2cWbCyc),
    .I2cWbStb_o                 (I2cWbStb),
    .I2cWbWe_o                  (I2cWbWe),
    .I2cWbAdr_ob12              (I2cWbAdr_b12),
    .I2cWbDat_ob8               (I2cWbDatMoSi_b8),
    .I2cWbDat_ib8               (I2cWbDatMiSo_b8),
    .I2cWbAck_i                 (I2cWbAck),
    .OctSeriesControl_ib16      (OctSeriesControl_b16),
    .OctParallelControl_ib16    (OctParallelControl_b16),
    .UniqueId_ib64              (UniqueId_b64),
    .UniqueIdValid_i            (UniqueIdValid),
    .Temp_ib16                  (Temp_b16),
    .TempRead_ip                (TempRead_p),
    .ChipTemp_ib8               (ChipTemp_b8),
    .ChipTempRead_ip            (ChipTempRead_p),
    .Voltage0_ib12              (Voltage0_b12),
    .Voltage1_ib12              (Voltage1_b12),
    .Voltage2_ib12              (Voltage2_b12),
    .Voltage3_ib12              (Voltage3_b12),
    .Voltage4_ib12              (Voltage4_b12),
    .Voltage5_ib12              (Voltage5_b12),
    .Voltage6_ib12              (Voltage6_b12),
    .Voltage7_ib12              (Voltage7_b12),
    .VoltageOk_ib8              (VoltageOk_b8),
    .VoltageRead_ip             (VoltageRead_p),
    .VmeGa_inb5                 (VmeGa_nb5),
    .VmeGap_in                  (VmeGap_n));

endmodule
