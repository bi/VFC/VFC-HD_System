//----------------------------------------------------------------------
// Title      : VFC-HD System Part
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : VfcHdSystem.sv
// Author     : -
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// System specific functionality for the VFC-HD
//----------------------------------------------------------------------

`timescale 1ns/100ps

`include "VfcHdConfig.vh"

module VfcHdSystem # (
    parameter g_ClkFrequency,
    parameter g_Synthesis             = 1'b1,
    parameter g_IsGoldenImage         = 1'b0,
    parameter g_SystemReleaseMajor_b8 = 8'd02,
              g_SystemReleaseMinor_b8 = 8'd06,
              g_SystemReleaseTiny_b8  = 8'd02
) (
    //------------------------------------------------------------------
    // External ports
    //------------------------------------------------------------------

    // VME interface
    input              VmeAs_in,
    input      [  5:0] VmeAm_ib6,
    inout      [ 31:1] VmeA_iob31,
    inout              VmeLWord_ion,
    output             VmeAOe_oen,
    output             VmeADir_o,
    input      [  1:0] VmeDs_inb2,
    input              VmeWrite_in,
    inout      [ 31:0] VmeD_iob32,
    output             VmeDOe_oen,
    output             VmeDDir_o,
    output             VmeDtAckOe_o,
    output     [  7:1] VmeIrq_ob7,
    input              VmeIack_in,
    input              VmeIackIn_in,
    output             VmeIackOut_on,
    input              VmeSysClk_ik,
    input              VmeSysReset_irn,

    // I2C mux & I/O expanders
    inout              I2cMuxSda_ioz,
    inout              I2cMuxScl_iokz,
    input              I2CIoExpIntApp12_ian,
    input              I2CIoExpIntApp34_ian,
    input              I2CIoExpIntBstEth_ian,
    input              I2CIoExpIntBlmIn_ian,
    input              I2CIoExpIntLos_ian,

    // BST interface
    input              BstDataIn_i,
    input              CdrClkOut_ik,
    input              CdrDataOut_i,

    // ADC voltage monitoring
    input              VAdcDout_i,
    output             VAdcDin_o,
    output             VAdcCs_o,
    output             VAdcSclk_ok,

    // Clocks
    input              Clk20VCOx_ik,    // Used for the EEPROM interface and temp sensor

    // FMC Vadj control
    output             VadjCs_o,
    output             VadjSclk_ok,
    output             VadjDin_o,

    // PCB revision resistor network
    input      [  7:0] PcbRev_ib7,      // TODO: expose via a register

    // Specials
    inout              TempIdDq_ioz,
    output             ResetFpgaConfigN_orn,

    //------------------------------------------------------------------
    // System <=> Application interface
    //------------------------------------------------------------------

    // Clock & reset
    input              SysClk_ik,
    input              SysReset_iar,

    output reg         AppReset_oqr,

    // Application release ID and info string
    input      [ 31:0] AppRelease_ib32,
    input      [351:0] AppIdent_ib352,

    // Vadj control
    output             VfmcEnable_oen,
    input              VfmcAppEnable_i,
    input      [  2:0] VadjSelect_ib3,

    // WishBone interface
    output             WbMasterCyc_oq,
    output             WbMasterStb_oq,
    output     [ 24:0] WbMasterAdr_oqb25,
    output             WbMasterWr_oq,
    output     [ 31:0] WbMasterDat_oqb32,
    input      [ 31:0] WbMasterDat_ib32,
    input              WbMasterAck_i,
    input              WbSlaveCyc_i,
    input              WbSlaveStb_i,
    input      [ 24:0] WbSlaveAdr_ib25,
    input              WbSlaveWr_i,
    input      [ 31:0] WbSlaveDat_ib32,
    output     [ 31:0] WbSlaveDat_ob32,
    output             WbSlaveAck_o,

    // LEDs
    input      [  8:1] Led_ib8,
    output     [  8:1] LedStatus_ob8,

    // BST interface
    output             BstClk_ok,
    output             BstRst_or,
    output             BunchClkFlag_o,
    output             TurnClkFlag_o,
    output             BunchClkFlagDly_o,
    output             TurnClkFlagDly_o,
    output     [  7:0] BstByteAddress_ob8,
    output     [  7:0] BstByteData_ob8,
    output             BstByteStrobe_o,
    output             BstByteError_o,

    // Interrupt request
    input      [ 23:0] InterruptRequest_ib24,
    output     [ 23:0] InterruptAvailable_ob24,

    // GPIO direction (1=Output, 0=Input)
    input              GpIo1DirOut_i,
    output             GpIo1DirOutStatus_o,
    input              GpIo2DirOut_i,
    output             GpIo2DirOutStatus_o,
    input              GpIo34DirOut_i,
    output             GpIo34DirOutStatus_o,

    // GPIO termination (1=Enable, 0=Disable)
    input              GpIo1EnTerm_i,
    output             GpIo1EnTermStatus_o,
    input              GpIo2EnTerm_i,
    output             GpIo2EnTermStatus_o,
    input              GpIo3EnTerm_i,
    output             GpIo3EnTermStatus_o,
    input              GpIo4EnTerm_i,
    output             GpIo4EnTermStatus_o,

    // BLMIn P0 value
    output     [  7:0] BlmIn_ob8,

    // SFP control
    output             AppSfp1Present_oq,
    output             AppSfp1TxFault_oq,
    output             AppSfp1Los_oq,
    input              AppSfp1TxDisable_i,
    input              AppSfp1RateSelect_i,
    output             StatusAppSfp1TxDisable_oq,
    output             StatusAppSfp1RateSelect_oq,

    output             AppSfp2Present_oq,
    output             AppSfp2TxFault_oq,
    output             AppSfp2Los_oq,
    input              AppSfp2TxDisable_i,
    input              AppSfp2RateSelect_i,
    output             StatusAppSfp2TxDisable_oq,
    output             StatusAppSfp2RateSelect_oq,

    output             AppSfp3Present_oq,
    output             AppSfp3TxFault_oq,
    output             AppSfp3Los_oq,
    input              AppSfp3TxDisable_i,
    input              AppSfp3RateSelect_i,
    output             StatusAppSfp3TxDisable_oq,
    output             StatusAppSfp3RateSelect_oq,

    output             AppSfp4Present_oq,
    output             AppSfp4TxFault_oq,
    output             AppSfp4Los_oq,
    input              AppSfp4TxDisable_i,
    input              AppSfp4RateSelect_i,
    output             StatusAppSfp4TxDisable_oq,
    output             StatusAppSfp4RateSelect_oq,

    output             BstSfpPresent_oq,
    output             BstSfpTxFault_oq,
    output             BstSfpLos_oq,
    input              BstSfpTxDisable_i,
    input              BstSfpRateSelect_i,
    output             StatusBstSfpTxDisable_oq,
    output             StatusBstSfpRateSelect_oq,

    output             EthSfpPresent_oq,
    output             EthSfpTxFault_oq,
    output             EthSfpLos_oq,
    input              EthSfpTxDisable_i,
    input              EthSfpRateSelect_i,
    output             StatusEthSfpTxDisable_oq,
    output             StatusEthSfpRateSelect_oq,

    // CDR control
    output             CdrLos_oq,
    output             CdrLol_oq,

    // WishBone interface for the I2C slaves on the muxes
    input              I2cWbCyc_i,
    input              I2cWbStb_i,
    input              I2cWbWe_i,
    input      [ 11:0] I2cWbAdr_ib12,
    input      [  7:0] I2cWbDat_ib8,
    output     [  7:0] I2cWbDat_ob8,
    output             I2cWbAck_o,

    // Diagnostics
    output     [ 63:0] UniqueId_ob64,
    output             UniqueIdValid_o,
    output     [ 15:0] Temp_ob16,
    output             TempRead_op,
    output     [  7:0] ChipTemp_ob8,
    output             ChipTempRead_op,
    output     [ 11:0] Voltage0_ob12,   // PRE_VADJ
    output     [ 11:0] Voltage1_ob12,   // VCCPD_ADJ
    output     [ 11:0] Voltage2_ob12,   // VADJ
    output     [ 11:0] Voltage3_ob12,   // VIO_B_M2C
    output     [ 11:0] Voltage4_ob12,   // V3P3_FPGA
    output     [ 11:0] Voltage5_ob12,   // V3P3_FMC
    output     [ 11:0] Voltage6_ob12,   // V2P5
    output     [ 11:0] Voltage7_ob12,   // V12P0_FMC
    output     [  7:0] VoltageOk_ob8,
    output             VoltageRead_op,

    // VME geographical address read from the I2C I/O expander
    output     [  4:0] VmeGa_onb5,
    output             VmeGap_on
);

`include "VadjConfig.vh"

//----------------------------------------------------------------------
// Local signals
//----------------------------------------------------------------------

wire        Clk_k;

// All resets initialised high for power-on reset
reg  [ 1:0] SysReset_rx2       = 2'b11;
reg  [ 7:0] ResetCounter_c8    = 8'hFF;
reg         Reset_rq           = 1'b1;
reg         ResetEeprom_r      = 1'b1;
reg         ResetEeprom_x      = 1'b1;

wire        VmeLWord_n;
wire        VmeDtAck_n;
wire [31:1] VmeA_b31;
wire [31:0] VmeD_b32;

wire [ 7:1] VmeIrq_nb7;

wire        WbCyc, WbWe;
wire [20:0] WbAdr_b21;
wire [31:0] WbDatMoSi_b32;
wire        WbMasterCyc, WbMasterStb, WbMasterWe, WbMasterAck;
wire [21:0] WbMasterAdr_b22;
wire [31:0] WbMasterDatMiSo_b32, WbMasterDatMoSi_b32;

wire        WbAckSysAppIdReg, WbStbSysAppIdReg;
wire [31:0] WbDatSysAppIdReg_b32;
wire        WbAckIntManager, WbStbIntManager;
wire [31:0] WbDatIntManager_b32;
wire        WbAckUniqueIdReader, WbStbUniqueIdReader;
wire [31:0] WbDatUniqueIdReader_b32;
wire        WbStbI2cIoExpAndMux, WbAckI2cIoExpAndMux;
wire [31:0] WbDatI2cIoExpAndMux_b32;
wire        WbStbVadjCtrl, WbAckVadjCtrl;
wire [31:0] WbDatVadjCtrl_b32;
wire        WbStbSfpStatus, WbAckSfpStatus;
wire [31:0] WbDatSfpStatus_b32;
wire        WbAckVoltageMon, WbStbVoltageMon;
wire [31:0] WbDatVoltageMon_b32;
wire        WbAckBstDecoder, WbStbBstDecoder;
wire [31:0] WbDatBstDecoder_b32;
wire        WbStbVfcConf, WbAckVfcConf;
wire [31:0] WbDatVfcConf_b32;

wire        IntReq, IntAck;
wire [ 2:0] IntLevel_b3;
wire [ 7:0] IntVector_b8;
wire [31:0] IntRequest_b32;
wire [31:0] IntAvailable_b32;

wire        SpiClk_k, SpiMoSi;
wire [31:0] SpiMiSo_b32, SpiSs_nb32;

wire        IoExpWrReq, IoExpWrOn, IoExpRdReq, IoExpRdOn;
wire [ 2:0] IoExpAddr_b3;
wire [ 1:0] IoExpRegAddr_b2;
wire [ 7:0] IoExpData_b8;
wire        I2cSlaveWrReq, I2cSlaveWrOn, I2cSlaveRdReq, I2cSlaveRdOn, I2cMuxAddress;
wire [ 1:0] I2cMuxChannel_b2;
wire [ 6:0] I2cSlaveAddr_b7;
wire [ 7:0] I2cSlaveRegAddr_b8, I2cSlaveByte_b8;
wire        I2cExpMuxMstrBusy, I2cExpMuxMstrDav, I2cExpMuxMstrAckError;
wire [ 7:0] I2cExpMuxMstrData_b8;

wire        I2CMuxAndExpInitDone;
wire [23:0] VadjLim_b24, VccPdLim_b24, VioBLim_b24;

//----------------------------------------------------------------------
// Unused pins
//----------------------------------------------------------------------

assign ResetFpgaConfigN_orn = 1'b1;

//----------------------------------------------------------------------
// Clocks & reset
//----------------------------------------------------------------------

assign Clk_k = SysClk_ik;

// Synchronise reset
always @(posedge Clk_k) SysReset_rx2 <= #1 {SysReset_rx2[0], SysReset_iar};

// Generate a local reset for 256 clock cycles
always @(posedge Clk_k) begin
    if (SysReset_rx2[1]) begin
        ResetCounter_c8 <= #1 8'hFF;
    end else begin
        if (|ResetCounter_c8) begin
            ResetCounter_c8 <= #1 ResetCounter_c8 - 8'h1;
        end
    end
end

// System reset
always @(posedge Clk_k) Reset_rq <= #1 |ResetCounter_c8;

// VFC reconfiguration reset
always @(posedge Clk20VCOx_ik) {ResetEeprom_r, ResetEeprom_x} <= #1 {ResetEeprom_x, |ResetCounter_c8};

// Application reset
always @(posedge Clk_k) AppReset_oqr <= #1 Reset_rq;

// Assertion to warn about the removal of the BOOT_ROM
`ifdef BOOT_ROM
$error("BOOT_ROM is no longer available in the VFC-HD_System. It should now be instantiated your Application on the WbSlave port.");
`endif

//----------------------------------------------------------------------
// VME interface
//----------------------------------------------------------------------

wire WbMasterAckVme, WbMasterStbVme, WbMasterCycVme, WbMasterWeVme;
wire [31:0] WbMasterDatMiSoVme_b32;
wire [ 9:0] WbMasterAdrVmeUnconnected_b10 ;
wire [21:0] WbMasterAdrVme_b22;
wire [31:0] WbMasterDatMoSiVme_b32;

vme64x_core_verilog #(
    .g_ASYNC_DTACK      (0),
    .g_CLOCK_PERIOD     (int'(1e9 / g_ClkFrequency)),
    .g_DECODE_AM        (0),
    .g_ENABLE_CR_CSR    (0),
    .g_WB_GRANULARITY   ("WORD"),
    .g_MANUFACTURER_ID  (24'h080030), // CERN
    .g_BOARD_ID         (32'd423),    // VFC-HD (https://boardid.web.cern.ch/boardid/script/singleDev.php?id=423)
    .g_REVISION_ID      ({8'b0,
                          g_SystemReleaseMajor_b8[7:0],
                          g_SystemReleaseMinor_b8[7:0],
                          g_SystemReleaseTiny_b8[7:0]}),
    .g_PROGRAM_ID       (8'h02),
    .g_DECODER_0_ADEM   (32'hFF00_0000),
    .g_DECODER_0_AMCAP  (64'h0000_0000_0000_0B00),
    .g_DECODER_1_ADEM   (32'h0000_0000),
    .g_DECODER_1_AMCAP  (64'h0000_0000_0000_0000))
i_VmeInterfaceWb (
    .clk_i              (Clk_k),
    .rst_n_i            (I2CMuxAndExpInitDone),
    .rst_n_o            (),
    .vme_as_n_i         (VmeAs_in),
    .vme_rst_n_i        (VmeSysReset_irn),
    .vme_write_n_i      (VmeWrite_in),
    .vme_am_i           (VmeAm_ib6),
    .vme_ds_n_i         (VmeDs_inb2),
    .vme_ga_i           ({VmeGap_on, VmeGa_onb5}),
    .vme_lword_n_i      (VmeLWord_ion),
    .vme_data_i         (VmeD_iob32),
    .vme_addr_i         (VmeA_iob31),
    .vme_iack_n_i       (VmeIack_in),
    .vme_iackin_n_i     (VmeIackIn_in),
    .vme_iackout_n_o    (VmeIackOut_on),
    .vme_dtack_n_o      (VmeDtAck_n),
    .vme_dtack_oe_o     (),
    .vme_lword_n_o      (VmeLWord_n),
    .vme_data_o         (VmeD_b32),
    .vme_data_dir_o     (VmeDDir_o),
    .vme_data_oe_n_o    (VmeDOe_oen),
    .vme_addr_o         (VmeA_b31),
    .vme_addr_dir_o     (VmeADir_o),
    .vme_addr_oe_n_o    (VmeAOe_oen),
    .vme_retry_n_o      (),
    .vme_retry_oe_o     (),
    .vme_berr_n_o       (),
    .vme_irq_n_o        (VmeIrq_nb7),
    .wb_ack_i           (WbMasterAckVme),
    .wb_err_i           (1'b0),
    .wb_rty_i           (1'b0),
    .wb_stall_i         (!WbMasterAckVme && WbMasterStbVme && WbMasterCycVme),
    .wb_dat_i           (WbMasterDatMiSoVme_b32),
    .wb_cyc_o           (WbMasterCycVme),
    .wb_stb_o           (WbMasterStbVme),
    .wb_adr_o           ({WbMasterAdrVmeUnconnected_b10, WbMasterAdrVme_b22}),
    .wb_sel_o           (),
    .wb_we_o            (WbMasterWeVme),
    .wb_dat_o           (WbMasterDatMoSiVme_b32),
    .int_i              (IntReq),
    .irq_ack_o          (IntAck),
    .irq_level_i        (IntLevel_b3),
    .irq_vector_i       (IntVector_b8),
    .user_csr_addr_o    (),
    .user_csr_data_i    (8'h0),
    .user_csr_data_o    (),
    .user_csr_we_o      (),
    .user_cr_addr_o     (),
    .user_cr_data_i     (8'h0)
);

assign VmeLWord_ion = (VmeADir_o) ? VmeLWord_n :  1'bZ;
assign VmeA_iob31   = (VmeADir_o) ? VmeA_b31   : 31'hZZZZ_ZZZZ;
assign VmeD_iob32   = (VmeDDir_o) ? VmeD_b32   : 32'hZZZZ_ZZZZ;
assign VmeIrq_ob7   = ~VmeIrq_nb7;
assign VmeDtAckOe_o = ~VmeDtAck_n;

// WB Arbiter between the VME interface and the Application
 WbSysArbiter2M1S #( 
    .g_DataWidth (32), 
    .g_AddrWidth (22)
) i_WbSysArbiter2M1S (  
    .Clk_ik     (Clk_k),

    // Input WB from VME interface
    .CycM1_i    (WbMasterCycVme),
    .StbM1_i    (WbMasterStbVme),
    .AddrM1_ib  (WbMasterAdrVme_b22),
    .WeM1_i     (WbMasterWeVme),
    .DataM1_ib  (WbMasterDatMoSiVme_b32),
    .DataM1_ob  (WbMasterDatMiSoVme_b32),
    .AckM1_o    (WbMasterAckVme),

    // Input WB from Application
    .CycM2_i    (WbSlaveCyc_i),
    .StbM2_i    (WbSlaveStb_i),
    .AddrM2_ib  (WbSlaveAdr_ib25[21:0]),
    .WeM2_i     (WbSlaveWr_i),
    .DataM2_ib  (WbSlaveDat_ib32),
    .DataM2_ob  (WbSlaveDat_ob32),
    .AckM2_o    (WbSlaveAck_o),

    // Output WB to address decoder
    .CycS_o     (WbMasterCyc),
    .StbS_o     (WbMasterStb),
    .AddrS_ob   (WbMasterAdr_b22),
    .WeS_o      (WbMasterWe),
    .DataS_ob   (WbMasterDatMoSi_b32),
    .DataS_ib   (WbMasterDatMiSo_b32),
    .AckS_i     (WbMasterAck)); 

//----------------------------------------------------------------------
// Wishbone address decoder
//----------------------------------------------------------------------

wire        SelApp = WbMasterAdr_b22[21];
wire [31:0] WbDatSys_b32;
wire        WbAckSys;

AddrDecoderWbSys i_AddrDecoderWbSys (
    .Clk_ik                 (Clk_k),

    // WB in
    .Cyc_i                  (WbMasterCyc),
    .Stb_i                  (!SelApp && WbMasterStb),
    .Adr_ib21               (WbMasterAdr_b22[20:0]),
    .We_i                   (WbMasterWe),
    .DatMoSi_ib32           (WbMasterDatMoSi_b32),
    .DatMiSo_oqb32          (WbDatSys_b32),
    .Ack_oq                 (WbAckSys),

    // WB out
    .Cyc_oq                 (WbCyc),
    .Adr_oqb21              (WbAdr_b21),
    .We_oq                  (WbWe),
    .DatMoSi_oqb32          (WbDatMoSi_b32),
    
    .DatSysAppIdReg_ib32    (WbDatSysAppIdReg_b32),
    .AckSysAppIdReg_i       (WbAckSysAppIdReg),
    .StbSysAppIdReg_oq      (WbStbSysAppIdReg),

    .DatIntManager_ib32     (WbDatIntManager_b32),
    .AckIntManager_i        (WbAckIntManager),
    .StbIntManager_oq       (WbStbIntManager),

    .DatUniqueIdReader_ib32 (WbDatUniqueIdReader_b32),
    .AckUniqueIdReader_i    (WbAckUniqueIdReader),
    .StbUniqueIdReader_oq   (WbStbUniqueIdReader),

    .DatI2cIoExpAndMux_ib32 (WbDatI2cIoExpAndMux_b32),
    .AckI2cIoExpAndMux_i    (WbAckI2cIoExpAndMux),
    .StbI2cIoExpAndMux_oq   (WbStbI2cIoExpAndMux),

    .DatVadjCtrl_ib32       (WbDatVadjCtrl_b32),
    .AckVadjCtrl_i          (WbAckVadjCtrl),
    .StbVadjCtrl_oq         (WbStbVadjCtrl),

    .DatSfpStatus_ib32      (WbDatSfpStatus_b32),
    .AckSfpStatus_i         (WbAckSfpStatus),
    .StbSfpStatus_oq        (WbStbSfpStatus),

    .DatVoltageMon_ib32     (WbDatVoltageMon_b32),
    .AckVoltageMon_i        (WbAckVoltageMon),
    .StbVoltageMon_oq       (WbStbVoltageMon),

    .DatBstDecoder_ib32     (WbDatBstDecoder_b32),
    .AckBstDecoder_i        (WbAckBstDecoder),
    .StbBstDecoder_oq       (WbStbBstDecoder),

    .DatVfcConf_ib32        (WbDatVfcConf_b32),
    .AckVfcConf_i           (WbAckVfcConf),
    .StbVfcConf_oq          (WbStbVfcConf));

//----------------------------------------------------------------------
// System <=> Application Wishbone interface
//----------------------------------------------------------------------

assign WbMasterCyc_oq       = WbMasterCyc;
assign WbMasterDat_oqb32    = WbMasterDatMoSi_b32;
assign WbMasterAdr_oqb25    = {4'b0, WbMasterAdr_b22[20:0]}; // Only 21 bits are available
assign WbMasterWr_oq        = WbMasterWe;
assign WbMasterStb_oq       = SelApp && WbMasterStb;

assign WbMasterDatMiSo_b32  = (SelApp) ? WbMasterDat_ib32 : WbDatSys_b32;
assign WbMasterAck          = (SelApp) ? WbMasterAck_i    : WbAckSys;

//----------------------------------------------------------------------
// System/Application ID registers
//----------------------------------------------------------------------

SysAppIdRegs i_SysAppIdReg (
    .Clk_ik          (Clk_k),
    .Rst_irq         (Reset_rq),
    .Cyc_i           (WbCyc),
    .Stb_i           (WbStbSysAppIdReg),
    .Adr_ib4         (WbAdr_b21[3:0]),
    .Dat_oab32       (WbDatSysAppIdReg_b32),
    .Ack_oa          (WbAckSysAppIdReg),
    .SysRelease_ib32 ({8'b0, g_SystemReleaseMajor_b8, g_SystemReleaseMinor_b8, g_SystemReleaseTiny_b8}),
    .SysIdent_ib96   ({"VFC-HD", 48'h0}),
    .AppRelease_ib32 (AppRelease_ib32),
    .AppIdent_ib352  (AppIdent_ib352));

//----------------------------------------------------------------------
// Interrupt manager
//----------------------------------------------------------------------

assign IntRequest_b32[31:24]   = 8'b0;  // Interrupts 31..24 are reserved for the System
assign IntRequest_b32[23: 0]   = InterruptRequest_ib24;
assign InterruptAvailable_ob24 = IntAvailable_b32[23:0];

InterruptManagerWb i_InterruptManagerWb (
    .Rst_irq              (Reset_rq),
    .Clk_ik               (Clk_k),
    .Cyc_i                (WbCyc),
    .Stb_i                (WbStbIntManager),
    .We_i                 (WbWe),
    .Adr_ib3              (WbAdr_b21[2:0]),
    .Dat_ib32             (WbDatMoSi_b32),
    .Dat_oab32            (WbDatIntManager_b32),
    .Ack_oa               (WbAckIntManager),
    .IntRequest_ib32      (IntRequest_b32),
    .IntAvailable_ob32    (IntAvailable_b32),
    .IntLevel_ob3         (IntLevel_b3),
    .IntVector_ob8        (IntVector_b8),
    .IntReq_o             (IntReq),
    .IntAck_i             (IntAck));

//----------------------------------------------------------------------
// VFC reconfiguration
//----------------------------------------------------------------------

VfcConfiguration #(
    .g_Synthesis           (g_Synthesis),
    .AUTO_APPLICATION_LOAD (g_IsGoldenImage),
    .SETUP_LOAD_ONBOOT     (g_IsGoldenImage))
i_VfcConfiguration (
    .ClkEeprom_ik          (Clk20VCOx_ik),
    .RstEeprom_ir          (ResetEeprom_r),
    .Clk_ik                (Clk_k),
    .Cyc_i                 (WbCyc),
    .Stb_i                 (WbStbVfcConf),
    .We_i                  (WbWe),
    .Adr_ib15              (WbAdr_b21[14:0]),
    .Dat_ib32              (WbDatMoSi_b32),
    .Dat_oab32             (WbDatVfcConf_b32),
    .Ack_oa                (WbAckVfcConf));

//----------------------------------------------------------------------
// Voltage monitoring ADC
//----------------------------------------------------------------------

VoltageMonitoring #(
    .g_ClkFrequency   (g_ClkFrequency))
i_VoltageMonitoring (
    .Clk_ik           (Clk_k),
    .Rst_irq          (Reset_rq),

    .Cyc_i            (WbCyc),
    .Stb_i            (WbStbVoltageMon),
    .Adr_ib3          (WbAdr_b21[2:0]),
    .Dat_oab32        (WbDatVoltageMon_b32),
    .Ack_oa           (WbAckVoltageMon),

    .Voltage0_oqb12   (Voltage0_ob12),                      // PRE_VADJ
    .Voltage1_oqb12   (Voltage1_ob12),                      // VCCPD_ADJ
    .Voltage2_oqb12   (Voltage2_ob12),                      // VADJ
    .Voltage3_oqb12   (Voltage3_ob12),                      // VIO_B_M2C
    .Voltage4_oqb12   (Voltage4_ob12),                      // V3P3_FPGA
    .Voltage5_oqb12   (Voltage5_ob12),                      // V3P3_FMC
    .Voltage6_oqb12   (Voltage6_ob12),                      // V2P5
    .Voltage7_oqb12   (Voltage7_ob12),                      // V12P0_FMC

    .VoltageOk_ob8    (VoltageOk_ob8),

    .Limits0_ib24     (VadjLim_b24),                        // PRE_VADJ
    .Limits1_ib24     (VccPdLim_b24),                       // VCCPD_ADJ
    .Limits2_ib24     (VadjLim_b24),                        // VADJ
    .Limits3_ib24     (VioBLim_b24),                        // VIO_B_M2C
    .Limits4_ib24     ({c_Vmon_V3P30_Hi, c_Vmon_V3P30_Lo}), // V3P3_FPGA
    .Limits5_ib24     ({c_Vmon_V3P30_Hi, c_Vmon_V3P30_Lo}), // V3P3_FMC
    .Limits6_ib24     ({c_Vmon_V2P50_Hi, c_Vmon_V2P50_Lo}), // V2P5
    .Limits7_ib24     ({c_Vmon_V12P0_Hi, c_Vmon_V12P0_Lo}), // V12P0_FMC

    .MeasDone_o       (VoltageRead_op),

    .SClk_o           (VAdcSclk_ok),
    .Cs_on            (VAdcCs_o),
    .Miso_i           (VAdcDout_i),
    .Mosi_o           (VAdcDin_o));

//----------------------------------------------------------------------
// Vadj control
//----------------------------------------------------------------------

VadjControl #(
    .g_ClkFrequency   (g_ClkFrequency))
i_VadjControl (
    .Clk_ik           (Clk_k),
    .Rst_irq          (Reset_rq),

    .Cyc_i            (WbCyc),
    .Stb_i            (WbStbVadjCtrl),
    .We_i             (WbWe),
    .Adr_ib2          (WbAdr_b21[1:0]),
    .Dat_ib32         (WbDatMoSi_b32),
    .Dat_oab32        (WbDatVadjCtrl_b32),
    .Ack_oa           (WbAckVadjCtrl),

    .VfmcEnable_oen   (VfmcEnable_oen),
    .VfmcAppEnable_i  (VfmcAppEnable_i),

    .VadjSelect_ib3   (VadjSelect_ib3),
    .VadjVoltage_ib12 (Voltage0_ob12),
    .VadjMeasDone_i   (VoltageRead_op),

    .VadjLim_ob24     (VadjLim_b24),
    .VccPdLim_ob24    (VccPdLim_b24),
    .VioBLim_ob24     (VioBLim_b24),

    .SClk_o           (VadjSclk_ok),
    .Cs_on            (VadjCs_o),
    .Mosi_o           (VadjDin_o));

//----------------------------------------------------------------------
// Unique-ID & temp. sensor
//----------------------------------------------------------------------

UniqueIdTemp #(
    .g_ClkFrequency   (g_ClkFrequency))
i_UniqueIdTemp (
    .Clk20M_ik        (Clk20VCOx_ik),
    .Clk_ik           (Clk_k),
    .Rst_irq          (Reset_rq),
    .Cyc_i            (WbCyc),
    .Stb_i            (WbStbUniqueIdReader),
    .Adr_ib2          (WbAdr_b21[1:0]),
    .Dat_oab32        (WbDatUniqueIdReader_b32),
    .Ack_oa           (WbAckUniqueIdReader),
    .Uid_ob64         (UniqueId_ob64),
    .UidValid_o       (UniqueIdValid_o),
    .Temp_ob16        (Temp_ob16),
    .TempRead_o       (TempRead_op),
    .ChipTemp_ob8     (ChipTemp_ob8),
    .ChipTempRead_o   (ChipTempRead_op),
    .OneWireBus_io    (TempIdDq_ioz));

//----------------------------------------------------------------------
// I/O Expander & multiplexer I2C master
//----------------------------------------------------------------------

I2cExpAndMuxReqArbiter i_I2cExpAndMuxReqArbiter (
    .Clk_ik                     (Clk_k),
    .Rst_irq                    (Reset_rq),
    .IoExpWrReq_oq              (IoExpWrReq),
    .IoExpWrOn_i                (IoExpWrOn),
    .IoExpRdReq_oq              (IoExpRdReq),
    .IoExpRdOn_i                (IoExpRdOn),
    .IoExpAddr_oqb3             (IoExpAddr_b3),
    .IoExpRegAddr_oqb2          (IoExpRegAddr_b2),
    .IoExpData_oqb8             (IoExpData_b8),
    .I2cSlaveWrReq_oq           (I2cSlaveWrReq),
    .I2cSlaveWrOn_i             (I2cSlaveWrOn),
    .I2cSlaveRdReq_oq           (I2cSlaveRdReq),
    .I2cSlaveRdOn_i             (I2cSlaveRdOn),
    .I2cMuxAddress_oq           (I2cMuxAddress),
    .I2cMuxChannel_oqb2         (I2cMuxChannel_b2),
    .I2cSlaveAddr_oqb7          (I2cSlaveAddr_b7),
    .I2cSlaveRegAddr_oqb8       (I2cSlaveRegAddr_b8),
    .I2cSlaveByte_oqb8          (I2cSlaveByte_b8),
    .MasterBusy_i               (I2cExpMuxMstrBusy),
    .MasterNewByteRead_ip       (I2cExpMuxMstrDav),
    .MasterByteOut_ib8          (I2cExpMuxMstrData_b8),
    .MasterAckError_i           (I2cExpMuxMstrAckError),
    .IoExpApp12Int_ian          (I2CIoExpIntApp12_ian),
    .IoExpApp34Int_ian          (I2CIoExpIntApp34_ian),
    .IoExpBstEthInt_ian         (I2CIoExpIntBstEth_ian),
    .IoExpLosInt_ian            (I2CIoExpIntLos_ian),
    .IoExpBlmInInt_ian          (I2CIoExpIntBlmIn_ian),
    .InitDone_oq                (I2CMuxAndExpInitDone),
    .VmeGa_onqb5                (VmeGa_onb5),
    .VmeGaP_onq                 (VmeGap_on),
    .Led_iab8                   (Led_ib8),
    .StatusLed_ob8              (LedStatus_ob8),
    .GpIo1A2B_ia                (GpIo1DirOut_i),
    .EnGpIo1Term_ia             (GpIo1EnTerm_i),
    .GpIo2A2B_ia                (GpIo2DirOut_i),
    .EnGpIo2Term_ia             (GpIo2EnTerm_i),
    .GpIo34A2B_ia               (GpIo34DirOut_i),
    .EnGpIo3Term_ia             (GpIo3EnTerm_i),
    .EnGpIo4Term_ia             (GpIo4EnTerm_i),
    .StatusGpIo1A2B_oq          (GpIo1DirOutStatus_o),
    .StatusEnGpIo1Term_oq       (GpIo1EnTermStatus_o),
    .StatusGpIo2A2B_oq          (GpIo2DirOutStatus_o),
    .StatusEnGpIo2Term_oq       (GpIo2EnTermStatus_o),
    .StatusGpIo34A2B_oq         (GpIo34DirOutStatus_o),
    .StatusEnGpIo3Term_oq       (GpIo3EnTermStatus_o),
    .StatusEnGpIo4Term_oq       (GpIo4EnTermStatus_o),
    .BlmIn_oqb8                 (BlmIn_ob8),
    .AppSfp1Present_oq          (AppSfp1Present_oq),
    .AppSfp1TxFault_oq          (AppSfp1TxFault_oq),
    .AppSfp1Los_oq              (AppSfp1Los_oq),
    .AppSfp1TxDisable_ia        (AppSfp1TxDisable_i),
    .AppSfp1RateSelect_ia       (AppSfp1RateSelect_i),
    .StatusAppSfp1TxDisable_oq  (StatusAppSfp1TxDisable_oq),
    .StatusAppSfp1RateSelect_oq (StatusAppSfp1RateSelect_oq),
    .AppSfp2Present_oq          (AppSfp2Present_oq),
    .AppSfp2TxFault_oq          (AppSfp2TxFault_oq),
    .AppSfp2Los_oq              (AppSfp2Los_oq),
    .AppSfp2TxDisable_ia        (AppSfp2TxDisable_i),
    .AppSfp2RateSelect_ia       (AppSfp2RateSelect_i),
    .StatusAppSfp2TxDisable_oq  (StatusAppSfp2TxDisable_oq),
    .StatusAppSfp2RateSelect_oq (StatusAppSfp2RateSelect_oq),
    .AppSfp3Present_oq          (AppSfp3Present_oq),
    .AppSfp3TxFault_oq          (AppSfp3TxFault_oq),
    .AppSfp3Los_oq              (AppSfp3Los_oq),
    .AppSfp3TxDisable_ia        (AppSfp3TxDisable_i),
    .AppSfp3RateSelect_ia       (AppSfp3RateSelect_i),
    .StatusAppSfp3TxDisable_oq  (StatusAppSfp3TxDisable_oq),
    .StatusAppSfp3RateSelect_oq (StatusAppSfp3RateSelect_oq),
    .AppSfp4Present_oq          (AppSfp4Present_oq),
    .AppSfp4TxFault_oq          (AppSfp4TxFault_oq),
    .AppSfp4Los_oq              (AppSfp4Los_oq),
    .AppSfp4TxDisable_ia        (AppSfp4TxDisable_i),
    .AppSfp4RateSelect_ia       (AppSfp4RateSelect_i),
    .StatusAppSfp4TxDisable_oq  (StatusAppSfp4TxDisable_oq),
    .StatusAppSfp4RateSelect_oq (StatusAppSfp4RateSelect_oq),
    .BstSfpPresent_oq           (BstSfpPresent_oq),
    .BstSfpTxFault_oq           (BstSfpTxFault_oq),
    .BstSfpLos_oq               (BstSfpLos_oq),
    .BstSfpTxDisable_ia         (BstSfpTxDisable_i),
    .BstSfpRateSelect_ia        (BstSfpRateSelect_i),
    .StatusBstSfpTxDisable_oq   (StatusBstSfpTxDisable_oq),
    .StatusBstSfpRateSelect_oq  (StatusBstSfpRateSelect_oq),
    .EthSfpPresent_oq           (EthSfpPresent_oq),
    .EthSfpTxFault_oq           (EthSfpTxFault_oq),
    .EthSfpLos_oq               (EthSfpLos_oq),
    .EthSfpTxDisable_ia         (EthSfpTxDisable_i),
    .EthSfpRateSelect_ia        (EthSfpRateSelect_i),
    .StatusEthSfpTxDisable_oq   (StatusEthSfpTxDisable_oq),
    .StatusEthSfpRateSelect_oq  (StatusEthSfpRateSelect_oq),
    .CdrLos_oq                  (CdrLos_oq),
    .CdrLol_oq                  (CdrLol_oq),
    .I2cWbCyc_i                 (I2cWbCyc_i),
    .I2cWbStb_i                 (I2cWbStb_i),
    .I2cWbWe_i                  (I2cWbWe_i),
    .I2cWbAdr_ib12              (I2cWbAdr_ib12),
    .I2cWbDat_ib8               (I2cWbDat_ib8),
    .I2cWbDat_ob8               (I2cWbDat_ob8),
    .I2cWbAck_o                 (I2cWbAck_o),
    .WbCyc_i                    (WbCyc),
    .WbStb_i                    (WbStbI2cIoExpAndMux),
    .WbAdr_ib2                  (WbAdr_b21[1:0]),
    .WbWe_i                     (WbWe),
    .WbDat_ib32                 (WbDatMoSi_b32),
    .WbDat_oqb32                (WbDatI2cIoExpAndMux_b32),
    .WbAck_oa                   (WbAckI2cIoExpAndMux));

I2cExpAndMuxMaster #(
    .g_ClkFrequency      (g_ClkFrequency))
i_I2cExpAndMuxMaster (
    .Clk_ik              (Clk_k),
    .Rst_irq             (Reset_rq),
    .IoExpWrReq_i        (IoExpWrReq),
    .IoExpWrOn_oq        (IoExpWrOn),
    .IoExpRdReq_i        (IoExpRdReq),
    .IoExpRdOn_oq        (IoExpRdOn),
    .IoExpAddr_ib3       (IoExpAddr_b3),
    .IoExpRegAddr_ib2    (IoExpRegAddr_b2),
    .IoExpData_ib8       (IoExpData_b8),
    .I2cSlaveWrReq_i     (I2cSlaveWrReq),
    .I2cSlaveWrOn_o      (I2cSlaveWrOn),
    .I2cSlaveRdReq_i     (I2cSlaveRdReq),
    .I2cSlaveRdOn_o      (I2cSlaveRdOn),
    .I2cMuxAddress_i     (I2cMuxAddress),
    .I2cMuxChannel_ib2   (I2cMuxChannel_b2),
    .I2cSlaveAddr_ib7    (I2cSlaveAddr_b7),
    .I2cSlaveRegAddr_ib8 (I2cSlaveRegAddr_b8),
    .I2cSlaveByte_ib8    (I2cSlaveByte_b8),
    .Busy_o              (I2cExpMuxMstrBusy),
    .NewByteRead_op      (I2cExpMuxMstrDav),
    .ByteOut_ob8         (I2cExpMuxMstrData_b8),
    .AckError_op         (I2cExpMuxMstrAckError),
    .Scl_ioz             (I2cMuxScl_iokz),
    .Sda_ioz             (I2cMuxSda_ioz));

//----------------------------------------------------------------------
// SFP status registers
//----------------------------------------------------------------------

SfpStatusRegs i_SfpStatuses (
    .Clk_ik             (Clk_k),
    .Rst_irq            (Reset_rq),
    .Cyc_i              (WbCyc),
    .Stb_i              (WbStbSfpStatus),
    .Adr_ib3            (WbAdr_b21[2:0]),
    .Dat_oab32          (WbDatSfpStatus_b32),
    .Ack_oa             (WbAckSfpStatus),
    .AppSfp1Status_ib32 ({16'b0, {AppSfp1Present_oq, 3'b0},
                          {2'b0, AppSfp1TxFault_oq,   AppSfp1Los_oq},
                          {2'b0, AppSfp1TxDisable_i,  StatusAppSfp1TxDisable_oq},
                          {2'b0, AppSfp1RateSelect_i, StatusAppSfp1RateSelect_oq}}),
    .AppSfp2Status_ib32 ({16'b0, {AppSfp2Present_oq, 3'b0},
                          {2'b0, AppSfp2TxFault_oq,   AppSfp2Los_oq},
                          {2'b0, AppSfp2TxDisable_i,  StatusAppSfp2TxDisable_oq},
                          {2'b0, AppSfp2RateSelect_i, StatusAppSfp2RateSelect_oq}}),
    .AppSfp3Status_ib32 ({16'b0, {AppSfp3Present_oq, 3'b0},
                          {2'b0, AppSfp3TxFault_oq,   AppSfp3Los_oq},
                          {2'b0, AppSfp3TxDisable_i,  StatusAppSfp3TxDisable_oq},
                          {2'b0, AppSfp3RateSelect_i, StatusAppSfp3RateSelect_oq}}),
    .AppSfp4Status_ib32 ({16'b0, {AppSfp4Present_oq, 3'b0},
                          {2'b0, AppSfp4TxFault_oq,   AppSfp4Los_oq},
                          {2'b0, AppSfp4TxDisable_i,  StatusAppSfp4TxDisable_oq},
                          {2'b0, AppSfp4RateSelect_i, StatusAppSfp4RateSelect_oq}}),
    .EthSfpStatus_ib32  ({16'b0, {EthSfpPresent_oq,  3'b0},
                          {2'b0, EthSfpTxFault_oq,    EthSfpLos_oq},
                          {2'b0, EthSfpTxDisable_i,   StatusEthSfpTxDisable_oq},
                          {2'b0, EthSfpRateSelect_i,  StatusEthSfpRateSelect_oq}}),
    .BstSfpStatus_ib32  ({16'b0, {BstSfpPresent_oq,  3'b0},
                          {2'b0, BstSfpTxFault_oq,    BstSfpLos_oq},
                          {2'b0, BstSfpTxDisable_i,   StatusBstSfpTxDisable_oq},
                          {2'b0, BstSfpRateSelect_i,  StatusBstSfpRateSelect_oq}}));

//----------------------------------------------------------------------
// BST decoder
//----------------------------------------------------------------------

BstDecoder i_BstDecoder (
    .WbRst_ir            (Reset_rq),
    .WbClk_ik            (Clk_k),
    .WbCyc_i             (WbCyc),
    .WbStb_i             (WbStbBstDecoder),
    .WbWe_i              (WbWe),
    .WbAdr_ib9           (WbAdr_b21[8:0]),
    .WbDat_ib32          (WbDatMoSi_b32),
    .WbDat_ob32          (WbDatBstDecoder_b32),
    .WbAck_o             (WbAckBstDecoder),
    .CdrLos_i            (CdrLos_oq),
    .CdrLol_i            (CdrLol_oq),
    .SfpPresent_i        (BstSfpPresent_oq),
    .SfpTxFault_i        (BstSfpTxFault_oq),
    .SfpLos_i            (BstSfpLos_oq),
    .SfpTxDisable_i      (StatusBstSfpTxDisable_oq),
    .SfpRateSelect_i     (StatusBstSfpRateSelect_oq),
    .Reset_iar           (Reset_rq),
    .CdrClk_ik           (CdrClkOut_ik),
    .CdrDat_i            (CdrDataOut_i),
    .BstClk_ok           (BstClk_ok),
    .BstRst_or           (BstRst_or),
    .BunchClkFlag_o      (BunchClkFlag_o),
    .TurnClkFlag_o       (TurnClkFlag_o),
    .BunchClkFlagDly_o   (BunchClkFlagDly_o),
    .TurnClkFlagDly_o    (TurnClkFlagDly_o),
    .BstByteAddress_ob8  (BstByteAddress_ob8),
    .BstByteData_ob8     (BstByteData_ob8),
    .BstByteStrobe_o     (BstByteStrobe_o),
    .BstByteError_o      (BstByteError_o));

endmodule
