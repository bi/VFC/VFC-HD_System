//===================================================================//
//                     VFC-HD VADJ CONFIGURATION                     //
//===================================================================//

// Values for VadjSelect from Application to System
localparam c_VadjSelect_V1P20 = 3'h0,
           c_VadjSelect_V1P25 = 3'h1,
           c_VadjSelect_V1P35 = 3'h2,
           c_VadjSelect_V1P50 = 3'h3,
           c_VadjSelect_V1P80 = 3'h4,
           c_VadjSelect_V2P50 = 3'h5,
           c_VadjSelect_V3P00 = 3'h6,
           c_VadjSelect_V3P30 = 3'h7;

// ADC values expected for each Vadj setting and +/- 5% limits
//   Value = (Vadj/(2*2.5))*4096
localparam c_Vmon_V12P0 = 12'hD79, c_Vmon_V12P0_Hi = 12'hE25, c_Vmon_V12P0_Lo = 12'hCCC;
localparam c_Vmon_V3P30 = 12'hA8F, c_Vmon_V3P30_Hi = 12'hB16, c_Vmon_V3P30_Lo = 12'hA07;
localparam c_Vmon_V3P00 = 12'h99A, c_Vmon_V3P00_Hi = 12'hA14, c_Vmon_V3P00_Lo = 12'h91F;
localparam c_Vmon_V2P50 = 12'h800, c_Vmon_V2P50_Hi = 12'h866, c_Vmon_V2P50_Lo = 12'h799;
localparam c_Vmon_V1P80 = 12'h5C3, c_Vmon_V1P80_Hi = 12'h60C, c_Vmon_V1P80_Lo = 12'h579;
localparam c_Vmon_V1P50 = 12'h4CD, c_Vmon_V1P50_Hi = 12'h50A, c_Vmon_V1P50_Lo = 12'h48F;
localparam c_Vmon_V1P35 = 12'h452, c_Vmon_V1P35_Hi = 12'h489, c_Vmon_V1P35_Lo = 12'h41A;
localparam c_Vmon_V1P25 = 12'h400, c_Vmon_V1P25_Hi = 12'h433, c_Vmon_V1P25_Lo = 12'h3CC;
localparam c_Vmon_V1P20 = 12'h3D7, c_Vmon_V1P20_Hi = 12'h408, c_Vmon_V1P20_Lo = 12'h3A5;