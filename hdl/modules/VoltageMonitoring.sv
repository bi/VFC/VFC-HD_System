//----------------------------------------------------------------------
// Title      : Voltage Monitoring
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : VoltageMonitoring.sv
// Author     : T. Levens
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Readout of the AD7888 voltage monitoring ADC on the VFC-HD.
//
// The Wishbone interface exposes 8 registers of the format:
//
//   +----+----------------+---------+
//   | OF |                | Voltage |
//   +----+----------------+---------+
//     31   30    ..    12   11 .. 0
//
// The OF bit is set when the voltage is outwith +/-5% of the nominal
// value. The voltage is a 12-bit unsigned value which can be converted
// to volts with:
//
//   (Val / 4096) * 2.5 * Coef
//
// Where Coef is a scaling factor based on the resistor divider mounted
// on the PCB (2 for most channels).
//----------------------------------------------------------------------

`timescale 1ns/100ps

module VoltageMonitoring #(
    parameter         g_ClkFrequency,
    parameter         g_RefreshFrequency = 8 // 8 Hz
)(
    input             Clk_ik,
    input             Rst_irq,

    input             Cyc_i,
    input             Stb_i,
    input      [ 2:0] Adr_ib3,
    output reg [31:0] Dat_oab32,
    output reg        Ack_oa,

    output reg [11:0] Voltage0_oqb12,
    output reg [11:0] Voltage1_oqb12,
    output reg [11:0] Voltage2_oqb12,
    output reg [11:0] Voltage3_oqb12,
    output reg [11:0] Voltage4_oqb12,
    output reg [11:0] Voltage5_oqb12,
    output reg [11:0] Voltage6_oqb12,
    output reg [11:0] Voltage7_oqb12,

    output     [ 7:0] VoltageOk_ob8,

    input      [23:0] Limits0_ib24,
    input      [23:0] Limits1_ib24,
    input      [23:0] Limits2_ib24,
    input      [23:0] Limits3_ib24,
    input      [23:0] Limits4_ib24,
    input      [23:0] Limits5_ib24,
    input      [23:0] Limits6_ib24,
    input      [23:0] Limits7_ib24,

    output reg        MeasDone_o,

    output            SClk_o,
    output            Cs_on,
    input             Miso_i,
    output            Mosi_o
);

localparam int c_RefreshDelay = g_ClkFrequency / g_RefreshFrequency;
localparam int c_RefreshBits  = $clog2(c_RefreshDelay+1);

initial begin
    // check parameters
    assert (c_RefreshDelay > 2)
        else $error("The refresh frequency is too high for the given clock frequency!");
end

// Read delay counter
reg [c_RefreshBits-1:0] DelayCounter_c;
reg Start;

always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        DelayCounter_c       <= #1 1'h0;
        Start                <= #1 1'b0;
    end else begin
        if (DelayCounter_c == (c_RefreshBits)'(c_RefreshDelay)) begin
            DelayCounter_c   <= #1 1'h0;
            Start            <= #1 1'b1;
        end else begin
            DelayCounter_c   <= #1 DelayCounter_c + 1'h1;
            Start            <= #1 1'b0;
        end
    end
end

reg  [ 3:0] WordCount_b4;
reg         Go;
wire        Done;

always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        WordCount_b4         <= #1 4'h0;
        Go                   <= #1 1'b0;
        MeasDone_o           <= #1 1'b0;
    end else begin
        Go                   <= #1 1'b0;
        MeasDone_o           <= #1 1'b0;
        if (Start) begin
            WordCount_b4     <= #1 4'h0;
            Go               <= #1 1'b1;
        end else if (Done) begin
            WordCount_b4     <= #1 WordCount_b4 + 4'h1;
            if (WordCount_b4[3])
                MeasDone_o   <= #1 1'b1;
            else
                Go           <= #1 1'b1;
        end
    end
end

// Data input latch
wire [15:0] DataIn_b16;
reg  [ 7:0] DataOvr_b8;

always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        DataOvr_b8     <= #1  8'h00;
        Voltage0_oqb12 <= #1 12'h000;
        Voltage1_oqb12 <= #1 12'h000;
        Voltage2_oqb12 <= #1 12'h000;
        Voltage3_oqb12 <= #1 12'h000;
        Voltage4_oqb12 <= #1 12'h000;
        Voltage5_oqb12 <= #1 12'h000;
        Voltage6_oqb12 <= #1 12'h000;
        Voltage7_oqb12 <= #1 12'h000;
    end else begin
        if (Done) begin
            case (WordCount_b4)
                4'h1: begin
                    Voltage0_oqb12 <= #1 DataIn_b16[11:0];
                    DataOvr_b8[0]  <= #1 (DataIn_b16[11:0] > Limits0_ib24[23:12]) || (DataIn_b16[11:0] < Limits0_ib24[11:0]);
                end
                4'h2: begin
                    Voltage1_oqb12 <= #1 DataIn_b16[11:0];
                    DataOvr_b8[1]  <= #1 (DataIn_b16[11:0] > Limits1_ib24[23:12]) || (DataIn_b16[11:0] < Limits1_ib24[11:0]);
                end
                4'h3: begin
                    Voltage2_oqb12 <= #1 DataIn_b16[11:0];
                    DataOvr_b8[2]  <= #1 (DataIn_b16[11:0] > Limits2_ib24[23:12]) || (DataIn_b16[11:0] < Limits2_ib24[11:0]);
                end
                4'h4: begin
                    Voltage3_oqb12 <= #1 DataIn_b16[11:0];
                    DataOvr_b8[3]  <= #1 (DataIn_b16[11:0] > Limits3_ib24[23:12]) || (DataIn_b16[11:0] < Limits3_ib24[11:0]);
                end
                4'h5: begin
                    Voltage4_oqb12 <= #1 DataIn_b16[11:0];
                    DataOvr_b8[4]  <= #1 (DataIn_b16[11:0] > Limits4_ib24[23:12]) || (DataIn_b16[11:0] < Limits4_ib24[11:0]);
                end
                4'h6: begin
                    Voltage5_oqb12 <= #1 DataIn_b16[11:0];
                    DataOvr_b8[5]  <= #1 (DataIn_b16[11:0] > Limits5_ib24[23:12]) || (DataIn_b16[11:0] < Limits5_ib24[11:0]);
                end
                4'h7: begin
                    Voltage6_oqb12 <= #1 DataIn_b16[11:0];
                    DataOvr_b8[6]  <= #1 (DataIn_b16[11:0] > Limits6_ib24[23:12]) || (DataIn_b16[11:0] < Limits6_ib24[11:0]);
                end
                4'h8: begin
                    Voltage7_oqb12 <= #1 DataIn_b16[11:0];
                    DataOvr_b8[7]  <= #1 (DataIn_b16[11:0] > Limits7_ib24[23:12]) || (DataIn_b16[11:0] < Limits7_ib24[11:0]);
                end
            endcase
        end
    end
end

assign VoltageOk_ob8 = ~DataOvr_b8;

// SPI master
SpiMaster #(
    .g_ClkFrequency(g_ClkFrequency),
    .g_SpiFrequency(1e6), // 1 MHz
    .g_MaxBits     (16),
    .g_Cpol        (1'b1),
    .g_Cpha        (1))
i_SpiMaster (
    .Clk_ik        (Clk_ik),
    .Rst_ir        (Rst_irq),
    .Go_i          (Go),
    .Bits_ib       (5'd16),
    .Data_ib       ({2'b0, WordCount_b4[2:0], 3'b000, 8'h00}),
    .Data_ob       (DataIn_b16),
    .Done_o        (Done),
    .Cs_on         (Cs_on),
    .Sclk_o        (SClk_o),
    .Mosi_o        (Mosi_o),
    .Miso_i        (Miso_i));

// Wishbone read
always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        Dat_oab32 <= #1 32'h0;
        Ack_oa    <= #1  1'b0;
    end else begin
        if (~Ack_oa) begin
            case (Adr_ib3)
                3'b000: Dat_oab32 <= #1 {DataOvr_b8[0], 19'h0, Voltage0_oqb12};
                3'b001: Dat_oab32 <= #1 {DataOvr_b8[1], 19'h0, Voltage1_oqb12};
                3'b010: Dat_oab32 <= #1 {DataOvr_b8[2], 19'h0, Voltage2_oqb12};
                3'b011: Dat_oab32 <= #1 {DataOvr_b8[3], 19'h0, Voltage3_oqb12};
                3'b100: Dat_oab32 <= #1 {DataOvr_b8[4], 19'h0, Voltage4_oqb12};
                3'b101: Dat_oab32 <= #1 {DataOvr_b8[5], 19'h0, Voltage5_oqb12};
                3'b110: Dat_oab32 <= #1 {DataOvr_b8[6], 19'h0, Voltage6_oqb12};
                3'b111: Dat_oab32 <= #1 {DataOvr_b8[7], 19'h0, Voltage7_oqb12};
            endcase
        end
        Ack_oa <= #1 Stb_i && Cyc_i;
    end
end

endmodule
