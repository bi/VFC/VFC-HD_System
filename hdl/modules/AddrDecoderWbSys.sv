//----------------------------------------------------------------------
// Title      : System WB Address Decoder
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : AddrDecoderWbSys.sv
// Author     : A. Boccardi, M. Barros Marin, T. Levens
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// WishBone address decoder & crossbar for the VFC-HD System.
//----------------------------------------------------------------------

`timescale 1ns/100ps

module AddrDecoderWbSys
(
    // WB master connections
    input              Clk_ik,
    input       [20:0] Adr_ib21,
    output  reg [20:0] Adr_oqb21,
    input       [31:0] DatMoSi_ib32,
    output  reg [31:0] DatMoSi_oqb32,
    output  reg [31:0] DatMiSo_oqb32,
    input              Stb_i,
    input              Cyc_i,
    output  reg        Cyc_oq,
    input              We_i,
    output  reg        We_oq,
    output  reg        Ack_oq,

    // WB slave connections
    input       [31:0] DatSysAppIdReg_ib32,
    input              AckSysAppIdReg_i,
    output  reg        StbSysAppIdReg_oq,

    input       [31:0] DatIntManager_ib32,
    input              AckIntManager_i,
    output  reg        StbIntManager_oq,

    input       [31:0] DatUniqueIdReader_ib32,
    input              AckUniqueIdReader_i,
    output  reg        StbUniqueIdReader_oq,

    input       [31:0] DatI2cIoExpAndMux_ib32,
    input              AckI2cIoExpAndMux_i,
    output  reg        StbI2cIoExpAndMux_oq,

    input       [31:0] DatVadjCtrl_ib32,
    input              AckVadjCtrl_i,
    output  reg        StbVadjCtrl_oq,

    input       [31:0] DatSfpStatus_ib32,
    input              AckSfpStatus_i,
    output  reg        StbSfpStatus_oq,

    input       [31:0] DatVoltageMon_ib32,
    input              AckVoltageMon_i,
    output  reg        StbVoltageMon_oq,

    input       [31:0] DatBstDecoder_ib32,
    input              AckBstDecoder_i,
    output  reg        StbBstDecoder_oq,

    input       [31:0] DatVfcConf_ib32,
    input              AckVfcConf_i,
    output  reg        StbVfcConf_oq
);

enum {
    c_SelNothing,
    c_SelSysAppIdReg,
    c_SelIntManager,
    c_SelUniqueIdReader,
    c_SelIoExpAndMux,
    c_SelVadjCtrl,
    c_SelSfpStatus,
    c_SelVoltageMon,
    c_SelBstDecoder,
    c_SelVfcConf
} SelectedModule;

// Address decoder
always @(*)
    casez (Adr_ib21)
        21'b00000000000000000????: SelectedModule = c_SelSysAppIdReg;     // FROM 00_0000 TO 00_000F (WB) == FROM 000_0000 TO 000_003C (VME) <-  16 regs ( 64 B)
        21'b000000000000000010???: SelectedModule = c_SelIntManager;      // FROM 00_0010 TO 00_0017 (WB) == FROM 000_0040 TO 000_005C (VME) <-   8 regs ( 32 B)
        21'b0000000000000000110??: SelectedModule = c_SelUniqueIdReader;  // FROM 00_0018 TO 00_001B (WB) == FROM 000_0060 TO 000_006C (VME) <-   4 regs ( 16 B)
                                                                          // FROM 00_001C TO 00_001F (WB) == FROM 000_0070 TO 000_007C (VME) <-   4 regs ( 16 B)
        21'b0000000000000001000??: SelectedModule = c_SelIoExpAndMux;     // FROM 00_0020 TO 00_0023 (WB) == FROM 000_0080 TO 000_008C (VME) <-   4 regs ( 16 B)
        21'b0000000000000001001??: SelectedModule = c_SelVadjCtrl;        // FROM 00_0024 TO 00_0027 (WB) == FROM 000_0090 TO 000_009C (VME) <-   4 regs ( 16 B)
        21'b000000000000000101???: SelectedModule = c_SelSfpStatus;       // FROM 00_0028 TO 00_002F (WB) == FROM 000_00A0 TO 000_00BC (VME) <-   8 regs ( 32 B)
        21'b000000000000000110???: SelectedModule = c_SelVoltageMon;      // FROM 00_0030 TO 00_0037 (WB) == FROM 000_00C0 TO 000_00DC (VME) <-   8 regs ( 32 B)
                                                                          // FROM 00_0038 TO 00_003F (WB) == FROM 000_00E0 TO 000_00FC (VME) <-   8 regs ( 32 B)
                                                                          // FROM 00_0040 TO 00_007F (WB) == FROM 000_0100 TO 000_01FC (VME) <-  64 regs (256 B)
                                                                          // FROM 00_0080 TO 00_00FF (WB) == FROM 000_0200 TO 000_03FC (VME) <- 128 regs (512 B)
                                                                          // FROM 00_0100 TO 00_01FF (WB) == FROM 000_0400 TO 000_07FC (VME) <- 256 regs (  1 kB)
        21'b000000000001?????????: SelectedModule = c_SelBstDecoder;      // FROM 00_0200 TO 00_03FF (WB) == FROM 000_0800 TO 000_0FFC (VME) <- 512 regs (  2 kB)
                                                                          // FROM 00_0400 TO 00_07FF (WB) == FROM 000_1000 TO 000_1FFC (VME) <-  1k regs (  4 kB)
                                                                          // FROM 00_0800 TO 00_0FFF (WB) == FROM 000_2000 TO 000_3FFC (VME) <-  2k regs (  8 kB)
                                                                          // FROM 00_1000 TO 00_1FFF (WB) == FROM 000_4000 TO 000_7FFC (VME) <-  4k regs ( 16 kB)
                                                                          // FROM 00_2000 TO 00_3FFF (WB) == FROM 000_8000 TO 000_FFFC (VME) <-  8k regs ( 32 kB)
                                                                          // FROM 00_4000 TO 00_7FFF (WB) == FROM 001_0000 TO 001_FFFC (VME) <- 16k regs ( 64 kB)
        21'b000001???????????????: SelectedModule = c_SelVfcConf;         // FROM 00_8000 TO 00_FFFF (WB) == FROM 002_0000 TO 003_FFFC (VME) <- 32k regs (128 kB)
                                                                          // FROM 01_0000 TO 1F_FFFF (WB) == FROM 004_0000 TO 07F_FFFC (VME) <-   ? regs (  ? B)
        default:                   SelectedModule = c_SelNothing;
    endcase

// Signal mux
always @(posedge Clk_ik) begin
    Ack_oq                       <= #1  1'b0;
    DatMiSo_oqb32                <= #1 32'h0;
    StbSysAppIdReg_oq            <= #1  1'b0;
    StbIntManager_oq             <= #1  1'b0;
    StbUniqueIdReader_oq         <= #1  1'b0;
    StbI2cIoExpAndMux_oq         <= #1  1'b0;
    StbVadjCtrl_oq               <= #1  1'b0;
    StbSfpStatus_oq              <= #1  1'b0;
    StbVoltageMon_oq             <= #1  1'b0;
    StbBstDecoder_oq             <= #1  1'b0;
    StbVfcConf_oq                <= #1  1'b0;
    case (SelectedModule)
        c_SelSysAppIdReg: begin
            StbSysAppIdReg_oq    <= #1 Stb_i;
            DatMiSo_oqb32        <= #1 DatSysAppIdReg_ib32;
            Ack_oq               <= #1 AckSysAppIdReg_i;
        end
        c_SelIntManager: begin
            StbIntManager_oq     <= #1 Stb_i;
            DatMiSo_oqb32        <= #1 DatIntManager_ib32;
            Ack_oq               <= #1 AckIntManager_i;
        end
        c_SelUniqueIdReader: begin
            StbUniqueIdReader_oq <= #1 Stb_i;
            DatMiSo_oqb32        <= #1 DatUniqueIdReader_ib32;
            Ack_oq               <= #1 AckUniqueIdReader_i;
        end
        c_SelIoExpAndMux: begin
            StbI2cIoExpAndMux_oq <= #1 Stb_i;
            DatMiSo_oqb32        <= #1 DatI2cIoExpAndMux_ib32;
            Ack_oq               <= #1 AckI2cIoExpAndMux_i;
        end
        c_SelVadjCtrl: begin
            StbVadjCtrl_oq       <= #1 Stb_i;
            DatMiSo_oqb32        <= #1 DatVadjCtrl_ib32;
            Ack_oq               <= #1 AckVadjCtrl_i;
        end
        c_SelSfpStatus: begin
            StbSfpStatus_oq      <= #1 Stb_i;
            DatMiSo_oqb32        <= #1 DatSfpStatus_ib32;
            Ack_oq               <= #1 AckSfpStatus_i;
        end
         c_SelVoltageMon: begin
            StbVoltageMon_oq     <= #1 Stb_i;
            DatMiSo_oqb32        <= #1 DatVoltageMon_ib32;
            Ack_oq               <= #1 AckVoltageMon_i;
        end
        c_SelBstDecoder: begin
            StbBstDecoder_oq     <= #1 Stb_i;
            DatMiSo_oqb32        <= #1 DatBstDecoder_ib32;
            Ack_oq               <= #1 AckBstDecoder_i;
        end
        c_SelVfcConf: begin
            StbVfcConf_oq        <= #1 Stb_i;
            DatMiSo_oqb32        <= #1 DatVfcConf_ib32;
            Ack_oq               <= #1 AckVfcConf_i;
        end
        c_SelNothing: begin
            DatMiSo_oqb32         <= #1 32'hDEADBEEF;
            Ack_oq                <= #1 Stb_i;
        end
    endcase
end

// Pipelining of signals from master to slaves
always @(posedge Clk_ik) Cyc_oq        <= #1 Cyc_i;
always @(posedge Clk_ik) We_oq         <= #1 We_i;
always @(posedge Clk_ik) Adr_oqb21     <= #1 Adr_ib21;
always @(posedge Clk_ik) DatMoSi_oqb32 <= #1 DatMoSi_ib32;

endmodule
