files = [
    "AddrDecoderWbSys.sv",
    "I2cMuxAndExpMaster.sv",
    "I2cMuxAndExpReqArbiter.sv",
    "InterruptManagerWb.sv",
    "OneWireBus.sv",
    "SfpStatusRegs.sv",
    "SignalSyncer.vhd",
    "SpiMaster.sv",
    "SysAppIdRegs.sv",
    "UniqueIdTemp.sv",
    "VadjControl.sv",
    "VfcConfiguration.sv",
    "VfcHdSystem.sv",
    "VfcHdTop.sv",
    "VoltageMonitoring.sv",
    "WbCdc.sv",
    "WbSysArbiter2M1S.v",
    "WbTrnGen.sv",
    "altera_ip/fpga_temp_sensor/synthesis/fpga_temp_sensor.qip",
    "altera_ip/asmi_parallel/synthesis/asmi_parallel.qip",
    "altera_ip/remote_update/synthesis/remote_update.qip",
    "altera_ip/serial_flash_loader/synthesis/serial_flash_loader.qip"
]
