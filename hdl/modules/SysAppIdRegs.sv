//----------------------------------------------------------------------
// Title      : System/Application ID Registers
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : SysAppIdRegs.sv
// Author     : T. Levens
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Readback registers for the System & Application IDs.
//----------------------------------------------------------------------

`timescale 1ns/100ps

module SysAppIdRegs (
    input              Clk_ik,
    input              Rst_irq,
    input              Cyc_i,
    input              Stb_i,
    input      [  3:0] Adr_ib4,
    output reg [ 31:0] Dat_oab32,
    output reg         Ack_oa,
    input      [ 31:0] SysRelease_ib32,
    input      [ 95:0] SysIdent_ib96,
    input      [ 31:0] AppRelease_ib32,
    input      [351:0] AppIdent_ib352
);

always @(posedge Clk_ik)
    if (Rst_irq) begin
        Dat_oab32                  <= #1 32'h0;
        Ack_oa                     <= #1  1'b0;
    end else begin
        if (~Ack_oa) begin
            case (Adr_ib4)
                4'b0000: Dat_oab32 <= #1 SysRelease_ib32;
                4'b0001: Dat_oab32 <= #1 SysIdent_ib96[ 95: 64];
                4'b0010: Dat_oab32 <= #1 SysIdent_ib96[ 63: 32];
                4'b0011: Dat_oab32 <= #1 SysIdent_ib96[ 31:  0];
                4'b0100: Dat_oab32 <= #1 AppRelease_ib32;
                4'b0101: Dat_oab32 <= #1 AppIdent_ib352[351:320];
                4'b0110: Dat_oab32 <= #1 AppIdent_ib352[319:288];
                4'b0111: Dat_oab32 <= #1 AppIdent_ib352[287:256];
                4'b1000: Dat_oab32 <= #1 AppIdent_ib352[255:224];
                4'b1001: Dat_oab32 <= #1 AppIdent_ib352[223:192];
                4'b1010: Dat_oab32 <= #1 AppIdent_ib352[191:160];
                4'b1011: Dat_oab32 <= #1 AppIdent_ib352[159:128];
                4'b1100: Dat_oab32 <= #1 AppIdent_ib352[127: 96];
                4'b1101: Dat_oab32 <= #1 AppIdent_ib352[ 95: 64];
                4'b1110: Dat_oab32 <= #1 AppIdent_ib352[ 63: 32];
                4'b1111: Dat_oab32 <= #1 AppIdent_ib352[ 31:  0];
                default: Dat_oab32 <= #1 32'h0;
            endcase
        end
        Ack_oa                     <= #1 Stb_i && Cyc_i;
    end

endmodule
