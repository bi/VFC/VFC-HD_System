//----------------------------------------------------------------------
// Title      : Vadj Control
// Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
//----------------------------------------------------------------------
// File       : VadjControl.sv
// Author     : T. Levens
// Company    : CERN BE-BI
//----------------------------------------------------------------------
// Description:
//
// Control the Vadj voltage on the VFC-HD using a MAX5483 digital
// potentiometer.
//
// The Wishbone interface exposes a single registers of the format:
//
//   +--------------------------+--------+--------+-----------+
//   |  Write inverse of 15..0  |  Cmd   |        |   Value   |
//   +--------------------------+--------+--------+-----------+
//     31         ..         16   15..14   13..10    9  ..  0
//
// Note that for a write to be accepted to this register, the bits
// 31..6 must contain the inverse of the bits 15..0. These bits will
// read back as zero.
//
// The values of Cmd are:
//   0=Update pot with value in bits 9..0
//   1=Reserved
//   2=Store pot to NV memory
//   3=Load pot from NV memory
//
// The following features are still to be added:
//   * Automatic tuning of the preset values
//   * Protection against enabling the FMC power supplies when
//     Vadj is set to the wrong voltage
//----------------------------------------------------------------------

`timescale 1ns/100ps

module VadjControl #(
    parameter         g_ClkFrequency
)(
    input             Clk_ik,
    input             Rst_irq,

    input             Cyc_i,
    input             Stb_i,
    input             We_i,
    input      [ 1:0] Adr_ib2,
    input      [31:0] Dat_ib32,
    output reg [31:0] Dat_oab32,
    output reg        Ack_oa,

    output            VfmcEnable_oen,
    input             VfmcAppEnable_i,

    input      [ 2:0] VadjSelect_ib3,
    input      [11:0] VadjVoltage_ib12,
    input             VadjMeasDone_i,

    output reg [23:0] VadjLim_ob24,
    output reg [23:0] VccPdLim_ob24,
    output reg [23:0] VioBLim_ob24,

    output            SClk_o,
    output            Cs_on,
    output            Mosi_o
);

`include "VadjConfig.vh"

// LUT for voltage select and limits
// Pot value is calculated as:
//  Rset  = ((0.6*10000)/(Vadj-0.6))-1000
//  Rpot  = Rset-1100
//  Value = ((Rpot-70)/10000)*1023
reg [ 9:0] PotPreset_b10;
always @(*) begin
    case (VadjSelect_ib3)
        c_VadjSelect_V1P20: begin
            PotPreset_b10 = 10'h321;
            VadjLim_ob24  = {c_Vmon_V1P20_Hi, c_Vmon_V1P20_Lo}; // 1.20V+5% .. 1.20V-5%
            VccPdLim_ob24 = {c_Vmon_V2P50_Hi, c_Vmon_V2P50_Lo}; // 2.50V+5% .. 2.50V-5%
            VioBLim_ob24  = {c_Vmon_V2P50_Hi, c_Vmon_V1P20_Lo}; // 2.50V+5% .. 1.20V-5%
        end
        c_VadjSelect_V1P25: begin
            PotPreset_b10 = 10'h2D2;
            VadjLim_ob24  = {c_Vmon_V1P25_Hi, c_Vmon_V1P25_Lo}; // 1.25V+5% .. 1.25V-5%
            VccPdLim_ob24 = {c_Vmon_V2P50_Hi, c_Vmon_V2P50_Lo}; // 2.50V+5% .. 2.50V-5%
            VioBLim_ob24  = {c_Vmon_V2P50_Hi, c_Vmon_V1P25_Lo}; // 2.50V+5% .. 1.25V-5%
        end
        c_VadjSelect_V1P35: begin
            PotPreset_b10 = 10'h254;
            VadjLim_ob24  = {c_Vmon_V1P35_Hi, c_Vmon_V1P35_Lo}; // 1.35V+5% .. 1.35V-5%
            VccPdLim_ob24 = {c_Vmon_V2P50_Hi, c_Vmon_V2P50_Lo}; // 2.50V+5% .. 2.50V-5%
            VioBLim_ob24  = {c_Vmon_V2P50_Hi, c_Vmon_V1P35_Lo}; // 2.50V+5% .. 1.35V-5%
        end
        c_VadjSelect_V1P50: begin
            PotPreset_b10 = 10'h1CC;
            VadjLim_ob24  = {c_Vmon_V1P50_Hi, c_Vmon_V1P50_Lo}; // 1.50V+5% .. 1.50V-5%
            VccPdLim_ob24 = {c_Vmon_V2P50_Hi, c_Vmon_V2P50_Lo}; // 2.50V+5% .. 2.50V-5%
            VioBLim_ob24  = {c_Vmon_V2P50_Hi, c_Vmon_V1P50_Lo}; // 2.50V+5% .. 1.50V-5%
        end
        c_VadjSelect_V1P80: begin
            PotPreset_b10 = 10'h121;
            VadjLim_ob24  = {c_Vmon_V1P80_Hi, c_Vmon_V1P80_Lo}; // 1.80V+5% .. 1.80V-5%
            VccPdLim_ob24 = {c_Vmon_V2P50_Hi, c_Vmon_V2P50_Lo}; // 2.50V+5% .. 2.50V-5%
            VioBLim_ob24  = {c_Vmon_V2P50_Hi, c_Vmon_V1P80_Lo}; // 2.50V+5% .. 1.80V-5%
        end
        c_VadjSelect_V2P50: begin
            PotPreset_b10 = 10'h065;
            VadjLim_ob24  = {c_Vmon_V2P50_Hi, c_Vmon_V2P50_Lo}; // 2.50V+5% .. 2.50V-5%
            VccPdLim_ob24 = {c_Vmon_V2P50_Hi, c_Vmon_V2P50_Lo}; // 2.50V+5% .. 2.50V-5%
            VioBLim_ob24  = {c_Vmon_V3P30_Hi, c_Vmon_V2P50_Lo}; // 3.30V+5% .. 2.50V-5%
        end
        c_VadjSelect_V3P00: begin
            PotPreset_b10 = 10'h021;
            VadjLim_ob24  = {c_Vmon_V3P00_Hi, c_Vmon_V3P00_Lo}; // 3.00V+5% .. 3.00V-5%
            VccPdLim_ob24 = {c_Vmon_V3P00_Hi, c_Vmon_V3P00_Lo}; // 3.00V+5% .. 3.00V-5%
            VioBLim_ob24  = {c_Vmon_V3P30_Hi, c_Vmon_V2P50_Lo}; // 3.30V+5% .. 2.50V-5%
        end
        c_VadjSelect_V3P30: begin
            PotPreset_b10 = 10'h005;
            VadjLim_ob24  = {c_Vmon_V3P30_Hi, c_Vmon_V3P30_Lo}; // 3.30V+5% .. 3.30V-5%
            VccPdLim_ob24 = {c_Vmon_V3P30_Hi, c_Vmon_V3P30_Lo}; // 3.30V+5% .. 3.30V-5%
            VioBLim_ob24  = {c_Vmon_V3P30_Hi, c_Vmon_V2P50_Lo}; // 3.30V+5% .. 2.50V-5%
        end
    endcase
end

// Provide a pulse 1us after reset to initialise the pot
localparam int c_InitDelay = 1000; //ns
localparam int c_InitCounterValue = (32)'(((g_ClkFrequency/(1e9/c_InitDelay)+1) > 2) ? (g_ClkFrequency/(1e9/c_InitDelay)+1) : 2);
localparam int c_InitCounterWidth = $clog2(c_InitCounterValue+1);
reg [c_InitCounterWidth-1:0] Rst_c;
always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        Rst_c     <= #1 (c_InitCounterWidth)'(c_InitCounterValue);
    end else begin
        if (|Rst_c)
            Rst_c <= #1 Rst_c - (c_InitCounterWidth)'(1);
    end
end
wire Init = (Rst_c == (c_InitCounterWidth)'(1));

// Detect change of VadjSelect
reg  [2:0] LastVadjSelect_b3;
always @(posedge Clk_ik) LastVadjSelect_b3 <= #1 VadjSelect_ib3;
wire VadjSelStb = (VadjSelect_ib3 != LastVadjSelect_b3);

// Selection between programming sources
reg  [ 9:0] PotValue_b10;
reg  [ 1:0] Cmd_b2;
reg  [15:0] VadjCtrlReg_b15;
reg         VadjCtrlStb;
reg         Go;
wire        Done;

always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        Go                <= #1 1'b0;
        Cmd_b2            <= #1 2'b0;
        PotValue_b10      <= #1 9'h0;
    end else begin
        Go                <= #1 1'b0;
        if (VadjCtrlStb) begin
            Go            <= #1 1'b1;
            Cmd_b2        <= #1 VadjCtrlReg_b15[15:14];
            PotValue_b10  <= #1 VadjCtrlReg_b15[ 9: 0];
        end else if (Init || VadjSelStb) begin
            Go            <= #1 1'b1;
            Cmd_b2        <= #1 2'b0;
            PotValue_b10  <= #1 PotPreset_b10;
        end
    end
end

// FMC supplies enable -- currently just the request from the application
assign VfmcEnable_oen = !VfmcAppEnable_i;

// SPI master
SpiMaster #(
    .g_ClkFrequency(g_ClkFrequency),
    .g_SpiFrequency(1e6), // 1 MHz
    .g_MaxBits     (24),
    .g_Cpol        (1'b1),
    .g_Cpha        (1))
i_SpiMaster (
    .Clk_ik        (Clk_ik),
    .Rst_ir        (Rst_irq),
    .Go_i          (Go),
    .Bits_ib       (5'd24),
    .Data_ib       ({2'b0, Cmd_b2, 4'h0, PotValue_b10, 6'h0}),
    .Data_ob       (),
    .Done_o        (Done),
    .Cs_on         (Cs_on),
    .Sclk_o        (SClk_o),
    .Mosi_o        (Mosi_o),
    .Miso_i        (1'b0));

// WB write
always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        VadjCtrlReg_b15 <= #1 16'h0;
        VadjCtrlStb     <= #1  1'b0;
    end else begin
        VadjCtrlStb     <= #1  1'b0;
        if (Cyc_i && We_i && Stb_i && ~Ack_oa) begin
            case (Adr_ib2)
                  2'b00: begin
                    if (Dat_ib32[31:16] == ~Dat_ib32[15:0]) begin
                        VadjCtrlReg_b15 <= #1 Dat_ib32[15:0];
                        VadjCtrlStb     <= #1 1'b1;
                    end
                  end
            endcase
        end
    end
end

// WB read
always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        Dat_oab32 <= #1 32'h0;
        Ack_oa    <= #1 1'b0;
    end else begin
        case (Adr_ib2)
              2'b00: Dat_oab32 <= #1 {22'h0, PotValue_b10};
            default: Dat_oab32 <= #1 32'h0;
        endcase
        Ack_oa <= #1 Stb_i && Cyc_i;
    end
end

endmodule
