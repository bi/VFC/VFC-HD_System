//============================================================================================//
//################################   Test Bench Information   ################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: <tb_ModuleName>.v
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - <date>        <version>    <author>           <description>
//
// Language: Verilog 2005
//
// Module Under Test: <Module Name (ModuleName.v)>
//
// Targeted device:
//
//     - Vendor: <FPGA manufacturer if vendor specific code>
//     - Model:  <FPGA Model if model specific>
//
// Description:
//
//     <Brief description of the test bench.>
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

module tb_I2cMuxAndExpReqArbiter;
//=======================================  Declarations  =====================================//

//==== Local parameters ====//

localparam c_VmeSlot = 5'h4;

//==== Wires & Regs ====//
reg Clk_k = 1'b1;
wire Rst_rq;
wire I2cMuxScl, I2cMuxSda;
wire I2cIoExpIntApp12_an, I2cIoExpIntApp34_an, I2cIoExpIntBstEth_an, I2cIoExpIntBlmIn_an;
reg   [4:1] IoExpAppSfpTxFault_b4    = 4'b1010,
            IoExpAppSfpModeDef0_b4   = 4'b0000,
            IoExpAppSfpLos_b4        = 4'b0100;
wire [4:1]  IoExpAppSfpTxDisable_b4, IoExpAppSfpRateSelect_b4;
reg IoExpBstSfpTxFault = 1'b0,
    IoExpBstSfpModeDef0 = 1'b0,
    IoExpBstSfpLos      = 1'b1;
wire IoExpBstSfpTxDisable, IoExpBstSfpRateSelect;
reg IoExpEthSfpTxFault = 1'b1,
    IoExpEthSfpModeDef0= 1'b0,
    IoExpEthSfpLos = 1'b0;
wire     IoExpEthSfpRateSelect, IoExpEthSfpTxDisable;
reg  [7:0] IoExpBlmIn_b8 = 8'hab;
wire [7:0] IoExpFpLed_nb8;
wire [7:0] BlmIn_b8;
reg  EnTermGpIo4 = 0,
     EnTermGpIo3 = 0,
     EnTermGpIo2 = 0,
     EnTermGpIo1 = 0,
     GpIo34A2B = 0,
     GpIo2A2B = 0,
     GpIo1A2B = 0;
wire IoExpEnTermGpIo4, IoExpEnTermGpIo3, IoExpEnTermGpIo2, IoExpEnTermGpIo1, IoExpGpIo34A2B, IoExpGpIo2A2B, IoExpGpIo1A2B;
wire StatusEnTermGpIo4, StatusEnTermGpIo3, StatusEnTermGpIo2, StatusEnTermGpIo1, StatusGpIo34A2B, StatusGpIo2A2B, StatusGpIo1A2B;
wire Float_z = 1'bz;
wire P1P2Gap_n;
wire [4:0] P1P2Ga_nb5;
wire  AppSfp1ModeDef1, AppSfp1ModeDef2;
wire  AppSfp2ModeDef1, AppSfp2ModeDef2;
wire  AppSfp3ModeDef1, AppSfp3ModeDef2;
wire  AppSfp4ModeDef1, AppSfp4ModeDef2;
wire  SfpAppSfp1ModeDef1, SfpAppSfp1ModeDef2;
wire  SfpAppSfp2ModeDef1, SfpAppSfp2ModeDef2;
wire  SfpAppSfp3ModeDef1, SfpAppSfp3ModeDef2;
wire  SfpAppSfp4ModeDef1, SfpAppSfp4ModeDef2;
wire  BstSfpModeDef1, BstSfpModeDef2;
wire  SfpBstSfpModeDef1, SfpBstSfpModeDef2;
wire  EthSfpModeDef1, EthSfpModeDef2;
wire  SfpEthSfpModeDef1, SfpEthSfpModeDef2;
reg IoExpCdrLos = 1'b0,
    IoExpCdrLol = 1'b0;
wire Si57xSda, Si57xScl;
wire CdrScl, CdrSda;
wire I2cIoExpIntLos_an;
wire WireR321, WireR320;

wire IoExpWrReq;
wire IoExpWrOn;
wire IoExpRdReq;
wire IoExpRdOn;
wire [2:0] IoExpAddr_b3;
wire [1:0] IoExpRegAddr_b2;
wire [7:0] IoExpData_b8;
wire I2CSlaveWrReq;
wire I2CSlaveWrOn;
wire I2CSlaveRdReq;
wire I2CSlaveRdOn;
wire I2cMuxAddress;
wire [1:0] I2cMuxChannel_b2;
wire [6:0] I2CSlaveAddr_b7;
wire [7:0] I2CSlaveRegAddr_b8;
wire [7:0] I2CSlaveByte_b8;
wire I2cMasterBusy;
wire I2cMasterNewByteRead;
wire [7:0] I2cMasterByteOut_b8;
wire I2cMasterAckError;

wire        InitDone, VmeGaP_nq;
wire  [4:0] VmeGa_nqb5;
reg   [7:0] Led_b8 = 0;
wire  [7:0] StatusLed_b8;
wire  [4:1] AppSfpPresent_b4;
wire [15:0] AppSfpId_m4b16 [4:1];
wire  [4:1] AppSfpTxFault_b4, AppSfpLos_b4;
reg   [4:1] AppSfpTxDisable_b4 = 4'b0000, AppSfpRateSelect_b4 = 4'b0000;
wire  [4:1] StatusAppSfpTxDisable_b4, StatusAppSfpRateSelect_b4;
reg   [4:1] AppSfpI2cAccReq_b4 = 0;
wire  [4:1] AppSfpI2cAccGranted_b4;
reg   [4:1] AppSfpI2cIntSel_b4 = 0, AppSfpI2cAccModeWr_b4 = 0;
reg   [7:0] AppSfpI2cRegAddr_m4b8 [4:1];
reg   [7:0] AppSfpI2cRegDataIn_m4b8 [4:1];
wire  [7:0] AppSfpI2cRegDataOut_m4b8 [4:1];
wire        EthSfpPresent;
wire [15:0] EthSfpId_b16 ;
wire        EthSfpTxFault, EthSfpLos;
reg         EthSfpTxDisable = 0, EthSfpRateSelect = 0;
wire        StatusEthSfpTxDisable, StatusEthSfpRateSelect;
reg         EthSfpI2cAccReq = 0;
wire        EthSfpI2cAccGranted;
reg         EthSfpI2cIntSel, EthSfpI2cAccModeWr;
reg   [7:0] EthSfpI2cRegAddr_b8;
reg   [7:0] EthSfpI2cRegDataIn_b8;
wire  [7:0] EthSfpI2cRegDataOut_b8;
wire        BstSfpPresent;
wire [15:0] BstSfpId_b16 ;
wire        BstSfpTxFault, BstSfpLos;
reg         BstSfpTxDisable = 0, BstSfpRateSelect = 0;
wire        StatusBstSfpTxDisable, StatusBstSfpRateSelect;
reg         BstSfpI2cAccReq = 0;
wire        BstSfpI2cAccGranted;
reg         BstSfpI2cIntSel, BstSfpI2cAccModeWr;
reg   [7:0] BstSfpI2cRegAddr_b8;
reg   [7:0] BstSfpI2cRegDataIn_b8;
wire  [7:0] BstSfpI2cRegDataOut_b8;
wire        CdrLos, CdrLol;
reg         CdrI2cAccReq = 0;
wire        CdrI2cAccGranted;
reg         CdrI2cAccModeWr;
reg   [7:0] CdrI2cRegAddr_b8, CdrI2cRegDataIn_b8;
wire  [7:0] CdrI2cRegDataOut_b8;
reg         Si57xI2cAccReq = 0;
wire        Si57xI2cAccGranted;
reg         Si57xI2cAccModeWr;
reg   [7:0] Si57xI2cRegAddr_b8, Si57xI2cRegDataIn_b8;
wire  [7:0] Si57xI2cRegDataOut_b8;
wire [31:0] WbAdr_b32, WbDatMoSi_b32, WbDatMiSo_b32;
wire        WbWe, WbCyc, WbStb, WbAck;
wire        BstSfpI2cRdDataDav, EthSfpI2cRdDataDav, CdrI2cRdDataDav, Si57xI2cRdDataDav;
wire  [4:1] AppSfpI2cRdDataDav_b4;
reg   [7:0] Dummy_b8;

logic Error = 0;

//=====================================  Status & Control  ====================================//


//==== Stimulus ====//

localparam c_MaxServiceTime = 2_000_000; //8ms

initial begin
    $display("//============================================================================================//");
    $display("//################################### SIMULATION  STARTING  ##################################//");
    $display("//============================================================================================//");
    i_WbBus.Reset();
    //@(posedge InitDone) I2cWriteAppSfp(2, 0, 4, 8'hac);
    //#1000;
    //$stop();
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    Led_b8 <= #1 $urandom_range(0,255);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    {GpIo1A2B, EnTermGpIo1, GpIo2A2B, EnTermGpIo2, GpIo34A2B, EnTermGpIo3, EnTermGpIo4} <= #1 $urandom_range(0,127);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    {IoExpCdrLol, IoExpCdrLos} <= #1 $urandom_range(0,3);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    {IoExpAppSfpModeDef0_b4[1], IoExpAppSfpLos_b4[1], IoExpAppSfpTxFault_b4[1], AppSfpRateSelect_b4[1], AppSfpTxDisable_b4[1]} <= #1 $urandom_range(0,31);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    {IoExpAppSfpModeDef0_b4[2], IoExpAppSfpLos_b4[2], IoExpAppSfpTxFault_b4[2], AppSfpRateSelect_b4[2], AppSfpTxDisable_b4[2]} <= #1 $urandom_range(0,31);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    {IoExpAppSfpModeDef0_b4[3], IoExpAppSfpLos_b4[3], IoExpAppSfpTxFault_b4[3], AppSfpRateSelect_b4[3], AppSfpTxDisable_b4[3]} <= #1 $urandom_range(0,31);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    {IoExpAppSfpModeDef0_b4[4], IoExpAppSfpLos_b4[4], IoExpAppSfpTxFault_b4[4], AppSfpRateSelect_b4[4], AppSfpTxDisable_b4[4]} <= #1 $urandom_range(0,31);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    {IoExpBstSfpModeDef0, IoExpBstSfpLos, IoExpBstSfpTxFault, BstSfpRateSelect, BstSfpTxDisable} <= #1 $urandom_range(0,31);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    {IoExpEthSfpModeDef0, IoExpEthSfpLos, IoExpEthSfpTxFault, EthSfpRateSelect, EthSfpTxDisable} <= #1 $urandom_range(0,31);
end

always begin
    repeat ($urandom_range(500_000,1_000_000)) @(posedge Clk_k);
    IoExpBlmIn_b8 <= #1 $urandom_range(0,255);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    if ($urandom_range(0,1)) I2cWriteAppSfp(1, $urandom_range(0,1), $urandom_range(3,255), $urandom_range(0,255));
    else  I2cReadAppSfp(1, $urandom_range(0,1), $urandom_range(3,255), Dummy_b8);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    if ($urandom_range(0,1)) I2cWriteAppSfp(2, $urandom_range(0,1), $urandom_range(3,255), $urandom_range(0,255));
    else  I2cReadAppSfp(2, $urandom_range(0,1), $urandom_range(3,255), Dummy_b8);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    if ($urandom_range(0,1)) I2cWriteAppSfp(3, $urandom_range(0,1), $urandom_range(3,255), $urandom_range(0,255));
    else  I2cReadAppSfp(3, $urandom_range(0,1), $urandom_range(3,255), Dummy_b8);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    if ($urandom_range(0,1)) I2cWriteAppSfp(4, $urandom_range(0,1), $urandom_range(3,255), $urandom_range(0,255));
    else  I2cReadAppSfp(4, $urandom_range(0,1), $urandom_range(3,255), Dummy_b8);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    if ($urandom_range(0,1)) I2cWriteEthSfp($urandom_range(0,1), $urandom_range(3,255), $urandom_range(0,255));
    else  I2cReadEthSfp($urandom_range(0,1), $urandom_range(3,255), Dummy_b8);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    if ($urandom_range(0,1)) I2cWriteBstSfp($urandom_range(0,1), $urandom_range(3,255), $urandom_range(0,255));
    else  I2cReadBstSfp($urandom_range(0,1), $urandom_range(3,255), Dummy_b8);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    if ($urandom_range(0,1)) I2cWriteCdr($urandom_range(0,255), $urandom_range(0,255));
    else  I2cReadCdr($urandom_range(0,255), Dummy_b8);
end

always begin
    repeat (c_MaxServiceTime+$urandom_range(0,200_000)) @(posedge Clk_k);
    if ($urandom_range(0,1)) I2cWriteSi57x($urandom_range(0,255), $urandom_range(0,255));
    else  I2cReadSi57x($urandom_range(0,255), Dummy_b8);
end

//==== Tasks ====//

task I2cWriteAppSfp(input [2:0] AppSfpNum_b3, input Interface, input [7:0] RegAddress_b8, input [7:0] RegData_b8);
    if (~AppSfpPresent_b4[AppSfpNum_b3]) begin
        //$display("Error: Cannot access an SFP that is not present");
    end else begin
        @(posedge Clk_k) #1;
        AppSfpI2cAccReq_b4[AppSfpNum_b3] = 1'b1;
        AppSfpI2cIntSel_b4[AppSfpNum_b3] = Interface;
        AppSfpI2cAccModeWr_b4[AppSfpNum_b3] = 1'b1;
        AppSfpI2cRegAddr_m4b8[AppSfpNum_b3] = RegAddress_b8;
        AppSfpI2cRegDataIn_m4b8[AppSfpNum_b3] = RegData_b8;
        while(~AppSfpI2cAccGranted_b4[AppSfpNum_b3]) @(posedge Clk_k);
        #1 AppSfpI2cAccReq_b4[AppSfpNum_b3] = 1'b0;
        while(AppSfpI2cAccGranted_b4[AppSfpNum_b3]) @(posedge Clk_k);
        #1;
    end
endtask:I2cWriteAppSfp

task I2cReadAppSfp(input [2:0] AppSfpNum_b3, input Interface, input [7:0] RegAddress_b8, output [7:0] RegData_b8);
    if (~AppSfpPresent_b4[AppSfpNum_b3]) begin
        //$display("Error: Cannot access an SFP that is not present");
    end else begin
        case (AppSfpNum_b3)
        3'd1: begin
            if (Interface==0) i_AppSfp1I2c50.Mem_m[RegAddress_b8]=$urandom_range(0,255);
            else i_AppSfp1I2c51.Mem_m[RegAddress_b8]=$urandom_range(0,255);
        end
        3'd2: begin
            if (Interface==0) i_AppSfp2I2c50.Mem_m[RegAddress_b8]=$urandom_range(0,255);
            else i_AppSfp2I2c51.Mem_m[RegAddress_b8]=$urandom_range(0,255);
        end
        3'd3: begin
               if (Interface==0) i_AppSfp3I2c50.Mem_m[RegAddress_b8]=$urandom_range(0,255);
            else i_AppSfp3I2c51.Mem_m[RegAddress_b8]=$urandom_range(0,255);
        end
        3'd4: begin
            if (Interface==0) i_AppSfp4I2c50.Mem_m[RegAddress_b8]=$urandom_range(0,255);
            else i_AppSfp4I2c51.Mem_m[RegAddress_b8]=$urandom_range(0,255);
        end
        endcase
        @(posedge Clk_k) #1;
        AppSfpI2cAccReq_b4[AppSfpNum_b3] = 1'b1;
        AppSfpI2cIntSel_b4[AppSfpNum_b3] = Interface;
        AppSfpI2cAccModeWr_b4[AppSfpNum_b3] = 1'b0;
        AppSfpI2cRegAddr_m4b8[AppSfpNum_b3] = RegAddress_b8;
        while(~AppSfpI2cAccGranted_b4[AppSfpNum_b3]) @(posedge Clk_k);
        #1 AppSfpI2cAccReq_b4[AppSfpNum_b3] = 1'b0;
        while(~AppSfpI2cRdDataDav_b4[AppSfpNum_b3]) @(posedge Clk_k);
        RegData_b8 = AppSfpI2cRegDataOut_m4b8[AppSfpNum_b3];
        while(AppSfpI2cAccGranted_b4[AppSfpNum_b3]) @(posedge Clk_k);
        #1;
    end
endtask:I2cReadAppSfp

task I2cWriteEthSfp(input Interface, input [7:0] RegAddress_b8, input [7:0] RegData_b8);
    if (~EthSfpPresent) begin
        //$display("Error: Cannot access an SFP that is not present");
    end else begin
        @(posedge Clk_k) #1;
        EthSfpI2cAccReq = 1'b1;
        EthSfpI2cIntSel = Interface;
        EthSfpI2cAccModeWr = 1'b1;
        EthSfpI2cRegAddr_b8 = RegAddress_b8;
        EthSfpI2cRegDataIn_b8 = RegData_b8;
        while(~EthSfpI2cAccGranted) @(posedge Clk_k);
        #1 EthSfpI2cAccReq = 1'b0;
        while(EthSfpI2cAccGranted) @(posedge Clk_k);
        #1;
        if (Interface==0) begin
            p_EthSfpI2c50Write: assert (i_EthSfpI2c50.Mem_m[RegAddress_b8]==RegData_b8) else $error("p_EthSfpI2c50Write failed");
        end else begin
            p_EthSfpI2c51Write: assert (i_EthSfpI2c51.Mem_m[RegAddress_b8]==RegData_b8) else $error("p_EthSfpI2c51Write failed");
        end
    end
endtask:I2cWriteEthSfp

task I2cReadEthSfp(input Interface, input [7:0] RegAddress_b8, output [7:0] RegData_b8);
    if (~EthSfpPresent) begin
        //$display("Error: Cannot access an SFP that is not present");
    end else begin
        if (Interface==0) i_EthSfpI2c50.Mem_m[RegAddress_b8]=$urandom_range(0,255);
        else i_EthSfpI2c51.Mem_m[RegAddress_b8]=$urandom_range(0,255);
        @(posedge Clk_k) #1;
        EthSfpI2cAccReq = 1'b1;
        EthSfpI2cIntSel = Interface;
        EthSfpI2cAccModeWr = 1'b0;
        EthSfpI2cRegAddr_b8 = RegAddress_b8;
        while(~EthSfpI2cAccGranted) @(posedge Clk_k);
        #1 EthSfpI2cAccReq = 1'b0;
        while(~EthSfpI2cRdDataDav) @(posedge Clk_k);
        RegData_b8 = EthSfpI2cRegDataOut_b8;
        while(EthSfpI2cAccGranted) @(posedge Clk_k);
        #1;
       if (Interface==0) begin
            p_EthSfpI2c50Read: assert (i_EthSfpI2c50.Mem_m[RegAddress_b8]==RegData_b8) else $error("p_EthSfpI2c50Read failed");
        end else begin
            p_EthSfpI2c51Read: assert (i_EthSfpI2c51.Mem_m[RegAddress_b8]==RegData_b8) else $error("p_EthSfpI2c51Read failed");
        end
    end
endtask:I2cReadEthSfp

task I2cWriteBstSfp(input Interface, input [7:0] RegAddress_b8, input [7:0] RegData_b8);
    if (~BstSfpPresent) begin
        //$display("Error: Cannot access an SFP that is not present");
    end else begin
        @(posedge Clk_k) #1;
        BstSfpI2cAccReq = 1'b1;
        BstSfpI2cIntSel = Interface;
        BstSfpI2cAccModeWr = 1'b1;
        BstSfpI2cRegAddr_b8 = RegAddress_b8;
        BstSfpI2cRegDataIn_b8 = RegData_b8;
        while(~BstSfpI2cAccGranted) @(posedge Clk_k);
        #1 BstSfpI2cAccReq = 1'b0;
        while(BstSfpI2cAccGranted) @(posedge Clk_k);
        #1;
        if (Interface==0) begin
            p_BstSfpI2c50Write: assert (i_BstSfpI2c50.Mem_m[RegAddress_b8]==RegData_b8) else $error("p_BstSfpI2c50Write failed");
        end else begin
            p_BstSfpI2c51Write: assert (i_BstSfpI2c51.Mem_m[RegAddress_b8]==RegData_b8) else $error("p_BstSfpI2c51Write failed");
        end
    end
endtask:I2cWriteBstSfp

task I2cReadBstSfp(input Interface, input [7:0] RegAddress_b8, output [7:0] RegData_b8);
    if (~BstSfpPresent) begin
        //$display("Error: Cannot access an SFP that is not present");
    end else begin
        if (Interface==0) i_BstSfpI2c50.Mem_m[RegAddress_b8]=$urandom_range(0,255);
        else i_BstSfpI2c51.Mem_m[RegAddress_b8]=$urandom_range(0,255);
        @(posedge Clk_k) #1;
        BstSfpI2cAccReq = 1'b1;
        BstSfpI2cIntSel = Interface;
        BstSfpI2cAccModeWr = 1'b0;
        BstSfpI2cRegAddr_b8 = RegAddress_b8;
        while(~BstSfpI2cAccGranted) @(posedge Clk_k);
        #1 BstSfpI2cAccReq = 1'b0;
        while(~BstSfpI2cRdDataDav) @(posedge Clk_k);
        RegData_b8 = BstSfpI2cRegDataOut_b8;
        while(BstSfpI2cAccGranted) @(posedge Clk_k);
        #1;
       if (Interface==0) begin
            p_BstSfpI2c50Read: assert (i_BstSfpI2c50.Mem_m[RegAddress_b8]==RegData_b8) else $error("p_BstSfpI2c50Read failed");
        end else begin
            p_BstSfpI2c51Read: assert (i_BstSfpI2c51.Mem_m[RegAddress_b8]==RegData_b8) else $error("p_BstSfpI2c51Read failed");
        end
    end
endtask:I2cReadBstSfp


task I2cWriteCdr(input [7:0] RegAddress_b8, input [7:0] RegData_b8);
    @(posedge Clk_k) #1;
    CdrI2cAccReq = 1'b1;
    CdrI2cAccModeWr = 1'b1;
    CdrI2cRegAddr_b8 = RegAddress_b8;
    CdrI2cRegDataIn_b8 = RegData_b8;
    while(~CdrI2cAccGranted) @(posedge Clk_k);
    #1 CdrI2cAccReq = 1'b0;
    while(CdrI2cAccGranted) @(posedge Clk_k);
    #1;
    p_CdrI2cWrite: assert (i_CdrI2c.Mem_m[RegAddress_b8]==RegData_b8) else $error("p_CdrI2cWrite failed");
endtask:I2cWriteCdr

task I2cReadCdr(input [7:0] RegAddress_b8, output [7:0] RegData_b8);
    i_CdrI2c.Mem_m[RegAddress_b8]=$urandom_range(0,255);
    @(posedge Clk_k) #1;
    CdrI2cAccReq = 1'b1;
    CdrI2cAccModeWr = 1'b0;
    CdrI2cRegAddr_b8 = RegAddress_b8;
    while(~CdrI2cAccGranted) @(posedge Clk_k);
    #1 CdrI2cAccReq = 1'b0;
    while(~CdrI2cRdDataDav) @(posedge Clk_k);
    RegData_b8 = CdrI2cRegDataOut_b8;
    while(CdrI2cAccGranted) @(posedge Clk_k);
    #1;
    p_CdrI2cRead: assert (i_CdrI2c.Mem_m[RegAddress_b8]==RegData_b8) else $error("p_CdrI2cRead failed");
endtask:I2cReadCdr

task I2cWriteSi57x(input [7:0] RegAddress_b8, input [7:0] RegData_b8);
    @(posedge Clk_k) #1;
    Si57xI2cAccReq = 1'b1;
    Si57xI2cAccModeWr = 1'b1;
    Si57xI2cRegAddr_b8 = RegAddress_b8;
    Si57xI2cRegDataIn_b8 = RegData_b8;
    while(~Si57xI2cAccGranted) @(posedge Clk_k);
    #1 Si57xI2cAccReq = 1'b0;
    while(Si57xI2cAccGranted) @(posedge Clk_k);
    #1;
    p_Si57xI2cWrite: assert (i_Si57xI2c.Mem_m[RegAddress_b8]==RegData_b8) else $error("p_Si57xI2cWrite failed");
endtask:I2cWriteSi57x

task I2cReadSi57x(input [7:0] RegAddress_b8, output [7:0] RegData_b8);
    i_Si57xI2c.Mem_m[RegAddress_b8]=$urandom_range(0,255);
    @(posedge Clk_k) #1;
    Si57xI2cAccReq = 1'b1;
    Si57xI2cAccModeWr = 1'b0;
    Si57xI2cRegAddr_b8 = RegAddress_b8;
    while(~Si57xI2cAccGranted) @(posedge Clk_k);
    #1 Si57xI2cAccReq = 1'b0;
    while(~Si57xI2cRdDataDav) @(posedge Clk_k);
    RegData_b8 = Si57xI2cRegDataOut_b8;
    while(Si57xI2cAccGranted) @(posedge Clk_k);
    #1;
    p_Si57xI2cRead: assert (i_Si57xI2c.Mem_m[RegAddress_b8]==RegData_b8) else $error("p_Si57xI2cRead failed");
endtask:I2cReadSi57x


//==== Checkers and Assertions ====//
//Reset and initialization
p_ResetToInit            : assert property (@(posedge Clk_k) $fell(Rst_rq)   |=> ##[1:2]  ~InitDone ##[1:c_MaxServiceTime] InitDone) else $error("Missing InitDone sequence afer reset");
p_GaCheckInit            : assert property (@(posedge Clk_k) $rose(InitDone) |-> {P1P2Gap_n, P1P2Ga_nb5}==={VmeGaP_nq, VmeGa_nqb5}) else $error("GA/GAP Error");

//Int Clearing
p_LosLolIntReset         : assert property (@(posedge Clk_k) $fell(I2cIoExpIntLos_an)    |=> ##[1:c_MaxServiceTime] I2cIoExpIntLos_an)    else $error("LoSLoL interrupt not served in time");
p_AppSfp12IntReset       : assert property (@(posedge Clk_k) $fell(I2cIoExpIntApp12_an)  |=> ##[1:c_MaxServiceTime] I2cIoExpIntApp12_an)  else $error("AppSfp12 interrupt not served in time");
p_AppSfp34IntReset       : assert property (@(posedge Clk_k) $fell(I2cIoExpIntApp34_an)  |=> ##[1:c_MaxServiceTime] I2cIoExpIntApp34_an)  else $error("AppSfp34 interrupt not served in time");
p_BstEthSfpIntReset      : assert property (@(posedge Clk_k) $fell(I2cIoExpIntBstEth_an) |=> ##[1:c_MaxServiceTime] I2cIoExpIntBstEth_an) else $error("BstEth interrupt not served in time");
p_BlmInIntReset          : assert property (@(posedge Clk_k) $fell(I2cIoExpIntBlmIn_an)  |=> ##[1:c_MaxServiceTime] I2cIoExpIntBlmIn_an)  else $error("BlmIn interrupt not served in time");

//Leds
    //Initialization
p_LedInit                : assert property (@(posedge Clk_k) $rose(InitDone) |-> (Led_b8===StatusLed_b8 && StatusLed_b8===IoExpFpLed_nb8)) else $error("Leds not initialized after reset");
    //Change Check
p_Led0Check              : assert property (@(posedge Clk_k) InitDone and ($rose(Led_b8[0]!=StatusLed_b8[0]) or (Led_b8[0]!=$past(Led_b8[0]))) |=> ##[1:c_MaxServiceTime] (Led_b8===StatusLed_b8 && StatusLed_b8===IoExpFpLed_nb8)) else $error("Led0 not updated in time or wrong value");
p_Led1Check              : assert property (@(posedge Clk_k) InitDone and ($rose(Led_b8[1]!=StatusLed_b8[1]) or (Led_b8[1]!=$past(Led_b8[1]))) |=> ##[1:c_MaxServiceTime] (Led_b8===StatusLed_b8 && StatusLed_b8===IoExpFpLed_nb8)) else $error("Led1 not updated in time or wrong value");
p_Led2Check              : assert property (@(posedge Clk_k) InitDone and ($rose(Led_b8[2]!=StatusLed_b8[2]) or (Led_b8[2]!=$past(Led_b8[2]))) |=> ##[1:c_MaxServiceTime] (Led_b8===StatusLed_b8 && StatusLed_b8===IoExpFpLed_nb8)) else $error("Led2 not updated in time or wrong value");
p_Led3Check              : assert property (@(posedge Clk_k) InitDone and ($rose(Led_b8[3]!=StatusLed_b8[3]) or (Led_b8[3]!=$past(Led_b8[3]))) |=> ##[1:c_MaxServiceTime] (Led_b8===StatusLed_b8 && StatusLed_b8===IoExpFpLed_nb8)) else $error("Led3 not updated in time or wrong value");
p_Led4Check              : assert property (@(posedge Clk_k) InitDone and ($rose(Led_b8[4]!=StatusLed_b8[4]) or (Led_b8[4]!=$past(Led_b8[4]))) |=> ##[1:c_MaxServiceTime] (Led_b8===StatusLed_b8 && StatusLed_b8===IoExpFpLed_nb8)) else $error("Led4 not updated in time or wrong value");
p_Led5Check              : assert property (@(posedge Clk_k) InitDone and ($rose(Led_b8[5]!=StatusLed_b8[5]) or (Led_b8[5]!=$past(Led_b8[5]))) |=> ##[1:c_MaxServiceTime] (Led_b8===StatusLed_b8 && StatusLed_b8===IoExpFpLed_nb8)) else $error("Led5 not updated in time or wrong value");
p_Led6Check              : assert property (@(posedge Clk_k) InitDone and ($rose(Led_b8[6]!=StatusLed_b8[6]) or (Led_b8[6]!=$past(Led_b8[6]))) |=> ##[1:c_MaxServiceTime] (Led_b8===StatusLed_b8 && StatusLed_b8===IoExpFpLed_nb8)) else $error("Led6 not updated in time or wrong value");
p_Led7Check              : assert property (@(posedge Clk_k) InitDone and ($rose(Led_b8[7]!=StatusLed_b8[7]) or (Led_b8[7]!=$past(Led_b8[7]))) |=> ##[1:c_MaxServiceTime] (Led_b8===StatusLed_b8 && StatusLed_b8===IoExpFpLed_nb8)) else $error("Led7 not updated in time or wrong value");

//GPIO
    //Init
p_GpIo1A2BInit           : assert property (@(posedge Clk_k) $rose(InitDone) |->  (GpIo1A2B===StatusGpIo1A2B && GpIo1A2B===IoExpGpIo1A2B)) else $error("GpIo1A2B not initialized after reset");
p_EnTermGpIo1Init        : assert property (@(posedge Clk_k) $rose(InitDone) |->  (EnTermGpIo1===StatusEnTermGpIo1 && EnTermGpIo1===IoExpEnTermGpIo1)) else $error("EnTermGpIo1 not initialized after reset");
p_GpIo2A2BInit           : assert property (@(posedge Clk_k) $rose(InitDone) |->  (GpIo2A2B===StatusGpIo2A2B && GpIo2A2B===IoExpGpIo2A2B)) else $error("GpIo2A2B not initialized after reset");
p_EnTermGpIo2Init        : assert property (@(posedge Clk_k) $rose(InitDone) |->  (EnTermGpIo2===StatusEnTermGpIo2 && EnTermGpIo2===IoExpEnTermGpIo2)) else $error("EnTermGpIo2 not initialized after reset");
p_GpIo34A2BInit          : assert property (@(posedge Clk_k) $rose(InitDone) |->  (GpIo34A2B===StatusGpIo34A2B && GpIo34A2B===IoExpGpIo34A2B)) else $error("GpIo34A2B not initialized after reset");
p_EnTermGpIo3Init        : assert property (@(posedge Clk_k) $rose(InitDone) |->  (EnTermGpIo3===StatusEnTermGpIo3 && EnTermGpIo3===IoExpEnTermGpIo3)) else $error("EnTermGpIo3 not initialized after reset");
p_EnTermGpIo4Init        : assert property (@(posedge Clk_k) $rose(InitDone) |->  (EnTermGpIo4===StatusEnTermGpIo4 && EnTermGpIo4===IoExpEnTermGpIo4)) else $error("EnTermGpIo4 not initialized after reset");
    //Change Check
p_GpIo1A2BCheck          : assert property (@(posedge Clk_k) InitDone and ($rose(GpIo1A2B!=StatusGpIo1A2B) or $rose(GpIo1A2B!=$past(GpIo1A2B))) |=> ##[1:c_MaxServiceTime] (GpIo1A2B===StatusGpIo1A2B && GpIo1A2B===IoExpGpIo1A2B)) else $error("GpIo1A2B not updated in time or wrong value");
p_EnTermGpIo1Check       : assert property (@(posedge Clk_k) InitDone and ($rose(EnTermGpIo1!=StatusEnTermGpIo1) or $rose(EnTermGpIo1!=$past(EnTermGpIo1))) |=> ##[1:c_MaxServiceTime] (EnTermGpIo1===StatusEnTermGpIo1 && EnTermGpIo1===IoExpEnTermGpIo1)) else $error("EnTermGpIo1 not updated in time or wrong value");
p_GpIo2A2BCheck          : assert property (@(posedge Clk_k) InitDone and ($rose(GpIo2A2B!=StatusGpIo2A2B) or $rose(GpIo2A2B!=$past(GpIo2A2B))) |=> ##[1:c_MaxServiceTime] (GpIo2A2B===StatusGpIo2A2B && GpIo2A2B===IoExpGpIo2A2B)) else $error("GpIo2A2B not updated in time or wrong value");
p_EnTermGpIo2Check       : assert property (@(posedge Clk_k) InitDone and ($rose(EnTermGpIo2!=StatusEnTermGpIo2) or $rose(EnTermGpIo2!=$past(EnTermGpIo2))) |=> ##[1:c_MaxServiceTime] (EnTermGpIo2===StatusEnTermGpIo2 && EnTermGpIo2===IoExpEnTermGpIo2)) else $error("EnTermGpIo2 not updated in time or wrong value");
p_GpIo34A2BCheck         : assert property (@(posedge Clk_k) InitDone and ($rose(GpIo34A2B!=StatusGpIo34A2B) or $rose(GpIo34A2B!=$past(GpIo34A2B))) |=> ##[1:c_MaxServiceTime] (GpIo34A2B===StatusGpIo34A2B && GpIo34A2B===IoExpGpIo34A2B)) else $error("GpIo34A2B not updated in time or wrong value");
p_EnTermGpIo3Check       : assert property (@(posedge Clk_k) InitDone and ($rose(EnTermGpIo3!=StatusEnTermGpIo3) or $rose(EnTermGpIo3!=$past(EnTermGpIo3))) |=> ##[1:c_MaxServiceTime] (EnTermGpIo3===StatusEnTermGpIo3 && EnTermGpIo3===IoExpEnTermGpIo3)) else $error("EnTermGpIo3 not updated in time or wrong value");
p_EnTermGpIo4Check       : assert property (@(posedge Clk_k) InitDone and ($rose(EnTermGpIo4!=StatusEnTermGpIo4) or $rose(EnTermGpIo4!=$past(EnTermGpIo4))) |=> ##[1:c_MaxServiceTime] (EnTermGpIo4===StatusEnTermGpIo4 && EnTermGpIo4===IoExpEnTermGpIo4)) else $error("EnTermGpIo4 not updated in time or wrong value");

//APP SFP 1
    //Initialization
p_InitAppSfp1ModeDef0    : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpAppSfpModeDef0_b4[1]==~AppSfpPresent_b4[1]) else $error("AppSfpPresent_b4[1] not initialized");
p_InitAppSfp1Los         : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpAppSfpLos_b4[1]==AppSfpLos_b4[1]) else $error("AppSfpLos_b4[1] not initialized");
p_InitAppSfp1TxFault     : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpAppSfpTxFault_b4[1]==AppSfpTxFault_b4[1]) else $error("AppSfpTxFault_b4[1] not initialized");
p_InitAppSfp1RateSelect  : assert property (@(posedge Clk_k) $rose(InitDone) |->  (AppSfpRateSelect_b4[1]===StatusAppSfpRateSelect_b4[1] && AppSfpRateSelect_b4[1]===IoExpAppSfpRateSelect_b4[1])) else $error("AppSfpRateSelect_b4[1] not initialized after reset");
p_InitAppSfp1TxDisable   : assert property (@(posedge Clk_k) $rose(InitDone) |->  (AppSfpTxDisable_b4[1]===StatusAppSfpTxDisable_b4[1] && AppSfpTxDisable_b4[1]===IoExpAppSfpTxDisable_b4[1])) else $error("AppSfpTxDisable_b4[1] not initialized after reset");
    //Change Check
p_AppSfp1Id              : assert property (@(posedge Clk_k) $rose(AppSfpPresent_b4[1])  |-> AppSfpId_m4b16[1]===16'h01a1) else $error("Wrong ID detected for APP SFP1");
p_AppSfp1ModeDef0Check   : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpAppSfpModeDef0_b4[1]!=~AppSfpPresent_b4[1]) or $fell(I2cIoExpIntApp12_an) or $rose(IoExpAppSfpModeDef0_b4[1]!=$past(IoExpAppSfpModeDef0_b4[1]))) |=> ##[1:c_MaxServiceTime] IoExpAppSfpModeDef0_b4[1]==~AppSfpPresent_b4[1]) else $error("Missing AppSfpPresent_b4[1]");
p_AppSfp1LosCheck        : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpAppSfpLos_b4[1]!=AppSfpLos_b4[1]) or $fell(I2cIoExpIntLos_an) or $rose(IoExpAppSfpLos_b4[1]!=$past(IoExpAppSfpLos_b4[1]))) |=> ##[1:c_MaxServiceTime] IoExpAppSfpLos_b4[1]==AppSfpLos_b4[1]) else $error("Missing AppSfpLos_b4[1]");
p_AppSfp1TxFaultCheck    : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpAppSfpTxFault_b4[1]!=AppSfpTxFault_b4[1]) or $fell(I2cIoExpIntApp12_an) or $rose(IoExpAppSfpTxFault_b4[1]!=$past(IoExpAppSfpTxFault_b4[1]))) |=> ##[1:c_MaxServiceTime] IoExpAppSfpTxFault_b4[1]==AppSfpTxFault_b4[1]) else $error("Missing AppSfpTxFault_b4[1]");
p_AppSfp1RateSelectCheck : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpRateSelect_b4[1]!=StatusAppSfpRateSelect_b4[1]) or $rose(AppSfpRateSelect_b4[1]!=$past(AppSfpRateSelect_b4[1]))) |=> ##[1:c_MaxServiceTime] (AppSfpRateSelect_b4[1]===StatusAppSfpRateSelect_b4[1] && AppSfpRateSelect_b4[1]===IoExpAppSfpRateSelect_b4[1])) else $error("AppSfpRateSelect_b4[1] not updated in time or wrong value");
p_AppSfp1TxDisableCheck  : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpTxDisable_b4[1]!=StatusAppSfpTxDisable_b4[1]) or $rose(AppSfpTxDisable_b4[1]!=$past(AppSfpTxDisable_b4[1]))) |=> ##[1:c_MaxServiceTime] (AppSfpTxDisable_b4[1]===StatusAppSfpTxDisable_b4[1] && AppSfpTxDisable_b4[1]===IoExpAppSfpTxDisable_b4[1])) else $error("AppSfpTxDisable_b4[1] not updated in time or wrong value");
    //I2C accesses
p_AppSfp1I2c50Write      : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[1]) and AppSfpI2cAccModeWr_b4[1]  and ~AppSfpI2cIntSel_b4[1]) |=> ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[1]) && i_AppSfp1I2c50.Mem_m[AppSfpI2cRegAddr_m4b8[1]]==AppSfpI2cRegDataIn_m4b8[1]) else $error("p_AppSfp1I2c50Write failed");
p_AppSfp1I2c51Write      : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[1]) and AppSfpI2cAccModeWr_b4[1]  and AppSfpI2cIntSel_b4[1])  |=> ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[1]) && i_AppSfp1I2c51.Mem_m[AppSfpI2cRegAddr_m4b8[1]]==AppSfpI2cRegDataIn_m4b8[1]) else $error("p_AppSfp1I2c50Write failed");
p_AppSfp1I2c50Read       : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[1]) and ~AppSfpI2cAccModeWr_b4[1] and ~AppSfpI2cIntSel_b4[1]) |=> ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[1]) && i_AppSfp1I2c50.Mem_m[AppSfpI2cRegAddr_m4b8[1]]==AppSfpI2cRegDataOut_m4b8[1]) else $error("p_AppSfp1I2c50Write failed");
p_AppSfp1I2c51Read       : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[1]) and ~AppSfpI2cAccModeWr_b4[1] and AppSfpI2cIntSel_b4[1]) |=> ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[1]) && i_AppSfp1I2c51.Mem_m[AppSfpI2cRegAddr_m4b8[1]]==AppSfpI2cRegDataOut_m4b8[1]) else $error("p_AppSfp1I2c50Write failed");

//APP SFP 2
    //Initialization
p_InitAppSfp2ModeDef0    : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpAppSfpModeDef0_b4[2]==~AppSfpPresent_b4[2]) else $error("AppSfpPresent_b4[2] not initialized");
p_InitAppSfp2Los         : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpAppSfpLos_b4[2]==AppSfpLos_b4[2]) else $error("AppSfpLos_b4[2] not initialized");
p_InitAppSfp2TxFault     : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpAppSfpTxFault_b4[2]==AppSfpTxFault_b4[2]) else $error("AppSfpTxFault_b4[2] not initialized");
p_InitAppSfp2RateSelect  : assert property (@(posedge Clk_k) $rose(InitDone) |->  (AppSfpRateSelect_b4[2]===StatusAppSfpRateSelect_b4[2] && AppSfpRateSelect_b4[2]===IoExpAppSfpRateSelect_b4[2])) else $error("AppSfpRateSelect_b4[2] not initialized after reset");
p_InitAppSfp2TxDisable   : assert property (@(posedge Clk_k) $rose(InitDone) |->  (AppSfpTxDisable_b4[2]===StatusAppSfpTxDisable_b4[2] && AppSfpTxDisable_b4[2]===IoExpAppSfpTxDisable_b4[2])) else $error("AppSfpTxDisable_b4[2] not initialized after reset");
    //Change Check
p_AppSfp2Id              : assert property (@(posedge Clk_k) $rose(AppSfpPresent_b4[2])  |-> AppSfpId_m4b16[2]===16'h02a2) else $error("Wrong ID detected for APP SFP2");
p_AppSfp2ModeDef0Check   : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpAppSfpModeDef0_b4[2]!=~AppSfpPresent_b4[2]) or $fell(I2cIoExpIntApp12_an) or $rose(IoExpAppSfpModeDef0_b4[2]!=$past(IoExpAppSfpModeDef0_b4[2]))) |=> ##[1:c_MaxServiceTime] IoExpAppSfpModeDef0_b4[2]==~AppSfpPresent_b4[2]) else $error("Missing AppSfpPresent_b4[2]");
p_AppSfp2LosCheck        : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpAppSfpLos_b4[2]!=AppSfpLos_b4[2]) or $fell(I2cIoExpIntLos_an) or $rose(IoExpAppSfpLos_b4[2]!=$past(IoExpAppSfpLos_b4[2]))) |=> ##[1:c_MaxServiceTime] IoExpAppSfpLos_b4[2]==AppSfpLos_b4[2]) else $error("Missing AppSfpLos_b4[2]");
p_AppSfp2TxFaultCheck    : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpAppSfpTxFault_b4[2]!=AppSfpTxFault_b4[2]) or $fell(I2cIoExpIntApp12_an) or $rose(IoExpAppSfpTxFault_b4[2]!=$past(IoExpAppSfpTxFault_b4[2]))) |=> ##[1:c_MaxServiceTime] IoExpAppSfpTxFault_b4[2]==AppSfpTxFault_b4[2]) else $error("Missing AppSfpTxFault_b4[2]");
p_AppSfp2RateSelectCheck : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpRateSelect_b4[2]!=StatusAppSfpRateSelect_b4[2]) or $rose(AppSfpRateSelect_b4[2]!=$past(AppSfpRateSelect_b4[2]))) |=> ##[1:c_MaxServiceTime] (AppSfpRateSelect_b4[2]===StatusAppSfpRateSelect_b4[2] && AppSfpRateSelect_b4[2]===IoExpAppSfpRateSelect_b4[2])) else $error("AppSfpRateSelect_b4[2] not updated in time or wrong value");
p_AppSfp2TxDisableCheck  : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpTxDisable_b4[2]!=StatusAppSfpTxDisable_b4[2]) or $rose(AppSfpTxDisable_b4[2]!=$past(AppSfpTxDisable_b4[2]))) |=> ##[1:c_MaxServiceTime] (AppSfpTxDisable_b4[2]===StatusAppSfpTxDisable_b4[2] && AppSfpTxDisable_b4[2]===IoExpAppSfpTxDisable_b4[2])) else $error("AppSfpTxDisable_b4[2] not updated in time or wrong value");
    //I2C accesses
p_AppSfp2I2c50Write      : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[2]) and AppSfpI2cAccModeWr_b4[2] and ~AppSfpI2cIntSel_b4[2]) ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[2]) |->  i_AppSfp2I2c50.Mem_m[AppSfpI2cRegAddr_m4b8[2]]==AppSfpI2cRegDataIn_m4b8[2]) else $error("p_AppSfp2I2c50Write failed");
p_AppSfp2I2c51Write      : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[2]) and AppSfpI2cAccModeWr_b4[2] and  AppSfpI2cIntSel_b4[2]) ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[2]) |-> i_AppSfp2I2c51.Mem_m[AppSfpI2cRegAddr_m4b8[2]]==AppSfpI2cRegDataIn_m4b8[2]) else $error("p_AppSfp2I2c50Write failed");
p_AppSfp2I2c50Read       : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[2]) and ~AppSfpI2cAccModeWr_b4[2] and ~AppSfpI2cIntSel_b4[2]) |=> ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[2]) && i_AppSfp2I2c50.Mem_m[AppSfpI2cRegAddr_m4b8[2]]==AppSfpI2cRegDataOut_m4b8[2]) else $error("p_AppSfp1I2c50Write failed");
p_AppSfp2I2c51Read       : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[2]) and ~AppSfpI2cAccModeWr_b4[2] and AppSfpI2cIntSel_b4[2]) |=> ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[2]) && i_AppSfp2I2c51.Mem_m[AppSfpI2cRegAddr_m4b8[2]]==AppSfpI2cRegDataOut_m4b8[2]) else $error("p_AppSfp1I2c50Write failed");

//APP SFP 3
    //Initialization
p_InitAppSfp3ModeDef0    : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpAppSfpModeDef0_b4[3]==~AppSfpPresent_b4[3]) else $error("AppSfpPresent_b4[3] not initialized");
p_InitAppSfp3Los         : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpAppSfpLos_b4[3]==AppSfpLos_b4[3]) else $error("AppSfpLos_b4[3] not initialized");
p_InitAppSfp3TxFault     : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpAppSfpTxFault_b4[3]==AppSfpTxFault_b4[3]) else $error("AppSfpTxFault_b4[3] not initialized");
p_InitAppSfp3RateSelect  : assert property (@(posedge Clk_k) $rose(InitDone) |->  (AppSfpRateSelect_b4[3]===StatusAppSfpRateSelect_b4[3] && AppSfpRateSelect_b4[3]===IoExpAppSfpRateSelect_b4[3])) else $error("AppSfpRateSelect_b4[3] not initialized after reset");
p_InitAppSfp3TxDisable   : assert property (@(posedge Clk_k) $rose(InitDone) |->  (AppSfpTxDisable_b4[3]===StatusAppSfpTxDisable_b4[3] && AppSfpTxDisable_b4[3]===IoExpAppSfpTxDisable_b4[3])) else $error("AppSfpTxDisable_b4[3] not initialized after reset");
    //Change Check
p_AppSfp3Id              : assert property (@(posedge Clk_k) $rose(AppSfpPresent_b4[3])  |-> AppSfpId_m4b16[3]===16'h03a3) else $error("Wrong ID detected for APP SFP3");
p_AppSfp3ModeDef0Check   : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpAppSfpModeDef0_b4[3]!=~AppSfpPresent_b4[3]) or $fell(I2cIoExpIntApp34_an) or $rose(IoExpAppSfpModeDef0_b4[3]!=$past(IoExpAppSfpModeDef0_b4[3]))) |=> ##[1:c_MaxServiceTime] IoExpAppSfpModeDef0_b4[3]==~AppSfpPresent_b4[3]) else $error("Missing AppSfpPresent_b4[3]");
p_AppSfp3LosCheck        : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpAppSfpLos_b4[3]!=AppSfpLos_b4[3]) or $fell(I2cIoExpIntLos_an) or $rose(IoExpAppSfpLos_b4[3]!=$past(IoExpAppSfpLos_b4[3]))) |=> ##[1:c_MaxServiceTime] IoExpAppSfpLos_b4[3]==AppSfpLos_b4[3]) else $error("Missing AppSfpLos_b4[3]");
p_AppSfp3TxFaultCheck    : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpAppSfpTxFault_b4[3]!=AppSfpTxFault_b4[3]) or $fell(I2cIoExpIntApp34_an) or $rose(IoExpAppSfpTxFault_b4[3]!=$past(IoExpAppSfpTxFault_b4[3]))) |=> ##[1:c_MaxServiceTime] IoExpAppSfpTxFault_b4[3]==AppSfpTxFault_b4[3]) else $error("Missing AppSfpTxFault_b4[3]");
p_AppSfp3RateSelectCheck : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpRateSelect_b4[3]!=StatusAppSfpRateSelect_b4[3]) or $rose(AppSfpRateSelect_b4[3]!=$past(AppSfpRateSelect_b4[3]))) |=> ##[1:c_MaxServiceTime] (AppSfpRateSelect_b4[3]===StatusAppSfpRateSelect_b4[3] && AppSfpRateSelect_b4[3]===IoExpAppSfpRateSelect_b4[3]))  else $error("AppSfpRateSelect_b4[3] not updated in time or wrong value");
p_AppSfp3TxDisableCheck  : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpTxDisable_b4[3]!=StatusAppSfpTxDisable_b4[3]) or $rose(AppSfpTxDisable_b4[3]!=$past(AppSfpTxDisable_b4[3]))) |=> ##[1:c_MaxServiceTime] (AppSfpTxDisable_b4[3]===StatusAppSfpTxDisable_b4[3] && AppSfpTxDisable_b4[3]===IoExpAppSfpTxDisable_b4[3])) else $error("AppSfpTxDisable_b4[3] not updated in time or wrong value");
    //I2C accesses
p_AppSfp3I2c50Write      : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[3]) and AppSfpI2cAccModeWr_b4[3] and ~AppSfpI2cIntSel_b4[3]) ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[3]) |-> i_AppSfp3I2c50.Mem_m[AppSfpI2cRegAddr_m4b8[3]]==AppSfpI2cRegDataIn_m4b8[3]) else $error("p_AppSfp3I2c50Write failed");
p_AppSfp3I2c51Write      : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[3]) and AppSfpI2cAccModeWr_b4[3] and  AppSfpI2cIntSel_b4[3]) ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[3]) |-> i_AppSfp3I2c51.Mem_m[AppSfpI2cRegAddr_m4b8[3]]==AppSfpI2cRegDataIn_m4b8[3]) else $error("p_AppSfp3I2c50Write failed");
p_AppSfp3I2c50Read       : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[3]) and ~AppSfpI2cAccModeWr_b4[3] and ~AppSfpI2cIntSel_b4[3]) |=> ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[3]) && i_AppSfp3I2c50.Mem_m[AppSfpI2cRegAddr_m4b8[3]]==AppSfpI2cRegDataOut_m4b8[3]) else $error("p_AppSfp1I2c50Write failed");
p_AppSfp3I2c51Read       : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[3]) and ~AppSfpI2cAccModeWr_b4[3] and  AppSfpI2cIntSel_b4[3]) |=> ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[3]) && i_AppSfp3I2c51.Mem_m[AppSfpI2cRegAddr_m4b8[3]]==AppSfpI2cRegDataOut_m4b8[3]) else $error("p_AppSfp1I2c50Write failed");

//APP SFP 4
    //Initialization
p_InitAppSfp4ModeDef0    : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpAppSfpModeDef0_b4[4]==~AppSfpPresent_b4[4]) else $error("AppSfpPresent_b4[4] not initialized");
p_InitAppSfp4Los         : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpAppSfpLos_b4[4]==AppSfpLos_b4[4]) else $error("AppSfpLos_b4[4] not initialized");
p_InitAppSfp4TxFault     : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpAppSfpTxFault_b4[4]==AppSfpTxFault_b4[4]) else $error("AppSfpTxFault_b4[4] not initialized");
p_InitAppSfp4RateSelect  : assert property (@(posedge Clk_k) $rose(InitDone) |->  (AppSfpRateSelect_b4[4]===StatusAppSfpRateSelect_b4[4] && AppSfpRateSelect_b4[4]===IoExpAppSfpRateSelect_b4[4])) else $error("AppSfpRateSelect_b4[4] not initialized after reset");
p_InitAppSfp4TxDisable   : assert property (@(posedge Clk_k) $rose(InitDone) |->  (AppSfpTxDisable_b4[4]===StatusAppSfpTxDisable_b4[4] && AppSfpTxDisable_b4[4]===IoExpAppSfpTxDisable_b4[4])) else $error("AppSfpTxDisable_b4[4] not initialized after reset");
    //Change Check
p_AppSfp4Id              : assert property (@(posedge Clk_k) $rose(AppSfpPresent_b4[4])  |-> AppSfpId_m4b16[4]===16'h04a4) else $error("Wrong ID detected for APP SFP4");
p_AppSfp4ModeDef0Check   : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpAppSfpModeDef0_b4[4]!=~AppSfpPresent_b4[4]) or $fell(I2cIoExpIntApp34_an) or $rose(IoExpAppSfpModeDef0_b4[4]!=$past(IoExpAppSfpModeDef0_b4[4]))) |=> ##[1:c_MaxServiceTime] IoExpAppSfpModeDef0_b4[4]==~AppSfpPresent_b4[4]) else $error("Missing AppSfpPresent_b4[4]");
p_AppSfp4LosCheck        : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpAppSfpLos_b4[4]!=AppSfpLos_b4[4]) or $fell(I2cIoExpIntLos_an) or $rose(IoExpAppSfpLos_b4[4]!=$past(IoExpAppSfpLos_b4[4]))) |=> ##[1:c_MaxServiceTime] IoExpAppSfpLos_b4[4]==AppSfpLos_b4[4]) else $error("Missing AppSfpLos_b4[4]");
p_AppSfp4TxFaultCheck    : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpAppSfpTxFault_b4[4]!=AppSfpTxFault_b4[4]) or $fell(I2cIoExpIntApp34_an) or $rose(IoExpAppSfpTxFault_b4[4]!=$past(IoExpAppSfpTxFault_b4[4]))) |=> ##[1:c_MaxServiceTime] IoExpAppSfpTxFault_b4[4]==AppSfpTxFault_b4[4]) else $error("Missing AppSfpTxFault_b4[4]");
p_AppSfp4RateSelectCheck : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpRateSelect_b4[4]!=StatusAppSfpRateSelect_b4[4]) or $rose(AppSfpRateSelect_b4[4]!=$past(AppSfpRateSelect_b4[4]))) |=> ##[1:c_MaxServiceTime] (AppSfpRateSelect_b4[4]===StatusAppSfpRateSelect_b4[4] && AppSfpRateSelect_b4[4]===IoExpAppSfpRateSelect_b4[4])) else $error("AppSfpRateSelect_b4[4] not updated in time or wrong value");
p_AppSfp4TxDisableCheck  : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpTxDisable_b4[4]!=StatusAppSfpTxDisable_b4[4]) or $rose(AppSfpTxDisable_b4[4]!=$past(AppSfpTxDisable_b4[4]))) |=> ##[1:c_MaxServiceTime] (AppSfpTxDisable_b4[4]===StatusAppSfpTxDisable_b4[4] && AppSfpTxDisable_b4[4]===IoExpAppSfpTxDisable_b4[4])) else $error("AppSfpTxDisable_b4[4] not updated in time or wrong value");
    //I2C accesses
p_AppSfp4I2c50Write      : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[4]) and AppSfpI2cAccModeWr_b4[4] and ~AppSfpI2cIntSel_b4[4]) ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[4]) |-> i_AppSfp4I2c50.Mem_m[AppSfpI2cRegAddr_m4b8[4]]==AppSfpI2cRegDataIn_m4b8[4]) else $error("p_AppSfp4I2c50Write failed");
p_AppSfp4I2c51Write      : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[4]) and AppSfpI2cAccModeWr_b4[4] and  AppSfpI2cIntSel_b4[4]) ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[4]) |->  i_AppSfp4I2c51.Mem_m[AppSfpI2cRegAddr_m4b8[4]]==AppSfpI2cRegDataIn_m4b8[4]) else $error("p_AppSfp4I2c50Write failed");
p_AppSfp4I2c50Read       : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[4]) and ~AppSfpI2cAccModeWr_b4[4] and ~AppSfpI2cIntSel_b4[4]) |=> ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[4]) && i_AppSfp4I2c50.Mem_m[AppSfpI2cRegAddr_m4b8[4]]==AppSfpI2cRegDataOut_m4b8[4]) else $error("p_AppSfp1I2c50Write failed");
p_AppSfp4I2c51Read       : assert property (@(posedge Clk_k) InitDone and ($rose(AppSfpI2cAccReq_b4[4]) and ~AppSfpI2cAccModeWr_b4[4] and AppSfpI2cIntSel_b4[4]) |=> ##[1:c_MaxServiceTime] $fell(AppSfpI2cAccGranted_b4[4]) && i_AppSfp4I2c51.Mem_m[AppSfpI2cRegAddr_m4b8[4]]==AppSfpI2cRegDataOut_m4b8[4]) else $error("p_AppSfp1I2c50Write failed");

//ETH SFP
    //Initialization
p_InitEthSfpModeDef0     : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpEthSfpModeDef0==~EthSfpPresent) else $error("EthSfpPresent not initialized");
p_InitEthSfpLos          : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpEthSfpLos==EthSfpLos) else $error("EthSfpLos not initialized");
p_InitEthSfpTxFault      : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpEthSfpTxFault==EthSfpTxFault) else $error("EthSfpTxFault not initialized");
p_InitEthSfpRateSelect   : assert property (@(posedge Clk_k) $rose(InitDone) |->  (EthSfpRateSelect===StatusEthSfpRateSelect && EthSfpRateSelect===IoExpEthSfpRateSelect)) else $error("EthSfpRateSelect not initialized after reset");
p_InitEthSfpTxDisable    : assert property (@(posedge Clk_k) $rose(InitDone) |->  (EthSfpTxDisable===StatusEthSfpTxDisable && EthSfpTxDisable===IoExpEthSfpTxDisable)) else $error("EthSfpTxDisable not initialized after reset");
   //Change Check
p_EthSfpId               : assert property (@(posedge Clk_k) $rose(EthSfpPresent)  |-> EthSfpId_b16===16'h0ee0) else $error("Wrong ID detected for Eth SFP");
p_EthSfpModeDef0Check    : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpEthSfpModeDef0!=~EthSfpPresent) or $fell(I2cIoExpIntBstEth_an) or $rose(IoExpEthSfpModeDef0!=$past(IoExpEthSfpModeDef0))) |=> ##[1:c_MaxServiceTime] IoExpEthSfpModeDef0==~EthSfpPresent) else $error("Missing EthSfpPresent");
p_EthSfpLosCheck         : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpEthSfpLos!=EthSfpLos) or $fell(I2cIoExpIntLos_an) or $rose(IoExpEthSfpLos!=$past(IoExpEthSfpLos))) |=> ##[1:c_MaxServiceTime] IoExpEthSfpLos==EthSfpLos) else $error("Missing EthSfpLos");
p_EthSfpTxFaultCheck     : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpEthSfpTxFault!=EthSfpTxFault) or $fell(I2cIoExpIntBstEth_an) or $rose(IoExpEthSfpTxFault!=$past(IoExpEthSfpTxFault))) |=> ##[1:c_MaxServiceTime] IoExpEthSfpTxFault==EthSfpTxFault) else $error("Missing EthSfpTxFault");
p_EthSfpRateSelectCheck  : assert property (@(posedge Clk_k) InitDone and ($rose(EthSfpRateSelect!=StatusEthSfpRateSelect) or $rose(EthSfpRateSelect!=$past(EthSfpRateSelect))) |=> ##[1:c_MaxServiceTime] (EthSfpRateSelect===StatusEthSfpRateSelect && EthSfpRateSelect===IoExpEthSfpRateSelect)) else $error("EthSfpRateSelect not updated in time or wrong value");
p_EthSfpTxDisableCheck   : assert property (@(posedge Clk_k) InitDone and ($rose(EthSfpTxDisable!=StatusEthSfpTxDisable) or $rose(EthSfpTxDisable!=$past(EthSfpTxDisable))) |=> ##[1:c_MaxServiceTime] (EthSfpTxDisable===StatusEthSfpTxDisable && EthSfpTxDisable===IoExpEthSfpTxDisable)) else $error("EthSfpTxDisable not updated in time or wrong value");

//BST SFP
    //Initialization
p_InitBstSfpModeDef0     : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpBstSfpModeDef0==~BstSfpPresent) else $error("BstSfpPresent not initialized");
p_InitBstSfpLos          : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpBstSfpLos==BstSfpLos) else $error("BstSfpLos not initialized");
p_InitBstSfpTxFault      : assert property (@(posedge Clk_k) $rose(InitDone) |->  IoExpBstSfpTxFault==BstSfpTxFault) else $error("BstSfpTxFault not initialized");
p_InitBstSfpRateSelect   : assert property (@(posedge Clk_k) $rose(InitDone) |->  (BstSfpRateSelect===StatusBstSfpRateSelect && BstSfpRateSelect===IoExpBstSfpRateSelect)) else $error("BstSfpRateSelect not initialized after reset");
p_InitBstSfpTxDisable    : assert property (@(posedge Clk_k) $rose(InitDone) |->  (BstSfpTxDisable===StatusBstSfpTxDisable && BstSfpTxDisable===IoExpBstSfpTxDisable)) else $error("BstSfpTxDisable not initialized after reset");
  //Change Check
p_BstSfpId               : assert property (@(posedge Clk_k) $rose(BstSfpPresent)  |-> BstSfpId_b16===16'h0bb0) else $error("Wrong ID detected for BST SFP");
p_BstSfpModeDef0Check    : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpBstSfpModeDef0!=~BstSfpPresent) or $fell(I2cIoExpIntBstEth_an) or $rose(IoExpBstSfpModeDef0!=$past(IoExpBstSfpModeDef0))) |=> ##[1:c_MaxServiceTime] IoExpBstSfpModeDef0==~BstSfpPresent) else $error("Missing BstSfpPresent");
p_BstSfpLosCheck         : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpBstSfpLos!=BstSfpLos) or $fell(I2cIoExpIntLos_an) or $rose(IoExpBstSfpLos!=$past(IoExpBstSfpLos))) |=> ##[1:c_MaxServiceTime] IoExpBstSfpLos==BstSfpLos) else $error("Missing BstSfpLos");
p_BstSfpTxFaultCheck     : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpBstSfpTxFault!=BstSfpTxFault) or $fell(I2cIoExpIntBstEth_an) or $rose(IoExpBstSfpTxFault!=$past(IoExpBstSfpTxFault))) |=> ##[1:c_MaxServiceTime] IoExpBstSfpTxFault==BstSfpTxFault) else $error("Missing BstSfpTxFault");
p_BstSfpRateSelectCheck  : assert property (@(posedge Clk_k) InitDone and ($rose(BstSfpRateSelect!=StatusBstSfpRateSelect) or $rose(BstSfpRateSelect!=$past(BstSfpRateSelect))) |=> ##[1:c_MaxServiceTime] (BstSfpRateSelect===StatusBstSfpRateSelect && BstSfpRateSelect===IoExpBstSfpRateSelect)) else $error("BstSfpRateSelect not updated in time or wrong value");
p_BstSfpTxDisableCheck   : assert property (@(posedge Clk_k) InitDone and ($rose(BstSfpTxDisable!=StatusBstSfpTxDisable) or $rose(BstSfpTxDisable!=$past(BstSfpTxDisable))) |=> ##[1:c_MaxServiceTime] (BstSfpTxDisable===StatusBstSfpTxDisable && BstSfpTxDisable===IoExpBstSfpTxDisable)) else $error("BstSfpTxDisable not updated in time or wrong value");

//CDR
    //Initialization
p_InitCdrLol             : assert property (@(posedge Clk_k) $rose(InitDone) |-> ##[1:c_MaxServiceTime] IoExpCdrLol==CdrLol) else $error("CdrLol not initialized");
p_InitCdrLos             : assert property (@(posedge Clk_k) $rose(InitDone) |-> ##[1:c_MaxServiceTime] IoExpCdrLos==CdrLos) else $error("CdrLos not initialized");
    //Change Check
p_CdrLolCheck            : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpCdrLol!=CdrLol) or $fell(I2cIoExpIntLos_an) or $rose(IoExpCdrLol!=$past(IoExpCdrLol))) |=> ##[1:c_MaxServiceTime] IoExpCdrLol==CdrLol) else $error("Missing CdrLol");
p_CdrLosCheck            : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpCdrLos!=CdrLos) or $fell(I2cIoExpIntLos_an) or $rose(IoExpCdrLos!=$past(IoExpCdrLos))) |=> ##[1:c_MaxServiceTime] IoExpCdrLos==CdrLos) else $error("Missing CdrLos");

//BLM IN
    //Initialization
p_InitBlmIn              : assert property (@(posedge Clk_k) $rose(InitDone) |-> ##[1:c_MaxServiceTime] IoExpBlmIn_b8==BlmIn_b8) else $error("BlmIn not initialized");
    //Change Check
p_BlmInCheck             : assert property (@(posedge Clk_k) InitDone and ($rose(IoExpBlmIn_b8!=BlmIn_b8) or $fell(I2cIoExpIntBlmIn_an) or $rose(IoExpBlmIn_b8!=$past(IoExpBlmIn_b8))) |=> ##[1:c_MaxServiceTime] IoExpBlmIn_b8==BlmIn_b8) else $error("Missing BlmInUpdate");
    //Change Latency Determinism
p_BlmInLatencyDet        : assert property (@(posedge Clk_k) InitDone and i_I2cExpAndMuxReqArbiter.a_BlmInI2cIntTimeOutEnable and $rose(IoExpBlmIn_b8!=$past(IoExpBlmIn_b8)) |=> $stable(BlmIn_b8)[*(250_000+23_880)] ##1 IoExpBlmIn_b8==BlmIn_b8) else $error("BlmIn Latency not fixed"); //Comment: fixed but should be function of the actual mute delay


initial begin
    #100; //Comment: avoiding conflicts with the module default initialization
    i_AppSfp1I2c50.Mem_m[0] = 8'ha1;
    i_AppSfp1I2c50.Mem_m[1] = 8'h01;
    i_AppSfp2I2c50.Mem_m[0] = 8'ha2;
    i_AppSfp2I2c50.Mem_m[1] = 8'h02;
    i_AppSfp3I2c50.Mem_m[0] = 8'ha3;
    i_AppSfp3I2c50.Mem_m[1] = 8'h03;
    i_AppSfp4I2c50.Mem_m[0] = 8'ha4;
    i_AppSfp4I2c50.Mem_m[1] = 8'h04;
    i_EthSfpI2c50.Mem_m[0] = 8'he0;
    i_EthSfpI2c50.Mem_m[1] = 8'h0e;
    i_BstSfpI2c50.Mem_m[0] = 8'hb0;
    i_BstSfpI2c50.Mem_m[1] = 8'h0b;
end


//==== Clocks generation ====//
always #4 Clk_k     = ~Clk_k; // Comment: 8ns period (125MHz)

//=== Test logic: board model ====//

pca9544a i_Ic35(
    .A_ib3(3'b000),
    .Int_on(/*Not connected*/),
    .Scl_io(I2cMuxScl),
    .Sda_io(I2cMuxSda),
    .Int0_in(WireR320),
    .Sc0_io(AppSfp1ModeDef1),
    .Sd0_io(AppSfp1ModeDef2),
    .Int1_in(WireR320),
    .Sc1_io(AppSfp2ModeDef1),
    .Sd1_io(AppSfp2ModeDef2),
    .Int2_in(WireR320),
    .Sc2_io(AppSfp3ModeDef1),
    .Sd2_io(AppSfp3ModeDef2),
    .Int3_in(WireR320),
    .Sc3_io(AppSfp4ModeDef1),
    .Sd3_io(AppSfp4ModeDef2));

pullup i_R320(WireR320);

pullup i_PuAffSfp1Scl(AppSfp1ModeDef1);
pullup i_PuAffSfp1Sda(AppSfp1ModeDef2);

tranif0 AppSfp1Scl(AppSfp1ModeDef1, SfpAppSfp1ModeDef1, 1'b0); //IoExpAppSfpModeDef0_b4[1]);
tranif0 AppSfp1Sda(AppSfp1ModeDef2, SfpAppSfp1ModeDef2, 1'b0); //IoExpAppSfpModeDef0_b4[1]);

I2CSlave #(.g_Address(7'h50)) // Comment: SFP I2c interfaces use 2 addresses
    i_AppSfp1I2c50(
        .Scl_io(SfpAppSfp1ModeDef1),
        .Sda_io(SfpAppSfp1ModeDef2));

I2CSlave #(.g_Address(7'h51))   // Comment: SFP I2c interfaces use 2 addresses
    i_AppSfp1I2c51(
        .Scl_io(SfpAppSfp1ModeDef1),
        .Sda_io(SfpAppSfp1ModeDef2));

pullup i_PuAffSfp2Scl(AppSfp2ModeDef1);
pullup i_PuAffSfp2Sda(AppSfp2ModeDef2);

tranif0 AppSfp2Scl(AppSfp2ModeDef1, SfpAppSfp2ModeDef1, 1'b0); //IoExpAppSfpModeDef0_b4[2]);
tranif0 AppSfp2Sda(AppSfp2ModeDef2, SfpAppSfp2ModeDef2, 1'b0); //IoExpAppSfpModeDef0_b4[2]);

I2CSlave #(.g_Address(7'h50)) // Comment: SFP I2c interfaces use 2 addresses
    i_AppSfp2I2c50(
        .Scl_io(SfpAppSfp2ModeDef1),
        .Sda_io(SfpAppSfp2ModeDef2));

I2CSlave #(.g_Address(7'h51)) // Comment: SFP I2c interfaces use 2 addresses
    i_AppSfp2I2c51(
        .Scl_io(SfpAppSfp2ModeDef1),
        .Sda_io(SfpAppSfp2ModeDef2));

pullup i_PuAffSfp3Scl(AppSfp3ModeDef1);
pullup i_PuAffSfp3Sda(AppSfp3ModeDef2);

tranif0 AppSfp3Scl(AppSfp3ModeDef1, SfpAppSfp3ModeDef1, 1'b0); //IoExpAppSfpModeDef0_b4[3]);
tranif0 AppSfp3Sda(AppSfp3ModeDef2, SfpAppSfp3ModeDef2, 1'b0); //IoExpAppSfpModeDef0_b4[3]);

I2CSlave #(.g_Address(7'h50)) // Comment: SFP I2c interfaces use 2 addresses
    i_AppSfp3I2c50(
        .Scl_io(SfpAppSfp3ModeDef1),
        .Sda_io(SfpAppSfp3ModeDef2));

I2CSlave #(.g_Address(7'h51)) // Comment: SFP I2c interfaces use 2 addresses
    i_AppSfp3I2c51(
        .Scl_io(SfpAppSfp3ModeDef1),
        .Sda_io(SfpAppSfp3ModeDef2));

pullup i_PuAffSfp4Scl(AppSfp4ModeDef1);
pullup i_PuAffSfp4Sda(AppSfp4ModeDef2);

tranif0 AppSfp4Scl(AppSfp4ModeDef1, SfpAppSfp4ModeDef1, 1'b0); //IoExpAppSfpModeDef0_b4[4]);
tranif0 AppSfp4Sda(AppSfp4ModeDef2, SfpAppSfp4ModeDef2, 1'b0); //IoExpAppSfpModeDef0_b4[4]);

I2CSlave #(.g_Address(7'h50)) // Comment: SFP I2c interfaces use 2 addresses
    i_AppSfp4I2c50(
        .Scl_io(SfpAppSfp4ModeDef1),
        .Sda_io(SfpAppSfp4ModeDef2));

I2CSlave #(.g_Address(7'h51)) // Comment: SFP I2c interfaces use 2 addresses
    i_AppSfp4I2c51(
        .Scl_io(SfpAppSfp4ModeDef1),
        .Sda_io(SfpAppSfp4ModeDef2));

pca9544a i_Ic2(
    .A_ib3(3'b001),
    .Int_on(/*Not connected*/),
    .Scl_io(I2cMuxScl),
    .Sda_io(I2cMuxSda),
    .Int0_in(WireR321),
    .Sc0_io(BstSfpModeDef1),
    .Sd0_io(BstSfpModeDef2),
    .Int1_in(WireR321),
    .Sc1_io(EthSfpModeDef1),
    .Sd1_io(EthSfpModeDef2),
    .Int2_in(WireR321),
    .Sc2_io(Si57xScl),
    .Sd2_io(Si57xSda),
    .Int3_in(WireR321),
    .Sc3_io(CdrScl),
    .Sd3_io(CdrSda));

pullup i_R321(WireR321);

pullup i_PuBstSfpScl(BstSfpModeDef1);
pullup i_PuBstSfpSda(BstSfpModeDef2);

tranif0 BstSfpScl(BstSfpModeDef1, SfpBstSfpModeDef1, 1'b0); //IoExpBstSfpModeDef0);
tranif0 BstSfpSda(BstSfpModeDef2, SfpBstSfpModeDef2, 1'b0); // IoExpBstSfpModeDef0);

I2CSlave #(.g_Address(7'h50)) // Comment: SFP I2c interfaces use 2 addresses
    i_BstSfpI2c50(
        .Scl_io(SfpBstSfpModeDef1),
        .Sda_io(SfpBstSfpModeDef2));

I2CSlave #(.g_Address(7'h51)) // Comment: SFP I2c interfaces use 2 addresses
    i_BstSfpI2c51(
        .Scl_io(SfpBstSfpModeDef1),
        .Sda_io(SfpBstSfpModeDef2));

pullup i_PuEthSfpScl(EthSfpModeDef1);
pullup i_PuEthSfpSda(EthSfpModeDef2);

tranif0 EthSfpScl(EthSfpModeDef1, SfpEthSfpModeDef1, 1'b0); //IoExpEthSfpModeDef0);
tranif0 EthSfpSda(EthSfpModeDef2, SfpEthSfpModeDef2, 1'b0); //IoExpEthSfpModeDef0);

I2CSlave #(.g_Address(7'h50)) // Comment: SFP I2c interfaces use 2 addresses
    i_EthSfpI2c50(
        .Scl_io(SfpEthSfpModeDef1),
        .Sda_io(SfpEthSfpModeDef2));

I2CSlave #(.g_Address(7'h51)) // Comment: SFP I2c interfaces use 2 addresses
    i_EthSfpI2c51(
        .Scl_io(SfpEthSfpModeDef1),
        .Sda_io(SfpEthSfpModeDef2));

pullup i_PuSi57xScl(Si57xScl);
pullup i_PuSi57xSda(Si57xSda);

I2CSlave #(.g_Address(7'h55))
    i_Si57xI2c(
        .Scl_io(Si57xScl),
        .Sda_io(Si57xSda));

pullup i_PuCdrScl(CdrScl);
pullup i_PuCdrSda(CdrSda);

I2CSlave #(.g_Address(7'h40))
    i_CdrI2c(
        .Scl_io(CdrScl),
        .Sda_io(CdrSda));

wire [7:0] Ic21Io_b8 = {
    IoExpAppSfpModeDef0_b4[2],
    IoExpAppSfpRateSelect_b4[2],
    IoExpAppSfpTxDisable_b4[2],
    IoExpAppSfpTxFault_b4[2],
    IoExpAppSfpModeDef0_b4[1],
    IoExpAppSfpRateSelect_b4[1],
    IoExpAppSfpTxDisable_b4[1],
    IoExpAppSfpTxFault_b4[1]};

pca9534 i_Ic21(
    .A_ib3(3'b000),
    .IO_iob8(Ic21Io_b8),
    .Int_on (I2cIoExpIntApp12_an),
    .Scl_io(I2cMuxScl),
    .Sda_io(I2cMuxSda)
);

pullup i_PuInternalIntIc21(I2cIoExpIntApp12_an); //Internal!!!

wire [7:0] Ic36Io_b8 = {
    IoExpAppSfpModeDef0_b4[4],
    IoExpAppSfpRateSelect_b4[4],
    IoExpAppSfpTxDisable_b4[4],
    IoExpAppSfpTxFault_b4[4],
    IoExpAppSfpModeDef0_b4[3],
    IoExpAppSfpRateSelect_b4[3],
    IoExpAppSfpTxDisable_b4[3],
    IoExpAppSfpTxFault_b4[3]};

pca9534 i_Ic36(
    .A_ib3(3'b001),
    .IO_iob8(Ic36Io_b8),
    .Int_on (I2cIoExpIntApp34_an),
    .Scl_io(I2cMuxScl),
    .Sda_io(I2cMuxSda)
);

pullup i_PuInternalIntIc36(I2cIoExpIntApp34_an); //Internal!!!


wire [7:0] Ic4Io_b8 = {
    IoExpEthSfpModeDef0,
    IoExpEthSfpRateSelect,
    IoExpEthSfpTxDisable,
    IoExpEthSfpTxFault,
    IoExpBstSfpModeDef0,
    IoExpBstSfpRateSelect,
    IoExpBstSfpTxDisable,
    IoExpBstSfpTxFault};

pca9534 i_Ic4(
    .A_ib3(3'b010),
    .IO_iob8(Ic4Io_b8),
    .Int_on (I2cIoExpIntBstEth_an),
    .Scl_io(I2cMuxScl),
    .Sda_io(I2cMuxSda)
);

pullup i_PuInternalIntIc4(I2cIoExpIntBstEth_an); //Internal!!!

assign P1P2Ga_nb5 = ~c_VmeSlot;
assign P1P2Gap_n = ~^(~c_VmeSlot);

pca9534 i_Ic15(
    .A_ib3(3'b011),
    .IO_iob8({  Float_z,
                Float_z,
                P1P2Gap_n,
                P1P2Ga_nb5[4],
                P1P2Ga_nb5[3],
                P1P2Ga_nb5[2],
                P1P2Ga_nb5[1],
                P1P2Ga_nb5[0]}),
    .Int_on(/*Not connected*/),
    .Scl_io(I2cMuxScl),
    .Sda_io(I2cMuxSda)
);

pca9534 i_Ic22(
    .A_ib3(3'b100),
    .IO_iob8({  Float_z,
                IoExpEnTermGpIo4,
                IoExpEnTermGpIo3,
                IoExpEnTermGpIo2,
                IoExpEnTermGpIo1,
                IoExpGpIo34A2B,
                IoExpGpIo2A2B,
                IoExpGpIo1A2B}),
    .Int_on(/*Not connected*/),
    .Scl_io(I2cMuxScl),
    .Sda_io(I2cMuxSda)
);

pca9534 i_Ic3(
    .A_ib3(3'b101),
    .IO_iob8(IoExpFpLed_nb8),
    .Int_on(/*Not connected*/),
    .Scl_io(I2cMuxScl),
    .Sda_io(I2cMuxSda)
);

wire [7:0] a_IoExpBlmIn_b8 = IoExpBlmIn_b8;

pca9534 i_Ic28(
    .A_ib3(3'b110),
    .IO_iob8(a_IoExpBlmIn_b8),
    .Int_on(I2cIoExpIntBlmIn_an),
    .Scl_io(I2cMuxScl),
    .Sda_io(I2cMuxSda)
);

pullup i_PuInternalIntIc28(I2cIoExpIntBlmIn_an); //Internal!!!


wire [7:0] Ic46Io_b8 = {IoExpCdrLol, IoExpCdrLos, IoExpEthSfpLos, IoExpBstSfpLos, IoExpAppSfpLos_b4};

pca9534 i_Ic46(
    .A_ib3(3'b111),
    .IO_iob8(Ic46Io_b8),
    .Int_on(I2cIoExpIntLos_an),
    .Scl_io(I2cMuxScl),
    .Sda_io(I2cMuxSda)
);

pullup i_PuInternalIntIc46(I2cIoExpIntLos_an); //Internal!!!


//==== Test Logic: Wishbone Bus model ====//

WbMasterSim i_WbBus(
    .Rst_orq(Rst_rq),
    .Clk_ik(Clk_k),
    .Adr_obq32(WbAdr_b32),
    .Dat_obq32(WbDatMoSi_b32),
    .Dat_ib32(WbDatMiSo_b32),
    .We_oq(WbWe),
    .Cyc_oq(WbCyc),
    .Stb_oq(WbStb),
    .Ack_i(WbAck));

//==== DUT ====//

I2cExpAndMuxReqArbiter i_I2cExpAndMuxReqArbiter(
    //==== Clocks & Resets ====//
    .Clk_ik(Clk_k),
    .Rst_irq(Rst_rq),
    //==== Master interface ====//
    // IO Expanders parameters:
    .IoExpWrReq_oq(IoExpWrReq),
    .IoExpWrOn_i(IoExpWrOn),
    .IoExpRdReq_oq(IoExpRdReq),
    .IoExpRdOn_i(IoExpRdOn),
    .IoExpAddr_oqb3(IoExpAddr_b3),
    .IoExpRegAddr_oqb2(IoExpRegAddr_b2),
    .IoExpData_oqb8(IoExpData_b8),
    // I2c Mux parameters:
    .I2cSlaveWrReq_oq(I2CSlaveWrReq),
    .I2cSlaveWrOn_i(I2CSlaveWrOn),
    .I2cSlaveRdReq_oq(I2CSlaveRdReq),
    .I2cSlaveRdOn_i(I2CSlaveRdOn),
    .I2cMuxAddress_oq(I2cMuxAddress),
    .I2cMuxChannel_oqb2(I2cMuxChannel_b2),
    .I2cSlaveAddr_oqb7(I2CSlaveAddr_b7),
    .I2cSlaveRegAddr_oqb8(I2CSlaveRegAddr_b8),
    .I2cSlaveByte_oqb8(I2CSlaveByte_b8),
    // Status and results:
    .MasterBusy_i(I2cMasterBusy),
    .MasterNewByteRead_ip(I2cMasterNewByteRead),
    .MasterByteOut_ib8(I2cMasterByteOut_b8),
    .MasterAckError_i(I2cMasterAckError),
    //==== I2c IO expander interrupts ====//
    .IoExpApp12Int_ian(I2cIoExpIntApp12_an),
    .IoExpApp34Int_ian(I2cIoExpIntApp34_an),
    .IoExpBstEthInt_ian(I2cIoExpIntBstEth_an),
    .IoExpLosInt_ian(I2cIoExpIntLos_an),
    .IoExpBlmInInt_ian(I2cIoExpIntBlmIn_an),
    //==== System and Application Interface ====//
    .InitDone_oq(InitDone),
    // Vme Ga and GaP:
    .VmeGa_onqb5(VmeGa_nqb5),
    .VmeGaP_onq(VmeGaP_nq),
    // Leds:
    .Led_iab8(Led_b8),
    .StatusLed_ob8(StatusLed_b8),
    // GpIo:
    .GpIo1A2B_ia(GpIo1A2B),
    .EnGpIo1Term_ia(EnTermGpIo1),
    .GpIo2A2B_ia(GpIo2A2B),
    .EnGpIo2Term_ia(EnTermGpIo2),
    .GpIo34A2B_ia(GpIo34A2B),
    .EnGpIo3Term_ia(EnTermGpIo3),
    .EnGpIo4Term_ia(EnTermGpIo4),
    .StatusGpIo1A2B_oq(StatusGpIo1A2B),
    .StatusEnGpIo1Term_oq(StatusEnTermGpIo1),
    .StatusGpIo2A2B_oq(StatusGpIo2A2B),
    .StatusEnGpIo2Term_oq(StatusEnTermGpIo2),
    .StatusGpIo34A2B_oq(StatusGpIo34A2B),
    .StatusEnGpIo3Term_oq(StatusEnTermGpIo3),
    .StatusEnGpIo4Term_oq(StatusEnTermGpIo4),
    // BlmIn:
    .BlmIn_oqb8(BlmIn_b8),
    // AppSfp1:
    .AppSfp1Present_oq(AppSfpPresent_b4[1]),
    // .AppSfp1Id_oq16(AppSfpId_m4b16[1]),
    .AppSfp1TxFault_oq(AppSfpTxFault_b4[1]),
    .AppSfp1Los_oq(AppSfpLos_b4[1]),
    .AppSfp1TxDisable_ia(AppSfpTxDisable_b4[1]),
    .AppSfp1RateSelect_ia(AppSfpRateSelect_b4[1]),
    .StatusAppSfp1TxDisable_oq(StatusAppSfpTxDisable_b4[1]),
    .StatusAppSfp1RateSelect_oq(StatusAppSfpRateSelect_b4[1]),
    // .AppSfp1I2cAccReq_i(AppSfpI2cAccReq_b4[1]),
    // .AppSfp1I2cAccGranted_oq(AppSfpI2cAccGranted_b4[1]),
    // .AppSfp1I2cIntSel_i(AppSfpI2cIntSel_b4[1]),  //Comment: 0 for int 0x50 1 for int 0x51
    // .AppSfp1I2cAccModeWr_i(AppSfpI2cAccModeWr_b4[1]),
    // .AppSfp1I2cRegAddr_ib8(AppSfpI2cRegAddr_m4b8[1]),
    // .AppSfp1I2cRegData_ib8(AppSfpI2cRegDataIn_m4b8[1]),
    // .AppSfp1I2cRegData_ob8(AppSfpI2cRegDataOut_m4b8[1]),
    // .AppSfp1I2cRdDataDav_op(AppSfpI2cRdDataDav_b4[1]),
    // AppSfp2:
    .AppSfp2Present_oq(AppSfpPresent_b4[2]),
    // .AppSfp2Id_oq16(AppSfpId_m4b16[2]),
    .AppSfp2TxFault_oq(AppSfpTxFault_b4[2]),
    .AppSfp2Los_oq(AppSfpLos_b4[2]),
    .AppSfp2TxDisable_ia(AppSfpTxDisable_b4[2]),
    .AppSfp2RateSelect_ia(AppSfpRateSelect_b4[2]),
    .StatusAppSfp2TxDisable_oq(StatusAppSfpTxDisable_b4[2]),
    .StatusAppSfp2RateSelect_oq(StatusAppSfpRateSelect_b4[2]),
    // .AppSfp2I2cAccReq_i(AppSfpI2cAccReq_b4[2]),
    // .AppSfp2I2cAccGranted_oq(AppSfpI2cAccGranted_b4[2]),
    // .AppSfp2I2cIntSel_i(AppSfpI2cIntSel_b4[2]),  //Comment: 0 for int 0x50 1 for int 0x51
    // .AppSfp2I2cAccModeWr_i(AppSfpI2cAccModeWr_b4[2]),
    // .AppSfp2I2cRegAddr_ib8(AppSfpI2cRegAddr_m4b8[2]),
    // .AppSfp2I2cRegData_ib8(AppSfpI2cRegDataIn_m4b8[2]),
    // .AppSfp2I2cRegData_ob8(AppSfpI2cRegDataOut_m4b8[2]),
    // .AppSfp2I2cRdDataDav_op(AppSfpI2cRdDataDav_b4[2]),
    // AppSfp3:
    .AppSfp3Present_oq(AppSfpPresent_b4[3]),
    // .AppSfp3Id_oq16(AppSfpId_m4b16[3]),
    .AppSfp3TxFault_oq(AppSfpTxFault_b4[3]),
    .AppSfp3Los_oq(AppSfpLos_b4[3]),
    .AppSfp3TxDisable_ia(AppSfpTxDisable_b4[3]),
    .AppSfp3RateSelect_ia(AppSfpRateSelect_b4[3]),
    .StatusAppSfp3TxDisable_oq(StatusAppSfpTxDisable_b4[3]),
    .StatusAppSfp3RateSelect_oq(StatusAppSfpRateSelect_b4[3]),
    // .AppSfp3I2cAccReq_i(AppSfpI2cAccReq_b4[3]),
    // .AppSfp3I2cAccGranted_oq(AppSfpI2cAccGranted_b4[3]),
    // .AppSfp3I2cIntSel_i(AppSfpI2cIntSel_b4[3]),  //Comment: 0 for int 0x50 1 for int 0x51
    // .AppSfp3I2cAccModeWr_i(AppSfpI2cAccModeWr_b4[3]),
    // .AppSfp3I2cRegAddr_ib8(AppSfpI2cRegAddr_m4b8[3]),
    // .AppSfp3I2cRegData_ib8(AppSfpI2cRegDataIn_m4b8[3]),
    // .AppSfp3I2cRegData_ob8(AppSfpI2cRegDataOut_m4b8[3]),
    // .AppSfp3I2cRdDataDav_op(AppSfpI2cRdDataDav_b4[3]),
    // AppSfp4:
    .AppSfp4Present_oq(AppSfpPresent_b4[4]),
    // .AppSfp4Id_oq16(AppSfpId_m4b16[4]),
    .AppSfp4TxFault_oq(AppSfpTxFault_b4[4]),
    .AppSfp4Los_oq(AppSfpLos_b4[4]),
    .AppSfp4TxDisable_ia(AppSfpTxDisable_b4[4]),
    .AppSfp4RateSelect_ia(AppSfpRateSelect_b4[4]),
    .StatusAppSfp4TxDisable_oq(StatusAppSfpTxDisable_b4[4]),
    .StatusAppSfp4RateSelect_oq(StatusAppSfpRateSelect_b4[4]),
    // .AppSfp4I2cAccReq_i(AppSfpI2cAccReq_b4[4]),
    // .AppSfp4I2cAccGranted_oq(AppSfpI2cAccGranted_b4[4]),
    // .AppSfp4I2cIntSel_i(AppSfpI2cIntSel_b4[4]),  //Comment: 0 for int 0x50 1 for int 0x51
    // .AppSfp4I2cAccModeWr_i(AppSfpI2cAccModeWr_b4[4]),
    // .AppSfp4I2cRegAddr_ib8(AppSfpI2cRegAddr_m4b8[4]),
    // .AppSfp4I2cRegData_ib8(AppSfpI2cRegDataIn_m4b8[4]),
    // .AppSfp4I2cRegData_ob8(AppSfpI2cRegDataOut_m4b8[4]),
    // .AppSfp4I2cRdDataDav_op(AppSfpI2cRdDataDav_b4[4]),
    // BstSfp:
    .BstSfpPresent_oq(BstSfpPresent),
    // .BstSfpId_oq16(BstSfpId_b16),
    .BstSfpTxFault_oq(BstSfpTxFault),
    .BstSfpLos_oq(BstSfpLos),
    .BstSfpTxDisable_ia(BstSfpTxDisable),
    .BstSfpRateSelect_ia(BstSfpRateSelect),
    .StatusBstSfpTxDisable_oq(StatusBstSfpTxDisable),
    .StatusBstSfpRateSelect_oq(StatusBstSfpRateSelect),
    // .BstSfpI2cAccReq_i(BstSfpI2cAccReq),
    // .BstSfpI2cAccGranted_oq(BstSfpI2cAccGranted),
    // .BstSfpI2cIntSel_i(BstSfpI2cIntSel),  //Comment: 0 for int 0x50 1 for int 0x51
    // .BstSfpI2cAccModeWr_i(BstSfpI2cAccModeWr),
    // .BstSfpI2cRegAddr_ib8(BstSfpI2cRegAddr_b8),
    // .BstSfpI2cRegData_ib8(BstSfpI2cRegDataIn_b8),
    // .BstSfpI2cRegData_ob8(BstSfpI2cRegDataOut_b8),
    // .BstSfpI2cRdDataDav_op(BstSfpI2cRdDataDav),
    // EthSfp:
    .EthSfpPresent_oq(EthSfpPresent),
    // .EthSfpId_oq16(EthSfpId_b16),
    .EthSfpTxFault_oq(EthSfpTxFault),
    .EthSfpLos_oq(EthSfpLos),
    .EthSfpTxDisable_ia(EthSfpTxDisable),
    .EthSfpRateSelect_ia(EthSfpRateSelect),
    .StatusEthSfpTxDisable_oq(StatusEthSfpTxDisable),
    .StatusEthSfpRateSelect_oq(StatusEthSfpRateSelect),
    // .EthSfpI2cAccReq_i(EthSfpI2cAccReq),
    // .EthSfpI2cAccGranted_oq(EthSfpI2cAccGranted),
    // .EthSfpI2cIntSel_i(EthSfpI2cIntSel),  //Comment: 0 for int 0x50 1 for int 0x51
    // .EthSfpI2cAccModeWr_i(EthSfpI2cAccModeWr),
    // .EthSfpI2cRegAddr_ib8(EthSfpI2cRegAddr_b8),
    // .EthSfpI2cRegData_ib8(EthSfpI2cRegDataIn_b8),
    // .EthSfpI2cRegData_ob8(EthSfpI2cRegDataOut_b8),
    // .EthSfpI2cRdDataDav_op(EthSfpI2cRdDataDav),
    // CDR:
    .CdrLos_oq(CdrLos),
    .CdrLol_oq(CdrLol),
    // .CdrI2cAccReq_i(CdrI2cAccReq),
    // .CdrI2cAccGranted_oq(CdrI2cAccGranted),
    // .CdrI2cAccModeWr_i(CdrI2cAccModeWr),
    // .CdrI2cRegAddr_ib8(CdrI2cRegAddr_b8),
    // .CdrI2cRegData_ib8(CdrI2cRegDataIn_b8),
    // .CdrI2cRegData_ob8(CdrI2cRegDataOut_b8),
    // .CdrI2cRdDataDav_op(CdrI2cRdDataDav),
    // Si57x:
    // .Si57xI2cAccReq_i(Si57xI2cAccReq),
    // .Si57xI2cAccGranted_oq(Si57xI2cAccGranted),
    // .Si57xI2cAccModeWr_i(Si57xI2cAccModeWr),
    // .Si57xI2cRegAddr_ib8(Si57xI2cRegAddr_b8),
    // .Si57xI2cRegData_ib8(Si57xI2cRegDataIn_b8),
    // .Si57xI2cRegData_ob8(Si57xI2cRegDataOut_b8),
    // .Si57xI2cRdDataDav_op(Si57xI2cRdDataDav),
    //==== WishBone interface for the I2C slaves ====//
    .I2cWbCyc_i(),
    .I2cWbStb_i(),
    .I2cWbWe_i(),
    .I2cWbAdr_ib12(),
    .I2cWbDat_ib8(),
    .I2cWbDat_ob8(),
    .I2cWbAck_o(),
    //==== WishBone interface ====//
    .WbCyc_i(WbCyc),
    .WbStb_i(WbStb),
    .WbWe_i(WbWe),
    .WbDat_ib32(WbDatMoSi_b32),
    .WbDat_oqb32(WbDatMiSo_b32),
    .WbAck_oa(WbAck));

initial begin 
    $warning("Direct I2C accesses are not working! All the commented '*I2c*' ports of I2cExpAndMuxReqArbiter needs to be rewriten to use I2cWb interface.");
end


I2cExpAndMuxMaster i_I2cExpAndMuxMaster(
    .Clk_ik(Clk_k),
    .Rst_irq(Rst_rq),
    .IoExpWrReq_i(IoExpWrReq),
    .IoExpWrOn_oq(IoExpWrOn),
    .IoExpRdReq_i(IoExpRdReq),
    .IoExpRdOn_oq(IoExpRdOn),
    .IoExpAddr_ib3(IoExpAddr_b3),
    .IoExpRegAddr_ib2(IoExpRegAddr_b2),
    .IoExpData_ib8(IoExpData_b8),
    .I2cSlaveWrReq_i(I2CSlaveWrReq),
    .I2cSlaveWrOn_o(I2CSlaveWrOn),
    .I2cSlaveRdReq_i(I2CSlaveRdReq),
    .I2cSlaveRdOn_o(I2CSlaveRdOn),
    .I2cMuxAddress_i(I2cMuxAddress),
    .I2cMuxChannel_ib2(I2cMuxChannel_b2),
    .I2cSlaveAddr_ib7(I2CSlaveAddr_b7),
    .I2cSlaveRegAddr_ib8(I2CSlaveRegAddr_b8),
    .I2cSlaveByte_ib8(I2CSlaveByte_b8),
    .Busy_o(I2cMasterBusy),
    .NewByteRead_op(I2cMasterNewByteRead),
    .ByteOut_ob8(I2cMasterByteOut_b8),
    .AckError_op(I2cMasterAckError),
    .Scl_ioz(I2cMuxScl),
    .Sda_ioz(I2cMuxSda)
);


pullup i_InternalSclPu (I2cMuxScl);
pullup i_InternalSdaPu (I2cMuxSda);


endmodule
