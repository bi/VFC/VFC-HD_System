# Define here the paths used for the different folders:

set BI_HDL_CORES_PATH c:/Projects/BI_HDL_Cores
set CORES_SYN_PATH $BI_HDL_CORES_PATH/cores_for_synthesis
set CORES_SIM_PATH $BI_HDL_CORES_PATH/cores_for_simulation

set HDL_PATH ../../../../..
set FPGASYS_PATH $HDL_PATH/modules
set MODELS_PATH $HDL_PATH/simulation/models

set TB_PATH ../..

# Clearing the transcript window:
.main clear

###############################################
# Compiling simulation files
###############################################

file delete -force work

vlib work

echo ""
echo "-> Starting Compilation..."
echo ""

# Device Under Test files:
vlog -work work $FPGASYS_PATH/I2cMuxAndExpReqArbiter.v

# VFC HD hierarchy:
vlog -work work $FPGASYS_PATH/I2cMuxAndExpMaster.v
vlog -work work $CORES_SYN_PATH/StatusBusSynch.v

# VFC HD models files:
vlog -work work $MODELS_PATH/pca9534.v
vlog -work work $MODELS_PATH/pca9544a.v

# Test Bench files:
vlog -work work $CORES_SIM_PATH/I2CSlave.v
vlog -work work $CORES_SIM_PATH/wb_master_sim/WbMasterSim.sv
vlog -sv -work work $TB_PATH/tb_I2cMuxAndExpReqArbiter.sv

echo ""
echo "-> Compilation Done..."
echo ""
echo ""
