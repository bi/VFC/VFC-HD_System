onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb_I2cMuxAndExpReqArbiter/i_I2cExpAndMuxReqArbiter/InitDone_oq
add wave -noupdate /tb_I2cMuxAndExpReqArbiter/i_I2cExpAndMuxReqArbiter/Clk_ik
add wave -noupdate /tb_I2cMuxAndExpReqArbiter/i_I2cExpAndMuxReqArbiter/Rst_irq
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {6100 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 115
configure wave -valuecolwidth 119
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {45600 ps}
