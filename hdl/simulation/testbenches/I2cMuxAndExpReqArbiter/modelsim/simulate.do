#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Manoel Barros Marin (CERN BE-BI-QP) 02/03/2016
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

###############################################
# Running simulation
###############################################

echo ""
echo "Starting Simulation..."
echo ""

vsim -assertdebug -gui -novopt work.tb_I2cMuxAndExpReqArbiter
do wave.do
view assertions
# log -r /*
run -all
