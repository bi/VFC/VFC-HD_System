#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
#    Jan Pospisil (CERN BE-BI-QP) 16/02/2017     #
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#

# load last compilation time from file

set fp [open "LastCompilationTime.txt" r]
set LastCompilationTime [read $fp]
close $fp

puts [clock format $LastCompilationTime -format {Setting last compilation time to: %A, %d of %B, %Y - %H:%M:%S (loaded from file)}]