# Define here the paths used for the different folders:
quietly set VFC_HD_APP_PATH ../../../../modules/vfchd_golden
quietly set VFC_HD_SYS_PATH ../../../../
quietly set TESTBENCH_PATH ..
# TODO: get absolute path to the actual script automatically
quietly set THIS_SCRIPT ./compile.do

quietly set VERILOG_OPT "-suppress 2583"
    # vlog-2583: [SVCHK] - Some checking for conflicts with always_comb and always_latch variables not yet supported. Run vopt to provide additional design-level checks.
    # We are running vopt later, so we can supress this warning.
quietly set VHDL_OPT "-2008"
quietly set VHDL_MIXED_OPT "-2008 -mixedsvvh"
quietly set DEFAULT_LIB work
quietly set VSIM_OPT ""

# Clearing the transcript window:
.main clear

echo ""
echo "########################"
echo "# Starting Compilation #"
echo "########################"
quietly set CompilationStart [clock seconds]

###############################################
# Compile Processeses
###############################################

proc compile_file {type lib name {extraOptions ""}} {
    file stat $name result
    if {$::LastCompilationTime > $result(mtime)} {
        return;
    }

    if {[catch {
        switch -nocase $type {
            verilog {
                vlog {*}$::VERILOG_OPT {*}$extraOptions -work $lib $name
            }
            vhdl {
                vcom {*}$::VHDL_OPT {*}$extraOptions -work $lib $name
            }
            vhdl_mixed {
                vcom {*}$::VHDL_MIXED_OPT {*}$extraOptions -work $lib $name
            }
            copy {
                file copy -force $name .
            }
            default {
                return -code error "Unknown type '$type' of file '$name'."
            }
        }
    } errMsg opt]} {
        puts stderr "Error occurred when compiling files:"
        puts stderr "  $errMsg"
        return -options $opt $errMsg
    }
}

# another IP core compilation approach:
#  - use *.SPD files (Simulation Package Descriptor), can be found in most IP cores
#  - use ip-make-simscript.exe from c:\Programs\altera\15.1\quartus\sopc_builder\bin

# function to extract $IpCoreSimDir/mentor/msim_setup.tcl file
proc compile_ipcore {lib dir name} {
    return -code error "Deprecated. Try to use compileIpCore instead."
    # default paths for normal cores
    set IpCoreFile $dir/$name/$name.sip
    set IpCoreSimDir $dir/$name/$name\_sim
    if ![file exists $IpCoreFile] {
        # paths for QSys cores
        set IpCoreFile $dir/$name.qsys
        set IpCoreSimDir $dir/$name/simulation
    }
    file stat $IpCoreFile result
    if {$::LastCompilationTime < $result(mtime)} {
        set IpCoreScript $IpCoreSimDir/mentor/msim_setup.tcl
        set fp [open $IpCoreScript r]
        set FileData [read $fp]
        close $fp
        set FileLines [split $FileData "\n"]
        # compile files
        foreach Line $FileLines {
            if [regexp {(vlog|vcom) (-sv |())\$USER_DEFINED_COMPILE_OPTIONS\s+.*\"\$QSYS_SIMDIR(/[^\"]*)\"} $Line Match Command SvSwitch EmptyString File] {
                set FilePath $IpCoreSimDir$File
                if [string match "vcom" $Command] {
                    compile_file vhdl $lib $FilePath
                } else {
                    compile_file verilog $lib $FilePath
                }
            }
        }
        # copy files
        foreach Line $FileLines {
            if [regexp {file copy -force \$QSYS_SIMDIR(/.*) \./} $Line Match File] {
                set FilePath $IpCoreSimDir$File
                compile_file copy $lib $FilePath
            }
        }
    }
}

# fake function to extract IP library name and list of a simulation HDL files from SIP file
proc set_global_assignment args {
    upvar ipFiles ipFiles
    set nextArg ""
    set library ""
    set name ""
    set filePath ""
    set options ""
    # Parameter "options" is not official part of Quartus SIP. Hope he don't mind...
    foreach arg $args {
        if ![string equal $nextArg ""] {
            set $nextArg $arg
            set nextArg ""
        } elseif [string equal [string range $arg 0 0] "-"] {
            set nextArg [string range $arg 1 end]
        } else {
            set filePath $arg
        }
    }
    if [string equal $name "MISC_FILE"] {
        lappend ipFiles [list $library $filePath $options]
    }
}

proc extractIpFiles {sipFile} {
    set ::quartus(sip_path) [file dirname $sipFile]
    set ipFiles [list]
    source $sipFile
    return $ipFiles
}

# function to extract SIP file
proc compileIpCore {sipFile} {
    # We cannot skip this SIP file if no change was detected, as linked source
    # file can be changed.

    set ipFiles [extractIpFiles $sipFile]
    foreach file $ipFiles {
        set library [lindex $file 0]
        set filePath [lindex $file 1]
        set fileOptions [lindex $file 2]
        set dotPosition [string last "." $filePath]
        set extension [string range $filePath $dotPosition+1 end]

        set type unknown
        switch -nocase $extension {
            v -
            iv -
            vo -
            sv {
                set type verilog
            }
            vhd -
            vhdl {
                set type vhdl
            }
            hex {
                set type copy
            }
            sip {
                compileIpCore $filePath
                continue
            }
            c -
            h -
            tcl -
            qsys {
                # We know, that those files do not need to be processed.
                continue
            }
            default {
                return -code error "Unknown extension '$extension' of file '$filePath'."
            }
        }

        compile_file $type $library $filePath $fileOptions
    }
}

###############################################
# Defining Last Compilation
###############################################

echo ""
echo "-> Enabling Smart or Full Compilation..."
echo ""

# Checking whether a previous smart compilation exists:
quietly set no_sc 0;
if {![info exists LastCompilationTime]} {
    quietly set LastCompilationTime 0;
    quietly set no_sc 1;
    echo "No last compilation found."
}

# "clean" argument
if {($argc > 0 && [string equal $1 clean])} {
    quietly set LastCompilationTime 0;
    quietly set no_sc 1;
    echo "Cleaning and staring a new compilation..."
}

# Check whether this compile scripts is newer than last compilation, if yes, run full compilation
if {($argc > 0 && [string equal $1 ignore]) || [info exists IgnoreCompileScriptChange]} {
    echo "Possible compilation script change ignored."
} else {
    file stat $THIS_SCRIPT result
    if {$LastCompilationTime < $result(mtime)} {
        quietly set LastCompilationTime 0;
        quietly set no_sc 1;
        echo "Compilation script has changed."
    }
}

if "$no_sc == 1" {
    echo "Smart compilation not possible or not enabled. Running full compilation..."
    # Deleting pre-existing library
    if {[file exists $DEFAULT_LIB]} {vdel -all -lib $DEFAULT_LIB}
    # Creating working directory:
    vlib $DEFAULT_LIB
    quietly set no_sc 0;
} else {
    echo "Smart compilation enabled. Only out of date files will be compiled..."
    puts [clock format $LastCompilationTime -format {Previous compilation time: %A, %d of %B, %Y - %H:%M:%S}]
}

###############################################
# Compiling simulation files
###############################################

echo ""
echo "-> Starting Compilation..."
echo ""

# VFC HD System modules (synthesis):
    # VFC-HD_System settings (needs to be set before including VfcHdSystem.sip)
    quietly set VFC_HD_CONF_DIR $VFC_HD_APP_PATH
    # DDR3 settings
    quietly set DDR3_DATA_DIR data
    file mkdir $DDR3_DATA_DIR
    quietly lappend VSIM_OPT "+model_data+$DDR3_DATA_DIR"
    # choose one density according to which type of DDR3 chip is used on your board
    quietly set VFC_HD_DDR3_DENSITY den4096Mb
    # quietly set VFC_HD_DDR3_DENSITY den8192Mb
compileIpCore $VFC_HD_SYS_PATH/modules/VfcHdSystem.sip

# VFC HD System models (simulation):
compile_file verilog $DEFAULT_LIB $VFC_HD_SYS_PATH/simulation/models/VfcHd_v3_0.sv
compile_file verilog $DEFAULT_LIB $VFC_HD_SYS_PATH/simulation/models/I2CSlave.v
compile_file verilog $DEFAULT_LIB $VFC_HD_SYS_PATH/simulation/models/vme_bus_module/VmeBusModule.sv

# VFC HD Application modules (synthesis):
compile_file verilog $DEFAULT_LIB $VFC_HD_APP_PATH/VfcHdApplication.sv

# Test Bench files:
compile_file verilog $DEFAULT_LIB $TESTBENCH_PATH/tb_vfchd_golden.sv

###############################################
# Top file
###############################################

echo ""
echo "-> Setting Top File..."
echo ""

quietly set top_level $DEFAULT_LIB.tb_vfchd_golden

###############################################
# Acquiring Compilation Time
###############################################

echo ""
quietly set duration [expr [clock seconds]-$CompilationStart]
quietly set LastCompilationTime $CompilationStart
echo [format "Compilation duration: %d:%02d" [expr $duration/60] [expr $duration%60]]
puts [clock format $LastCompilationTime -format {Setting last compilation time to: %A, %d of %B, %Y - %H:%M:%S}]

quietly set fp [open "LastCompilationTime.txt" w]
puts $fp $LastCompilationTime
close $fp

echo ""
echo "-> Compilation Done..."
echo ""
echo ""
