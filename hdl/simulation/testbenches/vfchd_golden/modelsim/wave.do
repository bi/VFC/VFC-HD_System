onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group {VFC Hd Top} /tb_vfchd_golden/i_VfcHd/*
add wave -noupdate -group {FPGA Top} /tb_vfchd_golden/i_VfcHd/i_Fpga/*
add wave -noupdate -group {System Top} -radix hexadecimal /tb_vfchd_golden/i_VfcHd/i_Fpga/i_VfcHdSystem/*
add wave -noupdate -group VmeInterfaceWb -radix hexadecimal /tb_vfchd_golden/i_VfcHd/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/*
add wave -noupdate /tb_vfchd_golden/i_VmeBus/TclVmeWr_p
add wave -noupdate /tb_vfchd_golden/i_VmeBus/TclVmeRd_p
add wave -noupdate /tb_vfchd_golden/i_VmeBus/TclVmeReset_p
add wave -noupdate /tb_vfchd_golden/i_VmeBus/Ds_nb2
add wave -noupdate /tb_vfchd_golden/i_VmeBus/DtAck_n
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {97921748 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 340
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {202026563 ps}
