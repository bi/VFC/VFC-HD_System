# Define here the paths used for the different folders:

set HDL_PATH ../../../..
set FPGASYS_PATH $HDL_PATH/modules

set TB_PATH ..
set TB tb_SpiMaster

# Clearing the transcript window:
.main clear

###############################################
# Compiling simulation files
###############################################

if {[file exists work]} {vdel -all -lib work} 
vlib work

echo ""
echo "-> Starting Compilation..."
echo ""

# Device Under Test files:
vlog -work work $FPGASYS_PATH/SpiMaster.sv

# Test Bench files:
vlog -sv -work work $TB_PATH/$TB.sv

vopt work.$TB +acc -o _design_optimized

echo ""
echo "-> Compilation Done..."
echo ""
echo ""
