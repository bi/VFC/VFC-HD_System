###############################################
# Running simulation
###############################################

echo ""
echo "Starting Simulation..."
echo ""

vsim -assertdebug -gui _design_optimized
do wave.do
view assertions
# log -r /*
run -all
wave zoom full