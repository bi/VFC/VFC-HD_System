module tb_SpiMaster;

    timeunit 1ps / 1ps;

    //==== Local parameters ====//

    localparam int c_ClkFrequency = 32e6;
    localparam int c_SpiFrequency = 1e6;
    localparam int c_MaxBits = 24;
    localparam int c_Bits = 24;
    localparam logic c_Cpol = 1'b1;
    localparam int c_Cpha = 1;
    
    localparam time c_ClkPeriod = 1s/c_ClkFrequency;


    //==== Wires & Regs ====//
    
    logic Clk_ik = 1'b1;
    logic Rst_ir = 1'b0;
    logic Go_i = 1'b0;
    logic [$clog2(c_MaxBits+1)-1:0] Bits_ib = c_Bits;
    logic [c_MaxBits-1:0] Data_ib = 0;
    logic [c_MaxBits-1:0] Data_ob;
    logic Done_o;
    logic Cs_on;
    logic Sclk_o;
    logic Mosi_o;
    logic Miso_i = 1'b0;
    
    //==== Stimulus ====//

    initial begin
        $display("//============================================================================================//");
        $display("//################################### SIMULATION  STARTING  ##################################//");
        $display("//============================================================================================//");

        repeat (1) @(posedge Clk_ik);
        Rst_ir = 1;
        repeat (10) @(posedge Clk_ik);
        Rst_ir = 0;
        repeat (5) @(posedge Clk_ik);

        Data_ib = 154;
        repeat (1) @(posedge Clk_ik);
        Go_i = 1;
        repeat (1) @(posedge Clk_ik);
        Go_i = 0;
        @(posedge Done_o);
        repeat (20) @(posedge Clk_ik);
        
        $stop();
    end

    //==== Clocks generation ====//
    always #(c_ClkPeriod/2) Clk_ik = ~Clk_ik;

    //==== DUT ====//

    SpiMaster #(
        .g_ClkFrequency(c_ClkFrequency),
        .g_SpiFrequency(c_SpiFrequency),
        .g_MaxBits     (c_MaxBits),
        .g_Cpol        (c_Cpol),
        .g_Cpha        (c_Cpha)
    ) i_Dut (
        .*
    );

endmodule
