`ifndef DISABLE_VME_P2

 `ifndef DISABLE_VME_P2_P0
    `ifndef DIRECTION_VME_P2_P0
       inout 	  P2DataP0_io,
     `else
       `DIRECTION_VME_P2_P0 P2DataP0_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P1
    `ifndef DIRECTION_VME_P2_P1
       inout 	  P2DataP1_io,
     `else
       `DIRECTION_VME_P2_P1 P2DataP1_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P2
    `ifndef DIRECTION_VME_P2_P2
       inout 	  P2DataP2_io,
     `else
       `DIRECTION_VME_P2_P2 P2DataP2_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P3
    `ifndef DIRECTION_VME_P2_P3
       inout 	  P2DataP3_io,
     `else
       `DIRECTION_VME_P2_P3 P2DataP3_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P4
    `ifndef DIRECTION_VME_P2_P4
       inout 	  P2DataP4_io,
     `else
       `DIRECTION_VME_P2_P4 P2DataP4_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P5
    `ifndef DIRECTION_VME_P2_P5
       inout 	  P2DataP5_io,
     `else
       `DIRECTION_VME_P2_P5 P2DataP5_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P6
    `ifndef DIRECTION_VME_P2_P6
       inout 	  P2DataP6_io,
     `else
       `DIRECTION_VME_P2_P6 P2DataP6_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P7
    `ifndef DIRECTION_VME_P2_P7
       inout 	  P2DataP7_io,
     `else
       `DIRECTION_VME_P2_P7 P2DataP7_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P8
    `ifndef DIRECTION_VME_P2_P8
       inout 	  P2DataP8_io,
     `else
       `DIRECTION_VME_P2_P8 P2DataP8_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P9
    `ifndef DIRECTION_VME_P2_P9
       inout 	  P2DataP9_io,
     `else
       `DIRECTION_VME_P2_P9 P2DataP9_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P10
    `ifndef DIRECTION_VME_P2_P10
       inout 	  P2DataP10_io,
     `else
       `DIRECTION_VME_P2_P10 P2DataP10_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P11
    `ifndef DIRECTION_VME_P2_P11
       inout 	  P2DataP11_io,
     `else
       `DIRECTION_VME_P2_P11 P2DataP11_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P12
    `ifndef DIRECTION_VME_P2_P12
       inout 	  P2DataP12_io,
     `else
       `DIRECTION_VME_P2_P12 P2DataP12_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P13
    `ifndef DIRECTION_VME_P2_P13
       inout 	  P2DataP13_io,
     `else
       `DIRECTION_VME_P2_P13 P2DataP13_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P14
    `ifndef DIRECTION_VME_P2_P14
       inout 	  P2DataP14_io,
     `else
       `DIRECTION_VME_P2_P14 P2DataP14_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P15
    `ifndef DIRECTION_VME_P2_P15
       inout 	  P2DataP15_io,
     `else
       `DIRECTION_VME_P2_P15 P2DataP15_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P16
    `ifndef DIRECTION_VME_P2_P16
       inout 	  P2DataP16_io,
     `else
       `DIRECTION_VME_P2_P16 P2DataP16_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P17
    `ifndef DIRECTION_VME_P2_P17
       inout 	  P2DataP17_io,
     `else
       `DIRECTION_VME_P2_P17 P2DataP17_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P18
    `ifndef DIRECTION_VME_P2_P18
       inout 	  P2DataP18_io,
     `else
       `DIRECTION_VME_P2_P18 P2DataP18_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_P19
    `ifndef DIRECTION_VME_P2_P19
       inout 	  P2DataP19_io,
     `else
       `DIRECTION_VME_P2_P19 P2DataP19_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N0
    `ifndef DIRECTION_VME_P2_N0
       inout 	  P2DataN0_io,
     `else
       `DIRECTION_VME_P2_N0 P2DataN0_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N1
    `ifndef DIRECTION_VME_P2_N1
       inout 	  P2DataN1_io,
     `else
       `DIRECTION_VME_P2_N1 P2DataN1_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N2
    `ifndef DIRECTION_VME_P2_N2
       inout 	  P2DataN2_io,
     `else
       `DIRECTION_VME_P2_N2 P2DataN2_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N3
    `ifndef DIRECTION_VME_P2_N3
       inout 	  P2DataN3_io,
     `else
       `DIRECTION_VME_P2_N3 P2DataN3_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N4
    `ifndef DIRECTION_VME_P2_N4
       inout 	  P2DataN4_io,
     `else
       `DIRECTION_VME_P2_N4 P2DataN4_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N5
    `ifndef DIRECTION_VME_P2_N5
       inout 	  P2DataN5_io,
     `else
       `DIRECTION_VME_P2_N5 P2DataN5_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N6
    `ifndef DIRECTION_VME_P2_N6
       inout 	  P2DataN6_io,
     `else
       `DIRECTION_VME_P2_N6 P2DataN6_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N7
    `ifndef DIRECTION_VME_P2_N7
       inout 	  P2DataN7_io,
     `else
       `DIRECTION_VME_P2_N7 P2DataN7_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N8
    `ifndef DIRECTION_VME_P2_N8
       inout 	  P2DataN8_io,
     `else
       `DIRECTION_VME_P2_N8 P2DataN8_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N9
    `ifndef DIRECTION_VME_P2_N9
       inout 	  P2DataN9_io,
     `else
       `DIRECTION_VME_P2_N9 P2DataN9_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N10
    `ifndef DIRECTION_VME_P2_N10
       inout 	  P2DataN10_io,
     `else
       `DIRECTION_VME_P2_N10 P2DataN10_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N11
    `ifndef DIRECTION_VME_P2_N11
       inout 	  P2DataN11_io,
     `else
       `DIRECTION_VME_P2_N11 P2DataN11_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N12
    `ifndef DIRECTION_VME_P2_N12
       inout 	  P2DataN12_io,
     `else
       `DIRECTION_VME_P2_N12 P2DataN12_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N13
    `ifndef DIRECTION_VME_P2_N13
       inout 	  P2DataN13_io,
     `else
       `DIRECTION_VME_P2_N13 P2DataN13_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N14
    `ifndef DIRECTION_VME_P2_N14
       inout 	  P2DataN14_io,
     `else
       `DIRECTION_VME_P2_N14 P2DataN14_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N15
    `ifndef DIRECTION_VME_P2_N15
       inout 	  P2DataN15_io,
     `else
       `DIRECTION_VME_P2_N15 P2DataN15_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N16
    `ifndef DIRECTION_VME_P2_N16
       inout 	  P2DataN16_io,
     `else
       `DIRECTION_VME_P2_N16 P2DataN16_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N17
    `ifndef DIRECTION_VME_P2_N17
       inout 	  P2DataN17_io,
     `else
       `DIRECTION_VME_P2_N17 P2DataN17_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N18
    `ifndef DIRECTION_VME_P2_N18
       inout 	  P2DataN18_io,
     `else
       `DIRECTION_VME_P2_N18 P2DataN18_io,
    `endif
 `endif
 `ifndef DISABLE_VME_P2_N19
    `ifndef DIRECTION_VME_P2_N19
       inout 	  P2DataN19_io,
     `else
       `DIRECTION_VME_P2_N19 P2DataN19_io,
    `endif
 `endif
`endif // DISABLE_VME_P2
