`timescale 1ns/1ns

module MAX5483 (
    input   SPIUD_iq,
    input   CS_iqn,
    input   SclkInc_iqk,
    input   DinUD_iq);

parameter dly = 1;

reg [23:0] ParallelData_q;
reg [9:0] Latch10Bits_m, Latch10BitsSPI_m, Latch10BitsUD_m;
reg [9:0] NVRegister_m, NVRegisterSPI_m, NVRegisterUD_m;

initial begin
    ParallelData_q = 24'b0;
    Latch10Bits_m = 10'b0;
    NVRegister_m = 10'b0;
    Latch10BitsSPI_m = 10'b0;
    NVRegisterSPI_m = 10'b0;
    Latch10BitsUD_m = 10'b0;
    NVRegisterUD_m = 10'b0;
end

always @(posedge SclkInc_iqk)
    if (~CS_iqn) ParallelData_q <= #dly {ParallelData_q[22:0], DinUD_iq};

always @(posedge CS_iqn) begin
    if (SPIUD_iq) begin
        case (ParallelData_q[21:20])
        2'b00: Latch10BitsSPI_m = ParallelData_q[15:6];
        2'b10: NVRegisterSPI_m = Latch10BitsSPI_m;
        2'b11: Latch10BitsSPI_m = NVRegisterSPI_m;
        default: Latch10BitsSPI_m = 10'b1010101010;
        endcase
    end
end

always @(negedge SclkInc_iqk) begin
    if (~SPIUD_iq && ~CS_iqn) begin
        if (DinUD_iq) Latch10BitsUD_m <= #dly Latch10BitsUD_m + 1'b1;
        else if (~DinUD_iq) Latch10BitsUD_m <= #dly Latch10BitsUD_m - 1'b1;
    end
end

always @(posedge CS_iqn) if (SclkInc_iqk) NVRegisterUD_m <= #dly Latch10BitsUD_m;

always @* begin
    if (SPIUD_iq) begin
        Latch10Bits_m <= #dly Latch10BitsSPI_m;
        NVRegister_m <= #dly NVRegisterSPI_m;
    end
    else if (~SPIUD_iq) begin
        Latch10Bits_m <= #dly Latch10BitsUD_m;
        NVRegister_m <= #dly NVRegisterUD_m;
    end
end
endmodule



