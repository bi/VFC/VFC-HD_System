#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2006 David Belohrad
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street,
# Fifth Floor, Boston, MA  02110-1301, USA.
#
# You can dowload a copy of the GNU General Public License here:
# http://www.gnu.org/licenses/gpl.txt
#
# Author: David Belohrad
# Email:  david.belohrad@cern.ch
#

"""
This script generates P2Data, FmcLa/Ha/Hb bindings for VfcHd_v3_0
"""

# generates p2data conditional bindings and declarations
with open("p2data_binding.vh", "wt") as f:
    f.write("`ifndef DISABLE_VME_P2\n")
    for pos in ['P', 'N']:
        for signal in range(20):
            f.write("""
      `ifndef DISABLE_VME_P2_%s%d
          .P2Data%s%d_io(P2Data%s%d_io),
      `endif""" % (pos, signal,
                   pos, signal,
                   pos, signal))
    f.write("\n`endif // DISABLE_VME_P2\n")

with open("p2data_declaration.vh", "wt") as f:
    f.write("`ifndef DISABLE_VME_P2\n")
    for pos in ['P', 'N']:
        for signal in range(20):
            f.write("""
 `ifndef DISABLE_VME_P2_%s%d
    `ifndef DIRECTION_VME_P2_%s%d
       inout 	  P2Data%s%d_io,
     `else
       `DIRECTION_VME_P2_%s%d P2Data%s%d_io,
    `endif
 `endif""" % (pos, signal,
              pos, signal,
              pos, signal,
              pos, signal,
              pos, signal))
    f.write("\n`endif // DISABLE_VME_P2\n")

# and the same approach for fmc:
with open("fmc_binding.vh", "wt") as f:
    for pins, gate in ((34, "La"),
                       (24, "Ha"),
                       (22, "Hb")):
        f.write("`ifndef DISABLE_FMC_%s\n" % gate.upper())
        for pin in range(pins):
            for pos in ['P', 'N']:
                f.write("""
   `ifndef DISABLE_FMC_%s_%s%d
      .Fmc%s%s%d_io (Fmc%s%s%d_io),
   `endif""" % (gate.upper(), pos, pin,
                gate, pos, pin,
                gate, pos,pin))
        f.write("\n`endif // DISABLE_FMC_%s\n" % gate.upper())

with open("fmc_declaration.vh", "wt") as f:
    for pins, gate in ((34, "La"),
                       (24, "Ha"),
                       (22, "Hb")):
        f.write("`ifndef DISABLE_FMC_%s\n" % gate.upper())
        for pin in range(pins):
            for pos in ['P', 'N']:
                f.write("""
   `ifndef DISABLE_FMC_%s_%s%d
       `ifndef DIRECTION_FMC_%s_%s%d
          inout Fmc%s%s%d_io,
       `else
          `DIRECTION_FMC_%s_%s%d Fmc%s%s%d_io,
       `endif
   `endif""" % (gate.upper(), pos, pin,
                gate.upper(), pos, pin,
                gate, pos, pin,
                gate.upper(), pos, pin,
                gate, pos,pin))
        f.write("\n`endif // DISABLE_FMC_%s\n" % gate.upper())
