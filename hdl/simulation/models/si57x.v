`timescale 1ns/100ps

module si57x (
    inout oe,
    inout sda,
    inout scl,
    inout clk_p,
    inout clk_n);

reg Clk_s = 1'b0;

always #5 Clk_s = ~Clk_s;

assign clk_p = oe? Clk_s : 1'bz;
assign clk_n = oe? ~Clk_s : 1'bz;

I2CSlave
#( .g_Address(7'h55), .g_MinPerDown(3_000))
  i_I2cSlave(
    .Scl_io(scl),
    .Sda_io(sda)
);

endmodule
