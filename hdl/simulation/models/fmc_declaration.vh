`ifndef DISABLE_FMC_LA

   `ifndef DISABLE_FMC_LA_P0
       `ifndef DIRECTION_FMC_LA_P0
          inout FmcLaP0_io,
       `else
          `DIRECTION_FMC_LA_P0 FmcLaP0_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N0
       `ifndef DIRECTION_FMC_LA_N0
          inout FmcLaN0_io,
       `else
          `DIRECTION_FMC_LA_N0 FmcLaN0_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P1
       `ifndef DIRECTION_FMC_LA_P1
          inout FmcLaP1_io,
       `else
          `DIRECTION_FMC_LA_P1 FmcLaP1_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N1
       `ifndef DIRECTION_FMC_LA_N1
          inout FmcLaN1_io,
       `else
          `DIRECTION_FMC_LA_N1 FmcLaN1_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P2
       `ifndef DIRECTION_FMC_LA_P2
          inout FmcLaP2_io,
       `else
          `DIRECTION_FMC_LA_P2 FmcLaP2_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N2
       `ifndef DIRECTION_FMC_LA_N2
          inout FmcLaN2_io,
       `else
          `DIRECTION_FMC_LA_N2 FmcLaN2_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P3
       `ifndef DIRECTION_FMC_LA_P3
          inout FmcLaP3_io,
       `else
          `DIRECTION_FMC_LA_P3 FmcLaP3_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N3
       `ifndef DIRECTION_FMC_LA_N3
          inout FmcLaN3_io,
       `else
          `DIRECTION_FMC_LA_N3 FmcLaN3_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P4
       `ifndef DIRECTION_FMC_LA_P4
          inout FmcLaP4_io,
       `else
          `DIRECTION_FMC_LA_P4 FmcLaP4_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N4
       `ifndef DIRECTION_FMC_LA_N4
          inout FmcLaN4_io,
       `else
          `DIRECTION_FMC_LA_N4 FmcLaN4_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P5
       `ifndef DIRECTION_FMC_LA_P5
          inout FmcLaP5_io,
       `else
          `DIRECTION_FMC_LA_P5 FmcLaP5_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N5
       `ifndef DIRECTION_FMC_LA_N5
          inout FmcLaN5_io,
       `else
          `DIRECTION_FMC_LA_N5 FmcLaN5_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P6
       `ifndef DIRECTION_FMC_LA_P6
          inout FmcLaP6_io,
       `else
          `DIRECTION_FMC_LA_P6 FmcLaP6_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N6
       `ifndef DIRECTION_FMC_LA_N6
          inout FmcLaN6_io,
       `else
          `DIRECTION_FMC_LA_N6 FmcLaN6_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P7
       `ifndef DIRECTION_FMC_LA_P7
          inout FmcLaP7_io,
       `else
          `DIRECTION_FMC_LA_P7 FmcLaP7_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N7
       `ifndef DIRECTION_FMC_LA_N7
          inout FmcLaN7_io,
       `else
          `DIRECTION_FMC_LA_N7 FmcLaN7_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P8
       `ifndef DIRECTION_FMC_LA_P8
          inout FmcLaP8_io,
       `else
          `DIRECTION_FMC_LA_P8 FmcLaP8_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N8
       `ifndef DIRECTION_FMC_LA_N8
          inout FmcLaN8_io,
       `else
          `DIRECTION_FMC_LA_N8 FmcLaN8_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P9
       `ifndef DIRECTION_FMC_LA_P9
          inout FmcLaP9_io,
       `else
          `DIRECTION_FMC_LA_P9 FmcLaP9_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N9
       `ifndef DIRECTION_FMC_LA_N9
          inout FmcLaN9_io,
       `else
          `DIRECTION_FMC_LA_N9 FmcLaN9_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P10
       `ifndef DIRECTION_FMC_LA_P10
          inout FmcLaP10_io,
       `else
          `DIRECTION_FMC_LA_P10 FmcLaP10_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N10
       `ifndef DIRECTION_FMC_LA_N10
          inout FmcLaN10_io,
       `else
          `DIRECTION_FMC_LA_N10 FmcLaN10_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P11
       `ifndef DIRECTION_FMC_LA_P11
          inout FmcLaP11_io,
       `else
          `DIRECTION_FMC_LA_P11 FmcLaP11_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N11
       `ifndef DIRECTION_FMC_LA_N11
          inout FmcLaN11_io,
       `else
          `DIRECTION_FMC_LA_N11 FmcLaN11_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P12
       `ifndef DIRECTION_FMC_LA_P12
          inout FmcLaP12_io,
       `else
          `DIRECTION_FMC_LA_P12 FmcLaP12_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N12
       `ifndef DIRECTION_FMC_LA_N12
          inout FmcLaN12_io,
       `else
          `DIRECTION_FMC_LA_N12 FmcLaN12_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P13
       `ifndef DIRECTION_FMC_LA_P13
          inout FmcLaP13_io,
       `else
          `DIRECTION_FMC_LA_P13 FmcLaP13_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N13
       `ifndef DIRECTION_FMC_LA_N13
          inout FmcLaN13_io,
       `else
          `DIRECTION_FMC_LA_N13 FmcLaN13_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P14
       `ifndef DIRECTION_FMC_LA_P14
          inout FmcLaP14_io,
       `else
          `DIRECTION_FMC_LA_P14 FmcLaP14_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N14
       `ifndef DIRECTION_FMC_LA_N14
          inout FmcLaN14_io,
       `else
          `DIRECTION_FMC_LA_N14 FmcLaN14_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P15
       `ifndef DIRECTION_FMC_LA_P15
          inout FmcLaP15_io,
       `else
          `DIRECTION_FMC_LA_P15 FmcLaP15_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N15
       `ifndef DIRECTION_FMC_LA_N15
          inout FmcLaN15_io,
       `else
          `DIRECTION_FMC_LA_N15 FmcLaN15_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P16
       `ifndef DIRECTION_FMC_LA_P16
          inout FmcLaP16_io,
       `else
          `DIRECTION_FMC_LA_P16 FmcLaP16_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N16
       `ifndef DIRECTION_FMC_LA_N16
          inout FmcLaN16_io,
       `else
          `DIRECTION_FMC_LA_N16 FmcLaN16_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P17
       `ifndef DIRECTION_FMC_LA_P17
          inout FmcLaP17_io,
       `else
          `DIRECTION_FMC_LA_P17 FmcLaP17_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N17
       `ifndef DIRECTION_FMC_LA_N17
          inout FmcLaN17_io,
       `else
          `DIRECTION_FMC_LA_N17 FmcLaN17_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P18
       `ifndef DIRECTION_FMC_LA_P18
          inout FmcLaP18_io,
       `else
          `DIRECTION_FMC_LA_P18 FmcLaP18_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N18
       `ifndef DIRECTION_FMC_LA_N18
          inout FmcLaN18_io,
       `else
          `DIRECTION_FMC_LA_N18 FmcLaN18_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P19
       `ifndef DIRECTION_FMC_LA_P19
          inout FmcLaP19_io,
       `else
          `DIRECTION_FMC_LA_P19 FmcLaP19_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N19
       `ifndef DIRECTION_FMC_LA_N19
          inout FmcLaN19_io,
       `else
          `DIRECTION_FMC_LA_N19 FmcLaN19_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P20
       `ifndef DIRECTION_FMC_LA_P20
          inout FmcLaP20_io,
       `else
          `DIRECTION_FMC_LA_P20 FmcLaP20_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N20
       `ifndef DIRECTION_FMC_LA_N20
          inout FmcLaN20_io,
       `else
          `DIRECTION_FMC_LA_N20 FmcLaN20_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P21
       `ifndef DIRECTION_FMC_LA_P21
          inout FmcLaP21_io,
       `else
          `DIRECTION_FMC_LA_P21 FmcLaP21_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N21
       `ifndef DIRECTION_FMC_LA_N21
          inout FmcLaN21_io,
       `else
          `DIRECTION_FMC_LA_N21 FmcLaN21_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P22
       `ifndef DIRECTION_FMC_LA_P22
          inout FmcLaP22_io,
       `else
          `DIRECTION_FMC_LA_P22 FmcLaP22_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N22
       `ifndef DIRECTION_FMC_LA_N22
          inout FmcLaN22_io,
       `else
          `DIRECTION_FMC_LA_N22 FmcLaN22_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P23
       `ifndef DIRECTION_FMC_LA_P23
          inout FmcLaP23_io,
       `else
          `DIRECTION_FMC_LA_P23 FmcLaP23_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N23
       `ifndef DIRECTION_FMC_LA_N23
          inout FmcLaN23_io,
       `else
          `DIRECTION_FMC_LA_N23 FmcLaN23_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P24
       `ifndef DIRECTION_FMC_LA_P24
          inout FmcLaP24_io,
       `else
          `DIRECTION_FMC_LA_P24 FmcLaP24_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N24
       `ifndef DIRECTION_FMC_LA_N24
          inout FmcLaN24_io,
       `else
          `DIRECTION_FMC_LA_N24 FmcLaN24_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P25
       `ifndef DIRECTION_FMC_LA_P25
          inout FmcLaP25_io,
       `else
          `DIRECTION_FMC_LA_P25 FmcLaP25_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N25
       `ifndef DIRECTION_FMC_LA_N25
          inout FmcLaN25_io,
       `else
          `DIRECTION_FMC_LA_N25 FmcLaN25_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P26
       `ifndef DIRECTION_FMC_LA_P26
          inout FmcLaP26_io,
       `else
          `DIRECTION_FMC_LA_P26 FmcLaP26_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N26
       `ifndef DIRECTION_FMC_LA_N26
          inout FmcLaN26_io,
       `else
          `DIRECTION_FMC_LA_N26 FmcLaN26_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P27
       `ifndef DIRECTION_FMC_LA_P27
          inout FmcLaP27_io,
       `else
          `DIRECTION_FMC_LA_P27 FmcLaP27_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N27
       `ifndef DIRECTION_FMC_LA_N27
          inout FmcLaN27_io,
       `else
          `DIRECTION_FMC_LA_N27 FmcLaN27_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P28
       `ifndef DIRECTION_FMC_LA_P28
          inout FmcLaP28_io,
       `else
          `DIRECTION_FMC_LA_P28 FmcLaP28_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N28
       `ifndef DIRECTION_FMC_LA_N28
          inout FmcLaN28_io,
       `else
          `DIRECTION_FMC_LA_N28 FmcLaN28_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P29
       `ifndef DIRECTION_FMC_LA_P29
          inout FmcLaP29_io,
       `else
          `DIRECTION_FMC_LA_P29 FmcLaP29_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N29
       `ifndef DIRECTION_FMC_LA_N29
          inout FmcLaN29_io,
       `else
          `DIRECTION_FMC_LA_N29 FmcLaN29_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P30
       `ifndef DIRECTION_FMC_LA_P30
          inout FmcLaP30_io,
       `else
          `DIRECTION_FMC_LA_P30 FmcLaP30_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N30
       `ifndef DIRECTION_FMC_LA_N30
          inout FmcLaN30_io,
       `else
          `DIRECTION_FMC_LA_N30 FmcLaN30_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P31
       `ifndef DIRECTION_FMC_LA_P31
          inout FmcLaP31_io,
       `else
          `DIRECTION_FMC_LA_P31 FmcLaP31_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N31
       `ifndef DIRECTION_FMC_LA_N31
          inout FmcLaN31_io,
       `else
          `DIRECTION_FMC_LA_N31 FmcLaN31_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P32
       `ifndef DIRECTION_FMC_LA_P32
          inout FmcLaP32_io,
       `else
          `DIRECTION_FMC_LA_P32 FmcLaP32_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N32
       `ifndef DIRECTION_FMC_LA_N32
          inout FmcLaN32_io,
       `else
          `DIRECTION_FMC_LA_N32 FmcLaN32_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_P33
       `ifndef DIRECTION_FMC_LA_P33
          inout FmcLaP33_io,
       `else
          `DIRECTION_FMC_LA_P33 FmcLaP33_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_LA_N33
       `ifndef DIRECTION_FMC_LA_N33
          inout FmcLaN33_io,
       `else
          `DIRECTION_FMC_LA_N33 FmcLaN33_io,
       `endif
   `endif
`endif // DISABLE_FMC_LA
`ifndef DISABLE_FMC_HA

   `ifndef DISABLE_FMC_HA_P0
       `ifndef DIRECTION_FMC_HA_P0
          inout FmcHaP0_io,
       `else
          `DIRECTION_FMC_HA_P0 FmcHaP0_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N0
       `ifndef DIRECTION_FMC_HA_N0
          inout FmcHaN0_io,
       `else
          `DIRECTION_FMC_HA_N0 FmcHaN0_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P1
       `ifndef DIRECTION_FMC_HA_P1
          inout FmcHaP1_io,
       `else
          `DIRECTION_FMC_HA_P1 FmcHaP1_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N1
       `ifndef DIRECTION_FMC_HA_N1
          inout FmcHaN1_io,
       `else
          `DIRECTION_FMC_HA_N1 FmcHaN1_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P2
       `ifndef DIRECTION_FMC_HA_P2
          inout FmcHaP2_io,
       `else
          `DIRECTION_FMC_HA_P2 FmcHaP2_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N2
       `ifndef DIRECTION_FMC_HA_N2
          inout FmcHaN2_io,
       `else
          `DIRECTION_FMC_HA_N2 FmcHaN2_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P3
       `ifndef DIRECTION_FMC_HA_P3
          inout FmcHaP3_io,
       `else
          `DIRECTION_FMC_HA_P3 FmcHaP3_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N3
       `ifndef DIRECTION_FMC_HA_N3
          inout FmcHaN3_io,
       `else
          `DIRECTION_FMC_HA_N3 FmcHaN3_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P4
       `ifndef DIRECTION_FMC_HA_P4
          inout FmcHaP4_io,
       `else
          `DIRECTION_FMC_HA_P4 FmcHaP4_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N4
       `ifndef DIRECTION_FMC_HA_N4
          inout FmcHaN4_io,
       `else
          `DIRECTION_FMC_HA_N4 FmcHaN4_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P5
       `ifndef DIRECTION_FMC_HA_P5
          inout FmcHaP5_io,
       `else
          `DIRECTION_FMC_HA_P5 FmcHaP5_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N5
       `ifndef DIRECTION_FMC_HA_N5
          inout FmcHaN5_io,
       `else
          `DIRECTION_FMC_HA_N5 FmcHaN5_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P6
       `ifndef DIRECTION_FMC_HA_P6
          inout FmcHaP6_io,
       `else
          `DIRECTION_FMC_HA_P6 FmcHaP6_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N6
       `ifndef DIRECTION_FMC_HA_N6
          inout FmcHaN6_io,
       `else
          `DIRECTION_FMC_HA_N6 FmcHaN6_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P7
       `ifndef DIRECTION_FMC_HA_P7
          inout FmcHaP7_io,
       `else
          `DIRECTION_FMC_HA_P7 FmcHaP7_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N7
       `ifndef DIRECTION_FMC_HA_N7
          inout FmcHaN7_io,
       `else
          `DIRECTION_FMC_HA_N7 FmcHaN7_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P8
       `ifndef DIRECTION_FMC_HA_P8
          inout FmcHaP8_io,
       `else
          `DIRECTION_FMC_HA_P8 FmcHaP8_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N8
       `ifndef DIRECTION_FMC_HA_N8
          inout FmcHaN8_io,
       `else
          `DIRECTION_FMC_HA_N8 FmcHaN8_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P9
       `ifndef DIRECTION_FMC_HA_P9
          inout FmcHaP9_io,
       `else
          `DIRECTION_FMC_HA_P9 FmcHaP9_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N9
       `ifndef DIRECTION_FMC_HA_N9
          inout FmcHaN9_io,
       `else
          `DIRECTION_FMC_HA_N9 FmcHaN9_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P10
       `ifndef DIRECTION_FMC_HA_P10
          inout FmcHaP10_io,
       `else
          `DIRECTION_FMC_HA_P10 FmcHaP10_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N10
       `ifndef DIRECTION_FMC_HA_N10
          inout FmcHaN10_io,
       `else
          `DIRECTION_FMC_HA_N10 FmcHaN10_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P11
       `ifndef DIRECTION_FMC_HA_P11
          inout FmcHaP11_io,
       `else
          `DIRECTION_FMC_HA_P11 FmcHaP11_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N11
       `ifndef DIRECTION_FMC_HA_N11
          inout FmcHaN11_io,
       `else
          `DIRECTION_FMC_HA_N11 FmcHaN11_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P12
       `ifndef DIRECTION_FMC_HA_P12
          inout FmcHaP12_io,
       `else
          `DIRECTION_FMC_HA_P12 FmcHaP12_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N12
       `ifndef DIRECTION_FMC_HA_N12
          inout FmcHaN12_io,
       `else
          `DIRECTION_FMC_HA_N12 FmcHaN12_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P13
       `ifndef DIRECTION_FMC_HA_P13
          inout FmcHaP13_io,
       `else
          `DIRECTION_FMC_HA_P13 FmcHaP13_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N13
       `ifndef DIRECTION_FMC_HA_N13
          inout FmcHaN13_io,
       `else
          `DIRECTION_FMC_HA_N13 FmcHaN13_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P14
       `ifndef DIRECTION_FMC_HA_P14
          inout FmcHaP14_io,
       `else
          `DIRECTION_FMC_HA_P14 FmcHaP14_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N14
       `ifndef DIRECTION_FMC_HA_N14
          inout FmcHaN14_io,
       `else
          `DIRECTION_FMC_HA_N14 FmcHaN14_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P15
       `ifndef DIRECTION_FMC_HA_P15
          inout FmcHaP15_io,
       `else
          `DIRECTION_FMC_HA_P15 FmcHaP15_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N15
       `ifndef DIRECTION_FMC_HA_N15
          inout FmcHaN15_io,
       `else
          `DIRECTION_FMC_HA_N15 FmcHaN15_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P16
       `ifndef DIRECTION_FMC_HA_P16
          inout FmcHaP16_io,
       `else
          `DIRECTION_FMC_HA_P16 FmcHaP16_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N16
       `ifndef DIRECTION_FMC_HA_N16
          inout FmcHaN16_io,
       `else
          `DIRECTION_FMC_HA_N16 FmcHaN16_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P17
       `ifndef DIRECTION_FMC_HA_P17
          inout FmcHaP17_io,
       `else
          `DIRECTION_FMC_HA_P17 FmcHaP17_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N17
       `ifndef DIRECTION_FMC_HA_N17
          inout FmcHaN17_io,
       `else
          `DIRECTION_FMC_HA_N17 FmcHaN17_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P18
       `ifndef DIRECTION_FMC_HA_P18
          inout FmcHaP18_io,
       `else
          `DIRECTION_FMC_HA_P18 FmcHaP18_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N18
       `ifndef DIRECTION_FMC_HA_N18
          inout FmcHaN18_io,
       `else
          `DIRECTION_FMC_HA_N18 FmcHaN18_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P19
       `ifndef DIRECTION_FMC_HA_P19
          inout FmcHaP19_io,
       `else
          `DIRECTION_FMC_HA_P19 FmcHaP19_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N19
       `ifndef DIRECTION_FMC_HA_N19
          inout FmcHaN19_io,
       `else
          `DIRECTION_FMC_HA_N19 FmcHaN19_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P20
       `ifndef DIRECTION_FMC_HA_P20
          inout FmcHaP20_io,
       `else
          `DIRECTION_FMC_HA_P20 FmcHaP20_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N20
       `ifndef DIRECTION_FMC_HA_N20
          inout FmcHaN20_io,
       `else
          `DIRECTION_FMC_HA_N20 FmcHaN20_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P21
       `ifndef DIRECTION_FMC_HA_P21
          inout FmcHaP21_io,
       `else
          `DIRECTION_FMC_HA_P21 FmcHaP21_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N21
       `ifndef DIRECTION_FMC_HA_N21
          inout FmcHaN21_io,
       `else
          `DIRECTION_FMC_HA_N21 FmcHaN21_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P22
       `ifndef DIRECTION_FMC_HA_P22
          inout FmcHaP22_io,
       `else
          `DIRECTION_FMC_HA_P22 FmcHaP22_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N22
       `ifndef DIRECTION_FMC_HA_N22
          inout FmcHaN22_io,
       `else
          `DIRECTION_FMC_HA_N22 FmcHaN22_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_P23
       `ifndef DIRECTION_FMC_HA_P23
          inout FmcHaP23_io,
       `else
          `DIRECTION_FMC_HA_P23 FmcHaP23_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HA_N23
       `ifndef DIRECTION_FMC_HA_N23
          inout FmcHaN23_io,
       `else
          `DIRECTION_FMC_HA_N23 FmcHaN23_io,
       `endif
   `endif
`endif // DISABLE_FMC_HA
`ifndef DISABLE_FMC_HB

   `ifndef DISABLE_FMC_HB_P0
       `ifndef DIRECTION_FMC_HB_P0
          inout FmcHbP0_io,
       `else
          `DIRECTION_FMC_HB_P0 FmcHbP0_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N0
       `ifndef DIRECTION_FMC_HB_N0
          inout FmcHbN0_io,
       `else
          `DIRECTION_FMC_HB_N0 FmcHbN0_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P1
       `ifndef DIRECTION_FMC_HB_P1
          inout FmcHbP1_io,
       `else
          `DIRECTION_FMC_HB_P1 FmcHbP1_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N1
       `ifndef DIRECTION_FMC_HB_N1
          inout FmcHbN1_io,
       `else
          `DIRECTION_FMC_HB_N1 FmcHbN1_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P2
       `ifndef DIRECTION_FMC_HB_P2
          inout FmcHbP2_io,
       `else
          `DIRECTION_FMC_HB_P2 FmcHbP2_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N2
       `ifndef DIRECTION_FMC_HB_N2
          inout FmcHbN2_io,
       `else
          `DIRECTION_FMC_HB_N2 FmcHbN2_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P3
       `ifndef DIRECTION_FMC_HB_P3
          inout FmcHbP3_io,
       `else
          `DIRECTION_FMC_HB_P3 FmcHbP3_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N3
       `ifndef DIRECTION_FMC_HB_N3
          inout FmcHbN3_io,
       `else
          `DIRECTION_FMC_HB_N3 FmcHbN3_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P4
       `ifndef DIRECTION_FMC_HB_P4
          inout FmcHbP4_io,
       `else
          `DIRECTION_FMC_HB_P4 FmcHbP4_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N4
       `ifndef DIRECTION_FMC_HB_N4
          inout FmcHbN4_io,
       `else
          `DIRECTION_FMC_HB_N4 FmcHbN4_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P5
       `ifndef DIRECTION_FMC_HB_P5
          inout FmcHbP5_io,
       `else
          `DIRECTION_FMC_HB_P5 FmcHbP5_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N5
       `ifndef DIRECTION_FMC_HB_N5
          inout FmcHbN5_io,
       `else
          `DIRECTION_FMC_HB_N5 FmcHbN5_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P6
       `ifndef DIRECTION_FMC_HB_P6
          inout FmcHbP6_io,
       `else
          `DIRECTION_FMC_HB_P6 FmcHbP6_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N6
       `ifndef DIRECTION_FMC_HB_N6
          inout FmcHbN6_io,
       `else
          `DIRECTION_FMC_HB_N6 FmcHbN6_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P7
       `ifndef DIRECTION_FMC_HB_P7
          inout FmcHbP7_io,
       `else
          `DIRECTION_FMC_HB_P7 FmcHbP7_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N7
       `ifndef DIRECTION_FMC_HB_N7
          inout FmcHbN7_io,
       `else
          `DIRECTION_FMC_HB_N7 FmcHbN7_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P8
       `ifndef DIRECTION_FMC_HB_P8
          inout FmcHbP8_io,
       `else
          `DIRECTION_FMC_HB_P8 FmcHbP8_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N8
       `ifndef DIRECTION_FMC_HB_N8
          inout FmcHbN8_io,
       `else
          `DIRECTION_FMC_HB_N8 FmcHbN8_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P9
       `ifndef DIRECTION_FMC_HB_P9
          inout FmcHbP9_io,
       `else
          `DIRECTION_FMC_HB_P9 FmcHbP9_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N9
       `ifndef DIRECTION_FMC_HB_N9
          inout FmcHbN9_io,
       `else
          `DIRECTION_FMC_HB_N9 FmcHbN9_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P10
       `ifndef DIRECTION_FMC_HB_P10
          inout FmcHbP10_io,
       `else
          `DIRECTION_FMC_HB_P10 FmcHbP10_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N10
       `ifndef DIRECTION_FMC_HB_N10
          inout FmcHbN10_io,
       `else
          `DIRECTION_FMC_HB_N10 FmcHbN10_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P11
       `ifndef DIRECTION_FMC_HB_P11
          inout FmcHbP11_io,
       `else
          `DIRECTION_FMC_HB_P11 FmcHbP11_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N11
       `ifndef DIRECTION_FMC_HB_N11
          inout FmcHbN11_io,
       `else
          `DIRECTION_FMC_HB_N11 FmcHbN11_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P12
       `ifndef DIRECTION_FMC_HB_P12
          inout FmcHbP12_io,
       `else
          `DIRECTION_FMC_HB_P12 FmcHbP12_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N12
       `ifndef DIRECTION_FMC_HB_N12
          inout FmcHbN12_io,
       `else
          `DIRECTION_FMC_HB_N12 FmcHbN12_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P13
       `ifndef DIRECTION_FMC_HB_P13
          inout FmcHbP13_io,
       `else
          `DIRECTION_FMC_HB_P13 FmcHbP13_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N13
       `ifndef DIRECTION_FMC_HB_N13
          inout FmcHbN13_io,
       `else
          `DIRECTION_FMC_HB_N13 FmcHbN13_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P14
       `ifndef DIRECTION_FMC_HB_P14
          inout FmcHbP14_io,
       `else
          `DIRECTION_FMC_HB_P14 FmcHbP14_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N14
       `ifndef DIRECTION_FMC_HB_N14
          inout FmcHbN14_io,
       `else
          `DIRECTION_FMC_HB_N14 FmcHbN14_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P15
       `ifndef DIRECTION_FMC_HB_P15
          inout FmcHbP15_io,
       `else
          `DIRECTION_FMC_HB_P15 FmcHbP15_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N15
       `ifndef DIRECTION_FMC_HB_N15
          inout FmcHbN15_io,
       `else
          `DIRECTION_FMC_HB_N15 FmcHbN15_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P16
       `ifndef DIRECTION_FMC_HB_P16
          inout FmcHbP16_io,
       `else
          `DIRECTION_FMC_HB_P16 FmcHbP16_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N16
       `ifndef DIRECTION_FMC_HB_N16
          inout FmcHbN16_io,
       `else
          `DIRECTION_FMC_HB_N16 FmcHbN16_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P17
       `ifndef DIRECTION_FMC_HB_P17
          inout FmcHbP17_io,
       `else
          `DIRECTION_FMC_HB_P17 FmcHbP17_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N17
       `ifndef DIRECTION_FMC_HB_N17
          inout FmcHbN17_io,
       `else
          `DIRECTION_FMC_HB_N17 FmcHbN17_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P18
       `ifndef DIRECTION_FMC_HB_P18
          inout FmcHbP18_io,
       `else
          `DIRECTION_FMC_HB_P18 FmcHbP18_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N18
       `ifndef DIRECTION_FMC_HB_N18
          inout FmcHbN18_io,
       `else
          `DIRECTION_FMC_HB_N18 FmcHbN18_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P19
       `ifndef DIRECTION_FMC_HB_P19
          inout FmcHbP19_io,
       `else
          `DIRECTION_FMC_HB_P19 FmcHbP19_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N19
       `ifndef DIRECTION_FMC_HB_N19
          inout FmcHbN19_io,
       `else
          `DIRECTION_FMC_HB_N19 FmcHbN19_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P20
       `ifndef DIRECTION_FMC_HB_P20
          inout FmcHbP20_io,
       `else
          `DIRECTION_FMC_HB_P20 FmcHbP20_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N20
       `ifndef DIRECTION_FMC_HB_N20
          inout FmcHbN20_io,
       `else
          `DIRECTION_FMC_HB_N20 FmcHbN20_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_P21
       `ifndef DIRECTION_FMC_HB_P21
          inout FmcHbP21_io,
       `else
          `DIRECTION_FMC_HB_P21 FmcHbP21_io,
       `endif
   `endif
   `ifndef DISABLE_FMC_HB_N21
       `ifndef DIRECTION_FMC_HB_N21
          inout FmcHbN21_io,
       `else
          `DIRECTION_FMC_HB_N21 FmcHbN21_io,
       `endif
   `endif
`endif // DISABLE_FMC_HB
