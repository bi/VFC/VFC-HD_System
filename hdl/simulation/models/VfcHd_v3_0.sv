//============================================================================================//
//##################################   Module Information   ##################################//
//============================================================================================//
//
// Company: CERN (BE-BI)
//
// File Name: VfcHd_v3_0.v
//
// File versions history:
//
//     DATE        VERSION  AUTHOR                   DESCRIPTION
//     30-11-2016  3.0      andrea.boccardi@cern.ch  Board behavioural model
//     16-10-2017  3.1      j.pospisil@cern.ch       SV; fixed I2cMuxScl_iokz direction; added
//                                                   GpIo* workaround
//
// Language: SystemVerilog
//
//
// Description:
//
//     This is a behavioural model of the VFC-HD v3.0 board
//
//============================================================================================//
//############################################################################################//
//============================================================================================//

`timescale 1ns/100ps

// use the same configuration as the system part to derive components here
`include "VfcHdConfig.vh"

module VfcHd_v3_0
(
    //==== VME interface ====//
 input 	       As_in,
 input [5:0]   AM_ib6,
 inout [31:1]  A_iob31,
 inout 	       LWord_io,
 input [1:0]   Ds_inb2,
 input 	       Wr_in,
 inout [31:0]  D_iob32,
 output        DtAck_on,
 output [7:1]  Irq_onb7,
 input 	       Iack_in,
 input 	       IackIn_in,
 output        IackOut_on,
 input 	       SysResetN_irn,
 input 	       SysClk_ik,
 inout [4:0]   Ga_ionb5,
 inout 	       Gap_ion,
 //==== FMC Connector ====//
`include "fmc_declaration.vh"
 input 	       FmcPrsntM2c_in,
 output        FmcTck_ok,
 output        FmcTms_o,
 output        FmcTdi_o,
 input 	       FmcTdo_i,
 output        FmcTrstL_orn,
 inout 	       FmcScl_iok,
 inout 	       FmcSda_io,
 input 	       FmcPgM2c_i,
 output        FmcPgC2m_o,
 input 	       FmcClk0M2cCmos_ik,
 input 	       FmcClk1M2cCmos_ik,
`ifdef ENABLE_FMC_CLK2_BIDIR
 inout 	       FmcClk2Bidir_iok,
`endif
`ifdef ENABLE_FMC_CLK3_BIDIR
 inout 	       FmcClk3Bidir_iok,
`endif
 input 	       FmcClkDir_i,
 output [ 9:0] FmcDpC2m_ob10,
 input [ 9:0]  FmcDpM2c_ib10,
 input 	       FmcGbtClk0M2c_ik,
 input 	       FmcGbtClk1M2c_ik,
 output        FmcGa0_o,
 output        FmcGa1_o,
 //==== P2Data ====//
`include "p2data_declaration.vh"
 //==== SW1 ====//
 input [7:0]   DipSw_ib8,
 //==== Miscellaneus ====//
 input 	       PushButton_i,
 inout [4:1]   GpIoLemo_iob4
);

//=======================================  Declarations  =====================================//

wire VmeDOeN_n, VmeDDir, VmeIackN_n, VmeSysClk_k, VmeAs_n, VmeAOeN_n, VmeADir, VmeDtAck_e, VmeWr_n, VmeLWord_n;
wire [31:0] VmeD_b32;
wire [31:1] VmeA_b31;
wire [7:1] VmeIrq_b7;
wire VmeIackInN_n, VmeIackOutN_n, VmeSysReset_rn, VmeGapN_n;
wire [4:0] VmeGaN_nb5;
wire [1:0] VmeDsN_nb2;
wire [5:0] VmeAm_b6;
wire [4:1] GpIo_b4;
wire [7:0] a3_Ic19;
wire [7:0] b3_Ic19;
wire GpIo1A2B, GpIo2A2B, GpIo34A2B;
wire OeSi57x, Si57xSda, Si57xScl, Si57xClk_k;
reg  GbitTrxClkRefR_k = 1'b0;
reg  Clk20Vcxo_k = 1'b1;
wire PushButtonN_n;
wire TempIdDq;
wire WrPromScl, WrPromSda;

//=======================================  Schematic  =======================================//

//==== Page 2 : DDR3 ====//
wire OctRzqin;
wire I2cMuxScl_iokz, I2cMuxSda_ioz;

assign (pull1, pull0) OctRzqin = 1'b0;
assign (pull1, pull0) I2cMuxScl_iokz = 1'b1;
assign (pull1, pull0) I2cMuxSda_ioz = 1'b1;




`ifdef ENABLE_DDR3_BANKA
wire Ddr3aCk_k;
wire Ddr3aCk_kn;
wire Ddr3aCke;
wire Ddr3aReset_rn;
wire Ddr3aRas_n;
wire Ddr3aCas_n;
wire Ddr3aCs_n;
wire Ddr3aWe_n;
wire Ddr3aOdt;
wire [3-1:0] Ddr3aBa_b3;
wire [16-1:0] Ddr3aAdr_b16;
wire [2-1:0] Ddr3aDm_b2;
wire [2-1:0] Ddr3aDqs_b2;
wire [2-1:0] Ddr3aDqs_b2n;
wire [16-1:0] Ddr3aDq_b16;

assign (pull1, pull0) Ddr3aCke = 0;
assign (pull1, pull0) Ddr3aReset_rn = 1;
assign (pull1, pull0) Ddr3aRas_n = 1;
assign (pull1, pull0) Ddr3aCas_n = 1;
assign (pull1, pull0) Ddr3aCs_n = 1;
assign (pull1, pull0) Ddr3aWe_n = 1;
assign (pull1, pull0) Ddr3aOdt = 1;
assign (pull1, pull0) Ddr3aBa_b3 = 3'b111;
assign (pull1, pull0) Ddr3aAdr_b16 = 16'hffff;

ddr3 i_Ddr1 (
    .rst_n(Ddr3aReset_rn),
    .ck(Ddr3aCk_k),
    .ck_n(Ddr3aCk_kn),
    .cke(Ddr3aCke),
    .cs_n(Ddr3aCs_n),
    .ras_n(Ddr3aRas_n),
    .cas_n(Ddr3aCas_n),
    .we_n(Ddr3aWe_n),
    .dm_tdqs(Ddr3aDm_b2),
    .ba(Ddr3aBa_b3),
`ifndef DDR4G
    .addr(Ddr3aAdr_b16), // 8 Gb version
`else
    .addr(Ddr3aAdr_b16[14:0]), // 4 Gb version
`endif
    .dq(Ddr3aDq_b16),
    .dqs(Ddr3aDqs_b2),
    .dqs_n(Ddr3aDqs_b2n),
    .tdqs_n(),
    .odt(Ddr3aOdt)
);
`endif //  `ifdef ENABLE_DDR3_BANKA

`ifdef ENABLE_DDR3_BANKB
wire Ddr3bCk_k;
wire Ddr3bCk_kn;
wire Ddr3bCke;
wire Ddr3bReset_rn;
wire Ddr3bRas_n;
wire Ddr3bCas_n;
wire Ddr3bCs_n;
wire Ddr3bWe_n;
wire Ddr3bOdt;
wire [3-1:0] Ddr3bBa_b3;
wire [16-1:0] Ddr3bAdr_b16;
wire [2-1:0] Ddr3bDm_b2;
wire [2-1:0] Ddr3bDqs_b2;
wire [2-1:0] Ddr3bDqs_b2n;
wire [16-1:0] Ddr3bDq_b16;

assign (pull1, pull0) Ddr3bCke = 0;
assign (pull1, pull0) Ddr3bReset_rn = 1;
assign (pull1, pull0) Ddr3bRas_n = 1;
assign (pull1, pull0) Ddr3bCas_n = 1;
assign (pull1, pull0) Ddr3bCs_n = 1;
assign (pull1, pull0) Ddr3bWe_n = 1;
assign (pull1, pull0) Ddr3bOdt = 1;
assign (pull1, pull0) Ddr3bBa_b3 = 3'b111;
assign (pull1, pull0) Ddr3bAdr_b16 = 16'hffff;


ddr3 i_Ddr2 (
    .rst_n(Ddr3bReset_rn),
    .ck(Ddr3bCk_k),
    .ck_n(Ddr3bCk_kn),
    .cke(Ddr3bCke),
    .cs_n(Ddr3bCs_n),
    .ras_n(Ddr3bRas_n),
    .cas_n(Ddr3bCas_n),
    .we_n(Ddr3bWe_n),
    .dm_tdqs(Ddr3bDm_b2),
    .ba(Ddr3bBa_b3),
`ifndef DDR4G
    .addr(Ddr3bAdr_b16), // 8 Gb version
`else
    .addr(Ddr3bAdr_b16[14:0]), // 4 Gb version
`endif
    .dq(Ddr3bDq_b16),
    .dqs(Ddr3bDqs_b2),
    .dqs_n(Ddr3bDqs_b2n),
    .tdqs_n(),
    .odt(Ddr3bOdt)
);
`endif //  `ifdef ENABLE_DDR3_BANKB


//==== Page 3..8 : FPGA ====//

logic [3:0] ClkFb_kb4;

VfcHdTop #(.g_Synthesis(1'b0))
    i_Fpga(
   // VME interface:
       .VmeAs_in(VmeAs_n),
       .VmeAm_ib6(VmeAm_b6),
       .VmeA_iob31(VmeA_b31),
       .VmeLWord_ion(VmeLWord_n),
       .VmeAOe_oen(VmeAOeN_n),
       .VmeADir_o(VmeADir),
       .VmeDs_inb2(VmeDsN_nb2),
       .VmeWrite_in(VmeWr_n),
       .VmeD_iob32(VmeD_b32),
       .VmeDOe_oen(VmeDOeN_n),
       .VmeDDir_o(VmeDDir),
       .VmeDtAckOe_o(VmeDtAck_e),
       .VmeIrq_ob7(VmeIrq_b7),
       .VmeIack_in(VmeIackN_n),
       .VmeIackIn_in(VmeIackInN_n),
       .VmeIackOut_on(VmeIackOutN_n),
       .VmeSysClk_ik(VmeSysClk_k),
       .VmeSysReset_irn(VmeSysReset_rn),
   // SFP Gbit:
   //@TODO: if really used in the future, should be exported to top
`ifdef ENABLE_APPSFP1
        .AppSfpRx1_ib(1'b0),
        .AppSfpTx1_ob(),
`endif
`ifdef ENABLE_APPSFP2
        .AppSfpRx2_ib(1'b0),
        .AppSfpTx2_ob(),
`endif
`ifdef ENABLE_APPSFP3
        .AppSfpRx3_ib(1'b0),
        .AppSfpTx3_ob(),
`endif
`ifdef ENABLE_APPSFP4
        .AppSfpRx4_ib(1'b0),
        .AppSfpTx4_ob(),
`endif
`ifdef ENABLE_BSTSFP
        .BstSfpRx_i(),
        .BstSfpTx_o(),
`endif
`ifdef ENABLE_ETHSFP
        .EthSfpRx_i(),
        .EthSfpTx_o(),
`endif

`ifdef ENABLE_DDR3_BANKA
        .Ddr3aCk_ok(Ddr3aCk_k),
        .Ddr3aCk_okn(Ddr3aCk_kn),
        .Ddr3aCke_o(Ddr3aCke),
        .Ddr3aReset_orn(Ddr3aReset_rn),
        .Ddr3aRas_on(Ddr3aRas_n),
        .Ddr3aCas_on(Ddr3aCas_n),
        .Ddr3aCs_on(Ddr3aCs_n),
        .Ddr3aWe_on(Ddr3aWe_n),
        .Ddr3aOdt_o(Ddr3aOdt),
        .Ddr3aBa_ob3(Ddr3aBa_b3),
        .Ddr3aAdr_ob16(Ddr3aAdr_b16),
        .Ddr3aDm_ob2(Ddr3aDm_b2),
        .Ddr3aDqs_iob2(Ddr3aDqs_b2),
        .Ddr3aDqs_iob2n(Ddr3aDqs_b2n),
        .Ddr3aDq_iob16(Ddr3aDq_b16),
`endif //  `ifdef ENABLE_DDR3_BANKA
`ifdef ENABLE_DDR3_BANKB
        .Ddr3bCk_ok(Ddr3bCk_k),
        .Ddr3bCk_okn(Ddr3bCk_kn),
        .Ddr3bCke_o(Ddr3bCke),
        .Ddr3bReset_orn(Ddr3bReset_rn),
        .Ddr3bRas_on(Ddr3bRas_n),
        .Ddr3bCas_on(Ddr3bCas_n),
        .Ddr3bCs_on(Ddr3bCs_n),
        .Ddr3bWe_on(Ddr3bWe_n),
        .Ddr3bOdt_o(Ddr3bOdt),
        .Ddr3bBa_ob3(Ddr3bBa_b3),
        .Ddr3bAdr_ob16(Ddr3bAdr_b16),
        .Ddr3bDm_ob2(Ddr3bDm_b2),
        .Ddr3bDqs_iob2(Ddr3bDqs_b2),
        .Ddr3bDqs_iob2n(Ddr3bDqs_b2n),
        .Ddr3bDq_iob16(Ddr3bDq_b16),
`endif //  `ifdef ENABLE_DDR3_BANKB
        .OctRzqin_i(OctRzqin),
   //TestIo:
       .TestIo1_io(),
       .TestIo2_io(),
   //I2C Mux and IO expanders:
       .I2cMuxSda_ioz(I2cMuxSda_ioz),
       .I2cMuxScl_iokz(I2cMuxScl_iokz),
       .I2CIoExpIntApp12_in(),
       .I2CIoExpIntApp34_in(),
       .I2CIoExpIntBstEth_in(),
       .I2CIoExpIntBlmIn_in(),
       .I2CIoExpIntLos_in(),
   //BST:
       .BstDataIn_i(),
       .CdrClkOut_ik(),
       .CdrDataOut_i(),
   //ADC Voltage monitoring:
       .VAdcDout_i(),
       .VAdcDin_o(),
       .VAdcCs_o(),
       .VAdcSclk_ok(),
   //FMC connector:
`include "fmc_binding.vh"
       .FmcPrsntM2c_in(FmcPrsntM2c_in),
       .FmcTck_ok(FmcTck_ok),
       .FmcTms_o(FmcTms_o),
       .FmcTdi_o(FmcTdi_o),
       .FmcTdo_i(FmcTdo_i),
       .FmcTrstL_orn(FmcTrstL_orn),
       .FmcScl_iok(FmcScl_iok),
       .FmcSda_io(FmcSda_io),
       .FmcPgM2c_i(FmcPgM2c_i),
       .FmcPgC2m_o(FmcPgC2m_o),
       .FmcClk0M2cCmos_ik(FmcClk0M2cCmos_ik),
       .FmcClk1M2cCmos_ik(FmcClk1M2cCmos_ik),
`ifdef ENABLE_FMC_CLK2_BIDIR
       .FmcClk2Bidir_iok(FmcClk2Bidir_iok),
`endif
`ifdef ENABLE_FMC_CLK3_BIDIR
       .FmcClk3Bidir_iok(FmcClk3Bidir_iok),
`endif
       .FmcClkDir_i(FmcClkDir_i),
`ifdef ENABLE_FMC_DP0_C2M
	.FmcDpC2m0_o(FmcDpC2m_ob10[0]),
`endif
`ifdef ENABLE_FMC_DP1_C2M
	.FmcDpC2m1_o(FmcDpC2m_ob10[1]),
`endif
`ifdef ENABLE_FMC_DP2_C2M
	.FmcDpC2m2_o(FmcDpC2m_ob10[2]),
`endif
`ifdef ENABLE_FMC_DP3_C2M
	.FmcDpC2m3_o(FmcDpC2m_ob10[3]),
`endif
`ifdef ENABLE_FMC_DP4_C2M
	.FmcDpC2m4_o(FmcDpC2m_ob10[4]),
`endif
`ifdef ENABLE_FMC_DP5_C2M
	.FmcDpC2m5_o(FmcDpC2m_ob10[5]),
`endif
`ifdef ENABLE_FMC_DP6_C2M
	.FmcDpC2m6_o(FmcDpC2m_ob10[6]),
`endif
`ifdef ENABLE_FMC_DP7_C2M
	.FmcDpC2m7_o(FmcDpC2m_ob10[7]),
`endif
`ifdef ENABLE_FMC_DP8_C2M
	.FmcDpC2m8_o(FmcDpC2m_ob10[8]),
`endif
`ifdef ENABLE_FMC_DP9_C2M
	.FmcDpC2m9_o(FmcDpC2m_ob10[9]),
`endif

`ifdef ENABLE_FMC_DP0_M2C
        .FmcDpM2c0_i(FmcDpM2c_ib10[0]),
`endif
`ifdef ENABLE_FMC_DP1_M2C
        .FmcDpM2c1_i(FmcDpM2c_ib10[1]),
`endif
`ifdef ENABLE_FMC_DP2_M2C
        .FmcDpM2c2_i(FmcDpM2c_ib10[2]),
`endif
`ifdef ENABLE_FMC_DP3_M2C
        .FmcDpM2c3_i(FmcDpM2c_ib10[3]),
`endif
`ifdef ENABLE_FMC_DP4_M2C
        .FmcDpM2c4_i(FmcDpM2c_ib10[4]),
`endif
`ifdef ENABLE_FMC_DP5_M2C
        .FmcDpM2c5_i(FmcDpM2c_ib10[5]),
`endif
`ifdef ENABLE_FMC_DP6_M2C
        .FmcDpM2c6_i(FmcDpM2c_ib10[6]),
`endif
`ifdef ENABLE_FMC_DP7_M2C
        .FmcDpM2c7_i(FmcDpM2c_ib10[7]),
`endif
`ifdef ENABLE_FMC_DP8_M2C
        .FmcDpM2c8_i(FmcDpM2c_ib10[8]),
`endif
`ifdef ENABLE_FMC_DP9_M2C
        .FmcDpM2c9_i(FmcDpM2c_ib10[9]),
`endif
`ifdef ENABLE_FMC_GBT_CLK0_M2C_LEFT
        .FmcGbtClk0M2cLeft_ik(FmcGbtClk0M2c_ik),
`endif
`ifdef ENABLE_FMC_GBT_CLK1_M2C_LEFT
        .FmcGbtClk1M2cLeft_ik(FmcGbtClk1M2c_ik),
`endif
`ifdef ENABLE_FMC_GBT_CLK0_M2C_RIGHT
        .FmcGbtClk0M2cRight_ik(FmcGbtClk0M2c_ik),
`endif
`ifdef ENABLE_FMC_GBT_CLK1_M2C_RIGHT
        .FmcGbtClk1M2cRight_ik(FmcGbtClk1M2c_ik),
`endif
   //Clock sources and control:
       .OeSi57x_oe(OeSi57x),
       .Si57xClk_ik(Si57xClk_k),
       .ClkFb_ikb4(ClkFb_kb4),
       .ClkFb_okb4(ClkFb_kb4),
       .Clk20VCOx_ik(Clk20Vcxo_k),
`ifdef ENABLE_WR
       .PllDac20Sync_o(),
       .PllDac25Sync_o(),
       .PllDacSclk_ok(),
       .PllDacDin_o(),
`endif
       .PllRefSda_ioz(),
       .PllRefScl_iokz(),
       .PllRefInt_i(),
       .PllSourceMuxOut_ok(),
`ifdef ENABLE_PLL_REFCLK_OUT
       //@TODO: probably requires top-level port
       .PllRefClkOut_ik(),
`endif
`ifdef ENABLE_GBIT_TRX_CLK_REF_R
       .GbitTrxClkRefR_ik(GbitTrxClkRefR_k),
`endif
   //Fmc Voltage control:
       .VadjCs_o(),
       .VadjSclk_ok(),
       .VadjDin_o(),
       .VfmcEnable_oen(),
   //SW1:
       .DipSw_ib8(DipSw_ib8),
   //Pcb Revision resistor network:
       .PcbRev_ib7(),
   //P2 RTM:
`include "p2data_binding.vh"
   //P0 Timing:
       .P0HwHighByte_ib8(),
       .P0HwLowByte_ib8(),
       .DaisyChain1Cntrl_o(),
       .DaisyChain2Cntrl_o(),
       .VmeP0BunchClk_ik(),
       .VmeP0Tclk_ik(),
   //WR PROM:
       .WrPromSda_io(WrPromSda),
       .WrPromScl_ok(WrPromScl),
   //GPIO:
       .GpIo_iob4(GpIo_b4),
   //Specials:
       .PushButtonN_in(PushButtonN_n),
       .TempIdDq_ioz(TempIdDq),
       .ResetFpgaConfigN_orn()
);

//==== Page 6 : IO expanders ====//
// TODO

// workaround
assign GpIo1A2B = i_Fpga.i_VfcHdSystem.GpIo1DirOut_i;
assign GpIo2A2B = i_Fpga.i_VfcHdSystem.GpIo2DirOut_i;
assign GpIo34A2B = i_Fpga.i_VfcHdSystem.GpIo34DirOut_i;


//==== Page 9 : ClockGeneration ====//
assign (pull1, pull0) OeSi57x = 1'b1;
assign (pull1, pull0) Si57xSda = 1'b1;
assign (pull1, pull0) Si57xScl = 1'b1;
si57x
    i_Osc1(
        .oe(OeSi57x),
        .sda(Si57xSda),
        .scl(Si57xScl),
        .clk_p(Si57xClk_k),
        .clk_n());

always #4 GbitTrxClkRefR_k = ~GbitTrxClkRefR_k;
always #25 Clk20Vcxo_k = ~Clk20Vcxo_k;

//==== Page 10 : FrontPanelAndMiscellaneous ====//
// Push button:
assign (pull1, strong0) PushButtonN_n = PushButton_i ? 1'b0 : 1'b1;

// WR PROM:
assign (pull1, pull0)  WrPromSda = 1'b1;
assign (pull1, pull0)  WrPromScl = 1'b1;

I2CSlave #(.g_Address(7'h51), .g_MinPerDown(3_000))
    i_Ic26(
        .Scl_io(WrPromScl),
        .Sda_io(WrPromSda));

// DS18B20U (1-Wire Digital Thermometer with unique 64-bit serial code)
assign (pull1, pull0) TempIdDq = 1'b1;
// TODO: i_Ic27

// GPIO:
assign a3_Ic19 = {GpIo_b4[3], 3'bz, GpIo_b4[4], 3'bz};
assign b3_Ic19 = {GpIoLemo_iob4[3], 3'bz, GpIoLemo_iob4[4], 3'bz};

sn74vmeh22501
    i_Ic19(
        .oeab1(GpIo1A2B),
        .oeby1_n(GpIo1A2B),
        .a1(GpIo_b4[1]),
        .y1(GpIo_b4[1]),
        .b1(GpIoLemo_iob4[1]),
        .oeab2(GpIo2A2B),
        .oeby2_n(GpIo2A2B),
        .a2(GpIo_b4[2]),
        .y2(GpIo_b4[2]),
        .b2(GpIoLemo_iob4[2]),
        .oe_n(1'b0),
        .dir(GpIo34A2B),
        .a3(a3_Ic19),
        .b3(b3_Ic19),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));

//==== Page 11 : FmcHpcConnector ====//
assign                FmcGa0_o      = 1'b0;
assign                FmcGa1_o      = 1'b0;
assign (pull1, pull0) FmcScl_iok    = 1'b1;
assign (pull1, pull0) FmcSda_io     = 1'b1;

//==== Page 13 : VmeConnectors ====//
assign (pull1, pull0) VmeIrq_b7     = 7'b0;
assign (pull1, pull0) VmeIrq_b7     = 7'b0;
assign (pull1, pull0) VmeDOeN_n     = 1'b1;
assign (pull1, pull0) VmeAOeN_n     = 1'b1;
assign (pull1, pull0) VmeADir       = 1'b0;
assign (pull1, pull0) VmeDtAck_e    = 1'b0;
assign (pull1, pull0) VmeIackOutN_n = VmeIackInN_n;
assign (pull1, pull0) Ga_ionb5      = 5'b11111;
assign (pull1, pull0) Gap_ion       = 1'b1;

// VME GA hack (until full I2C stuff will be modelled here)
initial force i_Fpga.i_VfcHdSystem.VmeGa_onb5 = Ga_ionb5;
initial force i_Fpga.i_VfcHdSystem.VmeGap_on = Gap_ion;

sn74vmeh22501
    i_Ic1(
        .oeab1(VmeIrq_b7[1]),
        .oeby1_n(1'b1),
        .a1(1'b0),
        .y1(),
        .b1(Irq_onb7[1]),
        .oeab2(1'b0),
        .oeby2_n(1'b0),
        .a2(1'b0),
        .y2(VmeIackN_n),
        .b2(Iack_in),
        .oe_n(VmeDOeN_n),
        .dir(VmeDDir),
        .a3(VmeD_b32[7:0]),
        .b3(D_iob32[7:0]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));

sn74vmeh22501
    i_Ic5(
        .oeab1(VmeIrq_b7[3]),
        .oeby1_n(1'b1),
        .a1(1'b0),
        .y1(),
        .b1(Irq_onb7[3]),
        .oeab2(VmeIrq_b7[2]),
        .oeby2_n(1'b1),
        .a2(1'b0),
        .y2(),
        .b2(Irq_onb7[2]),
        .oe_n(VmeDOeN_n),
        .dir(VmeDDir),
        .a3(VmeD_b32[15:8]),
        .b3(D_iob32[15:8]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));

sn74vmeh22501
    i_Ic18(
        .oeab1(1'b0),
        .oeby1_n(1'b0),
        .a1(1'b0),
        .y1(VmeSysClk_k),
        .b1(SysClk_ik),
        .oeab2(1'b0),
        .oeby2_n(1'b0),
        .a2(1'b0),
        .y2(VmeAs_n),
        .b2(As_in),
        .oe_n(VmeAOeN_n),
        .dir(VmeADir),
        .a3(VmeA_b31[15:8]),
        .b3(A_iob31[15:8]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));

sn74vmeh22501
    i_Ic17(
        .oeab1(VmeDtAck_e),
        .oeby1_n(1'b1),
        .a1(1'b0),
        .y1(),
        .b1(DtAck_on),
        .oeab2(1'b0),
        .oeby2_n(1'b0),
        .a2(1'b0),
        .y2(VmeWr_n),
        .b2(Wr_in),
        .oe_n(VmeAOeN_n),
        .dir(VmeADir),
        .a3(VmeA_b31[23:16]),
        .b3(A_iob31[23:16]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));

sn74vmeh22501
    i_Ic39(
        .oeab1(VmeIrq_b7[7]),
        .oeby1_n(1'b1),
        .a1(1'b0),
        .y1(),
        .b1(Irq_onb7[7]),
        .oeab2(VmeIrq_b7[6]),
        .oeby2_n(1'b1),
        .a2(1'b0),
        .y2(),
        .b2(Irq_onb7[6]),
        .oe_n(VmeDOeN_n),
        .dir(VmeDDir),
        .a3(VmeD_b32[23:16]),
        .b3(D_iob32[23:16]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));

sn74vmeh22501
    i_Ic44(
        .oeab1(VmeIrq_b7[5]),
        .oeby1_n(1'b1),
        .a1(1'b0),
        .y1(),
        .b1(Irq_onb7[5]),
        .oeab2(VmeIrq_b7[4]),
        .oeby2_n(1'b1),
        .a2(1'b0),
        .y2(),
        .b2(Irq_onb7[4]),
        .oe_n(VmeDOeN_n),
        .dir(VmeDDir),
        .a3(VmeD_b32[31:24]),
        .b3(D_iob32[31:24]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));

sn74vmeh22501
    i_Ic38(
        .oeab1(1'b0),
        .oeby1_n(1'b0),
        .a1(1'b0),
        .y1(VmeIackInN_n),
        .b1(IackIn_in),
        .oeab2(1'b1),
        .oeby2_n(1'b1),
        .a2(VmeIackOutN_n),
        .y2(),
        .b2(IackOut_on),
        .oe_n(VmeAOeN_n),
        .dir(VmeADir),
        .a3(VmeA_b31[31:24]),
        .b3(A_iob31[31:24]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));

sn74vmeh22501
    i_Ic20(
        .oeab1(1'b0),
        .oeby1_n(1'b0),
        .a1(1'b0),
        .y1(VmeDsN_nb2[1]),
        .b1(Ds_inb2[1]),
        .oeab2(1'b0),
        .oeby2_n(1'b0),
        .a2(1'b0),
        .y2(VmeDsN_nb2[0]),
        .b2(Ds_inb2[0]),
        .oe_n(VmeAOeN_n),
        .dir(VmeADir),
        .a3({VmeA_b31[7:1], VmeLWord_n}),
        .b3({A_iob31[7:1], LWord_io}),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));

wire DummyA, DummyB;

sn74vmeh22501
    i_Ic13(
        .oeab1(1'b0),
        .oeby1_n(1'b1),
        .a1(),
        .y1(),
        .b1(),
        .oeab2(1'b0),
        .oeby2_n(1'b1),
        .a2(),
        .y2(),
        .b2(),
        .oe_n(1'b0),
        .dir(1'b0),
        .a3({VmeAm_b6, VmeSysReset_rn, DummyA}),
        .b3({AM_ib6, SysResetN_irn, DummyA}),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));

endmodule
