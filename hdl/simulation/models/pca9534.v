`timescale 1ns/1ns

module pca9534 (
    input  [2:0] A_ib3,
    inout  [7:0] IO_iob8,
    output       Int_on,
    inout        Scl_io,
    inout        Sda_io
);

localparam c_MinPerDown = 2500;
wire [6:0] SlaveAddress_b7 = {4'b0100, A_ib3};

reg [7:0] OutputPortRegister_b8        = 8'hFF;
reg [7:0] PolarityInversionRegister_b8 = 8'h00;
reg [7:0] ConfigurationRegister_b8     = 8'hFF;
integer ShCount = 0;
reg [3:0] State=1;
reg [7:0] ByteAddr_b8;
reg RdWrBit;
reg Scl_q = 1'bz, Sda_q = 1'bz;
integer i;
reg [7:0] ShReg;
wire [7:0] InputStatus_b8 = ConfigurationRegister_b8 | IO_iob8;
reg [7:0] SavedInputStatus_b8;

assign Scl_io = Scl_q;
assign Sda_io = Sda_q;

assign Int_on = (SavedInputStatus_b8 != InputStatus_b8) ? 1'b0 : 1'bz;

localparam  s_Idle          = 1,
            s_Addressing    = 2,
            s_WrRdSel       = 3,
            s_AckAddress    = 4,
            s_ByteAddr      = 5,
            s_AckByteAddr   = 6,
            s_GetByte       = 7,
            s_SendAckData   = 8,
            s_SendByte      = 9,
            s_GetAckByte    = 10;

always@(negedge Sda_io)
    if (Scl_io) begin
        State = s_Addressing;  //Start Bit
        ShCount = 'h0;
    end

always@(posedge Sda_io)
    if (Scl_io) begin
        State = s_Idle; //Stop Bit
    end


always@(posedge Scl_io) case (State)
    s_Addressing: begin
        ShReg = {ShReg[6:0], Sda_io};
        ShCount = ShCount + 1;
        if (ShCount == 7) begin
            State = (ShReg[6:0]==SlaveAddress_b7) ? s_WrRdSel : s_Idle;
        end
    end
    s_WrRdSel: begin
        RdWrBit = Sda_io;
        State = s_AckAddress;
    end
    s_AckAddress: begin
        ShCount = 0;
        if (RdWrBit) begin
            State = s_SendByte;
            if (ByteAddr_b8 == 8'h00) ShReg = IO_iob8^PolarityInversionRegister_b8;
            if (ByteAddr_b8 == 8'h01) ShReg = OutputPortRegister_b8;
            if (ByteAddr_b8 == 8'h02) ShReg = PolarityInversionRegister_b8;
            if (ByteAddr_b8 == 8'h03) ShReg = ConfigurationRegister_b8;
            if (ByteAddr_b8 == 8'h00) SavedInputStatus_b8  =  InputStatus_b8;
        end else begin
            State = s_ByteAddr;
        end
    end
    s_ByteAddr: begin
        ByteAddr_b8 = {ByteAddr_b8[6:0], Sda_io};
        ShCount = ShCount + 1;
        if (ShCount == 8) State = s_AckByteAddr;
    end
    s_AckByteAddr: begin
        State = s_GetByte;
        ShCount = 0;
    end
    s_GetByte: begin
        ShReg = {ShReg[6:0], Sda_io};
        ShCount = ShCount + 1;
        if (ShCount == 8) begin
            State = s_SendAckData;
            if (ByteAddr_b8 == 8'h01) OutputPortRegister_b8        = ShReg;
            if (ByteAddr_b8 == 8'h02) PolarityInversionRegister_b8 = ShReg;
            if (ByteAddr_b8 == 8'h03) ConfigurationRegister_b8     = ShReg;
        end
    end
    s_SendAckData:begin
        ShCount = 0;
        State = s_GetByte;
    end
    s_SendByte: begin
        ShReg = {ShReg[6:0], 1'b0};
        ShCount = ShCount + 1;
        if (ShCount == 8) begin
            State = s_GetAckByte;
            if (ByteAddr_b8 != 8'h00) ByteAddr_b8 = ByteAddr_b8 + 1;  //The increment isn't quite clear from the datasheet
            else ShReg = IO_iob8^PolarityInversionRegister_b8;  //New sampling
            if (ByteAddr_b8 == 8'h01) ShReg = OutputPortRegister_b8;
            if (ByteAddr_b8 == 8'h02) ShReg = PolarityInversionRegister_b8;
            if (ByteAddr_b8 == 8'h03) ShReg = ConfigurationRegister_b8;
        end
    end
    s_GetAckByte: begin
        ShCount = 0;
        State = Sda_io ? s_Idle : s_SendByte;
    end
endcase

always@(negedge Scl_io) begin
    Scl_q = 1'b0;
    #c_MinPerDown;
    case (State)
    s_Addressing: begin
       Sda_q = 1'bz;
       #100;
       Scl_q = 1'bz;
    end
    s_WrRdSel: begin
       Sda_q = 1'bz;
       #100;
       Scl_q = 1'bz;
    end
    s_AckAddress: begin
       Sda_q = 1'b0;
       #100;
       Scl_q = 1'bz;
    end
    s_ByteAddr: begin
       Sda_q = 1'bz;
       #100;
       Scl_q = 1'bz;
    end
    s_AckByteAddr: begin
       Sda_q = 1'b0;
       #100;
       Scl_q = 1'bz;
    end
    s_GetByte: begin
       Sda_q = 1'bz;
       #100;
       Scl_q = 1'bz;
    end
    s_SendAckData:begin
       Sda_q = 1'b0;
       #100;
       Scl_q = 1'bz;
    end
    s_SendByte: begin
       Sda_q = ShReg[7] ? 1'bz : 1'b0;
       #100;
       Scl_q = 1'bz;
    end
    s_GetAckByte: begin
       Sda_q = 1'bz;
       #100;
       Scl_q = 1'bz;
    end
    default: begin
        Sda_q = 1'bz;
       #100;
        Scl_q = 1'bz;
    end
    endcase
end

assign IO_iob8[0] = ConfigurationRegister_b8[0] ? 1'bz : OutputPortRegister_b8[0];
assign IO_iob8[1] = ConfigurationRegister_b8[1] ? 1'bz : OutputPortRegister_b8[1];
assign IO_iob8[2] = ConfigurationRegister_b8[2] ? 1'bz : OutputPortRegister_b8[2];
assign IO_iob8[3] = ConfigurationRegister_b8[3] ? 1'bz : OutputPortRegister_b8[3];
assign IO_iob8[4] = ConfigurationRegister_b8[4] ? 1'bz : OutputPortRegister_b8[4];
assign IO_iob8[5] = ConfigurationRegister_b8[5] ? 1'bz : OutputPortRegister_b8[5];
assign IO_iob8[6] = ConfigurationRegister_b8[6] ? 1'bz : OutputPortRegister_b8[6];
assign IO_iob8[7] = ConfigurationRegister_b8[7] ? 1'bz : OutputPortRegister_b8[7];

endmodule
