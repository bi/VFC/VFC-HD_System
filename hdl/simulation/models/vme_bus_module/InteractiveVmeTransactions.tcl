proc vmerd {Address} {
    change sim:/tb_BaseProject/i_VmeBus/TclVerbose 1
    change sim:/tb_BaseProject/i_VmeBus/TclVmeAddress_b32 $Address
    change sim:/tb_BaseProject/i_VmeBus/TclVmeRd_p 0
    run 1ns
    change sim:/tb_BaseProject/i_VmeBus/TclVmeRd_p 1
    run 1ns
    change sim:/tb_BaseProject/i_VmeBus/TclVmeRd_p 0
    run -all
}

proc vmewr {Address Data} {
    change sim:/tb_BaseProject/i_VmeBus/TclVerbose 1
    change sim:/tb_BaseProject/i_VmeBus/TclVmeAddress_b32 $Address
    change sim:/tb_BaseProject/i_VmeBus/TclVmeData_b32 $Data
    change sim:/tb_BaseProject/i_VmeBus/TclVmeWr_p 0
    run 1ns
    change sim:/tb_BaseProject/i_VmeBus/TclVmeWr_p 1
    run 1ns
    change sim:/tb_BaseProject/i_VmeBus/TclVmeWr_p 0
    run -all
}

proc vmereset {Dummy} {
    change sim:/tb_BaseProject/i_VmeBus/TclVerbose 1
    change sim:/tb_BaseProject/i_VmeBus/TclVmeReset_p 0
    change sim:/tb_BaseProject/i_VmeBus/TclVmeResetTime $Dummy
    run 1ns
    change sim:/tb_BaseProject/i_VmeBus/TclVmeReset_p 1
    run 1ns
    change sim:/tb_BaseProject/i_VmeBus/TclVmeReset_p 0
    run -all
}

echo "Loaded the function: vmerd _address_"
echo "Loaded the function: vmewr _address_ _data_"
echo "Loaded the function: vmereset"
