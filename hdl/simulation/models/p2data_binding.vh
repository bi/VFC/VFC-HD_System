`ifndef DISABLE_VME_P2

      `ifndef DISABLE_VME_P2_P0
          .P2DataP0_io(P2DataP0_io),
      `endif
      `ifndef DISABLE_VME_P2_P1
          .P2DataP1_io(P2DataP1_io),
      `endif
      `ifndef DISABLE_VME_P2_P2
          .P2DataP2_io(P2DataP2_io),
      `endif
      `ifndef DISABLE_VME_P2_P3
          .P2DataP3_io(P2DataP3_io),
      `endif
      `ifndef DISABLE_VME_P2_P4
          .P2DataP4_io(P2DataP4_io),
      `endif
      `ifndef DISABLE_VME_P2_P5
          .P2DataP5_io(P2DataP5_io),
      `endif
      `ifndef DISABLE_VME_P2_P6
          .P2DataP6_io(P2DataP6_io),
      `endif
      `ifndef DISABLE_VME_P2_P7
          .P2DataP7_io(P2DataP7_io),
      `endif
      `ifndef DISABLE_VME_P2_P8
          .P2DataP8_io(P2DataP8_io),
      `endif
      `ifndef DISABLE_VME_P2_P9
          .P2DataP9_io(P2DataP9_io),
      `endif
      `ifndef DISABLE_VME_P2_P10
          .P2DataP10_io(P2DataP10_io),
      `endif
      `ifndef DISABLE_VME_P2_P11
          .P2DataP11_io(P2DataP11_io),
      `endif
      `ifndef DISABLE_VME_P2_P12
          .P2DataP12_io(P2DataP12_io),
      `endif
      `ifndef DISABLE_VME_P2_P13
          .P2DataP13_io(P2DataP13_io),
      `endif
      `ifndef DISABLE_VME_P2_P14
          .P2DataP14_io(P2DataP14_io),
      `endif
      `ifndef DISABLE_VME_P2_P15
          .P2DataP15_io(P2DataP15_io),
      `endif
      `ifndef DISABLE_VME_P2_P16
          .P2DataP16_io(P2DataP16_io),
      `endif
      `ifndef DISABLE_VME_P2_P17
          .P2DataP17_io(P2DataP17_io),
      `endif
      `ifndef DISABLE_VME_P2_P18
          .P2DataP18_io(P2DataP18_io),
      `endif
      `ifndef DISABLE_VME_P2_P19
          .P2DataP19_io(P2DataP19_io),
      `endif
      `ifndef DISABLE_VME_P2_N0
          .P2DataN0_io(P2DataN0_io),
      `endif
      `ifndef DISABLE_VME_P2_N1
          .P2DataN1_io(P2DataN1_io),
      `endif
      `ifndef DISABLE_VME_P2_N2
          .P2DataN2_io(P2DataN2_io),
      `endif
      `ifndef DISABLE_VME_P2_N3
          .P2DataN3_io(P2DataN3_io),
      `endif
      `ifndef DISABLE_VME_P2_N4
          .P2DataN4_io(P2DataN4_io),
      `endif
      `ifndef DISABLE_VME_P2_N5
          .P2DataN5_io(P2DataN5_io),
      `endif
      `ifndef DISABLE_VME_P2_N6
          .P2DataN6_io(P2DataN6_io),
      `endif
      `ifndef DISABLE_VME_P2_N7
          .P2DataN7_io(P2DataN7_io),
      `endif
      `ifndef DISABLE_VME_P2_N8
          .P2DataN8_io(P2DataN8_io),
      `endif
      `ifndef DISABLE_VME_P2_N9
          .P2DataN9_io(P2DataN9_io),
      `endif
      `ifndef DISABLE_VME_P2_N10
          .P2DataN10_io(P2DataN10_io),
      `endif
      `ifndef DISABLE_VME_P2_N11
          .P2DataN11_io(P2DataN11_io),
      `endif
      `ifndef DISABLE_VME_P2_N12
          .P2DataN12_io(P2DataN12_io),
      `endif
      `ifndef DISABLE_VME_P2_N13
          .P2DataN13_io(P2DataN13_io),
      `endif
      `ifndef DISABLE_VME_P2_N14
          .P2DataN14_io(P2DataN14_io),
      `endif
      `ifndef DISABLE_VME_P2_N15
          .P2DataN15_io(P2DataN15_io),
      `endif
      `ifndef DISABLE_VME_P2_N16
          .P2DataN16_io(P2DataN16_io),
      `endif
      `ifndef DISABLE_VME_P2_N17
          .P2DataN17_io(P2DataN17_io),
      `endif
      `ifndef DISABLE_VME_P2_N18
          .P2DataN18_io(P2DataN18_io),
      `endif
      `ifndef DISABLE_VME_P2_N19
          .P2DataN19_io(P2DataN19_io),
      `endif
`endif // DISABLE_VME_P2
