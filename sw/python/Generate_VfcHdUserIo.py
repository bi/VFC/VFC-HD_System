#-----------------------------------------------------------------------
# Title      : VfcHdUserIo.vh Generator
# Project    : VFC-HD_System (https://gitlab.cern.ch/bi/VFC/VFC-HD_System)
#-----------------------------------------------------------------------
# File       : Generate_VfcHdUserIo.py
# Author     : T. Levens, J. Pospisil
# Company    : CERN BE-BI
#-----------------------------------------------------------------------
# Description:
#
# Python script to generate the Verilog include files for the user I/O
# pin definitions and unused pin declarations.
#-----------------------------------------------------------------------

import argparse
import os
import sys
import time

pin_data = {
    ('FMC', 'LA'): [
        ('N34',  'FmcLaP0_io'),
        ('N33',  'FmcLaN0_io'),
        ('C20',  'FmcLaP1_io'),
        ('D20',  'FmcLaN1_io'),
        ('D24',  'FmcLaP2_io'),
        ('E24',  'FmcLaN2_io'),
        ('D25',  'FmcLaP3_io'),
        ('E25',  'FmcLaN3_io'),
        ('F24',  'FmcLaP4_io'),
        ('G24',  'FmcLaN4_io'),
        ('C28',  'FmcLaP5_io'),
        ('D28',  'FmcLaN5_io'),
        ('A29',  'FmcLaP6_io'),
        ('A28',  'FmcLaN6_io'),
        ('F26',  'FmcLaP7_io'),
        ('G26',  'FmcLaN7_io'),
        ('E22',  'FmcLaP8_io'),
        ('F22',  'FmcLaN8_io'),
        ('B27',  'FmcLaP9_io'),
        ('C27',  'FmcLaN9_io'),
        ('C26',  'FmcLaP10_io'),
        ('D26',  'FmcLaN10_io'),
        ('M29',  'FmcLaP11_io'),
        ('N29',  'FmcLaN11_io'),
        ('F23',  'FmcLaP12_io'),
        ('G23',  'FmcLaN12_io'),
        ('A25',  'FmcLaP13_io'),
        ('B25',  'FmcLaN13_io'),
        ('B24',  'FmcLaP14_io'),
        ('C24',  'FmcLaN14_io'),
        ('M32',  'FmcLaP15_io'),
        ('N32',  'FmcLaN15_io'),
        ('D30',  'FmcLaP16_io'),
        ('D29',  'FmcLaN16_io'),
        ('G21',  'FmcLaP17_io'),
        ('H21',  'FmcLaN17_io'),
        ('A22',  'FmcLaP18_io'),
        ('A21',  'FmcLaN18_io'),
        ('N30',  'FmcLaP19_io'),
        ('P30',  'FmcLaN19_io'),
        ('N22',  'FmcLaP20_io'),
        ('P22',  'FmcLaN20_io'),
        ('P27',  'FmcLaP21_io'),
        ('R27',  'FmcLaN21_io'),
        ('N24',  'FmcLaP22_io'),
        ('P24',  'FmcLaN22_io'),
        ('C23',  'FmcLaP23_io'),
        ('D23',  'FmcLaN23_io'),
        ('R29',  'FmcLaP24_io'),
        ('T29',  'FmcLaN24_io'),
        ('M26',  'FmcLaP25_io'),
        ('N26',  'FmcLaN25_io'),
        ('A20',  'FmcLaP26_io'),
        ('B21',  'FmcLaN26_io'),
        ('B28',  'FmcLaP27_io'),
        ('C29',  'FmcLaN27_io'),
        ('R28',  'FmcLaP28_io'),
        ('T28',  'FmcLaN28_io'),
        ('L33',  'FmcLaP29_io'),
        ('M33',  'FmcLaN29_io'),
        ('R26',  'FmcLaP30_io'),
        ('T27',  'FmcLaN30_io'),
        ('L28',  'FmcLaP31_io'),
        ('M28',  'FmcLaN31_io'),
        ('T26',  'FmcLaP32_io'),
        ('T25',  'FmcLaN32_io'),
        ('M27',  'FmcLaP33_io'),
        ('N27',  'FmcLaN33_io')
    ],
    ('FMC', 'HA'): [
        ('C34',  'FmcHaP0_io'),
        ('D34',  'FmcHaN0_io'),
        ('G34',  'FmcHaP1_io'),
        ('H34',  'FmcHaN1_io'),
        ('M16',  'FmcHaP2_io'),
        ('N16',  'FmcHaN2_io'),
        ('F14',  'FmcHaP3_io'),
        ('G14',  'FmcHaN3_io'),
        ('F20',  'FmcHaP4_io'),
        ('G20',  'FmcHaN4_io'),
        ('F17',  'FmcHaP5_io'),
        ('G17',  'FmcHaN5_io'),
        ('N19',  'FmcHaP6_io'),
        ('P19',  'FmcHaN6_io'),
        ('N18',  'FmcHaP7_io'),
        ('P18',  'FmcHaN7_io'),
        ('C19',  'FmcHaP8_io'),
        ('D19',  'FmcHaN8_io'),
        ('F19',  'FmcHaP9_io'),
        ('G19',  'FmcHaN9_io'),
        ('R19',  'FmcHaP10_io'),
        ('T19',  'FmcHaN10_io'),
        ('P16',  'FmcHaP11_io'),
        ('R16',  'FmcHaN11_io'),
        ('E18',  'FmcHaP12_io'),
        ('F18',  'FmcHaN12_io'),
        ('A18',  'FmcHaP13_io'),
        ('A19',  'FmcHaN13_io'),
        ('R15',  'FmcHaP14_io'),
        ('T15',  'FmcHaN14_io'),
        ('B16',  'FmcHaP15_io'),
        ('C16',  'FmcHaN15_io'),
        ('A17',  'FmcHaP16_io'),
        ('A16',  'FmcHaN16_io'),
        ('E34',  'FmcHaP17_io'),
        ('F34',  'FmcHaN17_io'),
        ('R14',  'FmcHaP18_io'),
        ('T14',  'FmcHaN18_io'),
        ('D16',  'FmcHaP19_io'),
        ('E16',  'FmcHaN19_io'),
        ('C17',  'FmcHaP20_io'),
        ('D17',  'FmcHaN20_io'),
        ('E15',  'FmcHaP21_io'),
        ('F15',  'FmcHaN21_io'),
        ('G16',  'FmcHaP22_io'),
        ('H16',  'FmcHaN22_io'),
        ('B30',  'FmcHaP23_io'),
        ('C30',  'FmcHaN23_io')
    ],
    ('FMC', 'HB'): [
        ('C6',   'FmcHbP0_io'),
        ('D6',   'FmcHbN0_io'),
        ('D13',  'FmcHbP1_io'),
        ('E13',  'FmcHbN1_io'),
        ('M7',   'FmcHbP2_io'),
        ('N7',   'FmcHbN2_io'),
        ('P12',  'FmcHbP3_io'),
        ('R12',  'FmcHbN3_io'),
        ('C11',  'FmcHbP4_io'),
        ('D11',  'FmcHbN4_io'),
        ('B12',  'FmcHbP5_io'),
        ('C12',  'FmcHbN5_io'),
        ('A6',   'FmcHbP6_io'),
        ('B6',   'FmcHbN6_io'),
        ('A9',   'FmcHbP7_io'),
        ('A8',   'FmcHbN7_io'),
        ('M12',  'FmcHbP8_io'),
        ('N12',  'FmcHbN8_io'),
        ('B10',  'FmcHbP9_io'),
        ('C10',  'FmcHbN9_io'),
        ('D12',  'FmcHbP10_io'),
        ('E12',  'FmcHbN10_io'),
        ('C8',   'FmcHbP11_io'),
        ('D8',   'FmcHbN11_io'),
        ('A13',  'FmcHbP12_io'),
        ('A12',  'FmcHbN12_io'),
        ('C14',  'FmcHbP13_io'),
        ('D14',  'FmcHbN13_io'),
        ('E10',  'FmcHbP14_io'),
        ('F10',  'FmcHbN14_io'),
        ('P13',  'FmcHbP15_io'),
        ('R13',  'FmcHbN15_io'),
        ('A11',  'FmcHbP16_io'),
        ('A10',  'FmcHbN16_io'),
        ('E6',   'FmcHbP17_io'),
        ('E7',   'FmcHbN17_io'),
        ('K6',   'FmcHbP18_io'),
        ('L6',   'FmcHbN18_io'),
        ('A7',   'FmcHbP19_io'),
        ('B7',   'FmcHbN19_io'),
        ('F8',   'FmcHbP20_io'),
        ('G8',   'FmcHbN20_io'),
        ('D9',   'FmcHbP21_io'),
        ('E9',   'FmcHbN21_io')
    ],
    ('VME', 'P2'): [
        ('F32',  'P2DataP19_io'),
        ('J32',  'P2DataP18_io'),
        ('C33',  'P2DataP17_io'),
        ('A33',  'P2DataP16_io'),
        ('B31',  'P2DataP15_io'),
        ('C31',  'P2DataP14_io'),
        ('F30',  'P2DataP13_io'),
        ('F29',  'P2DataP12_io'),
        ('J29',  'P2DataP11_io'),
        ('L30',  'P2DataP10_io'),
        ('J34',  'P2DataP9_io'),
        ('H31',  'P2DataP8_io'),
        ('B34',  'P2DataP7_io'),
        ('C32',  'P2DataP6_io'),
        ('A31',  'P2DataP5_io'),
        ('J30',  'P2DataP4_io'),
        ('E31',  'P2DataP3_io'),
        ('F28',  'P2DataP2_io'),
        ('H28',  'P2DataP1_io'),
        ('AF21', 'P2DataP0_io'),
        ('G32',  'P2DataN19_io'),
        ('K32',  'P2DataN18_io'),
        ('D33',  'P2DataN17_io'),
        ('B33',  'P2DataN16_io'),
        ('A30',  'P2DataN15_io'),
        ('D31',  'P2DataN14_io'),
        ('G30',  'P2DataN13_io'),
        ('G29',  'P2DataN12_io'),
        ('K29',  'P2DataN11_io'),
        ('M30',  'P2DataN10_io'),
        ('K34',  'P2DataN9_io'),
        ('J31',  'P2DataN8_io'),
        ('A35',  'P2DataN7_io'),
        ('D32',  'P2DataN6_io'),
        ('A32',  'P2DataN5_io'),
        ('K30',  'P2DataN4_io'),
        ('F31',  'P2DataN3_io'),
        ('G28',  'P2DataN2_io'),
        ('J28',  'P2DataN1_io'),
        ('AE21', 'P2DataN0_io')
    ]
}


def uppercase_name(conn, bank, name):
    if conn == 'FMC':
        return 'FMC_{}_{}'.format(bank, name.split('_')[0][5:])
    else:
        return 'VME_{}_{}'.format(bank, name.split('_')[0][6:])


def print_header(title):
    filename = os.path.basename(__file__)
    now = time.ctime()
    print(f'//===================================================================//')
    print(f'//{title:^67s}//')
    print(f'//===================================================================//')
    print(f'// Automatically generated by {filename:<39s}//')
    print(f'// Last update: {now:<53s}//')
    print(f'//===================================================================//')
    print()


def generate_pins(filename):
    with open(filename, 'w') as fd:
        sys.stdout = fd

        print_header('FMC AND VME P2 USER I/O PIN DEFINITIONS')

        for (conn, bank), pins in pin_data.items():
            print(f'//============================== {conn} {bank} =============================//')
            print()
            print(f'`ifndef DISABLE_{conn}_{bank}')
            print()

            for pin, name in pins:
                uname = uppercase_name(conn, bank, name)
                if conn == 'FMC':
                    if bank == 'HB':
                        iostd = '`IO_STANDARD_VIOB'
                    else:
                        iostd = '`IO_STANDARD_VADJ'
                else:
                    if name in ('P2DataP0_io', 'P2DataN0_io'):
                        iostd = '"3.3-V LVCMOS"'
                    else:
                        iostd = '`IO_STANDARD_VADJ'

                print(f'    `ifndef DISABLE_{uname}')
                print(f'        `ifndef DIRECTION_{uname}')
                print(f'            `define DIRECTION_{uname} inout')
                print(f'        `endif')
                print(f'        `ifndef IOSTANDARD_{uname}')
                print(f'            `define IOSTANDARD_{uname} {iostd}')
                print(f'        `endif')
                print(f'        (* useioff = 1 *)(* chip_pin = "{pin}" *)')
                print(f'        `ifdef ATTRIBUTES_{uname}')
                print(f'        (* altera_attribute = {{"-name IO_STANDARD \\"", `IOSTANDARD_{uname}, "\\";", `ATTRIBUTES_{uname}}} *)')
                print(f'        `else')
                print(f'        (* altera_attribute = {{"-name IO_STANDARD \\"", `IOSTANDARD_{uname}, "\\""}} *)')
                print(f'        `endif')
                print(f'        `DIRECTION_{uname} {name},')
                print(f'    `endif')
                print()

            print(f'`endif')
            print()

        sys.stdout = sys.__stdout__


def generate_unused(filename):
    with open(filename, 'w') as fd:
        sys.stdout = fd

        print_header('FMC AND VME P2 USER I/O UNUSED PIN DEFINITIONS')

        for (conn, bank), pins in pin_data.items():
            print(f'//============================== {conn} {bank} =============================//')
            print()
            print(f'`ifdef DISABLE_{conn}_{bank}')

            for pin, name in pins:
                print(f'    wire {name} = 1\'b0;')

            print(f'`endif')
            print()

            print(f'`ifndef DISABLE_{conn}_{bank}')
            print()

            for pin, name in pins:
                uname = uppercase_name(conn, bank, name)
                print(f'    `ifdef DISABLE_{uname}')
                print(f'        wire {name} = 1\'b0;')
                print(f'    `endif')
                print()

            print(f'`endif')
            print()

        sys.stdout = sys.__stdout__

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate VFC-HD user I/O header files.')
    parser.add_argument('--pins', action='store', metavar='FILENAME',
                        help='generates module port (FPGA pins) definition include file (usually VfcHdUserIo.vh)')
    parser.add_argument('--unused', action='store', metavar='FILENAME',
                        help='generates conditional declaration for the unused pins (usually VfcHdUserIo_unused.vh)')
    args = parser.parse_args()

    if (args.pins is None) and (args.unused is None):
        parser.print_help()

    if args.pins is not None:
        generate_pins(args.pins)

    if args.unused is not None:
        generate_unused(args.unused)
