import sys
sys.path.append('/usr/local/encore')

import os
import ctypes
import encoreio
import thread
import struct
import math
from   array import *
from   time  import *

class ConColors:
    BLACK     = "\033[30m"
    RED       = "\033[31m"
    GREEN     = "\033[32m"
    YELLOW    = "\033[33m"
    BLUE      = "\033[34m" # Better readable with BOLD
    MAGENTA   = "\033[35m"
    CYAN      = "\033[36m"
    WHITE     = "\033[37m"
    NONE      = "\033[0m"
    BOLD      = "\033[1m"
    UNDERLINE = "\033[4m"

def HexN(Value, N=8):
    return hex(Value).lstrip('0x').rstrip('L').zfill(N)

def MemDump(DataArray, BitsPerWord = 32, AddressOffset = 0):
    # The ultimate MemDump function: first column is word-wise address + AddressOffset
    Columns = 8
    i       = 0
    for Word in DataArray:
        if i%Columns == 0:
            if i != 0:
                sys.stdout.write("\n")
            sys.stdout.write("%4x: " % (i + AddressOffset))
        sys.stdout.write(("%%0%dx "%(BitsPerWord/4))%Word)  # hex format
        i += 1
    sys.stdout.write("\n")

class VfcHd_System:

    '############################################################################################################'
    '####                                                                                                    ####'
    '####                              **** PLEASE, DO NOT MODIFY THIS FILE ****                             ####'
    '####                                                                                                    ####'
    '#### Note!! This is the PARENT class containing all attributes and methods related to the System module ####'
    '####        of the VFC-HD firmware.                                                                     ####'
    '####        All attributes and methods related to the Application module of the VFC-HD firmware must be ####'
    '####        declared in a separated CHILD class.                                                        ####'
    '####                                                                                                    ####'
    '############################################################################################################'

    def __init__(self, Driver, Lun, IntLevel=3, IntVector=0xA0, RORA="ROAK", Timeout=1000, Verbose=False):
        self.Driver               = Driver
        self.Lun                  = Lun
        self.VfcHd                = encoreio.Module.get_instance(Driver, Lun)
        if not self.VfcHd:
            raise Exception("Module " + Driver + "." + str(Lun) + " not found!")
        self.Verbose              = Verbose
        self.InterruptsInitDriver(IntLevel, IntVector, Mode=RORA)
        self.VfcHd.timeout(Timeout) # in [ms]

    # Verbose control:
    def SetVerbose(self, Verbose):
        self.Verbose = (Verbose == True)

    def PrintVerbose(self, Text):
        if (self.Verbose):
            print(Text)

    def PrintVerboseMem(self, Data, AddressOffset = 0):
        if (self.Verbose):
            MemDump(Data, AddressOffset=AddressOffset)

    # VME accesses:
    def Write(self, RegName, DataLw, Offset=0):
        Am             = -1 # Default
        Start          =  Offset
        To             =  Offset + 1
        Dma            =  0 # Disabled
        Time           =  0 # Transaction time report disabled
        RegNameVerbose = RegName
        if Offset > 0:
            RegNameVerbose = RegNameVerbose + " + " + hex(Offset)
        self.PrintVerbose(ConColors.CYAN+"VME_WRITE:"+ConColors.NONE+" ["+RegNameVerbose+"] <- 0x%08x"%DataLw)
        return self.VfcHd.write(RegName, (DataLw, ), Am, Start, To, Dma, Time)

    def WriteMem(self, RegName, DataLw, Offset=0, Dma=0, Time=0):
        DataLwTuple =  tuple(DataLw)
        Am          = -1 # Default
        Start       =  Offset
        To          =  Start + len(DataLwTuple)
        self.PrintVerbose(ConColors.CYAN+"VME_WRITE_MEM:"+ConColors.NONE+" ["+RegName+"]("+str(Start)+":"+str(To)+") <- ")
        self.PrintVerboseMem(DataLw)
        return self.VfcHd.write(RegName, DataLwTuple, Am, Start, To, Dma, Time)

    def Read(self, RegName, Offset=0):
        Am             = -1 # Default
        Start          =  Offset
        To             =  Offset + 1
        Dma            =  0 # Disabled
        Time           =  0 # Transaction time report disabled
        Result         = self.VfcHd.read(RegName, Am, Start, To, Dma, Time)[0]
        RegNameVerbose = RegName
        if Offset > 0:
            RegNameVerbose = RegNameVerbose + " + " + hex(Offset)
        self.PrintVerbose(ConColors.MAGENTA+"VME_READ:"+ConColors.NONE+" ["+RegNameVerbose+"] = 0x%08x"%Result)
        return Result

    def ReadMem(self, RegName, NbLongWords=-1, Offset=0, Dma=0, Time=0):
        Am     = -1  # Default
        Start  = Offset
        if NbLongWords == -1:
            To = -1 # Full memory block
        else:
            To = Start + NbLongWords
        Result = self.VfcHd.read(RegName, Am, Start, To, Dma, Time)
        self.PrintVerbose(ConColors.MAGENTA+"VME_READ_MULTI:"+ConColors.NONE+" ["+RegName+"]("+str(Start)+":"+str(To)+") = ")
        self.PrintVerboseMem(Result, Offset)
        return Result

    def RdModWr(self, RegName, DataLw, Mask, Offset=0): # In the Mask, set to 1 the bits to be modified
        if Mask == 0xFFFFFFFF:
            NewDataLw        = DataLw
        else:
            PrevDataLw       =  self.Read(RegName, Offset)
            PrevDataLwMasked =  PrevDataLw & (~Mask)
            DataLwMasked     =  DataLw     &   Mask
            NewDataLw        =  PrevDataLwMasked + DataLwMasked
        return self.Write(RegName, NewDataLw, Offset)

    def SetBit(self, RegName, Bit, Offset=0):
        self.RdModWr(RegName, 1<<Bit, 1<<Bit, Offset)

    def ClearBit(self, RegName, Bit, Offset=0):
        self.RdModWr(RegName, 0, 1<<Bit, Offset)

    def ToggleBit(self, RegName, Bit, Offset=0):
        data = self.Read(RegName, Offset)
        data = data ^ (1<<Bit)
        self.Write(RegName, data, Offset)

    def IsBitSet(self, RegName, Bit, Offset=0):
        Mask = 1<<Bit
        return (self.Read(RegName, Offset)&Mask == Mask)

    def WriteRegSlice(self, RegName, Data, SliceFirstBit, SliceLenght):
        Mask = 0xFFFFFFFF<<SliceFirstBit
        Mask = (Mask ^ Mask<<SliceLenght)%0x100000000
        Value = (Data<<SliceFirstBit)%0x100000000
        self.RdModWr(RegName, Value, Mask)

    def ReadRegSlice(self, RegName, Data, SliceFirstBit, SliceLenght):
        Mask = 0xFFFFFFFF<<SliceFirstBit
        Mask = (Mask ^ Mask<<SliceLenght)%0x100000000
        return (self.Read(RegName, Offset)&Mask)>>SliceFirstBit

    # System/Application revision ID & code readout:
    def DecodeRevId(self, RevId):
        if RevId == 0xFFFFFFFF:
            return "---"
        year = (RevId >> 24) & 0xFF
        month = (RevId >> 16) & 0xFF
        day = (RevId >> 8) & 0xFF
        version = RevId & 0xFF
        return "%d (20%02x/%02x/%02x)" % (version, year, month, day)

    def ReadSysRevId(self):
        return self.DecodeRevId(self.Read("Sys_SysRevId"))

    def ReadAppRevId(self):
        return self.DecodeRevId(self.Read("Sys_AppRevId"))

    def DecodeInfo(self, InfoMem):
        if InfoMem[0] == 0xFFFFFFFF:
            return "---"
        return struct.pack(">%dl" % len(InfoMem), *InfoMem).rstrip("\0")

    def ReadSysInfo(self):
        return self.DecodeInfo(self.ReadMem("Sys_SysInfo"))

    def ReadAppInfo(self):
        return self.DecodeInfo(self.ReadMem("Sys_AppInfo"))

    def WhoAreYou(self):
        print
        print "#################################################################"
        print "-> VFC-HD System has -Release ID- :", self.ReadSysRevId()
        print "-> VFC-HD System Code:", self.ReadSysInfo()
        print "#################################################################"
        print "-> Application has -Release ID-   :", self.ReadAppRevId()
        print "-> Application Code:", self.ReadAppInfo()
        print "#################################################################"
        print

    # System control registers access:
    def WriteSysCtrlReg(self, Data):
        self.Write("Sys_CtrlReg", Data)

    def ReadSysCtrlReg(self):
        return self.Read("Sys_CtrlReg")

    def WriteAppRst(self, RstLevel=0):
        self.RdModWr("Sys_CtrlReg", 0x00000000+RstLevel, 0x00000001)

    def ReadAppRst(self):
        return (self.Read("Sys_CtrlReg") & 0x00000001)

    # Interrupt functions:
    def InterruptsInitDriver(self, Level, Vector, Mode):
        self.FirstWaitInterrupt   = 1
        self.LastInterrupt        = 0
        self.WaitingInterrupt     = False
        self.ReceivedInterrupts   = 0
        self.ExpectedInterrupts   = 0
        self.CheckIntSourceManual = True
        self.InterruptsSetLevel(Level)
        self.InterruptsSetVector(Vector)
        self.InterruptMode        = Mode

    def InterruptsSetMask(self, Mask):
        self.Write("Sys_IntMask", Mask & 0xFFFFFFFF);

    def InterruptsSetVector(self, Vector):
        self.Write("Sys_IntVector", Vector & 0xFF);

    def InterruptsSetLevel(self, Level):
        self.Write("Sys_IntLevel", Level & 0x7);

    def EnableInterrupt(self, Mask=0):
        self.InterruptsSetMask(Mask)
        self.SetBit("Sys_IntConfig", 0) # interrupt enabled
        if (self.InterruptMode == "RORA"):
            self.SetBit("Sys_IntConfig", 1) # RORA mode
        elif (self.InterruptMode == "ROAK"):
            self.ClearBit("Sys_IntConfig", 1) # ROAK mode
        else:
            raise Exception("Unknown interrupt mode \"" + self.InterruptMode + "\"! Valid modes: RORA, ROAK")

    def DisableInterrupts(self):
        self.ClearBit("Sys_IntConfig", 0) # interrupt disabled

    def CheckForPendingInterrupts(self):
        intStatus = self.Read("Sys_IntStatus")
        while (intStatus) :
            print "VFC HD in slot %d @expected Interrupt %d FIFO was not empty: %08X" % (self.Lun, self.ExpectedInterrupts, intStatus)
            intStatus = self.Read("Sys_IntStatus")

    def WaitForInterrupt(self, ExpectedStatus=0):
        ReturnCode              = 0
        self.ExpectedInterrupts = self.ExpectedInterrupts + 1
        self.WaitingInterrupt   = True
        Status, irq_count, res  = self.VfcHd.wait() #self.VfcHd.wait_or_pending()
            # TODO: encoreio.wait() and encoreio.wait_or_pending() are deprecated, wait_irq() should be used instead
        if (res):
            print "VFC HD in slot", self.Lun, " @expected Interrupt ", self.ExpectedInterrupts, " ERROR =====> Interrupt missing"
            ReturnCode = 1
        else:
            if (self.CheckIntSourceManual):
                Status = self.Read("Sys_IntStatus")
                if ((not self.FirstWaitInterrupt)and(irq_count != self.LastInterrupt+1)):
                    print "VFC HD in slot", self.Lun, " @expected Interrupt ", self.ExpectedInterrupts, " ERROR =====> irq_count old: ", self.LastInterrupt , "irq_count: ", irq_count
                    ReturnCode = 1
                if ((ExpectedStatus) and (Status != ExpectedStatus)):
                    print "VFC HD in slot", self.Lun, " @expected Interrupt ", self.ExpectedInterrupts, " ERROR =====> Expected status ", ExpectedStatus," but received: ", Status
                    ReturnCode = 1
            self.LastInterrupt      = irq_count
            self.FirstWaitInterrupt = 0
            self.ReceivedInterrupts = self.ReceivedInterrupts + 1
            self.CheckForPendingInterrupts()
        self.WaitingInterrupt = False
        return ReturnCode

    ''' One possibility how to handle interrupt and bit-wise Status callbacks:
        #setup a dictionary
        intCallbackTable = {
           0: ...,
          12: VfcBase.redrawGraph,
            ....,
          31: ...
        }
        VfcBase.SetInterruptCalbacks(intCallbackTable)
        # or individual callback(s)
        VfcBase.SetInterruptCalback(12, VfcBase.redrawGraph)

        in_the_VfcSystem_class:
            def SetInterruptCalback(self, bit, function):
                self.intCallbackTable[bit] = function

            def SetInterruptCalbacks(self, intCallbackTable):
                self.intCallbackTable = intCallbackTable

            def CallInterrupt(self, Status):
                for i in range(31):
                    if (Status>>i) & 1:
                        if i is not in self.intCallbackTable:
                            raise Exception("Not in table...")
                        else:
                            self.intCallbackTable[i]()
    '''

    # 1-Wire interface:
    def ReadUniqueId(self):
        return self.Read("Sys_UniqueId")

    def ReadTemp(self):
        return self.ReadBoardTemp()

    def ReadBoardTemp(self):
        return self.Read("Sys_BoardTemp") / 16.0

    def ReadChipTemp(self):
        return self.Read("Sys_ChipTemp") / 16.0

    # I2C Mux and I/O expander arbiter control register:
    def WriteI2cExpAndMuxReqArb(self, Data):
        self.Write("Sys_I2cMAECntrl", Data)

    def ReadI2cExpAndMuxReqArb(self):
        return self.Read("Sys_I2cMAECntrl")

    # SFP status registers readout:
    def ReadSfpStatusReg(self, SfpToRead):
        if   SfpToRead == "SFP1":
            return self.Read("Sys_ApSfp1Status")
        elif SfpToRead == "SFP2":
            return self.Read("Sys_ApSfp2Status")
        elif SfpToRead == "SFP3":
            return self.Read("Sys_ApSfp3Status")
        elif SfpToRead == "SFP4":
            return self.Read("Sys_ApSfp4Status")
        elif SfpToRead == "SFP4":
            return self.Read("Sys_EthSfpStatus")
        elif SfpToRead == "SFP4":
            return self.Read("Sys_BstSfpStatus")
        else:
            return -1

    # ADC access:
    def ReadAdc(self):
        return [
            self.read("Sys_PreVadj")  / 4096.0 * 2.5 * 2.0,
            self.read("Sys_VccPdAdj") / 4096.0 * 2.5 * 2.0,
            self.read("Sys_Vadj")     / 4096.0 * 2.5 * 2.0,
            self.read("Sys_VioBM2C")  / 4096.0 * 2.5 * 2.0,
            self.read("Sys_V3p3Fpga") / 4096.0 * 2.5 * 2.0,
            self.read("Sys_V3p3Fmc")  / 4096.0 * 2.5 * 2.0,
            self.read("Sys_V2p5")     / 4096.0 * 2.5 * 2.0,
            self.read("Sys_V12p0Fmc") / 4096.0 * 2.5 * 5.7
        ]

    def PrintAdc(self):
        values = self.ReadAdc()
        print("Board voltages:")
        print("  PRE_VADJ  = %.3f V" % values[0])
        print("  VCCPD_ADJ = %.3f V" % values[1])
        print("  VADJ      = %.3f V" % values[2])
        print("  VIO_B_M2C = %.3f V" % values[3])
        print("  V3P3_FPGA = %.3f V" % values[4])
        print("  V3P3_FMC  = %.3f V" % values[5])
        print("  V2P5      = %.3f V" % values[6])
        print("  V12P0_FMC = %.3f V" % values[7])

    # BST decoder readout:
    def ReadBstStatReg(self):
        return self.Read("Sys_BstStatus")

    def ReadBstMessage(self):
        return self.ReadMem("Sys_BstMessage", 256, 0, 1)
